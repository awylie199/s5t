<?php
	session_start();
	$username = "rezak";
	
	if (isset($_GET) && isset($_GET['username'])) $username = trim($_GET['username']);
	
	$uuid = uniqid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

<title>Pusher Client REZAK</title>

</head>

<body>
<h1>Welcome to Pusher Client</h1>

<br /><br />
<input type="button" id="connectToPusher" value="Connect to Pusher" />
<input type="button" id="disconnectFromPusher" value="Disconnect from Pusher" /><br />
<br />
<input type="text" size=10 id="lookingAtAccount" />
<input type="button" value="Set" id="submit-lookingAtAccount" />

<script type="text/javascript">
$(document).ready(function(e) {
    console.log("ready");
	var presenseChannel = null, dataChannel = null;
	var pusher = null;
	var username = "<?php echo $username; ?>";
	var UUID = "<?php echo $uuid; ?>";
	
	console.log(username);
	
	Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };
	
	
	
	
	
	// UI Event handlers
	$("#connectToPusher").click(function(e) {
  		pusher = new Pusher('295bf81725f5f6a203da', {
		  encrypted: true,
		  authEndpoint: 'pusher_auth.php',
		  auth: {
			  params: {
				  'username' : username
			  }
		  }
				
		});
		
		pusher.connection.bind('connected', function () {
				console.log("connected!!!");
				
				presenseChannel = pusher.subscribe('presence-admin-OEC-LIVE');
				
				presenseChannel.bind('pusher:subscription_succeeded', function() {
				var me = presenseChannel.members.me;
				var userId = me.id;
				var userInfo = me.info;
				console.log("Subscribed to presense id: " + userId + " Info: " + userInfo);
				
				
				
				dataChannel = pusher.subscribe('private-data-OEC-live-' + username);
				dataChannel.bind('newTrade', function(msg) {
					console.log("newTrade");
					console.log(msg);
				});
				
				dataChannel.bind('marketData', function(msg) {
					console.log("marketData");
					console.log(msg);
				});
				
 				dataChannel.bind('client-NewMessage', function(msg) {
					console.log("client-NewMessage");
					console.log(msg);
				});


 				dataChannel.bind('client-InitialTrades', function(msg) {
					console.log("client-InitialTrades");
					console.log(msg);
				});
				
				dataChannel.bind('pusher:subscription_succeeded', function() {
					console.log("Subscribed to private-data-OEC-" + username);
					var triggered = presenseChannel.trigger('client-newSubscriber', { 'Username': username , 'UUID': UUID });
					console.log(triggered);			
				});
			  
			});
		
			presenseChannel.bind('pusher:subscription_error', function(status) {
				console.log(status);
			});
			
			presenseChannel.bind('pusher:unsubscribe', function(status) {
				console.log("unsubscribe from presence");
				console.log(status);
				//pusher.disconnect();
			});

		});
    });
	
	$("#disconnectFromPusher").click(function(e) {
        presenseChannel.unsubscribe();
		//pusher.disconnect();
    });
	
	$("#submit-lookingAtAccount").click(function(e) {
        var account = $("#lookingAtAccount").val();
		var triggered = presenseChannel.trigger('client-lookingAtAccount', { 'Username' : username, 'UUID': UUID, 'Account': account });
		console.log(triggered);
    });
	
});
</script>	


</body>
</html>