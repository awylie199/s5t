<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

<title>Pusher Test</title>
</head>

<body>
<h1>Welcome to Pusher Test</h1>
<br />
<?php 
	echo "REZA test <br />";
	echo uniqid();
	

?>

<br />
<label>Username</label><input type="text" id="userid" size=20 /><br /><br />
<input id="test" type="button" value="connect to data channel" />
<input id="connect" type="button" value="connect to pusher" />
<input id="connectPresense" type="button" value="connectPresense" />
<input id="disconnect" type="button" value="disconnect from Presence" />

<br /><br />
<label>Eventname: </label><input type="text" size=20 id="eventname" /><br />
<label>msg: </label><input type="text" size=20 id="msg" /><br />
<input type="button" value="send" id="send-msg"  />


<script type="text/javascript">
	var t1 = 1;
$(document).ready(function(e) {
	var t2 = 2;
	
    console.log("ready");
	console.log(navigator);
	console.log(window);
	var presenseChannel = null, adminChannel = null, dataChannel = null;
	var pusher = null;
	
	
	Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };
	
	$("#send-msg").click(function () {
		if (pusher != null) {
			var triggered = channel.trigger('client-someeventname', { your: "test data" });
			console.log(triggered);
		}
	});


	$("#connect").click(function(e) {
		var username = $("#userid").val();
		pusher = new Pusher('295bf81725f5f6a203da', {
		  encrypted: true,
		  authEndpoint: 'pusher_auth.php',
		  auth: {
			  params: {
				  'username' : username
			  }
		  }
				
		});

		pusher.connection.bind('connected', function () {
			console.log("connected!!!");
		});
		
		

		adminChannel = pusher.subscribe('private-controller-OEC');
		
		adminChannel.bind('newTrade', function(msg) {
			console.log(msg);
		});
		
		adminChannel.bind('pusher:subscription_succeeded', function() {
		  console.log("Subscribed to private-controller-OEC");
		});

		

   });
   
   $("#connectPresense").click(function(e) {
	
		presenseChannel = pusher.subscribe('presence-test');
		presenseChannel.bind('pusher:subscription_succeeded', function() {
		  var me = presenseChannel.members.me;
		  var userId = me.id;
		  var userInfo = me.info;
		  console.log("Subscribed to presense id: " + userId + " Info: " + userInfo);
		});

		
//		presenseChannel.bind('pusher:subscription_succeeded', function() {
//			console.log("success");
//			//var data = "jello";
//			//var triggered = channel.trigger('client-someeventname', { your: data });
//			//console.log(triggered);
//			
//			presenseChannel.bind_all(function(status) {
//				console.log('bind all');
//				console.log(status);
//			});
//		
//
//		});
		
		
		//channel.bind("test-event", function(data) { console.log(data); });

		presenseChannel.bind('pusher:subscription_error', function(status) {
			console.log(status);
		});
		

   });
	
	$("#disconnect").click(function(e) {
        presenseChannel.unsubscribe();
    });
	
	
	$("#test").click(function () {
		var triggered = adminChannel.trigger('client-newTrade', { your: "data" });
		console.log(triggered);

		
	});
	
});


</script>

</body>
</html>
