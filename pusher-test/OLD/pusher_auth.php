<?php
	require "Pusher.php";
	
	if ($_POST && $_POST['channel_name'] && $_POST['socket_id']) {

		$PRESENCE = "presence";
		$PRIVATE = "private";
		
			
		// TEST
		$app_id = '148813';
		$app_key = '295bf81725f5f6a203da';
		$app_secret = 'bb48ea249adc36b1ffe5';
		
		/*
		// LIVE
		$app_id = '160231';
		$app_key = '7d5aa72fa3f2c3234e6c';
		$app_secret = '663bb43f62e480d449d7';
		*/
		
		$pusher = new Pusher(
		  $app_key,
		  $app_secret,
		  $app_id,
		  array('encrypted' => true)
		);
		
		$channelName = trim($_POST['channel_name']);
		$socketId = trim($_POST['socket_id']);
		$username = $_POST['username'] ? $_POST['username'] : "no username";
		
		$userAuthenticated = true;
		if ($userAuthenticated) {
			// User was authenticated
			
			if (strpos($channelName, $PRESENCE) == 0) {
				// Presence Channel Auth Requested	
				$presence_data = array("name" => 'test-reza');
				echo $pusher->presence_auth($_POST['channel_name'], $_POST['socket_id'], $username, $presence_data);
				
				
			} elseif (strpos($channelName, $PRIVATE) == 0){
				// Private Channel Auth Requested	
				echo $pusher->socket_auth($_POST['channel_name'], $_POST['socket_id']);	
		
			} else {
				header('', true, 403);
				echo( "Incorrect channel name used." );
			}
		} else {
			header('', true, 403);
			echo( "User was not authenticated properly." );		
		}

	} else {
		header('', true, 403);
		echo( "Post metrics incorrectly set." );		
	}
?>