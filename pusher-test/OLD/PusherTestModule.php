<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PUSHER TEST MODULE</title>

<!-- CDNs -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" src="//code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="//js.pusher.com/3.0/pusher.min.js"></script>

<style>
span { color: grey; }
#log { font-size: .9em; height: 200px; overflow: auto; }
.container { display: block; width: 100%; border: thin dashed #ccc; min-height: 300px; }
.chart {display: block; width: 500px; height: 250px; margin: 10px 25px; }
.sec-container { display: inline-block; width: 550px; border: 1px dashed blue; }
</style>

</head>

<body>
<h1>RTA Test module</h1>
<div class="container">
	User: <?php echo $_GET['username']; ?><br />
    <input type="text" size=100 id="message-text" /><input type="button" id="message-submmit" value="Send Message" /><br />
    
    <br /><br />
    <div class="sec-container">
    	<div class="chart" id="1"></div>
        <input type="text" value="1" id="chart1-quant" size=5 /><input type="button" class="chart1action" value="BUY" /><input type="button"  class="chart1action" value="SELL" />
        <input style="display: inline-block; margin-left: 55px;" type="text" id="chart1-price" size=5 /><input type="text" id="chart1-pos" />
        <!--<input type="button" value="Set Price" class="price-action" />-->
	</div>        

    <div class="sec-container">
	    <div class="chart" id="2"></div>
        <input type="text" value="1" id="chart2-quant" size=5 /><input type="button"  class="chart2action" value="BUY" /><input type="button"  class="chart2action" value="SELL" />
        <input style="display: inline-block; margin-left: 55px;" type="text" id="chart2-price" size=5 /><input type="text" id="chart2-pos" />
        <!--<input type="button" value="Set Price" class="price-action" />-->
	</div>    
    <br />
    <label><input type="checkbox" id="live-data-onscroll" checked="checked" />Update live data on scroll</label>


<!--    <input type="button" value="Start" id="stop-start-data" /> -->

	<br /><br />
    <input type="button" id="send-init-trades" value="Send Inital Trades" />

	<br /><br />
    <input type="button" id="start-robot" value="Start Robot" /><input type="text" id="robot-val" value="2500" />
</div>
<div id="log">LOG<br /></div>

<script type="text/javascript">
var chart1, chart2;
var chart1Value = 2000, chart2Value = 4000;
var chart1Pos = 0, chart2Pos = 0;
var chart1Delta = 0.25, chart2Delta = 0.1;
var dataFlow = false;
var chart1FillID = 1, chart2FillID = 1;

var Account = "API002565";


var fill1 = {
	TickSize: 0.0001,
	ContractValue: 100000,
	ClosingTime: 6,
	Account: Account,
	Broker: "OEC",
	Contract: "6CH6",
	FillID: 1,
	HighSince: 0.7051,
	LowSince: 0.7048,
	OrderID: "1",
	Price: 0.7049,
	Quant: 2,
	Side: "Buy",
	TimestampUTC: moment().utc().valueOf(),
	TypeOfTrade: "O"
}

var fill2 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,	
	Account: Account,
	Broker: "OEC",
	Contract: "ESH6",
	FillID: 2,
	HighSince: 2015,
	LowSince: 2009.25,
	OrderID: "2",
	Price: 2010,
	Quant: 2,
	Side: "Sell",
	TimestampUTC: moment().utc().valueOf(),
	TypeOfTrade: "C"	
}

var initFills = [ fill1 ];

$(document).ready(function(e) {
	var username = "<?php echo trim($_GET['username']); ?>";
	if (username == "") username == "reza";
	
	$("#chart1-price").val(chart1Value);
	$("#chart2-price").val(chart2Value);
	
	
	console.log("Pusher TEST MODULE loaded.");
	
	window.pusher = null;
	window.presenseChannel = null;
	window.dataChannel = null;	
	
		
	Pusher.log = function(message) {
      if (window.console && window.console.log) {
        //window.console.log(message);
		$("#log").prepend("<span>" + message + "</span><br />");
      }
    };
	 
	window.pusher = new Pusher('295bf81725f5f6a203da', {
		encrypted: true,
		authEndpoint: 'pusher_auth.php',
		auth: {
		  params: {
			  'username' : username
		  }
		}		
	});
   
	window.pusher.connection.bind('connected', function () {
		console.log("Pusher Connected.");
		
		window.dataChannel = window.pusher.subscribe('private-data-OEC-live-' + username);

		window.dataChannel.bind('pusher:subscription_succeeded', function() {
			logger("Subscribed to private-data-OEC-" + username);
			var triggered = dataChannel.trigger('client-NewMessage', { 'Username': username , 'MessageType':'alert', 'Message': "Welcome message" });
		});
		
	});   
   
   
   $("#message-submmit").click(function (e) {
	  var msg = $("#message-text").val();
      var triggered = dataChannel.trigger('client-NewMessage', { 'Username': username , 'MessageType':'alert', 'Message': msg });	  
	  logger(triggered ? "sent" : "not sent");
   });
   
   $("#stop-start-data").click(function(e) {
    	if ( $(this).val() == "Start" ) {
			dataFlow = true;
			$(this).val("Stop");
		} else {
			dataFlow = false;
			$(this).val("Start");
		}
	});
	
	$("#send-init-trades").click(function(e) {
		var triggered = dataChannel.trigger('client-InitialTrades', { 'Username': username , 'Type':'InitialFills', 'Data': initFills });	  
		logger(triggered ? "sent" : "not sent");        
    });
	
	$(".chart1action").click(function(e) {
		var FillID = chart1FillID++;
		var OrderID = FillID + "1";
		var quant = Number($("#chart1-quant").val());
		var side = $(this).val() == "BUY" ? "Buy" : "Sell";
		var timestampUTC = moment().utc().valueOf();
		
		chart1Pos += ( side == "Buy" ? quant : -quant );
		$("#chart1-pos").val(chart1Pos);
		
		var fill = {
			TickSize: 0.25,
			ContractValue: 50,
			ClosingTime: 6,	
			Account: Account,
			Broker: "OEC",
			Contract: "ESH6",
			FillID: FillID,
			OrderID: OrderID,
			Price: chart1Value,
			Quant: quant,
			Side: side,
			TimestampUTC: timestampUTC
		}
		
		var triggered = dataChannel.trigger('client-NewTrade', { 'Username': username , 'Type':'NewTrade', 'Data': fill });	  
		logger(triggered ? "sent" : "not sent");        
		
    });


	$(".chart2action").click(function(e) {
		var FillID = chart2FillID++;
		var OrderID = FillID + "1";
		var quant = Number($("#chart2-quant").val());
		var side = $(this).val() == "BUY" ? "Buy" : "Sell";
		var timestampUTC = moment().utc().valueOf();

		chart2Pos += ( side == "Buy" ? quant : -quant );
		$("#chart2-pos").val(chart2Pos);
				
		var fill = {
			TickSize: 0.1,
			ContractValue: 100,
			ClosingTime: 6,	
			Account: Account,
			Broker: "OEC",
			Contract: "NQM6",
			FillID: FillID,
			OrderID: OrderID,
			Price: chart2Value,
			Quant: quant,
			Side: side,
			TimestampUTC: timestampUTC
		}
		
		var triggered = dataChannel.trigger('client-NewTrade', { 'Username': username , 'Type':'NewTrade', 'Data': fill });	  
		logger(triggered ? "sent" : "not sent");        
		
    });
	
	$("#chart1-price").bind('mousewheel', function (e) {
		if(e.originalEvent.wheelDelta > 0) {
			chart1Value += chart1Delta;
        }
        else{
			chart1Value -= chart1Delta;			
        }
		$(this).val(chart1Value);
		
		chart1.series[0].addPoint(chart1Value);
		if ($("#live-data-onscroll").is(":checked")) {
			var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
			var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
			
			var PriceArray = [PriceOBJ1, PriceOBJ2];			
			var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });	
		}
	});

	$("#chart2-price").bind('mousewheel', function (e) {
		logger('mouse event');
		if(e.originalEvent.wheelDelta > 0) {
			chart2Value += chart2Delta;
			chart2Value = Math.round(chart2Value * 100) / 100;
        }
        else{
			chart2Value -= chart2Delta;			
			chart2Value = Math.round(chart2Value * 100) / 100;			
        }
		$(this).val(chart2Value);

		chart2.series[0].addPoint(chart2Value);
		if ($("#live-data-onscroll").is(":checked")) {
			var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
			var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
			
			var PriceArray = [PriceOBJ1, PriceOBJ2];			
			var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });	
		}

	});
	
	$(".price-action").click(function(e) {
		chart1Value = Number($("#chart1-price").val());
		chart2Value = Number($("#chart2-price").val());
		
		var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
		var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
		
		var PriceArray = [PriceOBJ1, PriceOBJ2];			
		var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });	
		logger(triggered ? "sent" : "not sent");
    });
	
   
    $("#start-robot").click(function () {
		trade();
	});
	
	function trade () {
					
			var rand = Math.random() * 10;
			
			var FillID = chart1FillID++;
			var OrderID = FillID + "1";
			var quant = 1;
			var side = rand > 5 ? "Buy" : "Sell";
			var timestampUTC = moment().utc().valueOf();

			if (rand > 9.5 && chart1Pos != 0) {
				// flatten
				quant = chart1Pos;
				side = ( chart1Pos > 0 ? "Sell" : "Buy" );
			}
			
			chart1Value = chart1Value + Math.round(rand) - 5;			
			
			var fill = {
				TickSize: 0.25,
				ContractValue: 50,
				ClosingTime: 6,	
				Account: Account,
				Broker: "OEC",
				Contract: "ESH6",
				FillID: FillID,
				OrderID: OrderID,
				Price: chart1Value,
				Quant: quant,
				Side: side,
				TimestampUTC: timestampUTC
			}
			
			var triggered = dataChannel.trigger('client-NewTrade', { 'Username': username , 'Type':'NewTrade', 'Data': fill });	  
			logger(triggered ? "sent" : "not sent");        
			
		setTimeout( trade, Number($("#robot-val").val()) );
	}
   
    /* Chart 
	******************************************************************************************************************************************************************************
	*/
	
	Highcharts.setOptions(Highcharts.theme);
	
	Highcharts.setOptions({
		global: {
			useUTC: true
		}
	});

	
	var chart_options = {
		chart: {
			type: 'line'
		},
		tooltip: {
			formatter: function () {
				return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y + " %";
			}
		},
		legend: { enabled: false },
		yAxis: [{
			id: 'main-y-axis'
		}],
		xAxis: {
			minTickInterval: 1,
			min: 0
		}
	}
	
	
	var chart1options = {
		yAxis: [{
		}],
		series : [{
			id: 'main',
			type: 'line',
			data : [chart1Value]
		}],
		chart: {
			type: 'line',
			events: {
				click: function (e) {
					var y = e.yAxis[0].value;
					this.series[0].addPoint(y);
					chart1Value = Math.round(y*100)/100;

					var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
					var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
					
					var PriceArray = [PriceOBJ1, PriceOBJ2];			
					var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });	
				}
			}
		}				
	}
	
	var chart2options = {
		yAxis: [{
			min: 3950,
			max: 4080
		}],
		series : [{
			id: 'main',
			type: 'line',
			data : [chart2Value]
		}],
		chart: {
			type: 'line',
			events: {
				click: function (e) {
					var y = e.yAxis[0].value;
					this.series[0].addPoint(y);
					chart2Value = Math.round(y*100)/100;

					var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
					var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
					
					var PriceArray = [PriceOBJ1, PriceOBJ2];			
					var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });						
				}
			}
		}				
	}
	
	// Create the chart
	$(".chart#1").highcharts(Highcharts.merge(chart_options, chart1options));
	chart1 = $(".chart#1").highcharts();
	chart1.setTitle({text: "ESH6"});
	
	$(".chart#2").highcharts(Highcharts.merge(chart_options, chart2options));
	chart2 = $(".chart#2").highcharts();
	chart2.setTitle({text: "NQM6"});	
	
	setInterval( function () {
		if (dataFlow) {
			chart1Value += (Math.random() * 10 - 5);
			chart1.series[0].addPoint(chart1Value);
	
			chart2Value += (Math.random() * 20 - 10);
			chart2.series[0].addPoint(chart2Value);

			var PriceOBJ1 = { Symbol: "ESH6", TimestampUTC: moment().utc().valueOf(), Price: chart1Value };
			var PriceOBJ2 = { Symbol: "NQM6", TimestampUTC: moment().utc().valueOf(), Price: chart2Value };
			
			var PriceArray = [PriceOBJ1, PriceOBJ2];			
			var triggered = dataChannel.trigger('client-MarketData', { 'Username': username , 'PriceArray': PriceArray });	

		}
	}, 2500);
    
});

function logger(message) {
	$("#log").append(message + "<br />");
}
</script>


</body>
</html>
