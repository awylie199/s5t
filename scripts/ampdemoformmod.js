var states = new Array("United States",
"United Kingdom",
"Afghanistan",
"Albania",
"Algeria",
"Andorra",
"Angola",
"Anguilla",
"Antarctica",
"Antigua",
"Argentina",
"Armenia",
"Aruba",
"Australia",
"Austria",
"Azerbaijan",
"Bahamas",
"Bahrain",
"Bangladesh",
"Barbados",
"Belarus",
"Belize",
"Belgium",
"Benin",
"Bermuda",
"Bhutan",
"Bolivia",
"Bosnia And Herzegovina",
"Botswana",
"Brazil",
"British Virgin Islands",
"Brunei Darussalam",
"Bulgaria",
"Burkina Faso",
"Burundi",
"Cambodia",
"Cameroon",
"Canada",
"Cape Verde",
"Cayman Islands",
"Central African Republic",
"Chad",
"Chile",
"China",
"Christmas Island",
"Cocos (Keeling) Islands",
"Colombia",
"Comoros",
"Congo",
"Cook Islands",
"Costa Rica",
"Cote Divoire",
"Croatia",
"Cuba",
"Cyprus",
"Czech Republic",
"Denmark",
"Diego Garcia",
"Djibouti",
"Dominica",
"Dominican Republic",
"East Timor",
"Ecuador",
"Egypt",
"El Salvador",
"Equatorial Guinea",
"Eritrea",
"Estonia",
"Ethiopia",
"Falkland Islands (Malvinas)",
"Faroe Islands",
"Fiji",
"Finland",
"France",
"French Antilles",
"French Guiana",
"French Polynesia",
"Gabon",
"Gambia",
"Georgia",
"Germany",
"Ghana",
"Gibraltar",
"Greece",
"Greenland",
"Grenada",
"Guadeloupe",
"Guam",
"Guatemala",
"Guinea",
"Guinea-Bissau",
"Guyana",
"Haiti",
"Honduras",
"Hong Kong",
"Hungary",
"Iceland",
"India",
"Indonesia",
"Iran",
"Iraq",
"Ireland",
"Israel",
"Italy",
"Jamaica",
"Japan",
"Jordan",
"Kazakhstan",
"Kenya",
"Kiribati",
"Korea, Democratic Peoples Republic",
"Korea, Republic of",
"Kuwait",
"Kyrgyzstan",
"Laos, Peoples Democratic Republic of",
"Latvia",
"Lebanon",
"Lesotho",
"Liberia",
"Libyan Arab Jamahiriya",
"Liechtenstein",
"Lithuania",
"Luxembourg",
"Macau",
"Macedonia",
"Madagascar",
"Malawi",
"Malaysia",
"Maldives",
"Mali",
"Malta",
"Marshall Islands",
"Martinique",
"Mauritania",
"Mauritius",
"Mayotte",
"Mexico",
"Micronesia",
"Moldova",
"Monaco",
"Mongolia",
"Montenegro",
"Montserrat",
"Morocco",
"Mozambique",
"Myanmar",
"Namibia",
"Nauru",
"Nepal",
"Netherlands",
"Netherlands Antilles",
"New Caledonia",
"New Zealand",
"Nicaragua",
"Niger",
"Nigeria",
"Niue",
"Norfolk Island",
"Northern Mariana Islands",
"Norway",
"Oman",
"Pakistan",
"Palau",
"Palestinian Settlements",
"Panama",
"Papua New Guinea",
"Paraguay",
"Peru",
"Philippines",
"Poland",
"Portugal",
"Puerto Rico",
"Qatar",
"Reunion",
"Romania",
"Russia",
"Rwanda",
"Saint Helena",
"Saint Kitts And Nevis",
"Saint Lucia",
"Saint Pierre And Miquelon",
"Saint Vincent And the Grenadines",
"Samoa, African",
"Samoa, American",
"San Marino",
"Sao Tome And Principe",
"Saudi Arabia",
"Senegal",
"Serbia",
"Seychelles",
"Sierra Leone",
"Singapore",
"Slovakia",
"Slovenia",
"Solomon Islands",
"Somalia",
"South Africa",
"Spain",
"Sri Lanka",
"Sudan",
"Suriname",
"Swaziland",
"Sweden",
"Switzerland",
"Syrian Arab Republic",
"Taiwan",
"Tajikistan",
"Tanzania",
"Thailand",
"Togo",
"Tokelau",
"Tonga",
"Trinidad And Tobago",
"Tunisia",
"Turkey",
"Turkmenistan",
"Turks and Caicos Islands",
"Tuvalu",
"Uganda",
"Ukraine",
"United Arab Emirates",
"US Virgin Islands",
"Uruguay",
"Uzbekistan",
"Vanuatu",
"Vatican City State",
"Venezuela",
"Vietnam",
"Wallis And Futuna Islands",
"Yemen",
"Zambia",
"Zanzibar",
"Zimbabwe");

var phonecodes = new Array("+1", 
"+44", 
"+93", 
"+355", 
"+213", 
"+376", 
"+244", 
"+1264", 
"+672", 
"+1268", 
"+54", 
"+374", 
"+297", 
"+61", 
"+43", 
"+994", 
"+1242", 
"+973", 
"+880", 
"+1246", 
"+375", 
"+501", 
"+32", 
"+229", 
"+1441", 
"+975", 
"+591", 
"+387", 
"+267", 
"+55", 
"+1284", 
"+673", 
"+359", 
"+226", 
"+257", 
"+855", 
"+237", 
"+1", 
"+238", 
"+1345", 
"+236", 
"+235", 
"+56", 
"+86", 
"+618", 
"+61", 
"+57", 
"+269", 
"+242", 
"+682", 
"+506", 
"+225", 
"+385", 
"+53", 
"+357", 
"+420", 
"+45", 
"+246" , 
"+253", 
"+1767", 
"+1809", 
"+670", 
"+593", 
"+20", 
"+503", 
"+240", 
"+291", 
"+372", 
"+251", 
"+500", 
"+298", 
"+679", 
"+358", 
"+33", 
"+596", 
"+594", 
"+689", 
"+241", 
"+220", 
"+995", 
"+49", 
"+233", 
"+350", 
"+30", 
"+299", 
"+1473", 
"+590", 
"+1671", 
"+502", 
"+224", 
"+245", 
"+592", 
"+509", 
"+504", 
"+852", 
"+36", 
"+354", 
"+91", 
"+62", 
"+98", 
"+964", 
"+353", 
"+972", 
"+39", 
"+1876", 
"+81", 
"+962", 
"+7", 
"+254", 
"+686", 
"+850", 
"+82", 
"+965", 
"+996", 
"+856", 
"+371", 
"+961", 
"+266", 
"+231", 
"+218", 
"+423", 
"+370", 
"+352", 
"+853", 
"+389", 
"+261", 
"+265", 
"+60", 
"+960", 
"+223", 
"+356", 
"+692", 
"+596", 
"+222", 
"+230", 
"+269", 
"+52", 
"+691", 
"+373", 
"+377", 
"+976", 
"+382", 
"+1664", 
"+212", 
"+258", 
"+95", 
"+264", 
"+674", 
"+977", 
"+31", 
"+599", 
"+687", 
"+64", 
"+505", 
"+227", 
"+234", 
"+683", 
"+672", 
"+1670", 
"+47", 
"+968", 
"+92", 
"+680", 
"+970", 
"+507" , 
"+675", 
"+595", 
"+51", 
"+63", 
"+48", 
"+351", 
"+1787", 
"+974", 
"+262", 
"+40", 
"+7", 
"+250", 
"+290", 
"+1869", 
"+1758", 
"+508", 
"+1784", 
"+685", 
"+685", 
"+378", 
"+239", 
"+966", 
"+221", 
"+381", 
"+248", 
"+232", 
"+65", 
"+421", 
"+386", 
"+677", 
"+252", 
"+27", 
"+34", 
"+94", 
"+249", 
"+597", 
"+268", 
"+46", 
"+41", 
"+963", 
"+886", 
"+992", 
"+255", 
"+66", 
"+228", 
"+690", 
"+676", 
"+1868", 
"+216", 
"+90", 
"+993", 
"+1649", 
"+688", 
"+256", 
"+380", 
"+971", 
"+1340", 
"+598", 
"+998", 
"+678", 
"+39", 
"+58", 
"+84", 
"+681", 
"+967", 
"+260", 
"+255", 
"+263");


var initialOpt = document.createElement("OPTION");
initialOpt.text = "Select a country ...";
initialOpt.value = "";
document.getElementById("country").options.add(initialOpt);

for (var hi = 0; hi < states.length; hi++) {
    var optn = document.createElement("OPTION");
    optn.text = states[hi];
    optn.value = states[hi];
    if (states[hi] == "United States") {
        optn.value = "USA";
        //optn.selected = true;
        //document.getElementById('country_code').value = "+1";
    }
    document.getElementById("country").options.add(optn);
}

function ShowCountryCode() {
    switch (document.getElementById("country").value) {
        case "USA": document.getElementById('country_code').value = "+1"; break;
        case "United Kingdom": document.getElementById('country_code').value = "+44"; break;
        case "Afghanistan": document.getElementById('country_code').value = "+93"; break;
        case "Albania": document.getElementById('country_code').value = "+355"; break;
        case "Algeria": document.getElementById('country_code').value = "+213"; break;
        case "Andorra": document.getElementById('country_code').value = "+376"; break;
        case "Angola": document.getElementById('country_code').value = "+244"; break;
        case "Anguilla": document.getElementById('country_code').value = "+1264"; break;
        case "Antarctica": document.getElementById('country_code').value = "+672"; break;
        case "Antigua": document.getElementById('country_code').value = "+1268"; break;
        case "Argentina": document.getElementById('country_code').value = "+54"; break;
        case "Armenia": document.getElementById('country_code').value = "+374"; break;
        case "Aruba": document.getElementById('country_code').value = "+297"; break;
        case "Australia": document.getElementById('country_code').value = "+61"; break;
        case "Austria": document.getElementById('country_code').value = "+43"; break;
        case "Azerbaijan": document.getElementById('country_code').value = "+994"; break;
        case "Bahamas": document.getElementById('country_code').value = "+1242"; break;
        case "Bahrain": document.getElementById('country_code').value = "+973"; break;
        case "Bangladesh": document.getElementById('country_code').value = "+880"; break;
        case "Barbados": document.getElementById('country_code').value = "+1246"; break;
        case "Belarus": document.getElementById('country_code').value = "+375"; break;
        case "Belize": document.getElementById('country_code').value = "+501"; break;
        case "Belgium": document.getElementById('country_code').value = "+32"; break;
        case "Benin": document.getElementById('country_code').value = "+229"; break;
        case "Bermuda": document.getElementById('country_code').value = "+1441"; break;
        case "Bhutan": document.getElementById('country_code').value = "+975"; break;
        case "Bolivia": document.getElementById('country_code').value = "+591"; break;
        case "Bosnia And Herzegovina": document.getElementById('country_code').value = "+387"; break;
        case "Botswana": document.getElementById('country_code').value = "+267"; break;
        case "Brazil": document.getElementById('country_code').value = "+55"; break;
        case "British Virgin Islands": document.getElementById('country_code').value = "+1284"; break;
        case "Brunei Darussalam": document.getElementById('country_code').value = "+673"; break;
        case "Bulgaria": document.getElementById('country_code').value = "+359"; break;
        case "Burkina Faso": document.getElementById('country_code').value = "+226"; break;
        case "Burundi": document.getElementById('country_code').value = "+257"; break;
        case "Cambodia": document.getElementById('country_code').value = "+855"; break;
        case "Cameroon": document.getElementById('country_code').value = "+237"; break;
        case "Canada": document.getElementById('country_code').value = "+1"; break;
        case "Cape Verde": document.getElementById('country_code').value = "+238"; break;
        case "Cayman Islands": document.getElementById('country_code').value = "+1345"; break;
        case "Central African Republic": document.getElementById('country_code').value = "+236"; break;
        case "Chad": document.getElementById('country_code').value = "+235"; break;
        case "Chile": document.getElementById('country_code').value = "+56"; break;
        case "China": document.getElementById('country_code').value = "+86"; break;
        case "Christmas Island": document.getElementById('country_code').value = "+618"; break;
        case "Cocos (Keeling) Islands": document.getElementById('country_code').value = "+61"; break;
        case "Colombia": document.getElementById('country_code').value = "+57"; break;
        case "Comoros": document.getElementById('country_code').value = "+269"; break;
        case "Congo": document.getElementById('country_code').value = "+242"; break;
        case "Cook Islands": document.getElementById('country_code').value = "+682"; break;
        case "Costa Rica": document.getElementById('country_code').value = "+506"; break;
        case "Cote Divoire": document.getElementById('country_code').value = "+225"; break;
        case "Croatia": document.getElementById('country_code').value = "+385"; break;
        case "Cuba": document.getElementById('country_code').value = "+53"; break;
        case "Cyprus": document.getElementById('country_code').value = "+357"; break;
        case "Czech Republic": document.getElementById('country_code').value = "+420"; break;
        case "Denmark": document.getElementById('country_code').value = "+45"; break;
        case "Diego Garcia": document.getElementById('country_code').value = "+246"; break;
        case "Djibouti": document.getElementById('country_code').value = "+253"; break;
        case "Dominica": document.getElementById('country_code').value = "+1767"; break;
        case "Dominican Republic": document.getElementById('country_code').value = "+1809"; break;
        case "East Timor": document.getElementById('country_code').value = "+670"; break;
        case "Ecuador": document.getElementById('country_code').value = "+593"; break;
        case "Egypt": document.getElementById('country_code').value = "+20"; break;
        case "El Salvador": document.getElementById('country_code').value = "+503"; break;
        case "Equatorial Guinea": document.getElementById('country_code').value = "+240"; break;
        case "Eritrea": document.getElementById('country_code').value = "+291"; break;
        case "Estonia": document.getElementById('country_code').value = "+372"; break;
        case "Ethiopia": document.getElementById('country_code').value = "+251"; break;
        case "Falkland Islands (Malvinas)": document.getElementById('country_code').value = "+500"; break;
        case "Faroe Islands": document.getElementById('country_code').value = "+298"; break;
        case "Fiji": document.getElementById('country_code').value = "+679"; break;
        case "Finland": document.getElementById('country_code').value = "+358"; break;
        case "France": document.getElementById('country_code').value = "+33"; break;
        case "French Antilles": document.getElementById('country_code').value = "+596"; break;
        case "French Guiana": document.getElementById('country_code').value = "+594"; break;
        case "French Polynesia": document.getElementById('country_code').value = "+689"; break;
        case "Gabon": document.getElementById('country_code').value = "+241"; break;
        case "Gambia": document.getElementById('country_code').value = "+220"; break;
        case "Georgia": document.getElementById('country_code').value = "+995"; break;
        case "Germany": document.getElementById('country_code').value = "+49"; break;
        case "Ghana": document.getElementById('country_code').value = "+233"; break;
        case "Gibraltar": document.getElementById('country_code').value = "+350"; break;
        case "Greece": document.getElementById('country_code').value = "+30"; break;
        case "Greenland": document.getElementById('country_code').value = "+299"; break;
        case "Grenada": document.getElementById('country_code').value = "+1473"; break;
        case "Guadeloupe": document.getElementById('country_code').value = "+590"; break;
        case "Guam": document.getElementById('country_code').value = "+1671"; break;
        case "Guatemala": document.getElementById('country_code').value = "+502"; break;
        case "Guinea": document.getElementById('country_code').value = "+224"; break;
        case "Guinea-Bissau": document.getElementById('country_code').value = "+245"; break;
        case "Guyana": document.getElementById('country_code').value = "+592"; break;
        case "Haiti": document.getElementById('country_code').value = "+509"; break;
        case "Honduras": document.getElementById('country_code').value = "+504"; break;
        case "Hong Kong": document.getElementById('country_code').value = "+852"; break;
        case "Hungary": document.getElementById('country_code').value = "+36"; break;
        case "Iceland": document.getElementById('country_code').value = "+354"; break;
        case "India": document.getElementById('country_code').value = "+91"; break;
        case "Indonesia": document.getElementById('country_code').value = "+62"; break;
        case "Iran": document.getElementById('country_code').value = "+98"; break;
        case "Iraq": document.getElementById('country_code').value = "+964"; break;
        case "Ireland": document.getElementById('country_code').value = "+353"; break;
        case "Israel": document.getElementById('country_code').value = "+972"; break;
        case "Italy": document.getElementById('country_code').value = "+39"; break;
        case "Jamaica": document.getElementById('country_code').value = "+1876"; break;
        case "Japan": document.getElementById('country_code').value = "+81"; break;
        case "Jordan": document.getElementById('country_code').value = "+962"; break;
        case "Kazakhstan": document.getElementById('country_code').value = "+7"; break;
        case "Kenya": document.getElementById('country_code').value = "+254"; break;
        case "Kiribati": document.getElementById('country_code').value = "+686"; break;
        case "Korea, Democratic Peoples Republic": document.getElementById('country_code').value = "+850"; break;
        case "Korea, Republic of": document.getElementById('country_code').value = "+82"; break;
        case "Kuwait": document.getElementById('country_code').value = "+965"; break;
        case "Kyrgyzstan": document.getElementById('country_code').value = "+996"; break;
        case "Laos, Peoples Democratic Republic of": document.getElementById('country_code').value = "+856"; break;
        case "Latvia": document.getElementById('country_code').value = "+371"; break;
        case "Lebanon": document.getElementById('country_code').value = "+961"; break;
        case "Lesotho": document.getElementById('country_code').value = "+266"; break;
        case "Liberia": document.getElementById('country_code').value = "+231"; break;
        case "Libyan Arab Jamahiriya": document.getElementById('country_code').value = "+218"; break;
        case "Liechtenstein": document.getElementById('country_code').value = "+423"; break;
        case "Lithuania": document.getElementById('country_code').value = "+370"; break;
        case "Luxembourg": document.getElementById('country_code').value = "+352"; break;
        case "Macau": document.getElementById('country_code').value = "+853"; break;
        case "Macedonia": document.getElementById('country_code').value = "+389"; break;
        case "Madagascar": document.getElementById('country_code').value = "+261"; break;
        case "Malawi": document.getElementById('country_code').value = "+265"; break;
        case "Malaysia": document.getElementById('country_code').value = "+60"; break;
        case "Maldives": document.getElementById('country_code').value = "+960"; break;
        case "Mali": document.getElementById('country_code').value = "+223"; break;
        case "Malta": document.getElementById('country_code').value = "+356"; break;
        case "Marshall Islands": document.getElementById('country_code').value = "+692"; break;
        case "Martinique": document.getElementById('country_code').value = "+596"; break;
        case "Mauritania": document.getElementById('country_code').value = "+222"; break;
        case "Mauritius": document.getElementById('country_code').value = "+230"; break;
        case "Mayotte": document.getElementById('country_code').value = "+269"; break;
        case "Mexico": document.getElementById('country_code').value = "+52"; break;
        case "Micronesia": document.getElementById('country_code').value = "+691"; break;
        case "Moldova": document.getElementById('country_code').value = "+373"; break;
        case "Monaco": document.getElementById('country_code').value = "+377"; break;
        case "Mongolia": document.getElementById('country_code').value = "+976"; break;
        case "Montenegro": document.getElementById('country_code').value = "+382"; break;
        case "Montserrat": document.getElementById('country_code').value = "+1664"; break;
        case "Morocco": document.getElementById('country_code').value = "+212"; break;
        case "Mozambique": document.getElementById('country_code').value = "+258"; break;
        case "Myanmar": document.getElementById('country_code').value = "+95"; break;
        case "Namibia": document.getElementById('country_code').value = "+264"; break;
        case "Nauru": document.getElementById('country_code').value = "+674"; break;
        case "Nepal": document.getElementById('country_code').value = "+977"; break;
        case "Netherlands": document.getElementById('country_code').value = "+31"; break;
        case "Netherlands Antilles": document.getElementById('country_code').value = "+599"; break;
        case "New Caledonia": document.getElementById('country_code').value = "+687"; break;
        case "New Zealand": document.getElementById('country_code').value = "+64"; break;
        case "Nicaragua": document.getElementById('country_code').value = "+505"; break;
        case "Niger": document.getElementById('country_code').value = "+227"; break;
        case "Nigeria": document.getElementById('country_code').value = "+234"; break;
        case "Niue": document.getElementById('country_code').value = "+683"; break;
        case "Norfolk Island": document.getElementById('country_code').value = "+672"; break;
        case "Northern Mariana Islands": document.getElementById('country_code').value = "+1670"; break;
        case "Norway": document.getElementById('country_code').value = "+47"; break;
        case "Oman": document.getElementById('country_code').value = "+968"; break;
        case "Pakistan": document.getElementById('country_code').value = "+92"; break;
        case "Palau": document.getElementById('country_code').value = "+680"; break;
        case "Palestinian Settlements": document.getElementById('country_code').value = "+970"; break;
        case "Panama": document.getElementById('country_code').value = "+507"; break;
        case "Papua New Guinea": document.getElementById('country_code').value = "+675"; break;
        case "Paraguay": document.getElementById('country_code').value = "+595"; break;
        case "Peru": document.getElementById('country_code').value = "+51"; break;
        case "Philippines": document.getElementById('country_code').value = "+63"; break;
        case "Poland": document.getElementById('country_code').value = "+48"; break;
        case "Portugal": document.getElementById('country_code').value = "+351"; break;
        case "Puerto Rico": document.getElementById('country_code').value = "+1787"; break;
        case "Qatar": document.getElementById('country_code').value = "+974"; break;
        case "Reunion": document.getElementById('country_code').value = "+262"; break;
        case "Romania": document.getElementById('country_code').value = "+40"; break;
        case "Russia": document.getElementById('country_code').value = "+7"; break;
        case "Rwanda": document.getElementById('country_code').value = "+250"; break;
        case "Saint Helena": document.getElementById('country_code').value = "+290"; break;
        case "Saint Kitts And Nevis": document.getElementById('country_code').value = "+1869"; break;
        case "Saint Lucia": document.getElementById('country_code').value = "+1758"; break;
        case "Saint Pierre And Miquelon": document.getElementById('country_code').value = "+508"; break;
        case "Saint Vincent And the Grenadines": document.getElementById('country_code').value = "+1784"; break;
        case "Samoa, African": document.getElementById('country_code').value = "+685"; break;
        case "Samoa, American": document.getElementById('country_code').value = "+685"; break;
        case "San Marino": document.getElementById('country_code').value = "+378"; break;
        case "Sao Tome And Principe": document.getElementById('country_code').value = "+239"; break;
        case "Saudi Arabia": document.getElementById('country_code').value = "+966"; break;
        case "Senegal": document.getElementById('country_code').value = "+221"; break;
        case "Serbia": document.getElementById('country_code').value = "+381"; break;
        case "Seychelles": document.getElementById('country_code').value = "+248"; break;
        case "Sierra Leone": document.getElementById('country_code').value = "+232"; break;
        case "Singapore": document.getElementById('country_code').value = "+65"; break;
        case "Slovakia": document.getElementById('country_code').value = "+421"; break;
        case "Slovenia": document.getElementById('country_code').value = "+386"; break;
        case "Solomon Islands": document.getElementById('country_code').value = "+677"; break;
        case "Somalia": document.getElementById('country_code').value = "+252"; break;
        case "South Africa": document.getElementById('country_code').value = "+27"; break;
        case "Spain": document.getElementById('country_code').value = "+34"; break;
        case "Sri Lanka": document.getElementById('country_code').value = "+94"; break;
        case "Sudan": document.getElementById('country_code').value = "+249"; break;
        case "Suriname": document.getElementById('country_code').value = "+597"; break;
        case "Swaziland": document.getElementById('country_code').value = "+268"; break;
        case "Sweden": document.getElementById('country_code').value = "+46"; break;
        case "Switzerland": document.getElementById('country_code').value = "+41"; break;
        case "Syrian Arab Republic": document.getElementById('country_code').value = "+963"; break;
        case "Taiwan": document.getElementById('country_code').value = "+886"; break;
        case "Tajikistan": document.getElementById('country_code').value = "+992"; break;
        case "Tanzania": document.getElementById('country_code').value = "+255"; break;
        case "Thailand": document.getElementById('country_code').value = "+66"; break;
        case "Togo": document.getElementById('country_code').value = "+228"; break;
        case "Tokelau": document.getElementById('country_code').value = "+690"; break;
        case "Tonga": document.getElementById('country_code').value = "+676"; break;
        case "Trinidad And Tobago": document.getElementById('country_code').value = "+1868"; break;
        case "Tunisia": document.getElementById('country_code').value = "+216"; break;
        case "Turkey": document.getElementById('country_code').value = "+90"; break;
        case "Turkmenistan": document.getElementById('country_code').value = "+993"; break;
        case "Turks and Caicos Islands": document.getElementById('country_code').value = "+1649"; break;
        case "Tuvalu": document.getElementById('country_code').value = "+688"; break;
        case "Uganda": document.getElementById('country_code').value = "+256"; break;
        case "Ukraine": document.getElementById('country_code').value = "+380"; break;
        case "United Arab Emirates": document.getElementById('country_code').value = "+971"; break;
        case "US Virgin Islands": document.getElementById('country_code').value = "+1340"; break;
        case "Uruguay": document.getElementById('country_code').value = "+598"; break;
        case "Uzbekistan": document.getElementById('country_code').value = "+998"; break;
        case "Vanuatu": document.getElementById('country_code').value = "+678"; break;
        case "Vatican City State": document.getElementById('country_code').value = "+39"; break;
        case "Venezuela": document.getElementById('country_code').value = "+58"; break;
        case "Vietnam": document.getElementById('country_code').value = "+84"; break;
        case "Wallis And Futuna Islands": document.getElementById('country_code').value = "+681"; break;
        case "Yemen": document.getElementById('country_code').value = "+967"; break;
        case "Zambia": document.getElementById('country_code').value = "+260"; break;
        case "Zanzibar": document.getElementById('country_code').value = "+255"; break;
        case "Zimbabwe": document.getElementById('country_code').value = "+263"; break;
        default: document.getElementById('country_code').value = ""; break;
    }
}



function validate() {

    var errors = '';

    var input = document.forms["form"];

    if (input.first_name.value == "" || document.forms["form"].first_name.value == null) {
        errors = buildErrorMessage(errors, 'First Name');
    }

    if (input.last_name.value == "" || document.forms["form"].last_name.value == null) {
        errors = buildErrorMessage(errors, 'Last Name');
    }

    if (input.email.value == "" || document.forms["form"].email.value == null || validateEmail(input.email.value) == false) {
        errors = buildErrorMessage(errors, 'Email');
    }

    if (input.country.value == "" || document.forms["form"].country.value == null) {
        errors = buildErrorMessage(errors, 'Country');
    }

    if (input.phone.value == "" || document.forms["form"].phone.value == null) {
        errors = buildErrorMessage(errors, 'Phone');
    }
    if (errors != '') {
        alert("All fields are required and must be valid. Please enter the following: " + errors);
        return false;
    }
    else {
        return true;
    }

    //   Validation Rules Passed - Return True

}
//
//   Helper Functions
//     

function validateEmail(src) {
    var emailReg = "^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$";
    var regex = new RegExp(emailReg);
    return regex.test(src);
}

function buildErrorMessage(Message, Error) {
    if (Message == '')
        return Message + Error;
    else
        return Message + ', ' + Error;
}