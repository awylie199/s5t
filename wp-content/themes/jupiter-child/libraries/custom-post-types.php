<?php

/* Add Custom Post Types */
function cascade_custom_post_type() {
    register_post_type('cm_landing_slide',[
        'label' => 'Landing Slides',
        'description' => 'Creates a new slide on the "Landing Page" template',
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'supports' => ['title', 'thumbnail']
    ]);
}
add_action('init', 'cascade_custom_post_type');

/* Add Custom Fields for Custom Post Types */
function cascade_admin_init() {
    add_meta_box('slide_content_meta', 'Slide Content', 'cascade_slide_content', 'cm_landing_slide', 'normal');
}
add_action('admin_init', 'cascade_admin_init');

function cascade_slide_content() {
    global $post;

    $custom = get_post_custom($post->ID);

    $fields = [
        'slide_type' => 'slide-1',
        'slide_order' => '0',
        'slide_header' => '',
        'slide_secondary_header' => '',
        'slide_header_details' => '',
        'slide_secondary_header_details' => '',
        'slide_secondary_call_link' => '',
        'slide_secondary_call_details' => ''
    ];

    foreach ($fields as $field => $value) {
        if (isset($custom[$field])) {
            $fields[$field] = $custom[$field][0];
        }
    }
    ?>
    <div>
        <fieldset style="margin-bottom: 20px;">
            <div style="display: inline-block; width: 50%; margin: 0; padding: 0;">
                <label for="slide_type">Slide Type</label><br>
                <select name="slide_type" id="slide_type" style="width: 50%;">
                    <option value="slide-1"<?= $fields['slide_type'] == 'slide-1' ? ' selected="selected"' : '' ?>>Type A</option>
                    <option value="slide-2"<?= $fields['slide_type'] == 'slide-2' ? ' selected="selected"' : '' ?>>Type B</option>
                </select>
            </div>
            <div style="display: inline-block; width: 50%; margin: 0 0 0 -4px; padding: 0;">
                <label for="slide_order">Slide Order</label><br>
                <input id="slide_order" type="text" name="slide_order" value="<?= $fields['slide_order'] ?>" style="width: 50%;">
            </div>
        </fieldset>
        <fieldset style="margin-bottom: 20px;">
            <label for="slide_header">Header</label><br>
            <input id="slide_header" type="text" name="slide_header" value="<?= $fields['slide_header'] ?>" style="width: 100%;">

            <label for="slide_header_details">Header Details</label><br>
            <textarea name="slide_header_details" id="slide_header_details" rows="5" style="width: 100%;"><?= $fields['slide_header_details'] ?></textarea>
        </fieldset>
        <fieldset style="margin-bottom: 20px;">
            <label for="slide_secondary_header">Secondary Header</label><br>
            <input id="slide_secondary_header" type="text" name="slide_secondary_header" value="<?= $fields['slide_secondary_header'] ?>" style="width: 100%;">

            <label for="slide_secondary_header_details">Secondary Header Details</label><br>
            <textarea name="slide_secondary_header_details" id="slide_secondary_header_details" rows="5" style="width: 100%;"><?= $fields['slide_secondary_header_details'] ?></textarea>

            <label for="slide_secondary_call_link">Call to Action Link</label><br>
            <input id="slide_secondary_call_link" type="text" name="slide_secondary_call_link" value="<?= $fields['slide_secondary_call_link'] ?>" style="width: 100%;">

            <label for="slide_secondary_call_details">Call to Action Details</label><br>
            <input id="slide_secondary_call_details" type="text" name="slide_secondary_call_details" value="<?= $fields['slide_secondary_call_details'] ?>" style="width: 100%;">
        </fieldset>
    </div>

    <script type="text/javascript">
        (function ($) {
            $(function () {
                $('.upload-image').on('click', function (event) {
                    event.preventDefault();

                    var label = $(this).parent().find('label').text();

                    tb_show(label, 'media-upload.php?type=image&TB_iframe=true');

                    send_to_editor = function (html) {
                        var src = $(html).attr('src');
                        $(this).parent().find('input').val(src);
                        $(this).parent().find('img').attr('src', src);
                        tb_remove();
                    }.bind(this);
                });
            });
        })(jQuery);
    </script>
    <?php
}

function cascade_save_slide_content() {
    global $post;

    if (!$post) return;

    if (isset($_POST['post_type']) && $_POST['post_type'] === 'cm_landing_slide') {
        update_post_meta($post->ID, 'slide_type', $_POST['slide_type']);
        update_post_meta($post->ID, 'slide_order', $_POST['slide_order']);
        update_post_meta($post->ID, 'slide_header', $_POST['slide_header']);
        update_post_meta($post->ID, 'slide_header_details', $_POST['slide_header_details']);
        update_post_meta($post->ID, 'slide_secondary_header', $_POST['slide_secondary_header']);
        update_post_meta($post->ID, 'slide_secondary_header_details', $_POST['slide_secondary_header_details']);
        update_post_meta($post->ID, 'slide_secondary_call_link', $_POST['slide_secondary_call_link']);
        update_post_meta($post->ID, 'slide_secondary_call_details', $_POST['slide_secondary_call_details']);
    }
}
add_action('save_post', 'cascade_save_slide_content');
