/*function S5T_getForm(target)
{
  var xhr = new XMLHttpRequest();
  var url = "https://staging.stage5trading.com/wp-content/themes/jupiter-child/libraries/3rdPartyDemoForm/form.php";
  xhr.open("POST", url, true);

  //Send the proper header information along with the request
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  xhr.onreadystatechange = function() {//Call a function when the state changes.
      if(xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
      }
  }
  xhr.send();
}*/

function S5T_beforeSubmit(e)
{
  document.getElementById('S5T_recaptcha').click();
  e.preventDefault();
}

function S5T_submit(captcha)
{
  var xhr = new XMLHttpRequest();
  var url = "https://stage5trading.com/wp-content/themes/jupiter-child/libraries/3rdPartyDemoForm/gateway.php";

  var params = "FirstNameEDIT="+document.getElementById("FirstNameEDIT").value +
  "&LastNameEDIT="+document.getElementById("LastNameEDIT").value +
  "&EmailEDIT="+document.getElementById("EmailEDIT").value +
  "&PhoneEDIT="+document.getElementById("PhoneEDIT").value +
  "&PLNameEDIT="+document.getElementById("PLNameEDIT").value +
  "&referrerURL="+document.getElementById("referrerURL").value +
  "&partnerName="+document.getElementById("S5T_partnerName").value +
  "&g-recaptcha-response="+captcha;


  xhr.open("POST", url, true);

  //Send the proper header information along with the request
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  xhr.onreadystatechange = function() {//Call a function when the state changes.
      if(xhr.readyState == 4 && xhr.status == 200) {
        document.getElementById('S5T_errorMsg').innerHTML = "";
        document.getElementById('S5T_successMsg').innerHTML = "";
        console.log(xhr.responseText);
          var data = JSON.parse(xhr.responseText);

          if(data.success)
          {
            document.getElementById('S5T_successMsg').innerHTML = data.msg;
          }
          else {
            document.getElementById('S5T_errorMsg').innerHTML = data.msg;
          }
          document.getElementById("S5T_submitButton").disabled = false;
      }
  }
  xhr.send(params);
  document.getElementById("S5T_submitButton").disabled = true;
  grecaptcha.reset();
}
