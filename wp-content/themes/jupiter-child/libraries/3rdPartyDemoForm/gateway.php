<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

/*require_once("../../../../../wp-load.php");

$submission = GFAPI::submit_form(16, $input_values);
print_r($submission);*/

$success = false;
$msg = "No captcha";
if(isset($_POST['g-recaptcha-response']))
{
  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $fields = array(
    'secret' => "6LcmDx4UAAAAANPIMEm-nLsVmV0N1KmD4g0ArUum",
    'response' => urlencode($_POST['g-recaptcha-response']),
    'remoteip' => urlencode($_SERVER['REMOTE_ADDR'] ),
  );
  $fields_string = "";
  foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
  rtrim($fields_string, '&');

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_POST, count($fields));
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $result = curl_exec($ch);
  $result = json_decode($result);
  if($result->success)
  {
    require_once("../../../../../wp-load.php");


    $formId = getFormId();
    if($formId > 0)
    {
      $fields = GFAPI::get_form(16)['fields'];
      $fieldNameToId = array();
      foreach($fields as $field)
      {
        $label = strtolower($field->label);
        $label = str_replace(" ", "", $label);
        $fieldNameToId[$label] = $field->id;
      }
      $input_values = array(
        "input_".$fieldNameToId["firstname"]=>$_POST['FirstNameEDIT'],
        "input_".$fieldNameToId["lastname"]=>$_POST['LastNameEDIT'],
        "input_".$fieldNameToId["email"]=>$_POST['EmailEDIT'],
        "input_".$fieldNameToId["phone"]=>(isset($_POST['PhoneEDIT']) ? $_POST['PhoneEDIT'] : ""),
        "input_".$fieldNameToId["partnername"]=>$_POST['partnerName'],
        "input_".$fieldNameToId["ipaddress"]=>$_SERVER['REMOTE_ADDR'],
        "input_".$fieldNameToId["referrerurl"]=>$_POST['referrerURL'],
        "input_".$fieldNameToId["plnameedit"]=>$_POST['PLNameEDIT']
      );
      $submission = GFAPI::submit_form(16, $input_values);
      $success = $submission['is_valid'];
      if(!$succes)
      {
        if(count($submission['validation_messages']) > 0)
        {
          $msg = array_values($submission['validation_messages'])[0];
        }
        else if(strlen($CQGDS_errorMessage) > 0)
        {
          $msg = $CQGDS_errorMessage;
        }
        else {
          $msg = "Something went wrong, please try again later";
        }

      }
    }
    else {
      $msg = "Something went wrong, please try again later";
    }

  }
  curl_close($ch);
}
$response = new stdClass();
$response->success = $success;
$response->msg = $msg;
echo json_encode($response);

/*require_once("../../../../wp-load.php");
$input_values = array(
  "input_1"=>"test",
  "input_2"=>"testing",
  "input_3"=>"test@test.dk",
  "input_4"=>"12345678",
  "input_5"=>"AgenaTrader",
  "input_6"=>$_SERVER['REMOTE_ADDR'],
  "input_21"=>"testing.dk"
);
$submission = GFAPI::submit_form(16, $input_values);
print_r($submission);
//print_r(GFAPI::get_form(16)['fields']);

echo "hej";*/


function getFormId()
{
  $forms = GFAPI::get_forms();
  foreach($forms as $form)
  {
    $formName = strtolower($form['title']);
    $formName = str_replace(" ", "", $formName);
    if($formName == "s5tdemoformdummy")
    {
      return $form['id'];
    }
  }
  return -1;
}
?>
