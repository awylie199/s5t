<?php

/**
 * Separates a string by common delimeters: , ; |
 *
 * @param string $string
 *
 * @return array
 */
function trim_explode($string)
{
    return array_map('trim', preg_split('/,|;|\|/', $string));
}
