<?php

function cascade_ninja_forms_try_demo() {
    global $ninja_forms_processing;

    $formSettings = $ninja_forms_processing->get_all_form_settings();

    if (strtolower($formSettings['form_title']) != 'try a demo') return;

    $formFields = $ninja_forms_processing->get_all_fields();
    $processedFields = [];
    $errorField = 'general';

    foreach ($formFields as $id => $value) {
        $fieldSettings = $ninja_forms_processing->get_field_settings($id);
        $fieldData = $fieldSettings['data'];
        $fieldName = str_replace(' ', '_', strtolower($fieldData['label']));

        $processedFields[$fieldName] = $value;

        if ($fieldName == 'email') {
            $errorField = $id;
        }
    }

    try {
        $client = new SoapClient('https://ibportal.gainfutures.com/IBWeb/IBDemoManager/IBDemoManager.asmx?wsdl');
        $response = $client->demosetup(
            'G0!=@%fut40', 		                // AccessCode
            'S5TDemo', 		                    // NewUserCategoryName
            'S5TUser', 		                    // TemplateUserName
            'Indirect', 		                // CusType
            210, 		                        // WLabelID
            $processedFields['referred_by'],    // SCodeID
            105, 		                        // SoftID
            $processedFields['first_name'],		// FName
            $processedFields['last_name'], 		// LName
            $processedFields['email'],          // Email // @TODO: remove time()
            $processedFields['phone'],   		// Phone
            '', 		                        // Address
            '', 		                        // City
            '', 		                        // Zip
            '', 		                        // State
            '', 		                        // Country
            '', 		                        // CountryName
            '', 		                        // AssetTypes
            '', 		                        // How
            '', 		                        // MoreEmail
            $processedFields['ip_address'],		// RemoteAddr
            ''   		                        // CampaignID
        );

        if (strpos($response['Result'], 'Error:') === 0) {
            $error = str_replace('Error:', 'Error creating demo account with Gain:', $response['Result']);
            $ninja_forms_processing->add_error('response', $error, $errorField);
            return;
        }

        cascade_ninja_forms_try_demo_email(array_merge($processedFields, $response));

    } catch (Exception $e) {
        $error = 'Error creating demo account with Gain';
        $ninja_forms_processing->add_error('client', $error);
    }
}
add_action('ninja_forms_process', 'cascade_ninja_forms_try_demo');

function cascade_ninja_forms_try_demo_email($fields) {
    $headers  = "From: Stage 5 Trading Corp. <demo@s5trading.com>\r\n";
    $headers .= "Reply-To: S5 Demo <demo@s5trading.com>\r\n";
    $headers .= "Return-Path: S5 Demo <demo@s5trading.com>\r\n";
    $headers .= "Organization: Stage 5 Trading Corp\r\n";
    $headers .= "Content-Type: text/plain; charset=iso-8859-1\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

    $subject = "Stage 5 Practice Account";

    $message  = "Hello " . ucfirst($fields['first_name']) . ' ' . ucfirst($fields['last_name']) . ",\r\n\r\n";
    $message .= "Your simulated trading account has been created.\r\n\r\n";
    $message .= "Your S5 Trader Username and Password is:\r\n\r\n";
    $message .= "Username: " . $fields['DemoUName'] . "\r\n";
    $message .= "Password: " . $fields['DemoPass'] . "\r\n";
    $message .= "IP Address: " . $fields['ip_address'] . "\r\n";
    $message .= "(Note: Your username and password are case sensitive.)\r\n\r\n";
    $message .= "If you have not yet downloaded S5 Trader you can do so by going to: https://ibportal.gainfutures.com/DemoDownload?WLID=210\r\n\r\n";
    $message .= "If you have any questions about S5 Trader or opening an account, please email client-services@s5trading.com\r\n\r\n";
    $message .= "Best Regards,\r\n";
    $message .= "Stage 5 Trading Corp\r\n";
    $message .= "http://www.stage5trading.com\r\n\r\n";
    $message .= "Trading Futures and Options on Futures involves substantial risk of loss and is not suitable for all investors. You should carefully consider whether trading is suitable for you in light of your circumstances, knowledge, and financial resources. You may lose all or more of your initial investment. Opinions, market data, and recommendations are subject to change at any time.\r\n";

    mail($fields['email'], $subject, $message, $headers);
}








