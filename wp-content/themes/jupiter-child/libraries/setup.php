<?php

/* Disable Theme File Editor */
function cascade_remove_editor_menu() {
    remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'cascade_remove_editor_menu', 1);

/* Scripts and Stylesheets */
function cascade_scripts() {
    $template_uri = get_stylesheet_directory_uri();

    $assets = [
        'css' => $template_uri . '/resources/dist/css/main.css',
        // 'js' => $template_uri . '/resources/dist/js/main.js'
    ];

    wp_enqueue_style('cascade-main', $assets['css'], false, null);
    // wp_enqueue_script('cascade-main', $assets['js'], [], null, true);
}

function cascade_fonts() {
    wp_register_style('cascade-fonts-play', 'https://fonts.googleapis.com/css?family=Play:400,700');
    wp_register_style('cascade-fonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic');
    wp_register_style('cascade-fonts-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
    wp_enqueue_style('cascade-fonts-play');
    wp_enqueue_style('cascade-fonts-opensans');
    wp_enqueue_style('cascade-fonts-fontawesome');
}

add_action('wp_print_styles', 'cascade_fonts');

add_action('wp_enqueue_scripts', 'cascade_scripts', 100);

/* Login/Logout Menu Item */
//function cascade_authenticate($items, $args) {
//    $liClass = 'menu-item menu-item-type-post_type menu-item-object-page no-mega-menu';
//    $aClass = 'menu-item-link js-smooth-scroll';
//    $template = '<li class="%s"><a class="%s" href="%s">%s</a></li>';
//
//    if ($args->theme_location == 'primary-menu') {
//        if (is_user_logged_in()) {
//            $items .= sprintf($template, $liClass, $aClass, wp_logout_url(), 'Logout');
//        } else {
//            $items .= sprintf($template, $liClass, $aClass, wp_login_url(), 'Login');
//        }
//    }
//
//    return $items;
//}
//add_filter('wp_nav_menu_items', 'cascade_authenticate', 10, 2);
