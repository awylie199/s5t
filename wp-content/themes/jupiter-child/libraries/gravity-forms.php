<?php
// Update log
// February 2, 2017: Sending an error message when Gain returns an error or an e-mail address is blocked


function validate_vin($validation_result)
{
  $form = $validation_result['form'];
  $validation_result['is_valid'] = false;
  return $validation_result;
}
//add_filter( 'gform_validation_5', 'validate_vin' );


$GainGravityFormsError = FALSE;
function change_message( $message, $form ) {
    global $GainGravityFormsError;
  //if(strtolower($form['title']) == 'try a demo' && $form['fields'][0]->value !== FALSE)
  if(strtolower($form['title']) == 'try a demo' && $GainGravityFormsError !== FALSE)
  {
      //return "<div class='validation_error'>Error creating demo account with Gain: " . $form['fields'][0]->value . "test</div>";
    //return $form['fields'][0]->value;
    return $GainGravityFormsError;
  }
  return $message;
}
add_filter( 'gform_validation_message', 'change_message', 0, 2 );


function progress8_gravity_try_demo($validation_result) {
    global $GainGravityFormsError;
  $form = $validation_result['form'];
  $form['fields'][0]->value = false;
  if(!$validation_result['is_valid'])
  {
    return $validation_result;
  }

  if (strtolower($form['title']) != 'try a demo') return $validation_result;

  $formFields = $form['fields'];
  $processedFields = [];
  $errorField = 'general';
  $test = "";

  foreach ($formFields as $fieldId => $field) {
      $fieldName = str_replace(' ', '_', strtolower($field->label));
      $fieldValue = RGFormsModel::get_field_value($field);

      if(strlen($fieldValue) > 0)
      {
        $processedFields[$fieldName] = $fieldValue;
      }

      if ($fieldName == 'email') {
          $errorField = $fieldId;
      }
  }
  //$processedFields['ip_address'] = '202.93.219.18';
  error_log("demo");
  //return $validation_result;
  try {
      $client = new SoapClient('https://ibportal.gainfutures.com/IBWeb/IBDemoManager/IBDemoManager.asmx?wsdl');
      $response = $client->demosetup(
          'G0!=@%fut40', 		                // AccessCode
          'S5TDemo', 		                    // NewUserCategoryName
          'S5TUser', 		                    // TemplateUserName
          'Indirect', 		                // CusType
          210, 		                        // WLabelID
          $processedFields['referred_by'],    // SCodeID
          105, 		                        // SoftID
          $processedFields['first_name'],		// FName
          $processedFields['last_name'], 		// LName
          $processedFields['email'],          // Email // @TODO: remove time()
          $processedFields['phone'],   		// Phone
          '', 		                        // Address
          '', 		                        // City
          '', 		                        // Zip
          '', 		                        // State
          '', 		                        // Country
          '', 		                        // CountryName
          '', 		                        // AssetTypes
          '', 		                        // How
          '', 		                        // MoreEmail
          $processedFields['ip_address'],		// RemoteAddr
          ''   		                        // CampaignID
      );

      if (strpos($response['Result'], 'Error:') === 0) {
          $validation_result['is_valid'] = false;
          $GainGravityFormsError = "<div class='validation_error'>Error creating demo account with Gain: " . $response['Result'] . "</div>";
          $form['fields'][0]->value = "<div class='validation_error'>Error creating demo account with Gain: " . $response['Result'] . "</div>";
          error_log("Demo form error: " . $response['Result']);
          sendErrorMail("Gain error", $response['Result'], $processedFields);
          return $validation_result;
      }

      cascade_ninja_forms_try_demo_email(array_merge($processedFields, $response));

  } catch (Exception $e) {
      $error = "<div class='validation_error'>Error creating demo account with Gain</div>";
      $GainGravityFormsError = "<div class='validation_error'>Error creating demo account with Gain</div>";
      $validation_result['is_valid'] = false;
      sendErrorMail("Gain error", $response['Result'], $processedFields);
      $form['fields'][0]->value = $error;
  }

  return $validation_result;
}
//add_filter('gform_validation', 'progress8_gravity_try_demo', 10);


add_filter( 'gform_zohocrm_field_value', function ( $value, $form, $entry, $field_id ) {
    if (strtolower($form['title']) != 'try a demo') return $value;
    //error_log(strtolower($form['title']));
    $field = RGFormsModel::get_field( $form, $field_id );
    if ( is_object( $field ) && strtolower($field->label) == 'referring partner' ) {
      foreach($field->choices as $id=>$choice)
      {
        if($choice['value'] == $value)
        {
          $value = $choice['text'];
        }
      }
    }
    return $value;
}, 10, 4 );


function cascade_ninja_forms_try_demo_email($fields) {
    $headers  = "From: Stage 5 Trading Corp. <demo@s5trading.com>\r\n";
    $headers .= "Reply-To: S5 Demo <demo@s5trading.com>\r\n";
    $headers .= "Return-Path: S5 Demo <demo@s5trading.com>\r\n";
    $headers .= "Organization: Stage 5 Trading Corp\r\n";
    $headers .= "Content-Type: text/plain; charset=iso-8859-1\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

    $subject = "Stage 5 Practice Account";

    $message  = "Hello " . ucfirst($fields['first_name']) . ' ' . ucfirst($fields['last_name']) . ",\r\n\r\n";
    $message .= "Your simulated trading account has been created.\r\n\r\n";
    $message .= "Your S5 Trader Username and Password is:\r\n\r\n";
    $message .= "Username: " . $fields['DemoUName'] . "\r\n";
    $message .= "Password: " . $fields['DemoPass'] . "\r\n";
    $message .= "IP Address: " . $fields['ip_address'] . "\r\n";
    $message .= "(Note: Your username and password are case sensitive.)\r\n\r\n";
    $message .= "If you have not yet downloaded S5 Trader you can do so by going to: https://ibportal.gainfutures.com/DemoDownload?WLID=210\r\n\r\n";
    $message .= "If you have any questions about S5 Trader or opening an account, please email client-services@s5trading.com\r\n\r\n";
    $message .= "Best Regards,\r\n";
    $message .= "Stage 5 Trading Corp\r\n";
    $message .= "http://www.stage5trading.com\r\n\r\n";
    $message .= "Trading Futures and Options on Futures involves substantial risk of loss and is not suitable for all investors. You should carefully consider whether trading is suitable for you in light of your circumstances, knowledge, and financial resources. You may lose all or more of your initial investment. Opinions, market data, and recommendations are subject to change at any time.\r\n";

    if(!mail($fields['email'], $subject, $message, $headers))
    {
    	sendErrorMail("Email error", "Could not send mail, but user is created", $fields);
    }
}
