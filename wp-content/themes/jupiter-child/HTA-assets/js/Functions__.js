// Functional JS
window.trades = null;
var selectedAccount = "", selectedContract = "";

$(document).ready(function(e) {
	
	init();
	
	function init() {
		// Initialize DatePickers
		// // REZA
		// $("#from-datepicker").val( moment("2016-09-28").format("MM/DD/YYYY") ).datepicker({
		// 	onSelect: function (dates) {
		// 		CheckDateRangeAndGetData();
		// 	}
		// });
		
		// $("#to-datepicker").val( moment("2016-09-28").format("MM/DD/YYYY") ).datepicker({
		// 	onSelect: function (dates) {
		// 		CheckDateRangeAndGetData();
		// 	}
		// });		


		$("#from-datepicker").val( moment().subtract(90,'days').format("MM/DD/YYYY") ).datepicker({
			onClose: function (dates) {
				CheckDateRangeAndGetData();
			}
		});
		
		$("#to-datepicker").val( moment().format("MM/DD/YYYY") ).datepicker({
			// OLD onSelect
			onClose: function (dates) {
				CheckDateRangeAndGetData();
			}
		});

		var LinkedAccounts_GAIN_LIVE_Mapped = $.map(LinkedAccounts_GAIN_LIVE, function (val, i) { return "GAIN_LIVE|" + val; });
			LinkedAccounts_GAIN_DEMO_Mapped = $.map(LinkedAccounts_GAIN_DEMO, function (val, i) { return "GAIN_DEMO|" + val; });
			LinkedAccounts_CQG_LIVE_Mapped = $.map(LinkedAccounts_CQG_LIVE, function (val, i) { return "CQG_LIVE|" + val; });
		var TotalArray = $.merge( $.merge(LinkedAccounts_GAIN_LIVE_Mapped, LinkedAccounts_GAIN_DEMO_Mapped), LinkedAccounts_CQG_LIVE_Mapped);

		if (typeof isAdmin != "undefined" && isAdmin) {
			$("#admin-account-selector").autocomplete({
			  source: TotalArray,
			  select: function (event, ui) {
			  	    selectedAccount = ui.item.value.trim();
						var Broker = selectedAccount.split('|')[0],
					Account = selectedAccount.split('|')[1];

					// after one second, black out the name of the account with ******XX
			  	 setTimeout ( function () { 
			  	 		$("#admin-account-selector").val(Broker + ("*").repeat(Account.length-3) + selectedAccount.substr(selectedAccount.length-3, 3));
			  	 	}, 1000 );

			  	CheckDateRangeAndGetData();
			  }
			});

		  	$("#admin-account-selector").click(function() { $(this).select(); } );

		} else {
			// Intialize first account as "selected"
			CheckDateRangeAndGetData();	
		}

		// Set daily loss limit parameters
		if (ResultsDisplayType == "Dollars") $("label#daily-loss-limit").text("$ ");
		else $("label#daily-loss-limit").text("(" + ResultsDisplayType + ") ");
		$("input#daily-loss-limit").val(0);


		// Reflow the page, as if split panes were adjusted
		SplitPaneAdjusted();
	}


	$("form#daily-loss-limit-form").submit(function (e) {
		e.preventDefault();
		e.stopPropagation();

		$("#analyze").trigger("click");
	});

	$("#analyze").click(function(e) {
		var self = this;
		$(self).attr("disabled", true).css("background-color", "#333");
		
		var Broker = selectedAccount.split('|')[0],
			Account = selectedAccount.split('|')[1],
			Contracts = $("#contract-selector").multiselect("getChecked").map(function(){ return this.value; }).get(),
			StartDateStr = $("#from-datepicker").val(),
			EndDateStr = $("#to-datepicker").val();

		var	StartDate = moment(StartDateStr, "MM-DD-YYYY").hours(0).minutes(0).seconds(0),
			EndDate = moment(EndDateStr, "MM-DD-YYYY").hours(23).minutes(59).seconds(59);
		
		if (EndDate < StartDate) {
			// If EndDate is before Start Date then error
			if (typeof popupNotification == "function") popupNotification("End Date should be set after Start Date.", "warning", 5000);
		} else if (Contracts.length == 0) {
			// If no contracts are selected, show warning
			if (typeof popupNotification == "function") popupNotification("At least one contract must be selected.", "warning", 5000);
		} else {
			if (Broker != null && Broker != "" && Account != null && Account != "" && StartDate.isValid() && EndDate.isValid()) {
				// Show working Icon
	            $("#contract-loading-icon").show();
				var startTime = (new Date()).getTime();

				$.ajax({
					url: "includes/HTA-GetFillData.php",
					type: "post",
					method: "post",
					dataType: "json",
					data: { Username: username, Broker: Broker, Account: Account, Contracts: Contracts, StartDate: StartDate.utc().format('YYYY-MM-DD HH:mm:ss'), EndDate: EndDate.utc().format('YYYY-MM-DD HH:mm:ss') },
					error: function (err) {
						if (typeof popupNotification == "function") popupNotification(err.responseText, "error", 30000);
						$("#contract-loading-icon").hide();
						$(self).attr("disabled", false).css("background-color", "#0066ff");
					},
					success: function (data) {
						//console.log("time to get data from server " + ((new Date()).getTime() - startTime) + " milliseconds");
						//$("#contract-loading-icon").hide();
						if (data != null && data.OutputType != null && data.OutputType == "DATA" && data.Data != null){
							if (data.Data.length > 0) {								
								// Initiate the trades Handler to process all trades and metrics
								// if (isAdmin) {
								// 	var Account = $("#admin-account-selector").val();
								// } else {
								// 	var Account = $("#account-selector").val();
								// }
								
								window.trades = new TradeHandler(data.Data, data.ContractDetails, data.DaysAndProductsTradedArray, CommissionFee, selectedAccount);
								
							} else {
								$("#contract-loading-icon").hide();
								$(self).attr("disabled", false).css("background-color", "#0066ff");
								if (typeof popupNotification == "function") popupNotification("No 'completed' trades were found for the specified timeline.<br /><br />Try other contracts, or extending your time-range.", "warning", 5000);
							}
						} else {
							if (typeof popupNotification == "function") popupNotification("There was an error.  Message: " + data.Data, "error", 5000);
							$("#contract-loading-icon").hide();
							$(self).attr("disabled", false).css("background-color", "#0066ff");
						}
					}
				});

			} else {
				$("#contract-loading-icon").hide();
				if (typeof popupNotification == "function") popupNotification("An unknown error occured while getting trades. Please contact Admin.", "error", 30000);
			}				
		}
    });


});

function CheckDateRangeAndGetData () {
	if (typeof selectedAccount == 'undefined' || selectedAccount == null || selectedAccount == "") {
		// unknown selected Account
		if ($("#account-selector").length > 0) selectedAccount = $("#account-selector").val().trim();
	}

	var Broker = selectedAccount.split('|')[0],
		Account = selectedAccount.split('|')[1],
		StartDateStr = $("#from-datepicker").val(),
		EndDateStr = $("#to-datepicker").val();
		
	var	StartDate = moment(StartDateStr, "MM-DD-YYYY").hours(0).minutes(0).seconds(0),
		EndDate = moment(EndDateStr, "MM-DD-YYYY").hours(23).minutes(59).seconds(59);
	
	if (EndDate < StartDate) {
		if (typeof popupNotification == "function") popupNotification("End Date should be set after Start Date.", "warning", 5000);
	} else {
		
		if (Broker != null && Broker != "" && Account != null && Account != "" && StartDate.isValid() && EndDate.isValid()) {
			$("#contract-loading-icon").show();

			
			var AllBoxesOfPrevSearchChecked = false;
			try {
				AllBoxesOfPrevSearchChecked = $("#contract-selector").multiselect("getChecked").length == $("#contract-selector").multiselect("widget").find(":checkbox").length;
			} catch (err) {
				$("#contract-selector").multiselect();
			};

			$("#contract-selector").empty();

			$.ajax({
				url: "includes/HTA-GetContractData.php",
				type: "post",
				method: "post",
				dataType: "json",
				data: { Broker: Broker, Account: Account, StartDate: StartDate.utc().format('YYYY-MM-DD HH:mm:ss'), EndDate: EndDate.utc().format('YYYY-MM-DD HH:mm:ss') },
				error: function (err) {
					if (typeof popupNotification == "function") popupNotification(err.statusText, "error", 0);
					$("#contract-loading-icon").hide();
				},
				success: function (data) {
					if (data != null && data.Data != null) {
						if (data.OutputType == "DATA") {
							if (data.Data.length > 0) {
								$.each (data.Data, function (i, val) {
									$("#contract-selector").append( $("<option>", { value: val.Contract, text: val.Contract }) );
								});
							} else {
								if (typeof popupNotification == "function") popupNotification("No contracts were active in your time-range.  Please select a wider range.", "warning", 5000);	
							}
						} else if (data.OutputType == "ERROR") {
							if (typeof popupNotification == "function") popupNotification(data.Data, "error", 0);
						} else {
							if (typeof popupNotification == "function") popupNotification("An unknown error occured during loading of the contracts.  Please contact admin.", "error", 0);
						}
					}

					$("#contract-loading-icon").hide();
					$("#contract-selector").multiselect("destroy");
					$("#contract-selector").multiselect({
						//header: "Select a contract.",
						f: "Select All",
						selectedList: 3,
						click: function(event, ui){

							// console.log('# of contracts selected', $("#contract-selector").multiselect("getChecked").map(function(){ return this.value; }).get().length);

							if ($("#contract-selector").multiselect("getChecked").map(function(){ return this.value; }).get().length == 1) {
								// One contract selected
								// $("td#daily-loss-limit").css("background-color", "transparent");
								$($("td#daily-loss-limit")[0]).html("Risk Budget for Period");
								// $("input#daily-loss-limit").css("background-color", "#fff");
								// $("input#daily-loss-limit").attr("disabled", false);
								// $("div #netto-number-dial").show();
							} else {
								// zero or more than one contract selected
								// $("td#daily-loss-limit").css("background-color", "#333");
								$($("td#daily-loss-limit")[0]).html("Risk Budget for Period across Products<br /><small>Enter the Risk Budget permitted for this<br />time period across all contracts</small>");										
								// $("input#daily-loss-limit").css("background-color", "#333");
								// $("input#daily-loss-limit").attr("disabled", true);
								// $("div #netto-number-dial").hide();
							}

							$("input#daily-loss-limit").val(0);
						}
					});	

					if (AllBoxesOfPrevSearchChecked) $("#contract-selector").multiselect("checkAll");
					var selectedContracts = $("#contract-selector").multiselect("getChecked").map(function(){ return this.value; }).get().length;
					if (selectedContracts == 1 || selectedContracts == 0) {
						// One contract selected
						$($("td#daily-loss-limit")[0]).html("Risk Budget for Period");
					} else {
						// zero or more than one contract selected
						$($("td#daily-loss-limit")[0]).html("Risk Budget for Period across Products<br /><small>Enter the Risk Budget permitted for this<br />time period across all contracts</small>");										
					}

					$("input#daily-loss-limit").val(0);
				}
			});
			
			//$("#contract-selector").multiselect();
		}					
	}
}
	
function SplitPaneAdjusted ($splitPane) {
	// find width of right-component (right pane)
	var paneObj = $("#right-component");
	if (paneObj.length == 1) {
		// found
		var width = paneObj.width();

		if (width < 800) {
			
			trade_count_chart.setTitle({ style: {"fontSize": "11px"} });
			hourly_performance_chart.setTitle({ text: "Hourly", style: {"fontSize": "11px"} });
			dayofweek_performance_chart.setTitle({ text: "Day of Week", style: {"fontSize": "11px"} });

		} else {

			trade_count_chart.setTitle({ style: {"fontSize": "16px"} });
			hourly_performance_chart.setTitle({ text: "Hourly Performance", style: {"fontSize": "16px"} });
			dayofweek_performance_chart.setTitle({ text: "Day of Week Performance", style: {"fontSize": "16px"} });

		}


	}


	
	// This function is called by the split-pane-management js, split-pane.js, when any pane is adjusted
	// the 'ReflowCharts' function in the "charts.js" is then called which reflows the charts for auto adjustment
	ReflowCharts();
}

