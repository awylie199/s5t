// This is the trade handler JS file for the HTA
window.trades = null;
window.FillsDisplayWindow = null;

var TradeHandler = function (Trades, ContractDetailsArray, ProductsArray, CommissionsInput, Account) {
	$("#contract-loading-icon").show();

	this.TradesList = [];
	this.ContractDetails = [];
	this.ProductsArray = (ProductsArray != null ? ProductsArray : []);
	this.Commissions = 0;
	
	this.Comissions = (CommissionsInput != null ? CommissionsInput : 0);
	this.ContractDetails = ContractDetailsArray;

	this.Account = Account;
	
	// Calculated values

	// Reset calculated metrics
	this.NumberOfWinningTrades = 0;
	this.NumberOfLosingTrades = 0;
	this.NumberOfTotalTrades = 0;
	this.PercentWinners = 0;
	this.PercentLosers = 0;
	
	this.NumberOfLongTrades = 0;
	this.NumberOfShortTrades = 0;
	this.ContractsTradedLong = 0;
	this.ContractsTradedShort = 0;
	this.MaxLongPosition = 0;
	this.MaxShortPosition = 0;

	this.Expectancy = 0;
	this.Efficiancy = 0;
	
	this.AverageGain = 0;
	this.AverageDraw = 0;
	this.MaxGain = 0;
	this.MaxDraw = 0;
	this.NetGain = 0;
	this.NetLoss = 0;
	this.TotalTiTGain = 0;
	this.TotalTiTLoss = 0;
	this.AverageTiTGain = 0;
	this.AverageTiTLoss = 0;
	this.MaxTiTGain = 0;
	this.MaxTiTLoss = 0;	
	this.MaxMFE = 0;
	this.MaxMAE = 0;
	this.TotalMFE = 0;
	this.TotalMAE = 0;

	this.GrossGain = 0;	
	this.GrossLoss = 0;
	this.NumberOfMaxConsecutiveGains = 0;
	this.NumberOfMaxConsecutiveLosses = 0;

	this.MaxBSO = 0;
	this.TotalBSO = 0;
	this.AverageBSO = 0;
	this.PercentBSO = 0;
	this.NumberOfFullStops = 0;
	this.PercentFullStop = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	
	this.MostTradedProduct = [];
	this.LastTradeStatus = "FLAT";
	
	var startTime = (new Date()).getTime();
	var TmpTradeList = [];
	var ContractsNotFoundInContractDetails = [];
	var ContractsIncludedCalculations = [];
	if (Trades != null && Trades.length > 0) {
		$.each (Trades, function (i, tr) {
			if (tr != null && tr.length > 0) {
				var Symbol = tr[0].Contract;
				var ContractFound = false;
				if (Symbol != null && Symbol.length > 2) {				
					var TickSize = 0, ContractSize = 0;
					$.each(ContractDetailsArray, function (i, cd) {
						if (cd.BaseContract == Symbol.substring(0, Symbol.length-2)) {
							TickSize = cd.TickSize;
							ContractSize = cd.ContractSize;
							ContractFound = true;
							return;	
						}
					});
				}
				
				// If contract details are found, add the trade to TradeList, else add to "Contract Not Found" error					
				if (ContractFound) {
					TmpTradeList.push(new Trade(Symbol, tr, TickSize, ContractSize, this.Commissions));
					ContractsIncludedCalculations.push(Symbol);
				} else {
					ContractsNotFoundInContractDetails.push(Symbol.substring(0, Symbol.length-2));					
				}
			}
		});
	}
	
	// Were there any contracts in the trades that did not get it's Contract details from the PHP call return?
	if (ContractsNotFoundInContractDetails.length > 0) {
		ContractsNotFoundInContractDetails = $.unique(ContractsNotFoundInContractDetails);	// remove duplicates
		if (typeof popupNotification == "function") popupNotification("Contract details with base symbol(s) '" + ContractsNotFoundInContractDetails.join(", ")  + "' were not found.  These trades will be excluded from the calculations.<br /><br />Please contact admin!", "error", 20000);
	}
	
	// Update "Products" stat under "Captured Data for Selected Parameters"
	ContractsIncludedCalculations = $.unique(ContractsIncludedCalculations);

	var contractsTradedDisplayText  = "";
	if (ContractsIncludedCalculations.length > 3) {
		contractsTradedDisplayText = ContractsIncludedCalculations.slice(0, 3).join(", ") + " (+ " + (ContractsIncludedCalculations.length-3) + " more)";
	} else {
		contractsTradedDisplayText = ContractsIncludedCalculations.join(", ");
	}

	$("#products-traded").text(contractsTradedDisplayText).attr("title", ContractsIncludedCalculations.join(", "));
	
	this.TradesList = TmpTradeList;
	if (this.TradesList.length > 0) {
		// Proceed with calculations
		var DisplayStr;
		if (this.TradesList.length >= 500) 
			DisplayStr = "Successfully loaded " + this.TradesList.length + " trades. Processing the trades may take 10-30 seconds.  Please wait...";
		else if (this.TradesList.length >= 300) 
			DisplayStr = "Successfully loaded " + this.TradesList.length + " trades. Processing the trades may take up to 10 seconds.  Please wait...";
		else
			DisplayStr = "Successfully loaded " + this.TradesList.length + " trades.";
			
		if (typeof popupNotification == "function") popupNotification(DisplayStr, "success", 5000);	

		var ThisClass = this;
		setTimeout(function () {
			
			ThisClass.CalcAll();
			
			var timeLapsed = ((new Date()).getTime() - startTime);
			console.log("milliseconds lapsed: " + timeLapsed);
			var displayStr = "";
			if (timeLapsed < 1000) displayStr = "Metrics processing took " + timeLapsed + " milliseconds.";
			else displayStr = "Metrics processing took " + Math.round(timeLapsed/1000); + " seconds.";
			
			//if (typeof popupNotification == "function") popupNotification(displayStr, "alert", 5000);
			
			// Start of Product Array calculations
			var MaxProductsTradedPerDay = 0;
			if (ThisClass.ProductsArray.length > 0) {
				$.each(ThisClass.ProductsArray, function (i, val) {
					if (val.Products != null && val.Products.length > MaxProductsTradedPerDay) MaxProductsTradedPerDay = val.Products.length;
				});
			}
			
			$("#max-products-traded").text(MaxProductsTradedPerDay);
			$("#days-traded").text(ThisClass.ProductsArray.length);	

			$("#contract-loading-icon").hide();
			$("#analyze").attr("disabled", false).css("background-color", "#0066ff");
			

			setTimeout(function () {
				// in 1 second, load user metrics
				if (window.trades != null)
					window.trades.GetUserTradeMetricsFromDB();	
			}, 1000);
	
		}, 500);

	} else {
		if (typeof popupNotification == "function") popupNotification("No trades were loaded!", "warning", 5000);		
		$("#contract-loading-icon").hide();
		$("#analyze").attr("disabled", false).css("background-color", "#0066ff");
	}
	
};

TradeHandler.prototype.CalcAll = function () {
	console.log("Calc All Function...");
	ClearAllCharts();
	
	if (ResultsDisplayType == null) ResultsDisplayType = "Ticks";
	var RiskBudget = $("input#daily-loss-limit").val();
	if (jQuery.isNumeric(RiskBudget)) {
		RiskBudget = Number(RiskBudget);
		if (RiskBudget < 0) if (typeof popupNotification == "function") popupNotification("Risk Budget inputted was less than zero.", "warning", 15000);
	} else {
		RiskBudget = -1;
		if (typeof popupNotification == "function") popupNotification("Risk Budget inputted was non-numeric.", "warning", 15000);
	}
	
	// Reset PDF printing trades
	window.CompletedTradesRows = [];


	var Account = this.Account;

	// Reset calculated metrics
	this.NumberOfWinningTrades = 0;
	this.NumberOfLosingTrades = 0;
	this.NumberOfTotalTrades = 0;
	this.PercentWinners = 0;
	this.PercentLosers = 0;
	
	this.NumberOfLongTrades = 0;
	this.NumberOfShortTrades = 0;
	this.ContractsTradedLong = 0;
	this.ContractsTradedShort = 0;
	this.MaxLongPosition = 0;
	this.MaxShortPosition = 0;

	this.Expectancy = 0;
	this.Efficiancy = 0;
	
	this.AverageGain = 0;
	this.AverageDraw = 0;
	this.MaxGain = 0;
	this.MaxDraw = 0;
	this.NetGain = 0;
	this.NetLoss = 0;
	this.TotalTiTGain = 0;
	this.TotalTiTLoss = 0;
	this.AverageTiTGain = 0;
	this.AverageTiTLoss = 0;
	this.MaxTiTGain = 0;
	this.MaxTiTLoss = 0;	
	this.MaxMFE = 0;
	this.MaxMAE = 0;
	this.TotalMFE = 0;
	this.TotalMAE = 0;

	this.GrossGain = 0;	
	this.GrossLoss = 0;
	this.NumberOfMaxConsecutiveGains = 0;
	this.NumberOfMaxConsecutiveLosses = 0;

	this.MaxBSO = 0;
	this.TotalBSO = 0;
	this.AverageBSO = 0;
	this.PercentBSO = 0;
	this.NumberOfFullStops = 0;
	this.PercentFullStop = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	
	this.MostTradedProduct = [];
	this.LastTradeStatus = "FLAT";
	
	// Cummnulative metrics (change per trades)
	var RunningPnL = 0,
		NettoNumber = 0,
		MinNettoNumber = 0,
		MaxNettoNumber = 0,
		MinExpectancy = 0,
		MaxExpectancy = 0,
		NumberOfConsecutiveLosses = 0,
		NumberOfConsecutiveGains = 0,
		PeakGain = 0,
		MaxDraw = 0,
		PeakRunningGain = 0,
		PeakRunningDraw = 0;
	
	var TradeCountPerHalfHour = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		HourlyPerformance =     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		DayOfWeekPerformance =  [0, 0, 0, 0, 0, 0],
		DailyPnLArray = 		[],
		TradeDistributionArray= [],
		PercentBSOArray = 		[],
		MFEMAERatioArray = 		[];
	
	var FirstDateTime = null, LastDateTime = null, LastTradesExitTimestamp = null;

	var TradeTypeArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
	var TradeErrorArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

	// Cumulative metrics
	// TO DO separate the metrics that need to be totaled and metrics that need to keep track of cummulative data (refer to notes)	

	var htmlBuilder = "<table id='trades-table' class='fancyDarkTable' border=0 cellspacing=0 cellpadding=0><thead><tr class='header'><th title='Trade Number'>#</th><th>Contract</th><th>L/S</th><th title='Max. Favorable Excursion'>MFE<br />(" + ResultsDisplayType + ")</th><th title='Max. Adverse Excursion'>MAE<br />(" + ResultsDisplayType + ")</th><th title='Best Scale Out'>BSO<br />(" + ResultsDisplayType + ")</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(" + ResultsDisplayType + ")</th><th>Running P&L<br />(" + ResultsDisplayType + ")</th><th>Entry Time<br /><small>(mm/dd/yy hh:mm:ss)</small></th><th>Exit Time<br /><small>(mm/dd/yy hh:mm:ss)</small></th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win</th><th>Avg. Loss</th><th>Expectancy</th><th>Peak Gain<br />(" + ResultsDisplayType + ")</th><th>Max Drawdown<br />(" + ResultsDisplayType + ")</th><th>Efficiency %</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Error (1-10)</th><th>Trade Type</th></tr></thead><tbody>";

	if (this.TradesList != null && this.TradesList.length > 0) {
		var TradeCount = this.TradesList.length;
		
		for (var i = 0; i < TradeCount; i++) {
			var trade = this.TradesList[i];
			
			// Calculate per trade metrics
			trade.Calc(ResultsDisplayType);

			// Increment the total number of trades computed so far
			this.NumberOfTotalTrades++;
			
			
			// DailyPnL & Trade Distribution arrays
			DailyPnLArray.push([ trade.ExitDateTime, trade.PnL ]);
			TradeDistributionArray.push(trade.PnL);
			
			// Determine first / last dates for DateRange Metric
			if (FirstDateTime == null) { FirstDateTime = trade.EntryDateTime, LastDateTime = trade.ExitDateTime }
			else {
				if (trade.EntryDateTime < FirstDateTime) FirstDateTime = trade.EntryDateTime;
				if (trade.ExitDateTime > LastDateTime) LastDateTime = trade.ExitDateTime;
			}
			
			// This section will take care of the 3 histograms 1.TradeCountPerHalfHour, 2.HourlyPerformance, 3.DayOfWeekPerformance			
			var hourOfTrade = trade.ExitDateTime.format('H') == 24 ? 0 : trade.ExitDateTime.format('H'),
				dayOfWeekOfTrade = trade.ExitDateTime.format('E') % 7;		// MOD 7, so that if the day is 7 (sunday) it is shown as 0

			TradeCountPerHalfHour[hourOfTrade]++;
			HourlyPerformance[hourOfTrade] += trade.PnL;
			DayOfWeekPerformance[dayOfWeekOfTrade] += trade.PnL;			
			
			
			// Calculate overall metrics
			if (trade.IsLong) {
				 this.NumberOfLongTrades++;
				 this.ContractsTradedLong += (trade.TotalContractsTraded/2);
				 this.MaxLongPosition = Math.max(this.MaxLongPosition, trade.MaxPosition);
			} else {
				 this.NumberOfShortTrades++;
				 this.ContractsTradedShort += (trade.TotalContractsTraded/2);
				 this.MaxShortPosition = Math.max(this.MaxShortPosition, trade.MaxPosition);				 
			}
			
			if (trade.PnL > 0) {
				// Winner
				this.NumberOfWinningTrades++;
				if (this.LastTradeStatus == "WINNER") { NumberOfConsecutiveGains++; }
				else { this.LastTradeStatus = "WINNER"; NumberOfConsecutiveGains = 1; NumberOfConsecutiveLosses = 0; }
				this.NumberOfMaxConsecutiveGains = Math.max( NumberOfConsecutiveGains, this.NumberOfMaxConsecutiveGains );

				this.TotalTiTGain += trade.TiT;
				this.MaxTiTGain = Math.max(this.MaxTiTGain, trade.TiT);
				this.MaxGain = Math.max(this.MaxGain, trade.PnL);
				PeakRunningGain = Math.max(PeakRunningGain, RunningPnL + trade.PnL);		// newly added on Oct 5, 2017 be request from Morad

				// Calculate Running Totals
				this.GrossGain += trade.PnL_Gross;
				this.NetGain += trade.PnL;
				this.MaxGain = ( Math.abs(trade.PnL) > this.MaxGain ? Math.abs(trade.PnL) : this.MaxGain );
				this.AverageGain = (this.NetGain / this.NumberOfWinningTrades);
				this.AverageTiTGain = (this.TotalTiTGain / this.NumberOfWinningTrades);
				
			} else if (trade.PnL < 0) {
				// Loser
				this.NumberOfLosingTrades++;
				if (this.LastTradeStatus == "LOSER") { NumberOfConsecutiveLosses++; }
				else { this.LastTradeStatus = "LOSER"; NumberOfConsecutiveLosses = 1; NumberOfConsecutiveGains = 0; }
				this.NumberOfMaxConsecutiveLosses = Math.max ( NumberOfConsecutiveLosses, this.NumberOfMaxConsecutiveLosses );
				
				this.TotalTiTLoss += trade.TiT;
				this.MaxTiTLoss = Math.max(this.MaxTiTLoss, trade.TiT);
				this.MaxDraw = Math.max(this.MaxDraw, Math.abs(trade.PnL));
				PeakRunningDraw = Math.min(PeakRunningDraw, RunningPnL + trade.PnL);
				
				// Calculate Running Totals
				this.GrossLoss += trade.PnL_Gross;
				this.NetLoss += trade.PnL;
				this.MaxDraw = ( Math.abs(trade.PnL) > this.MaxDraw ? Math.abs(trade.PnL) : this.MaxDraw );
				this.AverageDraw = (this.NetLoss / this.NumberOfLosingTrades);				
				this.AverageTiTLoss = (this.TotalTiTLoss / this.NumberOfLosingTrades);
				
				// Full Stop calculation
				if (trade.BSO <= 0) {
					 this.NumberOfFullStops++;
					 this.PercentFullStop = this.NumberOfFullStops / this.NumberOfTotalTrades;
				}
				
			} else {
				// Flat 
				this.LastTradeStatus = "FLAT";
			}
			
			// Calculate cumulative metrics
			this.PercentWinners = this.NumberOfWinningTrades / this.NumberOfTotalTrades;
			this.PercentLosers = this.NumberOfLosingTrades / this.NumberOfTotalTrades;
			
			this.AverageGain = this.NumberOfWinningTrades > 0 ? (this.NetGain / this.NumberOfWinningTrades) : 0;
			this.AverageDraw = this.NumberOfLosingTrades > 0 ? (this.NetLoss / this.NumberOfLosingTrades) : 0;
			
			this.TotalMFE += trade.MFE;
			this.TotalMAE += trade.MAE;
			MFEMAERatioArray.push( Round(trade.MAE) > 0 ? Round(trade.MFE/trade.MAE, 2) : 1 );
			
			this.MaxMFE = Math.max(this.MaxMFE, trade.MFE);
			this.MaxMAE = Math.max(this.MaxMAE, trade.MAE);
			
			this.MaxBSO = Math.max(this.MaxBSO, trade.BSO);

			this.TotalBSO += trade.BSO;
			if (trade.IsBSOEligible) {
				this.TotalBSOEligibleTrades++;
				if (trade.BSO > 0) this.TotalBSOTrades++;
			}
			this.AverageBSO = this.TotalBSO / this.TotalBSOEligibleTrades;
			this.PercentBSO = this.TotalBSOTrades / this.TotalBSOEligibleTrades;
			PercentBSOArray.push(ToPercent(this.PercentBSO));

			this.NumberOfFullStops = 0;

			this.AverageTiTGain =  this.TotalTiTGain / this.NumberOfWinningTrades;
			this.AverageTiTLoss =  this.TotalTiTLoss / this.NumberOfLosingTrades;
			
			//this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / this.NumberOfTotalTrades;
			// how is expectancy calculated depending on the "expected-value-per-contract" checkbox in settings
			if ( $("#expected-value-per-contract").length > 0 && !$("#expected-value-per-contract").is(":checked") ) {
				// Expected value is calculated as total (not per contract)
				this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / this.NumberOfTotalTrades;
			} else {
				// Expected value is calculated per contract
				this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / (this.ContractsTradedLong + this.ContractsTradedShort);
			}

			this.Efficiancy = this.NetGain/this.TotalMFE;
			
			RunningPnL += trade.PnL;
			
			// Netto Number (if RiskBudget + PeakRunningDraw === 0, the NettoNumber = 0)
			var contractsSelected = $("#contract-selector").multiselect("getChecked").map(function(){ return this.value; }).get().length;
			if (contractsSelected == 0 || contractsSelected == 1)
				NettoNumber = (RiskBudget + Math.abs(this.MaxMAE) === 0) ?  0 : (RunningPnL * 2) / (RiskBudget + Math.abs(this.MaxMAE));
			else 
				NettoNumber = (RiskBudget + Math.abs(PeakRunningDraw) === 0) ?  0 : (RunningPnL * 2) / (RiskBudget + Math.abs(PeakRunningDraw));

			if (i == 0) {
				// First calc set Netto number min/max
				MinNettoNumber = NettoNumber;
				MaxNettoNumber = NettoNumber;

				// First calc set Expectancy min/max
				MaxExpectancy = this.Expectancy;
				MinExpectancy = this.Expectancy;
			} else {
				// Netto Number set min/max
				MinNettoNumber = Math.min(NettoNumber, MinNettoNumber);
				MaxNettoNumber = Math.max(NettoNumber, MaxNettoNumber);

				// Expectancy set min/max
				MaxExpectancy = Math.max( this.Expectancy, MaxExpectancy );
				MinExpectancy = Math.min( this.Expectancy, MinExpectancy );
			}

			// console.log('netto #', NettoNumber, MinNettoNumber, MaxNettoNumber);
			// console.log(RunningPnL, RiskBudget, PeakRunningDraw);
			// Figure out the TSB for the trade, time in standby since the last trade
			if (LastTradesExitTimestamp == null) {
				trade.TSB = 0;
				LastTradesExitTimestamp = trade.ExitDateTime;
			} else {
				trade.TSB = trade.EntryDateTime - LastTradesExitTimestamp;
				LastTradesExitTimestamp = trade.ExitDateTime;
			}

			
			// Do display work here (we are INSIDE the trade iteration)
			// This constructs the display table, and updates (without redraw) the chart points
			var bgColor = (trade.PnL > 0 ? " style='background-color: rgba(25,47,68,0.3);' " : (trade.PnL < 0 ? " style='background-color: rgba(194,28,10,0.3);' " : ""));
			htmlBuilder += "<tr " + bgColor + " id='" + trade.FillID + "'><td><a class='display-fills' id='" + trade.FillID + "' title='" + trade.FillsList.length + " fills.'>" + (i+1) + "</a></td>"
						 + "<td><a class='display-fills' id='" + trade.FillID + "' title='" + trade.FillsList.length + " fills.'>" + trade.Symbol + "</a></td>"
						 + "<td>" + (trade.IsLong ? "L" : "S")  + "</td>"
						 + "<td>" + Round(trade.MFE) + "</td>"
						 + "<td>" + Round(trade.MAE) + "</td>"
						 + "<td>" + Math.floor(trade.BSO) + "</td>"
						 + "<td>" + trade.MaxPosition + "</td>"
						 + "<td>" + trade.TotalContractsTraded + "</td>"
						 + "<td>" + MillisecondsToTime(trade.TiT) + "</td>"
						 + "<td>" + MillisecondsToTime(trade.TSB) + "</td>"
						 + "<td>" + Round(trade.PnL, ResultsDisplayType == "Ticks" ? 0 : 2) + "</td>"
						 + "<td>" + Round(RunningPnL, ResultsDisplayType == "Ticks" ? 0 : 2) + "</td>"
						 + "<td>" + trade.EntryDateTime.local().format('MM/DD/YY H:mm:ss') + "</td>"
						 + "<td>" + trade.ExitDateTime.local().format('MM/DD/YY H:mm:ss') + "</td>"
						 + "<td>" + trade.EntryPrice + "</td>"
						 + "<td>" + trade.ExitPrice + "</td>"
						 + "<td>" + ToPercent(this.PercentWinners) + "</td>"
						 + "<td>" + ToPercent(this.PercentLosers) + "</td>"
						 + "<td>" + Round(this.AverageGain, 2) + "</td>"
						 + "<td>" + Round(this.AverageDraw, 2) + "</td>"
						 + "<td>" + Round(this.Expectancy, 2) + "</td>"
						 + "<td>" + Round(this.MaxGain, 2) + "</td>"
						 + "<td>" + Round(this.MaxDraw, 2) + "</td>"
						 + "<td>" + ToPercent(this.Efficiancy) + "</td>"
						 + "<td>" + MillisecondsToTime(this.AverageTiTGain) + "</td>"
						 + "<td>" + MillisecondsToTime(this.AverageTiTLoss) + "</td>"
						 + "<td>" + ToPercent(this.PercentFullStop) + "</td>"
						 + "<td>" + ToPercent(this.PercentBSO) + "</td>";

				// Handle Trade Grade
				var TradeGradeHtmlBuilder = "<td><select class='trade-grade-selector' id='" + trade.FillID + "' data-contract='" + trade.Symbol + "' data-account='" + Account + "' data-tradeid='" + trade.FillID + "'><option value='--'>--</option>";
				for (var jj = 5; jj > 0; jj--) { TradeGradeHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";	}
				TradeGradeHtmlBuilder += "</select></td>";
				htmlBuilder += TradeGradeHtmlBuilder;

				var TradeErrorHtmlBuilder = "<td><select class='trade-error-selector' id='" + trade.FillID + "' data-contract='" + trade.Symbol + "' data-account='" + Account + "' data-tradeid='" + trade.FillID + "'><option value='--'>--</option>";
				for (var jj = 1; jj <= 10; jj++) { TradeErrorHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";	}
				TradeGradeHtmlBuilder += "</select></td>";
				htmlBuilder += TradeErrorHtmlBuilder;
				
				var TradeTypeHtmlBuilder = "<td><select class='trade-type-selector' id='" + trade.FillID + "' data-contract='" + trade.Symbol + "' data-account='" + Account + "' data-tradeid='" + trade.FillID + "'><option value='--'>--</option>";
				$.each(TradeTypeArray, function (hh, Type) { TradeTypeHtmlBuilder += "<option value='" + Type + "'>" + Type + "</option>"; });
				TradeTypeHtmlBuilder += "</select></td>";
				htmlBuilder += TradeTypeHtmlBuilder;


			// Update charts with new points
			win_loss_percentage_chart.get("PercentWin").addPoint(ToPercent(this.PercentWinners), false, false, false);
			win_loss_percentage_chart.get("PercentLoss").addPoint(ToPercent(this.PercentLosers), false, false, false);
			expectancy_chart.get("Expectancy").addPoint(Round(this.Expectancy,2), false, false, false);
			equity_curve_chart.get("MFE").addPoint(Round(trade.MFE), false, false, false);
			equity_curve_chart.get("MAE").addPoint(-Round(trade.MAE), false, false, false);
			equity_curve_chart.get("PnL").addPoint(Round(trade.PnL,2), false, false, false);
			equity_curve_chart.get("EquityCurve").addPoint(Round(RunningPnL,2), false, false, false);


			// Push trade data to JSON variable for Printing purposes
			window.CompletedTradesRows.push({
				TradeID: trade.FillID,
				id: i,
				contract: trade.Symbol,
				longShort: (trade.IsLong ? "L" : "S"),
				mfe: Round(trade.MFE),
				mae: Round(trade.MAE),
				bso: Math.floor(trade.BSO),
				maxPosition: trade.MaxPosition,
				TotalQtyTraded: trade.TotalContractsTraded,
				tit: MillisecondsToTime(trade.TiT),
				tsb: MillisecondsToTime(trade.TSB),
				pnl: Round(trade.PnL, ResultsDisplayType == "Ticks" ? 0 : 2),
				pnlRunning: Round(RunningPnL, ResultsDisplayType == "Ticks" ? 0 : 2),
				entryTime: trade.EntryDateTime.local().format('MM/DD/YY H:mm:ss'),
				exitTime: trade.ExitDateTime.local().format('MM/DD/YY H:mm:ss'),
				entryPrice: trade.EntryPrice,
				exitPrice: trade.ExitPrice,
				winPercentage: ToPercent(this.PercentWinners),
				lossPercentage: ToPercent(this.PercentLosers),
				avgWin: Round(this.AverageGain, 2),
				avgLoss: Round(this.AverageDraw, 2),
				expectancy: Round(this.Expectancy, 2),
				peakGain: Round(this.MaxGain, 2),
				maxDraw: Round(this.MaxDraw, 2),
				efficiency: ToPercent(this.Efficiancy),
				avgDurWin: MillisecondsToTime(this.AverageTiTGain),
				avgDurLoss: MillisecondsToTime(this.AverageTiTLoss),
				fullstopPercentage: ToPercent(this.PercentFullStop),
				bsoPercentage: ToPercent(this.PercentBSO),
				tradeGrade: "--",
				tradeError: "--",
				tradeType: "--"
			});
		}

		// Do display work here (we are OUTSIDE the trade iteration)
		// This writes numbers to the stats tables, and updates gauges with values
				
		// Display trades table
		htmlBuilder += "</tbody></table>";		

		$("#completed-trades-table").html(htmlBuilder);
		$("#trades-table").fixedHeaderTable({ footer: false });

		// Trade Distribution Chart calculations
		if (TradeDistributionArray.length == 0) {
			trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
			trade_distribution_chart.xAxis[0].addPlotLine({
				value: 0,
				color: 'rgba(255,0,0,.3)',
				width: 5,
				zIndex: 10,
				id: 'zeroLineValue'
			});

			//trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
			trade_distribution_chart.get("main").setData([[]]);

		} else if (TradeDistributionArray.length == 1) {
			// If list contains only one trade
			trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
			trade_distribution_chart.xAxis[0].addPlotLine({
				value: 0,
				color: 'rgba(255,0,0,.3)',
				width: 5,
				zIndex: 10,
				id: 'zeroLineValue'
			});

			//trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
			var PnL = TradeDistributionArray[0];
			trade_distribution_chart.get("main").setData([[Math.round(PnL), 1]]);
						
		} else if (TradeDistributionArray.length > 1) {
			TradeDistributionArray.sort();

			try {			

				var TD_max = Math.max.apply(Math, TradeDistributionArray),
					TD_min = Math.min.apply(Math, TradeDistributionArray),
					TD_Final_Array = [],
					Processed_bucket_size;		
				
				// All the PnLs are not the same			
				var bucket_size_proposed = (TD_max - TD_min) / 31;		// this is in decimal format
				if (bucket_size_proposed > 100) 
					Processed_bucket_size = Math.floor(bucket_size_proposed / 100) * 100;			//try to make 15 buckets on each side of zero
				else if (bucket_size_proposed > 10)
					Processed_bucket_size = Math.floor(bucket_size_proposed / 10) * 10;				//try to make 15 buckets on each side of zero
				else if (bucket_size_proposed > 1)
					Processed_bucket_size = Math.floor(bucket_size_proposed);						//try to make 15 buckets on each side of zero
				else 
					Processed_bucket_size = 1;														//try to make 15 buckets on each side of zero

				var StartPoint,
					PointsBeforeZero = 0,
					PointsAfterZero = 0,
					PointsTotal = 0;

				if (TD_min == TD_max) {
					StartPoint = TD_min;
					PointsTotal = 1;

				} else if (TD_min < 0 && TD_max <= 0) {
					// no PnL on the positive side
					PointsBeforeZero = Math.ceil(Math.abs(TD_min) / Processed_bucket_size) + 1;
					PointsTotal = PointsBeforeZero + 1;
					StartPoint = -1 * PointsBeforeZero * Processed_bucket_size;

				} else if (TD_min >= 0 && TD_max > 0) {
					// no PnL on the negative side
					PointsAfterZero = Math.ceil(Math.abs(TD_max) / Processed_bucket_size) + 1;
					PointsTotal = PointsAfterZero;
					StartPoint = 0;
				} else {
					// PnL spread on the negative and positive sides
					PointsBeforeZero = Math.ceil(Math.abs(TD_min) / Processed_bucket_size) + 1;
					PointsAfterZero = Math.ceil(Math.abs(TD_max) / Processed_bucket_size) + 1;
					PointsTotal = PointsBeforeZero + PointsAfterZero + 1;
					StartPoint = -1 * PointsBeforeZero * Processed_bucket_size;
				}

				for (var i = 0; i < PointsTotal; i++) {
					var point = StartPoint + (i * Processed_bucket_size);
					TD_Final_Array.push([point, 0]);
				}

				var max = 0;
				$.each(TradeDistributionArray, function (j, PnL) {
					var point = Math.floor((PnL - StartPoint)/Processed_bucket_size);

					// since the zero return has it's own data point where all the zero returns fall into, anything higher than zero will need one more notch
					//if (PointsBeforeZero > 0 && PointsAfterZero > 0 && PnL > 0) point++;
					
					TD_Final_Array[point][1]++;
					if (TD_Final_Array[point][1] > max) max = TD_Final_Array[point][1];
				});
							
				trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
				
				trade_distribution_chart.xAxis[0].addPlotLine({
					value: 0,
					color: 'rgba(255,0,0,.3)',
					width: 5,
					zIndex: 10,
					id: 'zeroLineValue'
				});		

				trade_distribution_chart.yAxis[0].setExtremes(0, max);

				trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
				trade_distribution_chart.setTitle({ text: "Trade Distribution (bucket size=" + Processed_bucket_size + ")"}, {}, false);
				trade_distribution_chart.get("main").setData(TD_Final_Array);
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in 'Trade Distribution' chart calculations.<br /><br />Error: " + err.toString(), "warning", 5000);	
			}
		}
		
		// Daily PnL Chart calculations
		// Sort by ascending closing date of trade
		try {
			DailyPnLArray.sort(function (a, b) { return a[0] - b[0]; });
			var DailyPnLDisplayArray = [];
			$.each (DailyPnLArray, function (i, val) {
				var dt = Number(val[0].hour(0).minute(0).second(0).format("X"))*1000, PnL = val[1];
				if (DailyPnLDisplayArray.length == 0) {
					DailyPnLDisplayArray.push([dt, PnL]);
				} else {
					var lastEntry = DailyPnLDisplayArray[ DailyPnLDisplayArray.length-1 ];
					if (lastEntry[0] == Number(dt)) {
						lastEntry[1] += PnL;	
					} else {
						DailyPnLDisplayArray.push([dt, PnL]);
					}
				}
			});
			daily_pnl_chart.get("main").setData(DailyPnLDisplayArray);
		} catch (err) {
			if (typeof popupNotification == "function") popupNotification("There was an error in 'DailyPnL' chart calculations.<br /><br />Error: " + err.toString(), "warning", 30000);	
		}
		
		// % BSO & line of best fit calculation
		bso_attained_chart.get('main').setData(PercentBSOArray);
		
		if (PercentBSOArray.length >=2 ) {
			try {
				var BSObestfit_data = [],
					xBAR = 0, yBAR = 0, xSUM = 0, ySUM = 0, n = 0, sumTOP = 0, sumBOTTOM = 0, a = 0, b = 0;
		
				$.each(PercentBSOArray, function (ii, val) {
					n++;
					sumTOP += n*val;
					sumBOTTOM += n*n;
					xSUM += n;
					ySUM += val;
				});
				
				xBAR = xSUM / n;
				yBAR = ySUM / n;
				a = (sumTOP - (n * xBAR * yBAR)) / (sumBOTTOM - (n * xBAR * xBAR));
				b = yBAR - (a * xBAR);
				
				$.each(PercentBSOArray, function (i, val) {
					var y = a * (i+1) + b;
					BSObestfit_data.push(y);
				});
				
				bso_attained_chart.get('lineofbestfit').setData(BSObestfit_data);
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in 'BSO line of best fit' calculations.<br /><br />Error: " + err.toString(), "warning", 30000);	
			}
		}
	
		// MFE / MAE & line of best fit calculation
		mfe_mae_chart.get('main').update({ data: MFEMAERatioArray });
		if (MFEMAERatioArray.length >=2 ) {
			try {
				var MFEMAEbestfit_data = [],
					xBAR = 0, yBAR = 0, xSUM = 0, ySUM = 0, n = 0, sumTOP = 0, sumBOTTOM = 0, a = 0, b = 0;
				
				$.each(MFEMAERatioArray, function (ii, val) {
					n++;
					sumTOP += n * val;
					sumBOTTOM += n*n;
					xSUM += n;
					ySUM += val;
				});
				
				xBAR = xSUM / n;
				yBAR = ySUM / n;
				a = (sumTOP - (n * xBAR * yBAR)) / (sumBOTTOM - (n * xBAR * xBAR));
				b = yBAR - (a * xBAR);
				
				$.each(MFEMAERatioArray, function (i, val) {
					var y = a * (i+1) + b;
					MFEMAEbestfit_data.push(y);
				});
				
				mfe_mae_chart.get('lineofbestfit').update({ data: MFEMAEbestfit_data });
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in 'MFE/MAE line of best fit' calculations.<br /><br />Error: " + err.toString(), "warning", 30000);	
			}
		}

		// Update Trade Count, Hourly Performace, and Day of Week Performace
		trade_count_chart.get("main").setData(TradeCountPerHalfHour);
		hourly_performance_chart.get("main").setData(HourlyPerformance);
		dayofweek_performance_chart.get("main").setData(DayOfWeekPerformance);
		
		// Redraw all charts
		expectancy_chart.redraw();
		trade_count_chart.redraw();
		hourly_performance_chart.redraw();
		dayofweek_performance_chart.redraw();
		trade_distribution_chart.redraw();
		daily_pnl_chart.redraw();
		win_loss_percentage_chart.redraw();
		equity_curve_chart.redraw();
		bso_attained_chart.redraw();
		mfe_mae_chart.redraw();
		
		// Display to Gauges
		percent_win_dial.highcharts().series[0].points[0].update(ToPercent(this.PercentWinners));
		percent_loss_dial.highcharts().series[0].points[0].update(ToPercent(this.PercentLosers));
		percent_bso_dial.highcharts().series[0].points[0].update(ToPercent(this.PercentBSO));
		expected_value_dial.highcharts().yAxis[0].setExtremes(Round(MinExpectancy, 2), Round(MaxExpectancy, 2));
		expected_value_dial.highcharts().series[0].points[0].update(Round(this.Expectancy, 2));
		
		if (RiskBudget > 0 && RunningPnL > 0) {
			netto_number_dial.highcharts().yAxis[0].setExtremes(Round(MinNettoNumber, 2), Round(MaxNettoNumber, 2));
			netto_number_dial.highcharts().series[0].points[0].update(Round(NettoNumber, 2));
		} else {
			// Risk budget not correctly set, reset gauge
			netto_number_dial.highcharts().yAxis[0].setExtremes(0,0);
			netto_number_dial.highcharts().series[0].points[0].update(0);
		}
		
		// Display stats chart info				
		// Summary Status	
		$("#completed-trades").text(this.NumberOfTotalTrades);
		$("#contract-count").text(this.ContractsTradedLong + this.ContractsTradedShort);
		$("#profit-factor").text(Math.round(Math.abs(this.NetGain / this.NetLoss * 100)) / 100);
		$("#avg-trades-per-day").text(Math.ceil(this.NumberOfTotalTrades / this.ProductsArray.length));
		$("#total-pnl").text(Round(RunningPnL, ResultsDisplayType == "Ticks" ? 0 : 2));

		$("#date-range").text( (FirstDateTime != null && LastDateTime != null) ? (FirstDateTime.format("MMM D, YY") + " - " + LastDateTime.format("MMM D, YY")) : "n.a." );

		// Status Toolbar
		$("#long-percent").text(ToPercent(this.NumberOfLongTrades/this.NumberOfTotalTrades) + " %");
		$("#short-percent").text(ToPercent(this.NumberOfShortTrades/this.NumberOfTotalTrades) + " %");
		$("#avg-long-position").text(Round(this.ContractsTradedLong / this.NumberOfLongTrades));
		$("#avg-short-position").text(Round(this.ContractsTradedShort / this.NumberOfShortTrades));
		$("#max-long-position").text(this.MaxLongPosition);
		$("#max-short-position").text(this.MaxShortPosition);
		

		$("#average-gain").text(Round(this.AverageGain, 2));
		$("#average-draw").text(Round(this.AverageDraw, 2));
		$("#max-gain").text(Round(this.MaxGain, 2));
		$("#max-draw").text(Round(this.MaxDraw, 2));
		$("#avg-tit-gain").text(MillisecondsToTime(this.AverageTiTGain));
		$("#avg-tit-draw").text(MillisecondsToTime(this.AverageTiTLoss));	
		$("#max-tit-gain").text(MillisecondsToTime(this.MaxTiTGain));
		$("#max-tit-draw").text(MillisecondsToTime(this.MaxTiTLoss));

		$("#max-mfe").text(Round(this.MaxMFE, 2));
		$("#max-mae").text(Round(this.MaxMAE, 2));
		$("#avg-mfe").text(Round(this.TotalMFE / this.NumberOfTotalTrades));
		$("#avg-mae").text(Round(this.TotalMAE / this.NumberOfTotalTrades));
		$("#peak-running-gain").text(Round(PeakRunningGain));
		$("#max-running-drawdown").text(Math.abs(Round(PeakRunningDraw)));
		$("#gross-gain").text(Round(this.GrossGain));
		$("#gross-draw").text(Math.abs(Round(this.GrossLoss)));
		$("#gain-available").text(Round(this.TotalMFE));
		$("#draw-available").text(Round(this.TotalMAE));
		$("#gain-efficiancy").text(Round(this.Efficiancy, 2));
		$("#draw-drag").text(Math.abs(Round(this.NetLoss/this.TotalMAE, 2)));

		$("#max-consecutive-gain").text(this.NumberOfMaxConsecutiveGains);
		$("#max-consecutive-draw").text(this.NumberOfMaxConsecutiveLosses);

		$("#max-bso").text(Round(this.MaxBSO, 2));
		$("#avg-bso").text(Round(this.TotalBSO / this.NumberOfTotalTrades, 2));

		// metric builder for printing
		window.StatsDataRows = [{
			winPercentage: ToPercent(this.PercentWinners),
			lossPercentage: ToPercent(this.PercentLosers),
			bsoPercentage: ToPercent(this.PercentBSO),
			expectancy: Round(this.Expectancy,2),
			nettoNumber: Round(NettoNumber, 2)
		}];

		window.UserDataRows = [
			{ title : "Start Date",				data : $("#from-datepicker").val() },
			{ title : "End Date",				data : $("#to-datepicker").val() },
			{ title : "Account",				data : selectedAccount },
			{ title : "Products",				data : $("#products-traded").text() },
			{ title : "Display Units",			data : $("#settings-display-results").val() },
			{ title : "Commissions",			data : $("#settings-commission-fee").val() },
			{ title : "Risk Budget for Period",	data : $("input#daily-loss-limit").val() },
			{ title : "Date Range",				data : $("#date-range").text() }
		];

		window.SummaryStatsDataRows = [
			{ title : "# of Days Traded",		data : $("#days-traded").text() },
			{ title : "Trades Completed",		data : $("#completed-trades").text() },
			{ title : "Average Trades / Day",	data : $("#avg-trades-per-day").text() },
			{ title : "Contract Count",			data : $("#contract-count").text() },
			{ title : "Profit Factor",			data : $("#profit-factor").text() },
			{ title : "Total PnL",				data : $("#total-pnl").text() },
			{ title : "",						data : "" },
			{ title : "",						data : "" }
		];

		window.StatsForLongs = [
			{ title : "Percent Long",	data : $("#long-percent").text() },
			{ title : "Average Long Position",	data : $("#avg-long-position").text() },
			{ title : "Max Long Position",	data : $("#max-long-position").text() }
		];

		window.StatsForShorts = [
			{ title : "Percent Short",	data : $("#short-percent").text() },
			{ title : "Average Short Position",	data : $("#avg-short-position").text() },
			{ title : "Max Short Position",	data : $("#max-short-position").text() }
		];

		window.FavorableStats = [
			{ title : "Average Gain",				data : $("#average-gain").text() },
			{ title : "Max Gain",					data : $("#max-gain").text() },
			{ title : "Average TiT Gain",			data : $("#avg-tit-gain").text() },
			{ title : "Max TiT Gain",				data : $("#max-tit-gain").text() },
			{ title : "Max MFE",					data : $("#max-mfe").text() },
			{ title : "Average MFE",				data : $("#avg-mfe").text() },
			{ title : "Peak Running Gain", 			data : $("#peak-running-gain").text() },
			{ title : "Gross Gain",					data : $("#gross-gain").text() }, 
			{ title : "Gain Available",				data : $("#gain-available").text() },
			{ title : "Trader Gain Efficiency",		data : $("#gain-efficiancy").text() },
			{ title : "Max Consecutive Gains",		data : $("#max-consecutive-gain").text() },
			{ title : "Max BSO",					data : $("#max-bso").text() },
			{ title : "Average BSO",				data : $("#avg-bso").text() }
		];


		window.AdverseStats = [
			{ title : "Average Loss",				data : $("#average-draw").text() },
			{ title : "Max LOss",					data : $("#max-draw").text() },
			{ title : "Average TiT Loss",			data : $("#avg-tit-draw").text() },
			{ title : "Max TiT Loss",				data : $("#max-tit-draw").text() },
			{ title : "Max MAE",					data : $("#max-mae").text() },
			{ title : "Average MAE",				data : $("#avg-mae").text() },
			{ title : "Max Running Drawdown", 		data : $("#max-running-drawdown").text() },
			{ title : "Gross Losses",				data : $("#gross-draw").text() },
			{ title : "Loss Available",				data : $("#draw-available").text() },
			{ title : "Trader Loss Drag",			data : $("#draw-drag").text() },
			{ title : "Max Consecutive Losses",		data : $("#max-consecutive-draw").text() },
			{ title : "Max Products Traded / Day",	data : $("#max-products-traded").text() },
			{ title : "Most Traded Product",		data : $("#most-traded-products").text() }
		];

	}
	
};

TradeHandler.prototype.DisplayFillsForTrade = function (FillID) {
	if (this.TradesList != null && this.TradesList.length > 0 && FillID != null) {
		$.each(this.TradesList, function (i, trade) { 
			if (trade.FillID == FillID) {
				if (trade.FillsList.length > 0) {
					var tableHtml = "<table border='1' cellspacing=0 cellpadding=0 width='100%'><thead><th>FillID</th><th>OrderID</th><th>Side</th><th>Quant</th><th>Price</th><th>Timestamp (local time)</th><th>Contracts Held</th><th>Type Of Fill</th></thead><tbody>";
					$.each(trade.FillsList, function (key, fill) {
						tableHtml += "<tr><td>" + fill.FillID + "</td><td>" + fill.OrderID + "</td><td>" + fill.Side + "</td><td>" + fill.Quant + "</td><td>" + fill.Price+ "</td><td>" + moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().format("MMM DD - hh:mm:ss a") + "</td><td>" + fill.ContractsHeld + "</td><td>" + fill.TypeOfTrade + "</td></tr>" ;
					});
					tableHtml += "</tbody></table>";
					
					var params = "TickSize: " + trade.TickSize + "<br />Direction: " + (trade.IsLong ? "Long" : "Short");
					var params2 = "Type: " + (trade.PnL > 0 ? "Winner": ( trade.PnL < 0 ? "Loser" : "Flat")) + "<br />P&L: " + trade.PnL + (ResultsDisplayType != null ? ' (' + ResultsDisplayType + ')' : "");
					
					if (window.FillsDisplayWindow != null && window.FillsDisplayWindow.closed != null && window.FillsDisplayWindow.closed == false) {
						$("#display-header", window.FillsDisplayWindow.document).text(trade.Symbol); 
						$("#display-params", window.FillsDisplayWindow.document).html(params);
						$("#display-params2", window.FillsDisplayWindow.document).html(params2); 
						$("#display-fills", window.FillsDisplayWindow.document).html(tableHtml);
						
						window.FillsDisplayWindow.focus();
					} else {							
						window.FillsDisplayWindow = window.open(_BASE_URL + "/display-fills/", "Fills Window", "menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no,height=350,width=850", "NO");
						window.FillsDisplayWindow.onload = function () {
							$("#display-header", window.FillsDisplayWindow.document).text(trade.Symbol); 
							$("#display-params", window.FillsDisplayWindow.document).html(params);
							$("#display-params2", window.FillsDisplayWindow.document).html(params2); 
							$("#display-fills", window.FillsDisplayWindow.document).html(tableHtml);
							
							window.FillsDisplayWindow.focus();
						}
					}
				} else {
					if (typeof popupNotification == "function") popupNotification("An error occured.  This trade showed no fills.", "error", 30000);	
				}
			}
		});
	}
};

TradeHandler.prototype.print = function (DisplayAllMetrics) {
	if (DisplayAllMetrics != null && DisplayAllMetrics == true) {
		console.log(trades);	
	} else {
		if (this.TradesList != null && this.TradesList.length > 0) {
			console.log("Trades");
			$.each(this.TradesList, function (i, trade) { 
				console.log(trade);
				
				/*
				console.log(trade.Symbol + " -------------------------");
				$.each(trade.FillsList, function (j, fill) {
					console.log(fill.FillID + "\t" + fill.Side + "\t" + fill.Quant + "\t" + fill.Price + "\t" + fill.TimestampUTC);
				});
				*/
			});
		} else {
			console.log("No trades in list.");
		}
	
		/*
		if (this.ContractDetails != null && this.ContractDetails.length > 0) {
			console.log("Contract Details");
			$.each(this.ContractDetails, function (i, cont) { console.log(cont); });
		} else {
			console.log("No contracts in list.");
		}
		*/
	}
};

TradeHandler.prototype.GetUserTradeMetricsFromDB = function () {
	// This function will load the User Trade Metrics from the DB, given the initial FillID of the trade
	var FillIDArray = [];
	$.each(this.TradesList, function (i, trade) {
		if (trade != null && trade.FillID != null) FillIDArray.push(trade.FillID);
	});

	if (FillIDArray.length > 0) {
		// There were some inital Fill IDs to use for getting trade metrics	
		$.ajax({
			url: "includes/HTA-GetSetUserMetrics.php",
			method: "post",
			type: "post",
			data: { Operation: "GET", Username: username, FillIDArray: FillIDArray },
			error: function (err) { 
				console.log(err);
				if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
			},
			success: function (data) { 
				parsedData = $.parseJSON(data);
				// console.log(parsedData);
				if (parsedData != null && typeof parsedData != 'undefined' && parsedData.OutputType != null) {
					if (parsedData.OutputType == "ERROR") {
						// Error occured, display msg to user
						if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
					} else {
						// Successful GET, change metrics accordingly
						if (parsedData.Data != null && $.isArray(parsedData.Data)) {
							if (parsedData.Data.length == 0) {
								if (typeof popupNotification == "function") popupNotification ("There were no User Trade Metrics to load.", "warning", 5000);
							} else {
								$.each(parsedData.Data, function (i, val) {
									// find and update TradeGrade, TradeType, and TradeError
									var TradeGradeSelector = $("#trades-table").find(".trade-grade-selector#" + val.FillID),
										TradeTypeSelector = $("#trades-table").find(".trade-type-selector#" + val.FillID),
										TradeErrorSelector = $("#trades-table").find(".trade-error-selector#" + val.FillID);
									
									if (TradeGradeSelector.length > 0) TradeGradeSelector.val(val.TradeGrade == null ? "--" : val.TradeGrade);
									if (TradeTypeSelector.length > 0) TradeTypeSelector.val(val.TradeType == null ? "--" : val.TradeType);
									if (TradeErrorSelector.length > 0) TradeErrorSelector.val(val.TradeError == null ? "--" : val.TradeError);

									// Find the trade in the TradesList and assign TradeGrade/Emoticon/Type
									$.each(window.CompletedTradesRows, function (i, trade) {
										if (trade.TradeID == val.FillID) {
											trade.tradeGrade = val.TradeGrade != null ? val.TradeGrade : "--";
											trade.TradeError = val.TradeError != null ? val.TradeError : "--";
											trade.tradeType = val.TradeType != null ? val.TradeType : "--" ;
											return false;
										}
									});
								});
							}
						}
					}
				}
			}
		});
	}
};

TradeHandler.prototype.UpdateTradeType = function (Username, Account, Contract, tradeFillID, newType) {
	// Update in TradeType to DB
	$.ajax({
		url: 'includes/HTA-GetSetUserMetrics.php',
		type: 'post',
		method: 'post',
		data: { Operation: "SET", Username: Username, Account: Account, Contract: Contract, FillID: tradeFillID, Metric: "TradeType", Value: newType },
		error: function (err) { 
			console.log(err);
			if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
		},
		success: function (data) { 
			console.log(data);
			parsedData = $.parseJSON(data);
			if (parsedData.OutputType == "ERROR") 
				if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
		}
	});
};

TradeHandler.prototype.UpdateTradeGrade = function (Username, Account, Contract, tradeFillID, newGrade) {
	console.log("UpdateTradeGrade " + tradeFillID + " new: " + newGrade);
	// Update in TradeType to DB
	$.ajax({
		url: 'includes/HTA-GetSetUserMetrics.php',
		type: 'post',
		method: 'post',
		data: { Operation: "SET", Username: Username, Account: Account, Contract: Contract, FillID: tradeFillID, Metric: "TradeGrade", Value: newGrade },
		error: function (err) { 
			console.log(err);
			if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
		},
		success: function (data) { 
			console.log(data);
			parsedData = $.parseJSON(data);
			if (parsedData.OutputType == "ERROR") 
				if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
		}
	});
};

TradeHandler.prototype.UpdateTradeError = function (Username, Account, Contract, tradeFillID, newError) {
	console.log("UpdateTradeError " + tradeFillID + " new: " + newError);
	// Update in TradeType to DB
	$.ajax({
		url: 'includes/HTA-GetSetUserMetrics.php',
		type: 'post',
		method: 'post',
		data: { Operation: "SET", Username: Username, Account: Account, Contract: Contract, FillID: tradeFillID, Metric: "TradeError", Value: newError },
		error: function (err) { 
			console.log(err);
			if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
		},
		success: function (data) { 
			console.log(data);
			parsedData = $.parseJSON(data);
			if (parsedData.OutputType == "ERROR") 
				if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
		}
	});	
};


var Trade = function (Symbol, FillsList, TickSizeInput, ContractSizeInput, Commission) {
	// Initiating values
	this.Symbol = Symbol;
	this.FillsList = FillsList;
	
	// Contract Details
	this.TickSize = TickSizeInput;
	this.ContractSize = ContractSizeInput;
	this.Commission = (Commission != null ? Commission : 0);
	
	// Trade Details
	this.EntryDateTime = moment.utc(FillsList[0].TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
	this.ExitDateTime = moment.utc(FillsList[FillsList.length-1].TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
	this.EntryPrice = FillsList[0].Price;
	this.ExitPrice = FillsList[FillsList.length-1].Price;
	
	// Calculated values
	this.TotalContractsTraded = 0;
	this.TotalCommissions = 0;
	this.Position = 0;
	this.IsLong = false;
	this.AveragePrice = 0;
	this.PnL_Points = 0;
	this.PnL_Ticks = 0;
	this.PnL_Dollars = 0;	
	this.PnL = 0;
	this.PnL_Units = "";
	this.PnL_Gross = 0;
	this.PnLRunning = 0;
	this.TotalBuyPoints = 0;
	this.TotalSellPoints = 0;
	this.TotalBuyContracts = 0;
	this.TotalSellContracts = 0;
	this.MFE = 0;
	this.MAE = 0;
	this.TiT = 0;
	this.TSB = 0;
	this.BSO = 0;
	this.MaxPosition = 0;
	this.IsBSOEligible = false;
	
	// Running totals for charting
	this._PercentWin = 0;
	this._PercentLoss = 0;
	this._RunningPnL = 0;
	this._AverageWin = 0;
	this._AverageLoss = 0;
	this._RunningExpectancy = 0;
	this._PeakGain = 0;
	this._MaxDrawdown = 0;
	this._Efficiancy = 0;
	this._AverageWinDuration = 0;
	this._AverageLossDuration = 0;
	this._PercentFullStop = 0;
	this._PercentBSO = 0;
	this._GrossGain = 0;
	this._GrossLoss = 0;
	this._NetGain = 0;
	this._NetLoss = 0;	
};

Trade.prototype.Calc = function (DisplayType) {
	// The major calculation manager for every contract, it calculates the trades from the Fills
	this.TotalContractsTraded = 0;
	this.TotalCommissions = 0;
	this.Position = 0;
	this.IsLong = false;
	this.AveragePrice = 0;
	this.PnL_Points = 0;
	this.PnL_Ticks = 0;
	this.PnL_Dollars = 0;	
	this.PnL = 0;
	this.PnL_Gross = 0;	
	this.PnLRunning = 0;	
	this.TotalBuyPoints = 0;
	this.TotalSellPoints = 0;
	this.TotalBuyContracts = 0;
	this.TotalSellContracts = 0;	
	this.MFE = 0;
	this.MAE = 0;
	this.TiT = 0;
	this.BSO = 0;
	this.IsBSOEligible = false;
	DisplayType = (DisplayType == null ? "Ticks" : DisplayType);
	this.PnL_Units = DisplayType;
	
	this.Commission = CommissionFee;
	this.OrderIDArrayForBSO = [];

	// Calculation
	if (this.FillsList != null) {
		var FillsListLength = this.FillsList.length;
		for (var i = 0; i < FillsListLength; i++) {
			var fill = this.FillsList[i];
			if (i==0) {
				// first fill

				// Set trade's FillID = if of opening fill
				this.FillID = fill.FillID;
				this.MFE = 0;
				this.MAE = 0;
				if (fill.Side == "Buy") {
					this.IsLong = true;				
					//this.MFE = (fill.HighSince - fill.Price) * fill.Quant;
					//this.MAE = (fill.LowSince - fill.Price) * fill.Quant;
				} else {
					this.IsLong = false;	
					//this.MFE = (fill.Price - fill.LowSince) * fill.Quant;
					//this.MAE = (fill.Price - fill.HighSince) * fill.Quant;
				}
			}
			
			// Total Commissions calculation
			this.TotalCommissions += fill.Quant * this.Commission;
			this.TotalContractsTraded += fill.Quant;
			this.Position += fill.Quant * (fill.Side == "Buy" ? 1 : -1);
			this.MaxPosition = Math.max( Math.abs(this.Position) , this.MaxPosition );
		

			// Set Average Price
			if (this.IsLong && fill.Side == "Buy") {
				// LONG & BUY 
				// Continuation trade, PnL does not change but Average Price changes
				this.TotalBuyPoints += (fill.Quant * fill.Price);
				this.TotalBuyContracts += fill.Quant;
				this.AveragePrice = (this.TotalBuyPoints / this.TotalBuyContracts);

				var NewMFE = ((fill.HighSince - this.AveragePrice) * Math.abs(this.Position)) + this.PnLRunning,
					NewMAE = ((this.AveragePrice - fill.LowSince) * Math.abs(this.Position)) - this.PnLRunning;

				this.MFE = Math.max(this.MFE, NewMFE);
				this.MAE = Math.max(this.MAE, NewMAE);
								
				// var NewMFE = ((fill.HighSince - this.AveragePrice) * Math.abs(this.Position)),
				// 	NewMAE = ((fill.LowSince - this.AveragePrice) * Math.abs(this.Position));
					
				// this.MFE = Math.max(this.MFE, NewMFE + this.PnLRunning);
				// this.MAE = Math.min(this.MAE, NewMAE + this.PnLRunning);
				
			} else if (!this.IsLong && fill.Side == "Sell") {
				// SHORT & SELL
				// Continuation trade, PnL does not change but Average Price changes 
				//this.AveragePrice = ((Math.abs(this.Position) * this.AveragePrice) + (fill.Quant * fill.Price)) / (Math.abs(this.Position) + fill.Quant);
				this.TotalSellPoints += (fill.Quant * fill.Price);
				this.TotalSellContracts += fill.Quant;
				this.AveragePrice = (this.TotalSellPoints / this.TotalSellContracts);				

				var NewMFE = ((this.AveragePrice - fill.LowSince) * Math.abs(this.Position)) + this.PnLRunning,
					NewMAE = ((fill.HighSince - this.AveragePrice) * Math.abs(this.Position)) - this.PnLRunning;

				this.MFE = Math.max(this.MFE, NewMFE);
				this.MAE = Math.max(this.MAE, NewMAE);

				// var NewMFE = ((this.AveragePrice - fill.LowSince) * Math.abs(this.Position)),
				// 	NewMAE = ((this.AveragePrice - fill.HighSince) * Math.abs(this.Position));
					
				// this.MFE = Math.max(this.MFE, NewMFE + this.PnLRunning);
				// this.MAE = Math.min(this.MAE, NewMAE + this.PnLRunning);

			} else if (this.IsLong && fill.Side == "Sell") {
				// LONG & SELL
				// Opposing trade, PnL changes but Average Price does not change
				this.TotalSellPoints += (fill.Quant * fill.Price);
				this.TotalSellContracts += fill.Quant;			
				
				this.PnLRunning += (fill.Quant * (fill.Price - (this.TotalBuyPoints/this.TotalBuyContracts)));

				this.OrderIDArrayForBSO.push(fill.OrderID);
				if (this.Position != 0) {
					this.BSO = Math.max(this.BSO, (fill.Price - this.AveragePrice));
					this.IsBSOEligible = true;
				}

				var NewMFE = ((fill.HighSince - this.AveragePrice) * this.Position) + this.PnLRunning,
					NewMAE = ((this.AveragePrice - fill.LowSince) * this.Position) - this.PnLRunning;

				this.MFE = Math.max(this.MFE, NewMFE);
				this.MAE = Math.max(this.MAE, NewMAE);
				
			} else if (!this.IsLong && fill.Side == "Buy") {
				// SHORT & BUY
				// Opposing trade, PnL changes but Average Price does not change
				this.TotalBuyPoints += (fill.Quant * fill.Price);
				this.TotalBuyContracts += fill.Quant;
				
				this.PnLRunning += (-fill.Quant * (fill.Price - (this.TotalSellPoints/this.TotalSellContracts)));								


				this.OrderIDArrayForBSO.push(fill.OrderID);
				if (this.Position != 0) {
					this.BSO = Math.max(this.BSO, (this.AveragePrice - fill.Price));
					this.IsBSOEligible = true;					
				}

				var NewMFE = ((this.AveragePrice - fill.LowSince) * Math.abs(this.Position)) + this.PnLRunning,
					NewMAE = ((fill.HighSince - this.AveragePrice) * Math.abs(this.Position)) - this.PnLRunning;
				
				this.MFE = Math.max(this.MFE, NewMFE);
				this.MAE = Math.max(this.MAE, NewMAE);
			}
		}

		// If the orders flips and there is only one order ID, then this is a single order and there should be no BSO so BSO is set at zero		
		if ($.unique(this.OrderIDArrayForBSO) == 1) this.BSO = 0;


		this.PnL_Points = this.TotalSellPoints - this.TotalBuyPoints;
		this.PnL_Ticks = (this.TicksSize != 0 ? this.PnL_Points/this.TickSize : 0);
		this.PnL_Dollars = (this.ContractSize != 0 ? (this.PnL_Points * this.ContractSize - this.TotalCommissions) : 0);
		this.PnL_Gross = this.PnL_Points * this.ContractSize;		

		if (DisplayType == "Ticks") {
			this.PnL = this.PnL_Ticks;
			this.PnL_Units = DisplayType;
			this.PnL_Gross = this.PnL_Ticks;

			this.MFE = (this.TicksSize != 0 ? Math.abs(this.MFE/this.TickSize) : 0);
			this.MAE = (this.TicksSize != 0 ? Math.abs(this.MAE/this.TickSize) : 0);

			this.BSO = (this.TicksSize != 0 ? Math.floor(this.BSO/this.TickSize) : 0);	
		} else if (DisplayType == "Dollars") {
			this.PnL = this.PnL_Dollars;
			this.PnL_Units = DisplayType;
			this.PnL_Gross = this.PnL_Dollars;

			this.MFE =  Math.abs(this.MFE) * this.ContractSize;
			this.MAE = Math.abs(this.MAE) * this.ContractSize;

			this.BSO = this.BSO * this.ContractSize;
		} else {
			// Points
			this.PnL = this.PnL_Points;
			this.PnL_Units = DisplayType;
			this.PnL_Gross = this.PnL_Points;

			this.MFE =  Math.abs(this.MFE);
			this.MAE = Math.abs(this.MAE);
		}
		
		this.TiT = this.ExitDateTime - this.EntryDateTime;

	}
};

function MillisecondsToTime (ms) {
	if (isNaN(ms)) {
		return "n.a.";
	} else {
		var dur = moment.duration(ms),
			days = Math.floor(dur.days()),
			hours = Math.floor(dur.hours()),
			minutes = Math.floor(dur.minutes()),
			seconds = Math.floor(dur.seconds());
		if (ms == 0)
			return 0;
		else if (days > 0) 
			return days + "d " + hours + "h " + minutes + "m " + seconds + "s";
		else if (hours > 0)
			return hours + "h " + minutes + "m " + seconds + "s";
		else if (minutes > 0)
			return minutes + "m " + seconds + "s";
		else
			return seconds + "s";
	}
}

function PointsToTick (points, tickSize) {
	if (tickSize > 0) {			
		tickSize = tickSize.toString();
		var decPlace = (tickSize.split('.')[1] || []).length;
		var decimalMultiplier = 10 ^ decPlace;
		return (Math.round(points * decimalMultiplier / tickSize) / decimalMultiplier);
	} else {
		return -1;
	}
}

function PointsToRound (points, tickSize) {
	if (tickSize > 0) {
		tickSize = tickSize.toString();
		var decPlace = (tickSize.split('.')[1] || []).length;
		return Number(Number(points).toFixed(decPlace));
		//var decimalMultiplier = Math.pow(10,decPlace);
		//return (Math.round(points * decimalMultiplier) / decimalMultiplier);
	} else {
		return -1;
	}
}

function Round (Input, DecimalPlaces) {
	if (DecimalPlaces == null) DecimalPlaces = 0;
	if ($.isNumeric(Input) && $.isNumeric(DecimalPlaces)) {
		var m = Math.pow(10, Number(DecimalPlaces));
		return (Math.round(Number(Input) * m) / m);
	} else if (Input == "NaN") {
		return 0;
	} else {
		return 0;
	}
}

function ToPercent(num) {
	return (num == 0 ? 0 : (Math.round(num * 10000)/100));
}

$(document).on("click", "a.display-fills", function () {
	var id = this.id;
	if ($.trim(id) != "") {
		if (window.trades != null) window.trades.DisplayFillsForTrade(id);	
	}
});

