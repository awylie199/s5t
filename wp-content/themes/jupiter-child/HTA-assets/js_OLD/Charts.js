// Charts JS (Options and Operations)

// Charts
var expectancy_chart = null, trade_count_chart = null, hourly_performance_chart = null, dayofweek_performance_chart = null, trade_distribution_chart = null, daily_pnl_chart, win_loss_percentage_chart = null, equity_curve_chart = null, bso_attained_chart = null, mfe_mae_chart = null;

// Gauges
var percent_win_dial = null, percent_loss_dial = null, percent_bso_dial = null, expected_value_dial = null, netto_number_dial = null;


var chartHeight = 230, chartWidth = 350;

// Options for different charts

var expectancy_options = {
	chart: {
		type: 'line',
		zoomType: 'xy'		
	},
	credits: { enabled: false },
	title: {
		text: 'Expectancy'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y;
		}
	},	
	yAxis: [{
		id: 'main-y-axis'
	}],
	xAxis: {
		minTickInterval: 1,
		min: 1
	},			
	series : [{
		id: 'Expectancy',
		name : "Expectancy",
		type: 'line',
		pointStart: 1,
		marker: { radius: 2	},				
		data : []
	}]
}

// Trade Count Chart
var trade_count_options = {
	chart: {
		type: 'bar',
		zoomType: 'xy'
	},
	credits: {
		enabled: false
	}, 
	title: {
		text: 'Trade Count',
		style: {
			fontSize: '1.3em',
			lineHeight: '1.3em'	
		}		
	},
	xAxis: {
		title: {
			text: 'Local Time of Day'
		},
		categories: ['0:00', '1:00', '2:00', '3:00', '4:00', '5:00', '6:00', '7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
		showLastLabel: true,
		endOnTick: true
	},
	yAxis: {
		title: {
			text: 'Trades'
		},
		min: 0,
		showFirstLabel: true,
		showLastLabel: true,
		minTickInterval: 1	
	},
	legend: {
		enabled: false
	},
	tooltip: {
		valueDecimals: 2
	},
	series: [{
		id: 'main',
		name: 'Trade Count',
		borderWidth: 0,
		data: []
	}]		
}

// Hourly Performance Chart
var hourly_performance_options = {
	chart: {
		type: 'bar',
		zoomType: 'xy'
	},
	credits: {
		enabled: false
	}, 
	title: {
		text: 'Hourly Performance',
		style: {
			fontSize: '1.3em',
			lineHeight: '1.3em'	
		}
	},
	xAxis: {
		title: {
			text: 'Local Time of Day'
		},
		categories: ['0:00', '1:00', '2:00', '3:00', '4:00', '5:00', '6:00', '7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
		showLastLabel: true,
		pointPlacement: 'on'
	},
	yAxis: {
		title: {
			text: 'Ticks'
		}
	},
	legend: {
		enabled: false
	},
	tooltip: {
		valueDecimals: 2
	},
	plotOptions: {
	},
	series: [{
		id: 'main',
		name: 'Hourly Performance',
		borderWidth: 0,
		data: []
	}]		
}

// Hourly Performance Chart
var dayofweek_performance_options = {
	chart: {
		type: 'bar',
		zoomType: 'xy'			
	},
	credits: { enabled: false }, 
	title: {
		text: 'Day of Week Performance',
		style: {
			fontSize: '1.3em',
			lineHeight: '1.3em'	
		}		
	},
	xAxis: {
		categories: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
		showLastLabel: true,
		endOnTick: true
	},
	yAxis: {
		title: {
			text: 'Ticks'
		}
	},
	legend: {
		enabled: false
	},
	tooltip: {
		valueDecimals: 2
	},
	plotOptions: {
	},
	series: [{
		id: 'main',
		name: 'Day of Week Performance',
		borderWidth: 0,
		data: []
	}]		
}

// Win/Loss Chart
var win_loss_percentage_options = {
	chart: {
		type: 'line',
		zoomType: 'xy'		
	},
	credits: { enabled: false }, 
	title: {
		text: 'Win / Loss Percentage'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y + " %";
		}
	},
	yAxis: [{
		id: 'main-y-axis',
		min: 0,
		max: 100
	}],
	xAxis: {
		minTickInterval: 1,
		min: 1
	},			
	series : [{
		id: 'PercentWin',
		name : "Percent Win",
		type: 'line',
		color: '#347bb2',
		pointStart: 1,
		marker: { radius: 2	},
		data : []
	}, {
		id: 'PercentLoss',
		name : "Percent Loss",
		type: 'line',
		color: '#da200b',
		pointStart: 1,	
		marker: { radius: 2	},			
		data : []
	}]

}

// Equity Curve Chart
var equity_curve_options = {
	chart: {
		type: 'line',
		zoomType: 'xy'		
	},
	credits: { enabled: false }, 
	title: {
		text: 'Equity Curve'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y;
		}
	},	
	xAxis: {
		tickmarkPlacement: 'on',
		min: 1,
		minTickInterval: 1	
	},
	yAxis: [{
		title: { text: "P&L" }
	}, {
		title: { text: "Equity Curve" },
		opposite: true
	}],
	series : [
	{
		id: 'EquityCurve',
		name : "Equity Curve",
		type: 'line',
		index: 9,
		yAxis: 1,
		pointInterval: 1,
		pointStart: 1,					
		marker: { radius: 2	},			
		data : []
	}, {
		id: 'PnL',
		name : "P&L",
		type: 'line',
		pointInterval: 1,
		pointStart: 1,			
		marker: { radius: 2	},		
		index: 19,
		data : []
	}, {
		id: 'MFE',
		name : "MFE",
		type: 'column',
		stacking: 'normal',
		color: '#347bb2',
		pointInterval: 1,
		pointStart: 1,			
		pointPlacement: 'on',
		data : []
	}, {
		id: 'MAE',
		name : "MAE",
		type: 'column',
		stacking: 'normal',
		color: '#da200b',
		pointInterval: 1,
		pointStart: 1,			
		pointPlacement: 'on',		
		data : []
	}]				
}

// Trade Distribution Chart
var trade_distribution_options = {
	chart: {
		type: 'column',
		zoomType: 'xy'			
	},
	credits: { enabled: false }, 
	title: {
		text: 'Trade Distribution'
	},
	xAxis: {
		title: {
			text: 'Ticks'
		},
		categories: [],
		minTickInterval: 1
	},
	yAxis: {
		title: {
			text: 'Number of Trades'
		},
		min: 0
	},
	legend: {
		enabled: false
	},
	tooltip: {
		formatter: function () {
			if (this.series != null && this.series.xAxis != null && this.series.xAxis.tickInterval != null) {
				if (this.x == 0)
					return "<b>Zero Return</b><br />" + this.series.name + " " + this.y;
				else 
					return (this.y + " returns between " + this.x + " and " + (this.x + this.series.xAxis.tickInterval) + ".");
			} else {
				return "<b>Return distribution " + this.x + "</b><br />" + this.series.name + " " + this.y;
			}
		}
	},
	plotOptions: {
		column: {
			groupPadding: 0.1,
			pointPadding: 0,
			borderWidth: 0
		}			
	},
	series: [{
		id: 'main',
		name: 'Trade Distribution',
		data: []
	}]		
}

// DailyPnL Chart
var daily_pnl_options = {
	chart: {
		zoomType: 'xy'			
	},
	credits: { enabled: false }, 
	title: {
		text: 'Daily P&L'
	},
	xAxis: {
		type: 'datetime'			
	},
	yAxis: [{
		title: {
			text: 'Per Day In Ticks'
		}
	}],
	legend: {
		enabled: true,
		align: 'center'
	},
	tooltip: {
		valueDecimals: 2
	},
	plotOptions: {
		series: {
			cursor: 'pointer',
			point: {
				events: {
					click: function () {
						var OBJ = document.getElementById(this.x);
						if (OBJ != null) OBJ.scrollIntoView();
					}
				}
			}
		}
	},
	series: [{
		id: 'main',
		type: 'column',
		marker: { radius: 2	},		
		name: 'Daily P&L (ticks)',
		pointInterval: 1,
		pointStart: 1,
		data: []			
	}]		
}

// % BSO Attained Chart
var bso_attained_options = {
	chart: {
		zoomType: 'xy'			
	},
	credits: { enabled: false }, 
	title: {
		text: 'Percent BSO Attained'
	},
	xAxis: {
		minTickInterval: 1
	},
	yAxis: {
		title: {
			text: 'Percentage'
		},
		min: 0,
		max: 100		
	},
	legend: {
		enabled: true
	},
	tooltip: {
		valueDecimals: 2
	},
	plotOptions: {
		series: {
			cursor: 'pointer',
			point: {
				events: {
					click: function () {
						var OBJ = document.getElementById(this.x);
						if (OBJ != null) {
							OBJ.scrollIntoView();
							var bkColor = $(OBJ).css("background-color");
							$(OBJ).css("background-color", "#FFFF9C").animate({ backgroundColor: bkColor}, 3000);;
						}
					}
				}
			}
		}
	},	
	series: [{
		id: 'main',
		name: 'BSO Attained',
		data: [],
		type: 'scatter',
		zIndex: 10,
		pointInterval: 1,
		pointStart: 1,					
		marker: { radius: 2	}
	}, {
		id: 'lineofbestfit',
		name: 'Line of Best Fit',
		data: [],
		pointInterval: 1,
		pointStart: 1,			
		marker: { enabled: false },			
		type: 'line'
	}]		
}

// MFE / MAE Ratio Chart
var mfe_mae_options = {
	chart: {
		zoomType: 'xy'
	},
	credits: {
		enabled: false
	}, 
	title: {
		text: 'MFE / MAE Ratio'
	},
	xAxis: {
		title: {
			text: 'Trade #'
		},
		minTickInterval: 1
	},
	yAxis: {
		 title: {
			text: 'Ratio'
		}
	},
	legend: {
		enabled: true
	},
	tooltip: {
		valueDecimals: 2
	},
	plotOptions: {
		series: {
			cursor: 'pointer',
			point: {
				events: {
					click: function () {
						var OBJ = document.getElementById(this.x);
						if (OBJ != null) {
							OBJ.scrollIntoView();
							var bkColor = $(OBJ).css("background-color");
							$(OBJ).css("background-color", "#FFFF9C").animate({ backgroundColor: bkColor}, 3000);;
						}
					}
				}
			}
		}
	},
	series: [{
		id: 'main',
		name: 'MFE/MAE Ratio',
		data: [],
		type: 'scatter',
		pointInterval: 1,
		pointStart: 1,		
		zIndex: 10,
		marker: { radius: 2 }

	}, {
		id: 'lineofbestfit',
		name: 'Line of Best Fit',
		marker: { enabled: false },
		pointInterval: 1,
		pointStart: 1,			
		data: [],
		type: 'line'
	}]		
}

var gaugeOptions = {
	chart: {
		type: 'solidgauge',
		backgroundColor: 'transparent',
		height: 150
	},
	credits: {
		enabled: false
	},
	title: {
		align: 'center',
		useHTML: true,
		style: { fontSize: '.9em' }
	},
	pane: {
		center: ['50%', '55%'],
		size: '110%',
		startAngle: -100,
		endAngle: 100,
		background: {
			backgroundColor: '#EEE',
			innerRadius: '60%',
			outerRadius: '90%',
			shape: 'arc'
		}
	},
	tooltip: {
		enabled: false
	},
	// the value axis
	yAxis: {
		min: 0,
		max: 100,
		// stops: [
		// 	[0.1, '#55BF3B'], // green
		// 	[0.5, '#DDDF0D'], // yellow
		// 	[0.9, '#DF5353'] // red
		// ],
		lineWidth: 0,
		minorTickInterval: null,
		tickPixelInterval: 50,
		tickWidth: 2,
		labels: {
			y: -2,
			padding: 15,
			distance: 2
		}
	},
	series: [{
		data: [{
			radius: 90,
			innerRadius: 60,
			y: 0
		}]
	}],
	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: -15,
				borderWidth: 0,
				useHTML: true,
				style: { fontSize: '1em' }
			}
		}
	}
};

var sparkline_options = {
	chart: {
		height: 69
	},
	credits: { enabled: false },
	legend: { enabled: false },
	title: { text: '' },
	tooltip: { enabled: false },
	yAxis: {
		title: { enabled: false },
		labels: { enabled: false },
		lineWidth: 0,
		gridLineWidth: 0,
		minorGridLineWidth: 0,
		minorTickLength: 0,
		lineColor: 'transparent',
		tickLength: 0
  	},
	xAxis: {
		visible: false,
		lineColor: 'transparent'
	},
	series: [{
		id: 'MFE',
		name: 'MFE',
		type: 'line',
		marker: { enabled: false },
		data: []
	}, {
		id: 'MAE',
		name: 'MAE',
		type: 'line',
		marker: { enabled: false },		
		data: []	
	}],
};

$(document).ready(function() {
	// Hide last 3 charts

	Highcharts.setOptions(Highcharts.theme);

	Highcharts.setOptions({
		global: {
			useUTC: true
		}
	});


	// Create the chart
	expectancy_chart = $(".chart#expectancy").highcharts(expectancy_options);
	trade_count_chart = $(".small-chart#trade-count").highcharts(trade_count_options);
	hourly_performance_chart = $(".small-chart#hourly-performance").highcharts(hourly_performance_options);
	dayofweek_performance_chart = $(".small-chart#dayofweek-performance").highcharts(dayofweek_performance_options);
	trade_distribution_chart = $(".chart#trade-distribution").highcharts(trade_distribution_options);
	daily_pnl_chart = $(".chart#daily-pnl").highcharts(daily_pnl_options);
	equity_curve_chart = $(".chart#equity-curve").highcharts(equity_curve_options);
	win_loss_percentage_chart = $(".chart#win-loss-percentage").highcharts(win_loss_percentage_options);
	bso_attained_chart = $(".chart#bso-attained").highcharts(bso_attained_options);			
	mfe_mae_chart = $(".chart#mfe-mae").highcharts(mfe_mae_options);				
	
	expectancy_chart = expectancy_chart.highcharts();
	trade_count_chart = trade_count_chart.highcharts();
	hourly_performance_chart = hourly_performance_chart.highcharts();
	dayofweek_performance_chart = dayofweek_performance_chart.highcharts();
	trade_distribution_chart = trade_distribution_chart.highcharts();
	daily_pnl_chart = daily_pnl_chart.highcharts();
	equity_curve_chart = equity_curve_chart.highcharts();
	win_loss_percentage_chart = win_loss_percentage_chart.highcharts();
	bso_attained_chart = bso_attained_chart.highcharts();
	mfe_mae_chart = mfe_mae_chart.highcharts();	

	// Gauges

	
	percent_win_dial = $('#percent-win-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Win %' },
		series: [{
			data: [{
				radius: 90,
				innerRadius: 60,
				y: 0,
				color: 'rgba(20, 47, 68, .9)'	// stage 5 blue
			}]
		}]
	}));
	
	percent_loss_dial = $('#percent-loss-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Loss %' }, 
		series: [{
			data: [{
				radius: 90,
				innerRadius: 60,
				y: 0,
				color: 'rgba(97, 14, 5, .9)'	// stage 5 red
			}]
		}]
	}));
	
	percent_bso_dial = $('#percent-bso-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'BSO %' },
		yAxis: {
			stops: [
				[0, 'rgba(97, 14, 5, .9)'],			// stage 5 red
				[0.5, 'rgba(20, 47, 68, .9)']		// stage 5 blue
			]
		}
	}));

	expected_value_dial = $('#expected-value-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Expectancy' }
	}));

	netto_number_dial = $('#netto-number-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Netto #' }
	}));

});

function ReflowCharts () {
		// This function resizes the charts in the charts toolbar, and should be called when the panes are resized AND when the window is resized
		expectancy_chart.reflow();		
		trade_count_chart.reflow();
		hourly_performance_chart.reflow();
		dayofweek_performance_chart.reflow();
		trade_distribution_chart.reflow();
		daily_pnl_chart.reflow();
		equity_curve_chart.reflow();
		win_loss_percentage_chart.reflow();
		bso_attained_chart.reflow();
		mfe_mae_chart.reflow();
	}


function ClearAllCharts() {
	var ChartsObjectArray = [ expectancy_chart, trade_count_chart, hourly_performance_chart, dayofweek_performance_chart, trade_distribution_chart, daily_pnl_chart, equity_curve_chart, win_loss_percentage_chart, bso_attained_chart, mfe_mae_chart ];
	
	$.each(ChartsObjectArray, function (i, chart) {
		if (chart != null && chart.series != null) {
			var seriesLength = chart.series.length;
			for(var j = seriesLength - 1; j > -1; j--)
				chart.series[j].setData([], false, false, false);
						
			chart.redraw();
		}
	});

	percent_win_dial.highcharts().series[0].points[0].update(0);
	percent_loss_dial.highcharts().series[0].points[0].update(0);
	percent_bso_dial.highcharts().series[0].points[0].update(0);
	expected_value_dial.highcharts().series[0].points[0].update(0);
	netto_number_dial.highcharts().series[0].points[0].update(0);
}
