	// This JS is used for creating a PDF for printing the metrics

var UserDataCols = [
	{ title: "Title", dataKey: "title" },
	{ title: "Data", dataKey: "data" }
];

var StatsDataCols = [
	{ title: "Win %", dataKey: "winPercentage" },
	{ title: "Loss %", dataKey: "lossPercentage" },
	{ title: "BSO %", dataKey: "bsoPercentage" },
	{ title: "Expectancy", dataKey: "expectancy" },
	{ title: "Netto Number", dataKey: "nettoNumber" }
];

var CompletedTradesCols = [
	{ title: "#", dataKey: "id" },
	{ title: "Contract", dataKey: "contract" },
	{ title: "L/S", dataKey: "longShort" },
	{ title: "MFE", dataKey: "mfe" },
	{ title: "MAE", dataKey: "mae" },
	{ title: "BSO", dataKey: "bso" },
	{ title: "MP", dataKey: "maxPosition" },
	{ title: "TQ", dataKey: "TotalQtyTraded" },
	{ title: "TiT", dataKey: "tit" },
	{ title: "TSB", dataKey: "tsb" },
	{ title: "P&L", dataKey: "pnl" },
	{ title: "P&L-R", dataKey: "pnlRunning" },
	{ title: "Entry Time", dataKey: "entryTime" },
	{ title: "Exit Time", dataKey: "exitTime" },
	{ title: "Entry Price", dataKey: "entryPrice" },
	{ title: "Exit Price", dataKey: "exitPrice" },
	{ title: "% Win", dataKey: "winPercentage" },
	{ title: "% Loss", dataKey: "lossPercentage" },
	{ title: "Avg Win", dataKey: "avgWin" },
	{ title: "Avg Loss", dataKey: "avgLoss" },
	{ title: "E", dataKey: "expectancy" },
	{ title: "PG", dataKey: "peakGain" },
	{ title: "MD", dataKey: "maxDraw" },
	{ title: "Efcny", dataKey: "efficiency" },
	{ title: "ADW", dataKey: "avgDurWin" },
	{ title: "ADL", dataKey: "avgDurLoss" },
	{ title: "FS%", dataKey: "fullstopPercentage" },
	{ title: "BSO%", dataKey: "bsoPercentage" },
	{ title: "Grade", dataKey: "tradeGrade" },
	{ title: "Emtcn", dataKey: "tradeEmoticon" },
	{ title: "Type", dataKey: "tradeType" }
];

window.UserDataRows = [
	{ title : "Start Date",	data : "0" },
	{ title : "End Date",	data : "0" },
	{ title : "Account",	data : "0" },
	{ title : "Products",	data : "0" },
	{ title : "Display Units",	data : "0" },
	{ title : "Commissions",	data : "0" },
	{ title : "Risk Budget for Period",	data : "0" },
	{ title : "Date Range",	data : "0" }
];

window.SummaryStatsDataRows = [
	{ title : "# of Days Traded",	data : "0" },
	{ title : "Trades Completed",	data : "0" },
	{ title : "Average Trades / Day",	data : "0" },
	{ title : "Contract Count",	data : "0" },
	{ title : "Profit Factor",	data : "0" },
	{ title : "Total PnL",	data : "0" },
	{ title : "",	data : "" },
	{ title : "",	data : "" }
];

window.StatsDataRows = [{
	  winPercentage: 0,
	  lossPercentage: 0,
	  bsoPercentage: 0,
	  expectancy: 0,
	  nettoNumber: 0
}];

window.StatsForLongs = [
	{ title : "Percent Long",	data : "0" },
	{ title : "Average Long Position",	data : "0" },
	{ title : "Max Long Position",	data : "0" }
];

window.StatsForShorts = [
	{ title : "Percent Short",	data : "0" },
	{ title : "Average Short Position",	data : "0" },
	{ title : "Max Short Position",	data : "0" }
];

window.FavorableStats = [
	{ title : "Average Gain",	data : "0" },
	{ title : "Max Gain",	data : "0" },
	{ title : "Average TiT Gain",	data : "0" },
	{ title : "Max TiT Gain",	data : "0" },
	{ title : "Max MFE",	data : "0" },
	{ title : "Average MFE",	data : "0" },
	{ title : "Gross Gain",	data : "0" },
	{ title : "Gain Available",	data : "0" },
	{ title : "Trader Gain Efficiency",	data : "0" },
	{ title : "Max Consecutive Gains",	data : "0" },
	{ title : "Max BSO",	data : "0" },
	{ title : "Average BSO",	data : "0" }
];


window.AdverseStats = [
	{ title : "Average Draw",	data : "0" },
	{ title : "Max Draw",	data : "0" },
	{ title : "Average TiT Draw",	data : "0" },
	{ title : "Max TiT Draw",	data : "0" },
	{ title : "Max MAE",	data : "0" },
	{ title : "Average MAE",	data : "0" },
	{ title : "Gross Draw",	data : "0" },
	{ title : "Draw Available",	data : "0" },
	{ title : "Trader Draw Drag",	data : "0" },
	{ title : "Max Consecutive Draw",	data : "0" },
	{ title : "Max Products Traded / Day",	data : "0" },
	{ title : "Most Traded Product",	data : "0" }
];

window.CompletedTradesRows = [];


$(document).ready(function () {

	// If the print button is pressed
	$(".icon#print").click(function () {
		console.log("hello");

	    var doc = new jsPDF('l', 'pt');
	    doc.setFontSize(12);

	    //doc.text("HELLO", 40, 60);
	    doc.save('Stage5-HTA-download.pdf');
	    return;
	    doc.addImage(watermarkImgData, 'PNG', 90, 155, 224, 112);
	    doc.addImage(watermarkImgData, 'PNG', 505, 285, 224, 112);
	    doc.addImage(watermarkImgData, 'PNG', doc.internal.pageSize.width - 300, doc.internal.pageSize.height - 125, 224, 112);
			    

	    // header
	    var header = function (data) {
	        doc.setFontSize(12);
	        doc.setTextColor(40);
	        doc.setFontStyle('normal');
	        doc.addImage(headerImgData, 'PNG', 25, 15, 150, 26);
	        doc.text("HTA Report created on August 10, 2016", doc.internal.pageSize.width - 250, 30);
	    };

	    // footer
	    var footer = function (data) {
	    	doc.text("DISCLAIMER: Derivatives trading is not suitable for all investors. Past performance is not indicative of future results.", 25, doc.internal.pageSize.height - 20);
	    };

	 	doc.text("User Data", 40, 65);

	 	// left user data table
	    doc.autoTable(UserDataCols, UserDataRows, {
	    	theme: 'grid',
	        beforePageContent: header,
	        afterPageContent: footer,
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },

	        //headerStyles: {rowHeight: 15, fontSize: 8},
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        startY: 75,
	        pageBreak: 'avoid',
	        margin: {right: 450}
	    });

	    // right summary stats table
	    doc.autoTable(UserDataCols, SummaryStatsDataRows, {
	    	theme: 'grid',
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        startY: 75,
	        pageBreak: 'avoid',
	        margin: {left: 450}
	    });

	    // Stats Continued text
		doc.text("Stats Continued", 40, doc.autoTableEndPosY() + 25);

		// Stats Continued table
	    doc.autoTable(StatsDataCols, StatsDataRows, {
	        startY: doc.autoTableEndPosY() + 30,
	        pageBreak: 'avoid'
	    });


	    // Stats Continued text
	    var StartYForTitles = doc.autoTableEndPosY() + 25;
		doc.text("Stats for Longs", 40, StartYForTitles);
		doc.text("Stats for Shorts", 450, StartYForTitles);


		var StartYForTables = doc.autoTableEndPosY() + 30;
		// Stats for Longs
	    doc.autoTable(UserDataCols, StatsForLongs, {
	    	theme: 'grid',
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },        	
	        startY: StartYForTables,
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        pageBreak: 'avoid',
	        margin: {right: 450}
	    });

		// Stats for Shorts
	    doc.autoTable(UserDataCols, StatsForShorts, {
	    	theme: 'grid',
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },        	
	        startY: StartYForTables,
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        pageBreak: 'avoid',
	        margin: {left: 450}            
	    });



	    // Stats Continued text
	    StartYForTitles = doc.autoTableEndPosY() + 25;
		doc.text("Favorable Stats", 40, StartYForTitles);
		doc.text("Adverse Stats", 450, StartYForTitles);


		StartYForTables = doc.autoTableEndPosY() + 30;
		// Favorable Stats
	    doc.autoTable(UserDataCols, FavorableStats, {
	    	theme: 'grid',
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },        	
	        startY: StartYForTables,
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        pageBreak: 'avoid',
	        margin: {right: 450}
	    });

		// Adverse Stats
	    doc.autoTable(UserDataCols, AdverseStats, {
	    	theme: 'grid',
	        drawHeaderRow: function() {
	            // Don't draw header row
	            return false;
	        },
	        columnStyles: {
	            title: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
	        },        	
	        startY: StartYForTables,
	    	bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'},
	        margin: {top: 40},
	        pageBreak: 'avoid',
	        margin: {left: 450}            
	    });

		doc.addPage();

		doc.addImage(watermarkImgData, 'PNG', doc.internal.pageSize.width - 250, 30, 224, 112);
	 	doc.text("Completed Trades", 40, 65);
		
		// Completed Trades
	    doc.autoTable(CompletedTradesCols, CompletedTradesRows, {
	    	headerStyles: {rowHeight: 15, fontSize: 6},
	    	bodyStyles: {rowHeight: 12, fontSize: 6, valign: 'middle'},
	        beforePageContent: header,
	        afterPageContent: footer,        	
	        startY: 75,
	        pageBreak: 'avoid'
	    });

	    //doc.text("HELLO", 40, 60);
	    doc.save('Stage5-HTA-download.pdf');
	});
});


var headerImgData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAAApCAYAAAALZKR1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAF+NJREFUeNrsXQd4XNWVPtMkjaRRty3bGHADFwyGxBgwEDCYEpIsSXbBCftlgcC3tFASipcEiIFsaKEneGGTZQlkw7IQCDFgMDg0g00oxoDBcsEFF0mWrC6NNDN7/vvOw09P974pmlEkfzr+jiXNvHffLaf859zyfGtr1tEwDdNgpvxgIS3f/Bh9XPsSleSPtD8ewfwS8/6aWz5i/hpzzO8LUGt0F5WHx9K8iZdRQbCYeuLRvaZvgsPiMUyDnaKxTjpy3Fnk43/rGt6ivEAYH+cx78tcqrllDHOYr29tjTZQJH8EnTTpcioMlVNXTwt/5dtr+sY/LB7DNNgpkYhRyJ9PB1YdS7FEz5cfM3cabulS/8U6qKxgNJ06+Woqzqukzu7mvUp5hz3wMA0R8lF7dxONKBpPU1iJV279X1bIKjif0YYbquKJWCzgD9Fx48+nkoKR1Na1i3y+vc9fDSvwMA0JilNM/dynZAa1jWykcKikkf+8hrnChSRx4YZ4It4R9OcxbC5TnndvVF6dAs9knsM8ATEE8gfMY6WD4h5ltDJvk7+BU1YxPyufO2kU83ek7GxnElBH4KtHpQ5OQrz0fUl8dPQx70SFzO8zv9iP5x/GfJT0XQFzsfRddwpjsEsYfbyR+S3m5Ybr5zIfKv2XyFLfBQR2PqoZs0zoCOZ5zI3Zqh/Hsz2t0cY/j4lM2zK+fBaxgqL9bzNXumQpwrzJpzonQR3dTYfEEz2n8jC3asb8NenrTGk289E2ZHfIIeR7JfMyMTBnigzGsijv0M1VQYfi3sp8OHNZlh6wnvkR5hsdn81g/k2OjdIyjQJfxHxXkvsaZEDSTcsfyXwT81cNCZVMqI35HeYFzCtc3/2Y+bQc9d3SDNqvox+Jwcyuhfb5u7vjnQ92R1XoC2P8jEFeNzBPlxh5P9bXXxqKrGGexdyUQXVgKP5I+iw46J/k57QcyvzbfqnAn5lPyqLygiYyLxThdlq+gaYS5stTuA6W8to0y4Yi/ZX5hCwqL6iI+TgZlxl90OTgp0COys1zyVKPEXHvQZcvilHX0WTm8zKsy+Ueygtk8H8Oj5yzfvaLRR+Xw4eg/CkDKHxuWHmeZYVTou8LPE2FYOwecAlVtgmTnre5Qp3uAey7TClXE609KRoz5+ed0ocmgtEek2Y9qkWuTfRTV116ctQf3X6JSXNJiDUOkN9DA2z9kaW8Ic244qYUrz0vx4bPphMkzhsIGuyZnoBLMRBj79ZwPfWeYnqBebEH8rooA+9rQqvwvK8MUH+Egh4VQRx2J/Or0nE+D4uNZM3ZzMcaypoicBBx5lbxKAlNWTFJdJm8WosMmskQxFxJqh8LhE4XFh/vAbtsmpfk+0+ZP2RuJ+9sPxIgk8haOaQdJInndiZ5HpI0fxHv589AMTplfHJJccmNdGTofZ35gFrmK8hazBF1OYxPNV4P4dyphr65kPke5roU6jFaZN1Et6TRpk+Y/ya6lW54iUTZ00EyZ8YQeF+fRoHIui03KEyb45pZ4unimsHtFkTwgOEZNzP/TirvJp8oQ60jvvlXQzmfMT/M/O+GjrtJYtCeJPDWRCj7YlHeVOleSf4ki/1M9AHz9wa5B60Xw7cpC2Uhy3+BGLcuV/7gNXE8zvFDUhBJr28bvPCVZE1LJaOfiJPR0RPM76bRhuuYn+pPJwQ94p7qNMv6mKwsbql0qJ1OLxTFtWlHknK84MfnIgSpEGKbiOG7u5kXCTw9UfM9ptK+y/y44f6Ih2eHd/l5mspL8qwfpRDTDWXKZvweEdSiSx4CCRZQ3ymxn8l46+TiEub/Eu9togM8xqhdyk+HOvrbCUEP634WWXOasFzbRSGTufk3BeptyUCAnQPjFaOmQtOl/jpC3Z6U3+8wKLBtHZ8T2K6L602wGPPhuzJo95sC3QsdCuuT319L4X7E44fLWGVCdWRempjNGDtbCa64ILtSA+JLGCDrwwYlRL9fxfxDj2de5aEvv2dem2YbYIA+yrD9UP76oDQ2bOjsOcKZdO5mgRS/zhJkSodu9IiTH3bEOi+J4swxGAHArUcMsXbcQ4F10BuZzvkiKD0G79TmMpI+R7z3fpI27ycxYqaLBc7oL5xLgZBvwdz2OoG66Tiaxa4+CAj01VGlRx7gPoHeOvnA3O2tBkXEtOiZHrmZWzPoj2sEumdCCBG+HhTIekYOLO3+YrHmi5dbO0DKO5vMmfVaiTWdhgaT/M8a0MWNknzbncbztxs82SnMv8qwTdenoMBOwc6ECgZgbOAoLs3wXqDBc11K8z+CPHocBi9PEkMmNFEjCasrDegPuYvLNN8t8ECHkKGNGbYr0/FCCBfyi4fsyuGgoYNvp4GborjD47ubNTH0Yg/PA692eZbq1Z+5wPYB6LfBvk2nS9MnL5O1kGapg99gfj1JvH2zR5iDKaVZrs+wNuAcw/U7BNUNNPlsaIL46hvMvxCYkIs5x6+TlbnbnuNGfYustak6+kDgk45ukDqGDcmNh5i/SCOGD2oUNn+QK0hikNcvroHJmBoa66o7BHuNKLfJ8DUJEltogOvXUu9s9TUennLRAMi1CeXG7ESMbb2wbnOmxGtjk3jNPLkuII3+CnOVRwxzQI4bmkfe0wBoy90S+/hdsac9Dxo2xFMLyJx91EE9KHGjJjbukGfoVhQFqH9LENG3d4lwpoN2fFKnNwe5ApdqxrPEAP2LU0AUWJ31LzJebjpNQrEV4n1NsS9g8539aNPTZC0ySRcBIY+CqdAOdyb1E+FMaCpZ86qne3imXBJi7aM8vj9YOBMCfLpfOi0Z5CwxwLeXBB2ENd/jb+zg6c+i908kVBnMFBPEty2NmNsnHvYxg2MwxZXJFAIGG4suHtR8ByP/E8kNXeVRxj2kn6VIlYAClvWnQ4OSHNEpFxYz/HcaZa2RePJ0j8HLFQUp82xeKlQkiSR7aqrRI0kyWvrgUdfnmD55z+MZ0z2+S2UJapgGP9VLjPlpBjF3QiNPu6TdCZdnbkxR3h4XRT1Q890pkpw62cNgLupnf+RnQ/BnGr77oSQENqQQH2EwDiLz3GuqQpgp/YD67trJNn1PLO5KEY5aw8DbcREUebUImc8jsVUmY3Cdx7NT2e5m76xp6kcbkSeoybEHbs1SzF0rEHisK8EF+Pw5pbZIolkSWr83IMYFSRJh/U3+ntdPZNptzwPr5uSOEbizRq4xwZVu6bRk+2F35kgoyqQzc00+iZuOk7/flT4yeezbHJ7X79F3qXjOVObRsT/2oX628TZKbTlhf/owW0gBMvePZCVH4y74DCO0PMVyHhf0dkiahi4bc+bfFe6XB24i86T6GEp/q5XJ0uVqHvhiMp+N9J58n461x9TRb0VA3ISVM1gQ/7yEF6lMMeUl6f+kVpasTREDQV00dAiG+woPqH5Lil4Y/Yv5/ifTePbCQdJXTUGxJJfk+EGPUfaOV3FSNXlvBUMM83aaZSLzOMsjpsZ02xKxwrcnSXJkg7BYYesACYTnmutEIk7hUCnlB4uos6cVx9UQzl3+OxHCj20GB1NH6WX0oQOYdjohhWuRrX96sFgxv8DP13P4jMXU+1idZBRII+j/qQdCgII9m2GdMTVgWn11qMNoXC1GIldzqAhh3BvHc7liKt+cQUqoo1k3N31AyzYsoi+aP6KK8D6Ekx81Ib4J0UX6og6fKgOHz4ELghF1AB0YvxflVVAoUGCSXdNzwjr0HvCFqChUro6oTfQdslS3Ad6UZp8GKXeHRxYFJTbFcTrzBR4iGbQPWXNNmVjFDQJh3hUB/FOaZeyW5E/CoRh5kvxY5bgO0zXjJUaPuiAr4M2VPEhdAV+QBcAaz1i8m+KJGOWxQCQSGp3z+ag71oGzh7f7yHeBI7HU44q9ptKe6Y1rpY3oO0xjHUTp7+Sy6QvpO7T/RfEM7mw3+mACZfdQO7QFmviZ9ktWpkiogjY0rqRXNvyGWrrqaGvzatrQsIKmjzyJysNj2BMHqSvWpvqX+w5rCg6T0MmuY6mgoc1QHoxLXqBQefX69o20aoe13z6SP5Jmjv4ml+GnFVv/SLVt62n6iBNpQsVspXSxeJTHqBN1wiKNNxx94ZQLxL8duB4IIT9QxPXqocbObfTS+nvpoJHzaELlEeqoWYciwwPfK+PYoenbAgmdlqTZt9ulnuWU3ZM5IOfbfJpXq5SS926bZBCsVTq0ox/CFPnS8O+xts4MJonAReSZMZdljvPAtETYY0RZIWvbNljHELL1xas16ts3EY4cdRMODR9ZNFFZ/NaueghJqQZacr8kYixgzUWq/HaGky02lAxJ/2WaqGkX49PqhK3wUPBEgKzRWGe+3+cPS52yqcDgNreQQcDxOpONje8o5cVlYfaMXardzezRKmhc2UxWihOptGC06tdW6wzmCikr4RivFi6vO5JXpe5v6txOq3cuYW/+MbfNAjzBQD6VF4xVvzd2fkFdDNWL+fpy9vYzRp1CIwr3V/VpjtbBWBSysXCf9ojntHO/dQIx9CSi/Jwd6rUsQA+oW1FeOR0//iLat3QGl1Ov3vjgoHIpL6FBhrsz7N9CqVc2t4VCzrt9Q//dSD5WzBIWmECvPgdc2tK8ijY2vEtr6l9RgzSqeDKVFYyhT2pfVorspmi8k6aNmEsTyg/naydRW7Sxz3nCVhxoPe/j2hepuvhAqizcj9pd1+K6fH6G8jKGKUkGdcrjwwDgXuc9KqOkPFqP+vyz+le5XrPV4eZtLOzus47RPig5DBaE3vkd4lZ4vIRGflAHGCH3Pcpq8XMj+VW0qfE9WrbRmvLEa02c8LMn3mUdpcLoZurIuTS+9Cu0b9mh1MIK1hPrkjITykOjHvi5pWkVfd74Nx6XZRRk/cM1QX++GIy4us9WZhhGvMsozuWjbfuXfZUmVx1N40pmUF4wrOoIwwbjq6wQX18YLFX3Ws95j9bUvUx+NoIKZvgLlPEI8HVzJ15C+5RMZ0SxCydeDk3pH0oKbEGvEHukoILAPoa8EO5VO55T3gDC8aWbZE+wdtfrPLjNBIufEGGDoBWEIup1HX2F2a8ED+WcNOlSFpZZSolwjzIKLAR5DMc2NLxNNQ3Laf2ut6iicBydOvkq9j7VSpCsWIvhYbCI1ta/xkL0oUr6aFO+PW00tvQgmlL1NfU7lHfdrjdp025rvcdEho0TK49iz/cAvb/taTqg6hhuywiqYoMxdcQJqm7KFLPwQchXbn2cRkemqleQIMlk1SVAq3YuVogioEEdeO640oO57GOVEvfCjFyfTbvfV7ATimQZloTRkLZEa6k4VEn7lR2m6gA0A2XBWMAYfsgw2R8I0br6N5TRKlbjkjqIgKKjjnhXEsqHMcHYHFJ9mjJeUH58D3nAtet5nNBuvBupd0191BlrVeN9yqQraAwrMV6ANqzAuVReVlQoBZQQnV3M0G194wr6tG4ZNXfupG6GSn7HdCsGE0kRCC3uTVdIYKkrGa7NqD6ZYdsEBTDrWtcryLeLITgUHZARgokY8Oh9z6GKsAX9drauo9W1L1BDxxauW60WrlveK6qEC151cuUcjjHfodqWGmrqsqbM8Ua90vxRtKOthgoCEWWkOpgBIcsYZkJwR0cOZCSwlI3F6+p5gI1ABLPHzVehwuodzysICS+ryxijDnj1SEV4nCoPSoU+Rl83tG9WsBlKhoRSsn70KUPSpWAq6j6mZBrN3mc+rdq+mDbufkfVAyOTybj0iTUYhcTFCMN4Th1xvDLu6Iumrh3ssXvUc+CJdc9BXYFicM+8yZfR2MhBLFd1NNTenTRkFNiCgAl6e8sfWBje5cEpVfATsAqey0f+rIWEShAZxnUzpEa5B7Lni3PZNew5YBig3LZgwJqjDoC6U6qOUzEX3qCH75zXmQU+qtqBNkTZGwJK4h4QkjVQCGf78DPGz4BnCwdLlHGAF0Vd8bcqj3+H8MI7wRPDk0J5dd7O2Vbcb4cieA4+Q7uQBEzLCPI/1A/lhLkeeCMg6pIfLM6JbMDgKgiukpCd2vDI1P9AAmjfiRMuVugFCbq/49TY3qrACeWJ1jWsoMWf/ZJKw6Mt5fH5RLDVQWnObGS3mNKgI4nwV7LO7SLJtB+uSSoEJHOKueBNNmxXWU8VkxU4Ex5nynOe6n2dTymgXHe4cJvmOS2SqV9nIwyJQaG9WJ3TRL2PQsVurm+SdQTLEvseGBQoHT8PbT1HstQrJRtsl1kq94b7JuTUssMldszrNIKO+01JFBycMJJ6L2pAthYnT76gcJC0y9Fv35EsfZerP/A3DpfY4noO1ojPkbH1ubKwDTLTsTMhkxYiD3hbxjRJRvkcibqAzLo8tydZB2PTqsKmkxlO71t6CDV3IUHWPSS88ZBQYMAcwMc3Nj+iYJ0199iLMAUwN0kxWCJ4m2Muz+sAMgjR+R5TBth3/Iz8DuOx1HDd78i8EVwlWsmaR3buIkLAhnW+NbTnPG0QTrK4R37HCqS7XWVFxPjgmc5znXDsKs4qnuUxc/AwWSd4pjrNgUwtVi4d73HNz6n3flsYDyxK+QePe7aK8XIegng96fft2rRR+vhVx2dYOOT1ahdM0Z0hRlIZGORKkPyqjhxAM0d/S4UpyGkM9peiDYm3EwLSLN/yGG1tWs0dO0IHBf+NrDWxnSJcfyDrpVULac/Bfas17b7d8hRfznnDC+xP1hazsw0KjLKca69vMXhz25uCThcPYmMzKApwHrYo4iwlLDhxn4YY0yiaTXfJ3/f2gin6+y4W5cV89aMut1Iuzz9XlDjVBT3fFuW9T4yi3+VNnxcFRvvs7NCJorz/KePid9V9jij4z8RAOqeFSAwqPLudEWyWdt0hXv1VzT1YcLNG+johCosNENhEgHnqZc6ZBcD+T+peYQ9cS/MmXqqy5u1qtZl/WIH7C6ERZ6oFGPo4d6UL2v2HeLClSQrGkjjdovcHxXPp6EKB4GdL/0EgL3Epk5ueMXyOwwIwPzPWocAJUfCoQYEvFI9zj3x2vwMiksaLTpOfuhd8bRZhRnkb0hiQI8XY3UH6ZZ7XUd/lufZe7UcN9+BFYVimekyfkNqi32qSHG+Ihw73EZg9CMi9ZrlY2jzKnSTFCi28EHxnaw29uP5uOn78BUqxu2NdwwqcjSRWijFJhPasLDKRvaF+gcTFTs9o78vVHdA9VjwLLLe9V/pcgeTwHnUGQQqRfpN/h8Fr6nNDFgFJnCTx4n1SJyAQ02tB3YYAwn417Tm+tkEgezp7U2MSw5uyhk8Iu2NmIvMKvxDpN+Lb9V8k3jwo9fbLWEEh1xru+bW0zX6BAdCT/WbHNabGFedXqaWiWxjxHVx9yrAC98v3YnFDoEjFvdrlj5mR7aWweP1oh9BAMOyjSnVHzAC6lonnniSf/UW8y+3k/coNHXU6YGeqVC11O01i/wUisDe4FN00xkgyXSmCb6+UCqXphYNyj2lAkFCcIvXrctWjx2NM4hrDa18/35GUSsjPModR091zhvxu32MbiFs19/SSuZL8arUABDEx4mEkKAdleDmYlRcZzMK8cmro3CoLEbJmbwoc3nOUZFJHinLYx+4c4boH0M5+5+vdAtFrBPaBzpJYWNe/phMSD03yvRdhUhWHEb4jMPJ+Q1kxl2I3SpxfLWjlB0kUS0eIJUs8vCkSUYup97nNLS7UQRq0AuVt0yAGknjXHiv75zxp71zDPUe57qmUei+gJMsasQCmNVovMwvDMXBGcS+WSGKRxgs1d6hkAuYpU1i5Y28a99qHG3J4XLfAf+qIlWwqk1gX9M8CVwMOBamUGO0hSca0upRnkUZgQhKLAcKvctU/SH13HQU1RnejIIjnJFlFGq+4TJJOrwtysNdbx6WNZ0k903lDALK42CWFV5H8ScoMiBEodSCRmCsPcIPEzU85+tD2jDBm2ETzK8NYRTVj9Zk8o9yQPIxmaByVnGFu2e9aojuswCkSFkG0sAVcsu5OVt5GVuaKL1feJHPcZM31eR0t0yLf617zkS/fOReuf0NgIbztY4Yyp0niZh7t2YFl18P0krUnJeHT5Ko/Yj33+dXtcl2nJt47VWLO4zSxMBJyUyX5NccFQwPyPOy8+iCN4VkqyrhQvJxdpv1ibWSIz6feZy/j7LXLREFni7Lbq2/s1/Y8ITkG3ViZDHEd9d1r3kj9O1poyND/CzAA3QXCUfMqNjcAAAAASUVORK5CYII=";
var watermarkImgData = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAAEAlgCWAAD//gAfTEVBRCBUZWNobm9sb2dpZXMgSW5jLiBWMS4wMQD/2wBDAAUFBQgFCAwHBwwMCQkJDA0MDAwMDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ3/xADSAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/AAAsIAHAA4AEBEQD/2gAIAQEAAD8A+y6KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKqS3sUJ2k8jt/8AXOB+tRf2nD7/APjv/wAVUsV7FKcA4Pof/rZFLJexQsUY4IqP+0oP71H9owf3qsvOiJ5hOF/xqBL+FyFDcmrlQTXUcHDnB9P89Pxqt/acPv8A+O//ABVOXUYScZx+X9CasSXCRKHJ+U9DVf8AtKD+9R/aUH96rUUyzDchyAcfyP8AWpKrzXUduQHOM1D/AGlB/epRqMB4DVzPiayvbmVGtLk2yhcFRJImTnrhODxW3prtZWca3UhlcDDPktuOT3bk/jWnDOk4yhyBUtVGvoVbbnkUsl7FCdrHBqP+0oP71KuoQscBuTXN/KJf3mcZ5xWhus/Q/kP8KhWKBn/duV54yOn+fYfWtefTo53LknJ/wx61gXkIt5Si9Bj+QP8AWthNKiKg5PIB/wA81S1GXLCBD8qcfj3/AM/X1qvcWjWoR/UZP1//AFYz9a6GyuBcRg9xwa52fM1wQe7Y/M1rvY21uMyZ/wA/hWdPb25/1Tgf72f6D/PrU8422oGQ2D1GfQHvj1z+NU7YW+398SD7VZxZerfp/hWzZpGkf7rO0knn1Hy/0q1XP6z99fpUUEFsyAuxDc5H48dvSp0trQkAOc9vx/CptRHzD5N/FWBAJ7YIV2nGQPQ//X6fjWRYzG1l2NwCcH/Pt+nPvW5e3H2eMsOp4H+P+e+KxtMt/OfzG6Dn8ai1P/Xn8KkUWeBktnAzjpnv2qaJLQuApbOeOn+FQvdRtIRJGBzzg8/0FTb7L+635/8A2VZ03lmT9zkLxjNdXACI1B6hR169O/vXN6l/r2+o/wDQVrcmn+zwBu5AA+uK52GKWZt6DJHt/wDWq7LFdyrtfkfQ/wCFQ2E5tZdrcA8H/P8AnvUVwDBOSezZH58f41ozX9vcKA4bj0x/X/Csy4aE4EII+uP6VbZStoM92/oo/mDUFtLDGuJFJNWftNr/AHD+dbFnKkkf7sbVGePqcn9atVz+s/fX6U+20yOaMOSwJz0x2JHp7VaTSoo2DAtwQe3b8Kh1LAYfOU4rQs/9UvO73rI1W3KMJl6Hr7H/AD/X2qhLO91tT+6MCuntYPs8YTv1P1rntT/15/CpVuLYAAocgev/ANapY7q2VgVQg1rSWcUhyV5/z+FR/wBnw/3f5f4VIlnFGchRkVZqvJaRSHcygk//AKv5CkuFhVMzYVE7scAfjVa3vbIERwyR5Y8AMMk/nUsmpWsLFHljVlOCCwBB96cLeC4/eqAwbkMDkH6EUbILocbX2naSDnBHY49PeqMh0+FijvGrDqCygip1gtNnnAqU/vZGPTqPerEnkErC5XJHyrkAkD0FR/ZLYN5eF343bc84zjOPTNU5JNOiYo7xKw4ILDIP51qwJGijysbTyCOQQe4NJPcxWq7pnWNegLEDn8arJJaagfkZJSv91s4/I03+0bO3PlebGhUn5dw4Oee/rVqS6hiQSu6qhxhiRg56YPSqoktL9sK6SMB0DAnH0Bpft9pbfuvNjQrxtLDI/WrhCXCDoyNyCOQfcGoks4ozuVQCKs1XktIpDuZQTTPsEH9wUCxhHRRVuiiiikZQ42sAQexGRWNptkIZrhmjCgy5Q7QONo5Xjp9KzYtLupJ7l1dIVeclRJAshYeXGNwYsvy5GMY6g+tPgju7TTRBCh+0OzIONoXcxy57KoHIHrjrUOl2t3pk65hCQSqEcJJ5mHA4lb5Vxnox56g1NGwtLi4862ll3y7lZYt4xtA6/WmCzlOn3CrGyGWUvHFgAhS6n7o6dC2O1WL7TDe3cZcMqpAQJF4KSbgVIPZh+optjHd/2hm6TiKBo/NH3ZMuhB9mwDkeorNUTW0HkCKRJVd9zfZfPVwWJGG3KO/UnNdPpEckNpEkqiNwvKjtySB1OOOozweKo6tGy3NvcGNpoovNDqi7yCygKdvfoee1Ps5Fnut8ds8KiMgyOpjOSwIUL/Fnk57dO9ZmnOLC3FvcWszyqW3MsO8NliQQ3fII5NIdOuTp1tABtlWYMcrv2KXdgWXIBCgjIzgdKsRabc299BLIyyoolBMcIiCErxuwzbgegBxg81FBpN1I87B0hV5nID26yEggfMGLDg9uO1dHY2gsYEt1O4RjGTxnueO3J4HYcVaoooooooooooorntUeeO4RY5ZER4pnKrsxmNVK4JQsM5Ofmz6EVlx67c28KI6oz/uh5jHCgPGX+cu6jdkYyXUEnOM8VoXeoTxSwEMAsqrmJGVm3MfvDI/eRjvsZSo+bkVBZatdGKPdslby5nkwCGzGwAQAHAY55yPTimjXrlgAixMST843FOIjIV+9ncpG089+gPFRjxLMxcokeI0DbM/PkrGc/fGVBdiflAAXlucjTsdTuLqSOMiIKyyMzKwcEKygbSjsoJzyCzYx78ZkOoX0QkuGO6ESPH+82bVPnbFb5AGCRpkuWPPGD1NMudemaIqQF3xy7Wj3ZYoWAZMNuRCF3BsOO24cZU6pcRMS58wxzuBGuVbYIC6hgCdyk9MjqCcngCb+3rkjCLE+N/zjcUIWISED5uoPyHnGeexFdHZ3iXabkPIC7hz8pKhsc+x7VbooooooooooooooooooooqqljBHKZ1RRIc5bvzjJ9MnAyR1q1RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRVeK7imkkhjbLwkBxg/KWG4DOMHjngnHQ81YoorBttfS6uDbRwXOFkeMy+WPJDISGy4Y8ZGBxnJGQK1oruKaSSGNsvCQHGD8pYbgM4weOeCcdDzViisiPWI5QxjjmfZcm2bagOGHVzhuIhnljyP7ta9FFFFFFFFFFFFcLfaXPcXc+nqrLZ3jLcPKAdqlUIZM4xveRUbGc7cnFUo9Pu7+1lvZ4mS8tlhSBWU782+HdkBGT5zEgYHPTnrST2d87G4WOQPqwaKZcN/o6l0EZfjjbDuDZAwSfpVm40WQ3EulQxmOxuSkxdQQibE2smcY3NIkbYznbk4q3o0lxEkmo3sE3mxpFbLGqEyFUxvdVOMh3bcT6KeuOc/UbSR9ZWeOCe4YPDgyI6wxqoDFopllC8dSrxsu/djJODJBp39nXt8YrWR3eNmgf5/KYMgLxltw+ZnPAHzcEKV4zQ0mxu7eSYWcMtss1nJtBSSIGYNhPvzz7XGTsLOhxzsA+YmlQxw31ikME9rJtnMgnJ+Z/KALqjOxAJBy2yMN0GduFfoVhcxX8b3CXEVyGfzpTC5jlGCdrTG4aNlPy7dsI+YKOozV61guItRAs1vogZne4E+37LsYkv5WOGYsRsI+bHJ6NiOPSls7vUFS3l3SwyGCVQ7RkPHl0znBdnPAwTwQMDGSfS/sulQILeSZpvJNwpE0jqVjOP3azRMNrYXaHRVHUHArHh0+6QLFeW1zLYRXEw+zxhgRlVMbBfNZjGCWwRKyqS37xt3zaK2dxBZs6wT4i1QT+WQXlMSkcjk+Ye24MwPJ3EAtVm80r+1Jb+eWGYBreF4VPmJmQQsQNqkK7o2BtO7axIxzzHdW8jS2st/Bc3ES2kYXy2ZTFOMF3kO+PyzjGXdgOOclTt1vC1wYtOtYQkhMomw4XcibXc/vGyMbui8HcfSsnSrKRFmX7PcR6qYZg11IW8pmJ+XY+/YSfk24UYAJycHJolkUvIHtra5tWRW+2STbwspKYGNzkOTJlgQFx97HpBa6LIlrZzbJ0uBdFHwZVZIHkk3/KCPLVhglwAec7sEV0+g2H9mT3cEavHbiRGiDbiDujUuVZs7vm4JyemCciukoooooooooooopkkayqY3GVYFSOmQRgjjnp6VmadoVjpLF7SIRswwWyzHHoC7MQD3AxnAznArWoooooorJ1DQrLVXWS7j8xkGFO51wM5xhWUHn1rTjjSFRHGoRFGFVQAAB0AA4A+lPoooooooooooooooooooooooooooooooooor//2Q==";