<?php 
date_default_timezone_set('UTC');

$output = array();

require ("Constants.php");

if (isset($_POST) && isset($_POST['Username']) && isset($_POST['Account']) && isset($_POST['Contract']) && isset($_POST['FillIDArray'])) {

	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_ADMIN, PROD_DB_PASSWORD_ADMIN, PROD_DB_NAME);
	
	if ($mysqliConn->connect_errno) {
		// Error in connection
		$output['OutputType'] = "ERROR";
		$output['Data'] = $mysqliConn->connect_error;	
	} else {
		// Connected
				
		$Username = strtolower( $mysqliConn->real_escape_string($_POST['Username']) );
		$Account = $mysqliConn->real_escape_string($_POST['Account']);
		$Contract = $mysqliConn->real_escape_string($_POST['Contract']);
		$FillIDArray = $_POST['FillIDArray'];
		
		$output["FillsArray"] = $FillIDArray;
		$output["isArray"] = is_array($FillIDArray);
		
		$StringOfFillIDs = implode(",", $FillIDArray);
		
		
		$query = "SELECT * FROM user_trade_metrics WHERE FillID in ($StringOfFillIDs)";

		$output['OutputType'] = "DATA";
		$output['Query'] = $query;
		
	}
} else {

	$output['OutputType'] = "ERROR";
	$output['Data'] = "Post metrics not correctly set.";

}

// Code returns here
echo json_encode($output);

exit();


?>