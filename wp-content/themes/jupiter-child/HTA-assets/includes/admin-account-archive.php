<?php
// set a 3 minute time limit to complete operations
set_time_limit(3 * 60);
$output = array();

require ("Constants.php");
include_once ('PHPMailer/PHPMailerAutoload.php');
if (!defined("EMAIL_SENT_TO_LIST_ADMIN")) define ("EMAIL_SENT_TO_LIST_ADMIN", 'rk@tradingalgox.com');

$starttime = microtime(true);
$outputString = "";

if (isset($_GET) && isset($_GET['Operation']) && trim($_GET["Operation"]) == "ArchiveAccountsCron") {

	//$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_ADMIN, PROD_DB_PASSWORD_ADMIN, PROD_DB_NAME);
	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_MASTER, PROD_DB_PASSWORD_MASTER, PROD_DB_NAME);

	
	if ($mysqliConn->connect_errno) {
		// Error in connection
		$output['OutputType'] = "ERROR";
		$output['Data'] = $mysqliConn->connect_error;	
	} else {
		// Connected

		/*
		 * Start processing the accounts that are "unprocessed"
		 *
		 *		1. Try to see if any accounts marked as IsTEMP are past 31 days since creation, if they are, put them in the table to be archived.
		 *		2. Process the accounts in the table of accounts to be archived
		 */

		// 1. Process IsTemp accounts (The account name should contain "DEMO")
		$IsTempAccount_Query = "SELECT * FROM admin_accounts WHERE (Broker='GAIN_DEMO' OR Broker='CQG_DEMO') AND IsTemp=1 AND Updated < DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 31 DAY)";
		$IsTempAccount_Result = $mysqliConn->query($IsTempAccount_Query);

		$TempAccountsOutputString = "";
		if ($IsTempAccount_Result != NULL && $IsTempAccount_Result->num_rows > 0) {
			// If there were any accounts from GAIN_DEMO or CQG_DEMO that are set as IsTemp AND past 31 days since initiation
			$TempAccountsToBeArchived_Array = array();
			while ($AccountsRow = $IsTempAccount_Result->fetch_assoc())
			{
				$AccountsRow['Name'] = utf8_encode($AccountsRow['Name']);
				array_push($TempAccountsToBeArchived_Array, $AccountsRow);
			}

			foreach ($TempAccountsToBeArchived_Array as $AccountToArchive) {
				$Brkr_str = $AccountToArchive["Broker"];
				$Acct_str = $AccountToArchive["Account"];
				$Name_str = $AccountToArchive["Name"];

				$InsertTempAccountIntoArchiveTable_Query = "INSERT INTO admin_archive_accounts (Broker, Account, OldAccountHolder, DeleteOldAccountFromAccountsTable, MethodOfDesignation) VALUES ('$Brkr_str', '$Acct_str', '$Name_str', 1, 'AUTO_31_DAY_LMT')";

				$InsertTempAccountIntoArchiveTable_Result = $mysqliConn->query($InsertTempAccountIntoArchiveTable_Query);
				if ($InsertTempAccountIntoArchiveTable_Result === TRUE) {
					// GOOD, accounts to be archived inserted
					$TempAccountsOutputString .= "$Acct_str ($Brkr_str) - $Name_str<br />";
				} else {
					// FAILED, did not insert into archived accounts table
					$TempAccountsOutputString .= "$Acct_str ($Brkr_str) - $Name_str ---> FAILED<br />";
				}

			}
		}

		$outputString .= (!empty($TempAccountsOutputString) ? "The following accounts were past their 31 days and are marked to be archived:<br />$TempAccountsOutputString <br />---------------------------<br />" : "");

		// 2. Process archive table
		$GetUnprocessedAccountsMarkedForArchive_Query = "SELECT * FROM admin_archive_accounts WHERE DateProcessed IS NULL ORDER BY Broker, Account ASC";
		$GetUnprocessedAccountsMarkedForArchive_Result = $mysqliConn->query($GetUnprocessedAccountsMarkedForArchive_Query);

		if ($GetUnprocessedAccountsMarkedForArchive_Result != NULL && $GetUnprocessedAccountsMarkedForArchive_Result->num_rows > 0) {
			$AccountsToBeArchived_Array = array();
			while ($AccountsRow = $GetUnprocessedAccountsMarkedForArchive_Result->fetch_assoc())
			{
				$AccountsRow['OldAccountHolder'] = utf8_encode($AccountsRow['OldAccountHolder']);
				$AccountsRow['NewAccountHolder'] = utf8_encode($AccountsRow['NewAccountHolder']);
				array_push($AccountsToBeArchived_Array, $AccountsRow);
			}

			/* there were accounts to be archived, perform the following
			 *		1.  IF account should be removed from admin_accounts, then remove it
			 *		2.	Move the fills from the (broker specific) fills table to the (broker specific) archived table
			 *		3.  Delete the fills from the (broker specific) fills table
			 *		4.	Mark the Account as processed in the admin_archive_accounts table
			 *		5.	Find out if any users had that account assigned to them, if yes then take it out.
			 */

			foreach ($AccountsToBeArchived_Array as $AccountArray) {
				$outputForProcessedAccount = "";

				try {
					// Process account
					$Broker = trim($AccountArray["Broker"]);
					$Account = trim($AccountArray["Account"]);
					$AccountUsername = trim($AccountArray["OldAccountHolder"]);

					if ($Broker != "" && $Account != "") {

						$outputForProcessedAccount .= "Processing <strong>$Broker - $Account </strong><br />";

						// 1. Is account to be removed from admin_accounts
						if ($AccountArray["DeleteOldAccountFromAccountsTable"] == "1") {
							$DeleteAccountFromAccountsTable_Query = "DELETE FROM admin_accounts WHERE Broker='$Broker' AND Account='$Account'";
							$DeleteAccountFromAccountsTable_Result = $mysqliConn->query($DeleteAccountFromAccountsTable_Query);

							if ($DeleteAccountFromAccountsTable_Result === TRUE) {
								if ($mysqliConn->affected_rows > 0) {
									$outputForProcessedAccount .= "GOOD: Account marked to be removed from Admin Accounts. (Affected Rows: " . $mysqliConn->affected_rows . ")<br />";
								} else {
									$outputForProcessedAccount .= "FAILED: Account marked to be removed from Admin Accounts. (NO ROWS AFFECTED)<br />";
								}
							} else {
								$outputForProcessedAccount .= "FAILED: Designated for removal from Admin Accounts, but no accounts were deleted.<br />";
							}
						}

						// 2.  Move the fills from the (broker specific) fills table to the (broker specific) archived table
						$ProdFillsTableName = strtolower("fills_" . $Broker);
						$ArchiveFillsTableName = $ProdFillsTableName . "_archive";
						$MoveFillsToArchiveTable_Query = "INSERT INTO $ArchiveFillsTableName SELECT * FROM $ProdFillsTableName WHERE Account='$Account' ORDER BY FillID ASC";
						$MoveFillsToArchiveTable_Result = $mysqliConn->query($MoveFillsToArchiveTable_Query);

						// was the fills copied into archive table?
						if ($MoveFillsToArchiveTable_Result === TRUE) {
							$outputForProcessedAccount .= "GOOD: All fills copied to archive table. (Affected Rows: " . $mysqliConn->affected_rows . ")<br />";

							//	3.  Delete the fills from the (broker specific) fills table
							$DeleteFillsFromProdTable_Query = "DELETE FROM $ProdFillsTableName WHERE Account='$Account'";
							$DeleteFillsFromProdTable_Results = $mysqliConn->query($DeleteFillsFromProdTable_Query);

							if ($DeleteFillsFromProdTable_Results === TRUE) {
								// Good, delete fills works
								$outputForProcessedAccount .= "GOOD: All fills deleted from fills table for Account $Account. (Affected Rows: " . $mysqliConn->affected_rows . ")<br />";

								// 	Everything went well
								// 	4.	Mark the Account as processed in the admin_archive_accounts table
								$MarkAccountAsProcessedInArchiveTable_Query = "UPDATE admin_archive_accounts SET DateProcessed=CURRENT_TIMESTAMP WHERE Broker='$Broker' AND Account='$Account' AND OldAccountHolder='$AccountUsername'";
								$MarkAccountAsProcessedInArchiveTable_Results = $mysqliConn->query($MarkAccountAsProcessedInArchiveTable_Query);

								if ($MarkAccountAsProcessedInArchiveTable_Results === TRUE) {
									// GOOD, successfully processed 
									if ($mysqliConn->affected_rows > 0) {
										$outputForProcessedAccount .= "GOOD: Account marked as processed in admin_archive_accounts table. (Affected Rows: " . $mysqliConn->affected_rows . ")<br />";
									} else {
										$outputForProcessedAccount .= "FAILED: Account marked as processed in admin_archive_accounts table. (NO ROWS AFFECTED)<br />";
									}

									// Everything was successfully processed
									// 5. Find out if any users had that account assigned to them, if yes then take it out.
									$FindIfAccountIsAssigned_Query = "SELECT * FROM user_access_control where GAIN_LIVE like '%$Account%' OR GAIN_DEMO like '%$Account%' OR CQG_LIVE like '%$Account' OR CQG_DEMO like '%$Account'";
									$FindIfAccountIsAssigned_Results = $mysqliConn->query($FindIfAccountIsAssigned_Query);
									if ($FindIfAccountIsAssigned_Results != NULL && $FindIfAccountIsAssigned_Results->num_rows > 0) {
										$UserPermissionsContainingArchivedAccount = array();
										while ($AccountsRow = $FindIfAccountIsAssigned_Results->fetch_assoc())
										{
											array_push($UserPermissionsContainingArchivedAccount, $AccountsRow);
										}

										foreach ($UserPermissionsContainingArchivedAccount as $UserPermissionRow) {
											$Username = $UserPermissionRow["Username"];
											$BrokerSpecificAccountsArray = explode(",", $UserPermissionRow[$Broker]);

											// Check all array accounts to see if they match the one to be removed
											if (in_array($Account, $BrokerSpecificAccountsArray)) {
												// Account does exist in Array

												// remove it
												if(($key = array_search($Account, $BrokerSpecificAccountsArray)) !== false) {
													// Find the account in the accounts array & remove 
												    unset($BrokerSpecificAccountsArray[$key]);
												}

												// put together the accounts string again (comma separated accounts)
												$UserPermissionsAccountString = implode(",", $BrokerSpecificAccountsArray);

												// Write this to user table again
												$writeAccountStringToUserPermissionsTable_Query = "UPDATE user_access_control SET $Broker = '$UserPermissionsAccountString' WHERE Username='$Username'";
												$writeAccountStringToUserPermissionsTable_Results = $mysqliConn->query($writeAccountStringToUserPermissionsTable_Query);

												if ($writeAccountStringToUserPermissionsTable_Results === TRUE) {
													if ($mysqliConn->affected_rows > 0) {
														$outputForProcessedAccount .= "GOOD: The username $Username had this account ($Account) permission, this has been removed. (Affected Rows: " . $mysqliConn->affected_rows . ")<br />";
													} else {
														$outputForProcessedAccount .= "FAILED: The username $Username had this account ($Account) permission, this has been removed. (NO ROWS AFFECTED)<br />";
													}

												} else {
													$outputForProcessedAccount .= "FAILED: The username $Username had this account ($Account) permission, their account permissions FAILED to be udpated. Message: " .json_encode($mysqliConn->error) . "<br />";
												}

											}
										}
									}									

								} else {
									// FAILED, to mark the Account as processed
									$outputForProcessedAccount .= "FAILED: Account could NOT be marked as processed in admin_archive_accounts table.  Message: " . json_encode($mysqliConn->error) . "<br />";
								}


							} else {
								// Error, delete fills from prod table failed
								$outputForProcessedAccount .= "FAILED: Fills were not successfully delete from $ProdFillsTableName.  Message: " . json_encode($mysqliConn->error) . "<br />";
							}
						} else {
							// Error, copy fills failed
							$outputForProcessedAccount .= "FAILED:  There was an issue inserting the fills into archive table.  Message: " . json_encode($mysqliConn->error) . "<br />";
						}
					} else {
						$outputForProcessedAccount .= "There was an error, there was an account with null Broker($Broker)/Account($Account).<br />";
					}

				} catch (Exception $ex) {
					$outputForProcessedAccount .= "Error processing account with following code: " . json_encode($AccountArray) . " Msg: " . $ex->Message . "<br />";
				}


				$outputString .= (!empty($outputForProcessedAccount) ? "$outputForProcessedAccount<br />---------------------------<br /><br />" : "");
			}

			$output['OutputType'] = "Data";
			$output['Data'] = $outputString;
		} else {
			$output['OutputType'] = "Data";
			$output['Data'] = "Process completed! There were no accounts marked to be archived.";
		}

	}

	// try to close the MySQL connection
	try { $mysqliConn->close();	}
	catch (Exception $ex) { /* If you can't, that's OK, don't do anything */ }
	
} else {

	$output['OutputType'] = "ERROR";
	$output['Data'] = "Post metrics not correctly set.";

}

echo $output["Data"] . "<br />";

// Work done, email results
if (!empty($outputString)) {

 	// Email to be sent to admin
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->Host = EMAIL_HOST;
	$mail->CharSet = 'UTF-8';		
	$mail->SMTPAuth = SMTP_AUTH;
	$mail->Username = EMAIL_USERNAME;
	$mail->Password = EMAIL_PASSWORD;
	$mail->SMTPSecure = EMAIL_SMTPS_SECURE;
	$mail->Port = EMAIL_PORT;
	//$mail->SMTPDebug  = 2;
	$mail->SetFrom (EMAIL_USERNAME, 'Stage 5 Tech Support');
	$mail->AddReplyTo("no-reply@stage5trading.com", "S5 Account Archiver Daemon");
	
	$CommaSeparatedListOfEmails = explode(',', preg_replace('/\s+/', '_', EMAIL_SENT_TO_LIST_ADMIN));
	foreach ($CommaSeparatedListOfEmails as $key=>$email)
		$mail->addAddress($email);
		
	$mail->isHTML(true);
	$mail->Subject = "S5 Account Archiver " . (strpos($outputString, 'FAIL') !== false ? "(ATTENTION)" : "");
	
	if ($output["OutputType"] == "ERROR") {
		$outputString = "Error occured: " . $output["Data"] . "<br /><br />" . $outputString;
	}

	$mail->Body    = "Processed: " . date("y-m-d H:i:s T") . "<br /><br />$outputString";

    $AdminMailSentSuccessfully = $mail->send();

    if ($AdminMailSentSuccessfully) {
        echo "<br /<br />EMAIL SENT<br /><br />";
    } else {
        echo "<br /<br />Error sending email: " . $mail->ErrorInfo . "<br /><br />";
    }

	try {
		$timediff = microtime(true) - $starttime;
		echo "Time taken to complete script was " . round($timediff) . " second(s).";
	} catch (Exception $ex) {}
}

return;


?>