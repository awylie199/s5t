<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

$output = array();

require ("Constants.php");

if (isset($_POST) && isset($_POST['Operation'])) {

	$Operation = trim($_POST['Operation']);
	if (in_array($Operation, array(
			"GetAccounts",
			"GetUsers",
			"AddAccountToUser",
			"DeleteAccountFromUser",
			"EditCheckboxPermissions",
			"FindDoublyAssignedAccounts", 
			"ManuallyRefreshPusherDataFromDB",
			"SetDEMOAccountToTempSetting",
			"ArchiveAccount"
		))) {

		/*
		$Operation == "GetAccounts" || $Operation == "GetUsers" || $Operation == "AddAccountToUser" || $Operation == "DeleteAccountFromUser" || $Operation == "EditCheckboxPermissions"  || $Operation == "FindDoublyAssignedAccounts" || $Operation == "ManuallyRefreshPusherDataFromDB" || $Operation == "SetDEMOAccountToTempSetting" || $Operation == "ArchiveAccount") {
		*/

		$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_ADMIN, PROD_DB_PASSWORD_ADMIN);
		
		if ($mysqliConn->connect_errno) {
			// Error in connection
			$output['OutputType'] = "ERROR";
			$output['Data'] = $mysqliConn->connect_error;	
		} else {
			// Connected
			
			switch ($Operation) {
				case "GetAccounts":
					// Get Accounts
					//$accountsQuery = "SELECT * FROM " . PROD_DB_NAME . ".admin_accounts WHERE UPPER(Broker)=UPPER('" . strtolower($Broker) . "') order by Account";
					$accountsQuery = "SELECT *  FROM " . PROD_DB_NAME . ".admin_accounts ORDER BY Broker, Account ASC LIMIT 50000";
					$accountResults = $mysqliConn->query($accountsQuery);

					$output['OutputType'] = "Data";
					$output['addr'] = $_SERVER["REMOTE_ADDR"];
					$output["Data"] = array();

					$output['Query'] = $accountsQuery;

					if ($accountResults != NULL && $accountResults->num_rows > 0) {

						while ($AccountsRow = $accountResults->fetch_assoc())
						{
							try {
								if (isset($AccountsRow['Name'])) $AccountsRow['Name'] = utf8_encode($AccountsRow['Name']);
								array_push($output["Data"], $AccountsRow);							
							} catch (Exception $ex) {}
						}	
					}

					break;
				
				case "GetUsers":

					$UserQuery = "select * from (select Users.ID, Users.user_login, Users.display_name, UserMeta.meta_value from " . PROD_WP_NAME . ".wp_users Users join " . PROD_WP_NAME . ".wp_usermeta UserMeta on Users.ID = UserMeta.user_id where UserMeta.meta_key='wp_capabilities') as AllUsers left join " . PROD_DB_NAME . ".user_access_control on AllUsers.user_login = user_access_control.Username limit 50000";

					$UserResults = $mysqliConn->query($UserQuery);

					$output['OutputType'] = "Data";
					$output['Query'] = $UserQuery;
					$output["Data"] = array();

					if ($UserResults != NULL && $UserResults->num_rows > 0) {
						while ($UserRow = $UserResults->fetch_assoc())
						{
							try {
								if (isset($UserRow['display_name'])) $UserRow['display_name'] = utf8_encode($UserRow['display_name']);
								array_push($output["Data"], $UserRow);	
							}
							catch (Exception $ex) {}
						}
					}

					//$UserResults->free();

					break;
				
				case "AddAccountToUser":
					$Broker = $mysqliConn->real_escape_string( trim($_POST['Broker']) );
					$Accounts = $mysqliConn->real_escape_string( trim($_POST['Account']) );
					$Username = $mysqliConn->real_escape_string( trim($_POST['Username']) );

					if ($Broker != "" && $Accounts != "" && $Username != "") {					
						
						//$AddQuery = "UPDATE analyzer_data.user_access_control SET $Broker = '$Accounts' WHERE Username = '$Username'";
						$AddQuery = "INSERT INTO " . PROD_DB_NAME . ".user_access_control (Username, CanAccessRTA, CanAccessHTA, CommissionsEnabled, $Broker) VALUES ('$Username', 1, 1, 1, '$Accounts') ON DUPLICATE KEY UPDATE $Broker = '$Accounts'";


						$UserResults = $mysqliConn->real_query($AddQuery);
						
						if ($UserResults === TRUE){
							// Successful edit
							$output["OutputType"] = "Data";
							$output["Data"] = "OK";
						} else {
							// Unsuccesful update
							$output["OutputType"] = "ERROR";
							$output["Data"] = $mysqliConn->error;
						}
					} else {
						$output['OutputType'] = "ERROR";
						$output['Data'] = "Post metrics not for Operation 'AddAccountToUser' not correctly set.";
					}


					break;
				
				case "DeleteAccountFromUser":
					$Broker = $mysqliConn->real_escape_string( trim($_POST['Broker']) );
					$Accounts = $mysqliConn->real_escape_string( trim($_POST['Account']) );
					$Username = $mysqliConn->real_escape_string( trim($_POST['Username']) );

					if ($Broker != "" && $Username != "") {					
						
						$AddQuery = "UPDATE " . PROD_DB_NAME . ".user_access_control SET $Broker = '$Accounts' WHERE Username = '$Username'";

						$UserResults = $mysqliConn->real_query($AddQuery);
						
						if ($UserResults === TRUE){
							// Successful edit
							$output["OutputType"] = "Data";
							$output["Data"] = "OK";
						} else {
							// Unsuccesful update
							$output["OutputType"] = "ERROR";
							$output["Data"] = $mysqliConn->error;
						}
					} else {
						$output['OutputType'] = "ERROR";
						$output['Data'] = "Post metrics not for Operation 'AddAccountToUser' not correctly set.";
					}


					break;

				case "EditCheckboxPermissions":
					$Type = $mysqliConn->real_escape_string( trim($_POST['Type']) );
					$Value = $mysqliConn->real_escape_string( trim($_POST['Value']) );
					$Username = $mysqliConn->real_escape_string( trim($_POST['Username']) );


					if ($Type != "" && $Value != "" && $Username != "") {					

						$UpdateQuery = "UPDATE " . PROD_DB_NAME . ".user_access_control SET $Type = $Value WHERE Username = '$Username'";

						$UserResults = $mysqliConn->real_query($UpdateQuery);
						
						if ($UserResults === TRUE) {
							// Successful edit
							$output["OutputType"] = "Data";
							$output["Data"] = "OK";
						} else {
							// Unsuccesful update
							$output["OutputType"] = "ERROR";
							$output["Data"] = $mysqliConn->error;
						}
					} else {
						$output['OutputType'] = "ERROR";
						$output['Data'] = "Post metrics not for Operation 'EditCheckboxPermissions' not correctly set.";
					}

					break;

				case "FindDoublyAssignedAccounts":
					$UserQuery = "select * from " . PROD_DB_NAME . ".user_access_control limit 5000";

					$UserResults = $mysqliConn->query($UserQuery);

					// key = account name, value = array of usernames assigned to that account
					$accoutsAssignedArray = array();

					if ($UserResults != NULL && $UserResults->num_rows > 0) {
						while ($UserRow = $UserResults->fetch_assoc())
						{
							$username = $UserRow["Username"];
							$arrayOfAccountsAssignedToUser = array_merge(explode(",", $UserRow["GAIN_LIVE"]), explode(",", $UserRow["GAIN_DEMO"]), explode(",", $UserRow["CQG_LIVE"]), explode(",", $UserRow["CQG_DEMO"]));

							foreach ($arrayOfAccountsAssignedToUser as $acct) {
								if (trim($acct) != "") {
									if (isset( $accoutsAssignedArray[$acct] )) {
										// Is the account set as key under the account array?
										array_push($accoutsAssignedArray[$acct], $username);
									} else {
										$accoutsAssignedArray[$acct] = array($username);
									}								
								}
							}
						}

						$doubleAssignedAccounts = array();
						foreach ($accoutsAssignedArray as $key=>$val) {
							// key = account, $val = array of usernames
							if (count($val) > 1) {
								// there are more than 1 username assigned to this account
								array_push($doubleAssignedAccounts, array(
										"Account"	=> $key,
										"Usernames" => $val
									));
							}
						}

						$output['OutputType'] = "Data";
						$output['Data'] = json_encode($doubleAssignedAccounts);

					}
					break;

				case "ManuallyRefreshPusherDataFromDB":
					$xmlResult = file_get_contents("http://webhook.stage5trading.com:1234/RefreshUserPermissions");
					$output['OutputType'] = "Data";
					$output['Data'] = $xmlResult;
					break;

				case "SetDEMOAccountToTempSetting":
					if (isset($_POST["Account"]) && isset($_POST["IsTemp"])) {
						$Account = trim($_POST["Account"]);
						$IsTemp = trim($_POST["IsTemp"]);

						if ($Account != "") {
							
							// If the Account is set
							$UpdateQuery = "UPDATE " . PROD_DB_NAME . ".admin_accounts SET IsTemp=$IsTemp WHERE Account='$Account'";

							$QueryResults = $mysqliConn->real_query($UpdateQuery);
							
							if ($QueryResults === TRUE) {
								// Successful edit
								$output["OutputType"] = "Data";
								$output["Data"] = "OK";
							} else {
								// Unsuccesful update
								$output["OutputType"] = "ERROR";
								$output["Data"] = $mysqliConn->error;
							}

						} else {

							// Account incorrectly set
							$output["OutputType"] = "ERROR";
							$output["Data"] = "Setting DEMO account failed.  Account name was null or empty.";
						}

					} else {				
						$output['OutputType'] = "ERROR";
						$output['Data'] = "Post metrics Operation $Operation not correctly set.";
					}
					break;

				case "ArchiveAccount": 
					if (isset($_POST["Broker"]) && isset($_POST["Account"])) {
						$Broker = trim($_POST["Broker"]);
						$Account = $mysqliConn->real_escape_string(trim($_POST["Account"]));

						// Check is broker name is correct
						if (in_array($Broker, array("GAIN_LIVE", "GAIN_DEMO", "CQG_LIVE", "CQG_DEMO"))) {

							$insertAccountIntoArchiveAccountsTable_Query = "INSERT INTO " . PROD_DB_NAME . ".admin_archive_accounts (Broker, Account, OldAccountHolder, NewAccountHolder, DeleteOldAccountFromAccountsTable, MethodOfDesignation) (SELECT Broker, Account, NAME AS OldAccountHolder, '' AS NewAccountHolder, 1 AS DeleteOldAccountFromAccountsTable, 'MANUAL' as MethodOfDesignation FROM " . PROD_DB_NAME . ".admin_accounts WHERE Broker='$Broker' AND Account='$Account')";
							$insertAccountIntoArchiveAccountsTable_Result = $mysqliConn->query($insertAccountIntoArchiveAccountsTable_Query);

							if ($insertAccountIntoArchiveAccountsTable_Result === TRUE) {

								$output['OutputType'] = "Data";
								$output['Data'] = "Account ($Account) successfully marked for archiving.";

							} else {

								$output['OutputType'] = "ERROR";
								$output['Query'] = $insertAccountIntoArchiveAccountsTable_Query;
								$output['Data'] = "Broker: $Broker Account: $Account could not be inserted into the archive accounts.  Message: " . json_encode($mysqliConn->error);

							}

						} else {

							$output['OutputType'] = "ERROR";
							$output['Data'] = "Incorrect Broker set (Broker = '$Broker')";
						}

					} else {

						$output['OutputType'] = "ERROR";
						$output['Data'] = "Post metrics not for Operation not correctly set. ($Operation)";
					}
					break;

				default:
					$output['OutputType'] = "ERROR";
					$output['Data'] = "Post metrics not for Operation not correctly set. ($Operation)";
					break;

			}
		}

		try {
			// try to close the connection
			$mysqliConn->close();	

		} catch (Exception $ex) {
			// If you can't, that's OK, don't do anything
		}
		
	} else {
		$output['OutputType'] = "ERROR";
		$output['Data'] = "Invalid operation metric set.";
	}
	
} else {

	$output['OutputType'] = "ERROR";
	$output['Data'] = "Post metrics not correctly set.";

}

// Code returns here
echo json_encode($output);

return;
?>