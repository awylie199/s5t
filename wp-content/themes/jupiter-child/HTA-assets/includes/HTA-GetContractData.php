<?php 
$output = array();

require ("Constants.php");

if (isset($_POST) && isset($_POST['Broker'])  && isset($_POST['Account']) && isset($_POST['StartDate']) && isset($_POST['EndDate'])) {

	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
	
	if ($mysqliConn->connect_errno) {
		// Error in connection
		$output['OutputType'] = "ERROR";
		$output['Data'] = $mysqliConn->connect_error;	
	} else {
		// Connected
		$output['OutputType'] = "DATA";
			
		$Broker = strtolower( $mysqliConn->real_escape_string($_POST['Broker']) );
		$Account = $mysqliConn->real_escape_string($_POST['Account']);

		
		$StartDate = $_POST['StartDate'];
		$EndDate = $_POST['EndDate'];
		
		$query = "SELECT DISTINCT(Contract) FROM fills_" . $Broker . " WHERE Account='$Account' AND TimestampUTC <= '$EndDate' AND TimestampUTC >= '$StartDate' AND TypeOfTrade='O' ORDER BY Contract ASC";
		$results = $mysqliConn->query($query);
		
		$dataArray = array();
		if ($results != NULL && $results->num_rows > 0) {
			while ($row = $results->fetch_assoc()) {
				array_push($dataArray, $row);
			}
			$output['Data'] = $dataArray;
			$results->free();				
		} else {
			$output['Data'] = "";
		}
			
		$mysqliConn->close();

	}
} else {
	$output['OutputType'] = "ERROR";
	$output['Data'] = "Post metrics not correctly set.";
}

echo json_encode($output);
?>