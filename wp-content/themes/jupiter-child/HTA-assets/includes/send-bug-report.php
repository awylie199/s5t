<?php
header("Content-type: application/json");
date_default_timezone_set("America/Chicago");

$returnArray = array();
$dateTime = date('Y-m-d H:i:s');
$emailSubject = "HTA Bug Report ($dateTime)";

if (isset($_POST) && isset($_POST['username']) && isset($_POST['reportbody']) && isset($_POST['account']) && isset($_POST['contracts']) && isset($_POST['daterange'])) {
	$emailBodyStr = strip_tags(trim($_POST['reportbody']));
	$username = strip_tags(trim($_POST['username']));
	$account = trim($_POST['account']);
	$contracts = trim( $_POST['contracts'] );
	$dateRange = trim( $_POST['daterange'] );
	$replyToAddress = "";
	$replyToName = "";

	$emailBody = "'$username' @ $dateTime<br />Account: $account, Contract(s): $contracts<br />Selected Date Range: $dateRange<br />";


	require_once ('Constants.php');
	include_once ('PHPMailer/PHPMailerAutoload.php');

	// try to get the user's email from the WP database
	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_ADMIN, PROD_DB_PASSWORD_ADMIN);

	if ($mysqliConn->connect_errno) {
		// Error in connection
		// do nothing

	} else {
		// Connected
		$emailQuery = "select user_email, display_name from " . PROD_WP_NAME . ".wp_users where user_login='$username'";
		$emailResults = $mysqliConn->query($emailQuery);

		if ($emailResults != NULL && $emailResults->num_rows > 0) {
			$emailObj = $emailResults->fetch_assoc();
			$replyToAddress = $emailObj['user_email'];
			$replyToName = $emailObj['display_name'];

			$emailBody .= "ReplyTo: '$replyToName ($replyToAddress)'<br />";
		}
	}

	// close DB connection
	$mysqliConn->close();
	
	$emailBody .= "<br /><br />$emailBodyStr";

    // Email to be sent to admin
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->Host = EMAIL_HOST;
	$mail->CharSet = 'UTF-8';		
	$mail->SMTPAuth = SMTP_AUTH;
	$mail->Username = EMAIL_USERNAME;
	$mail->Password = EMAIL_PASSWORD;
	$mail->SMTPSecure = EMAIL_SMTPS_SECURE;
	$mail->Port = EMAIL_PORT;
	//$mail->SMTPDebug  = 2;
	$mail->SetFrom (EMAIL_USERNAME, 'Stage 5 Tech Support');
	if ($replyToAddress != "")
		$mail->AddReplyTo($replyToAddress, $replyToName);
	
	$CommaSeparatedListOfEmails = explode(',', preg_replace('/\s+/', '_', EMAIL_SENT_TO_LIST));
	foreach ($CommaSeparatedListOfEmails as $key=>$email)
		$mail->addAddress($email);
		
	$mail->isHTML(true);
	$mail->Subject = $emailSubject;
	$mail->Body    = $emailBody;
	$mail->AltBody = $emailBody;

    // Email copy to be sent to user who submitted the bug report
    $clientMail = new PHPMailer;
    $clientMail->isSMTP();
    $clientMail->Host = EMAIL_HOST;
    $clientMail->CharSet = 'UTF-8';
    $clientMail->SMTPAuth = SMTP_AUTH;
    $clientMail->Username = EMAIL_USERNAME;
    $clientMail->Password = EMAIL_PASSWORD;
    $clientMail->SMTPSecure = EMAIL_SMTPS_SECURE;
    $clientMail->Port = EMAIL_PORT;
    $clientMail->SetFrom (EMAIL_USERNAME, 'Stage 5 Tech Support');
    if ($replyToAddress != "")
        $clientMail->AddReplyTo("no-reply@s5trading.com");

    $clientMail->addAddress($replyToAddress, $replyToName);

    $clientMail->isHTML(true);
    $clientMail->Subject = "S5 Trade Analyzer Bug Reporter";

    $emailBody = "Thank you for submitting a bug report. We've received it and are looking into the issue." .
        "<br />If you have questions please sent it to <a href='mailto:bugs@s5trading.com' target='_top'>bugs@s5trading.com</a>." .
        "<br />Here's a copy of what you sent us:<br /><br />" . $emailBody;
    $clientMail->Body    = $emailBody;
    $clientMail->AltBody = $emailBody;

    $AdminMailSentSuccessfully = $mail->send();
    $ClientEmailSentSuccessfully = $clientMail->send();

    if ($AdminMailSentSuccessfully && $ClientEmailSentSuccessfully) {
        $returnArray = array("Status"=>"OK");
    } else {
        $AdminEmailErrorStr = (!$AdminMailSentSuccessfully ? ("Error sending bug report to admin.  Message: " . $mail->ErrorInfo) : "");
        $ClientEmailErrorStr = (!$ClientEmailSentSuccessfully ? ("Error sending bug report confirmation to client.  Message: " . $clientMail->ErrorInfo) : "");
        $ErrorOutput = $AdminEmailErrorStr . $ClientEmailErrorStr;

        $returnArray = array("Status"=>"ERROR", "Data"=> $ErrorOutput);
    }


//	if(!$mail->send()) {
//		$returnArray = array("Status"=>"ERROR", "Data"=>"Error sending email.  Message: " . $mail->ErrorInfo);
//	} else {
//		$returnArray = array("Status"=>"OK");
//	}
} else {
	$returnArray['Status'] = "ERROR";
	$returnArray['Data'] = "Post metrics not properly set.";
}

echo json_encode($returnArray);
return;
?>