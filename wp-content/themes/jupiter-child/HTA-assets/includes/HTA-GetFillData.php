<?php

// get the session data
session_start();
$Username = $_SESSION["S5-USERNAME"];

date_default_timezone_set('UTC');

$output = array();

require ("Constants.php");

if (isset($_POST) && isset($_POST["Username"]) && isset($_POST['Broker']) && isset($_POST['Account']) && isset($_POST['Contracts']) && isset($_POST['StartDate']) && isset($_POST['EndDate'])) {

	// You have to check the username's access before 
	if ($Username != $_POST["Username"]) {
		$output['OutputType'] = "ERROR";
		$output['Data'] = "Username authentication error.";	
		
		echo json_encode($output);
		return;
	}

	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
	
	if ($mysqliConn->connect_errno) {
		// Error in connection
		$output['OutputType'] = "ERROR";
		$output['Data'] = $mysqliConn->connect_error;	
	} else {
		// Connected
		
		$Broker = strtolower( $mysqliConn->real_escape_string($_POST['Broker']) );
		$Account = $mysqliConn->real_escape_string($_POST['Account']);
		

		if ($Username != "ta-admin") {
			$Broker_CAPITAL = strtoupper($Broker);
			$verify_query = "SELECT * FROM user_access_control WHERE Username='$Username' AND $Broker_CAPITAL LIKE '%$Account%'";
			$ver_res = $mysqliConn->query($verify_query);

			if (!($ver_res && $ver_res->num_rows > 0)) {
				// if the user did not return as part of access-control
				$output['OutputType'] = "ERROR";
				$output['Data'] = "Username authentication error, you do not have access to account $Account.";	
				
				echo json_encode($output);
				return;
			}			
		}
		
		$StartDate = $_POST['StartDate'];
		$EndDate = $_POST['EndDate'];
		$ListOfContracts = $_POST['Contracts'];
		
		$output['OutputType'] = "DATA";
		
		// Iterate DB and get all fills within a certain timeframe for all contracts selected
		$AllDataArray = array();
		$DistinctContracts = array();
		$AllFills = array();
		foreach ($ListOfContracts as $key=>$Contract) {
			$FillsArray = array();
			
			// Find the opening fill after the Start Date
			$query =  "SELECT FillID FROM fills_" . $Broker . " WHERE Account='$Account' AND Contract='$Contract' AND TimestampUTC <= '$EndDate' AND TimestampUTC >= '$StartDate' AND (TypeOfTrade='O' || TypeOfTrade='S') ORDER BY FillID ASC LIMIT 1";
			$results = $mysqliConn->query($query);
			if ($results != NULL && $results->num_rows > 0) {
				$StartingFillIDOBJ = $results->fetch_assoc();
				$results->free();
				
				// If an opening fill is found, then try to find the Closing fill (if it exists)
				$queryClosingFill = "SELECT FillID FROM fills_" . $Broker . " WHERE Account='$Account' AND Contract='$Contract' AND TimestampUTC <= '$EndDate' AND TimestampUTC >= '$StartDate' AND (TypeOfTrade='C' || TypeOfTrade='S') ORDER BY FillID DESC LIMIT 1";
				$resultsClosingFill = $mysqliConn->query($queryClosingFill);
								if ($resultsClosingFill != NULL && $resultsClosingFill->num_rows > 0) {
					$EndingFillFillIDOBJ = $resultsClosingFill->fetch_assoc();
					$resultsClosingFill->free();
					
					$StartingFill = $StartingFillIDOBJ['FillID'];
					$EndingFill = $EndingFillFillIDOBJ['FillID'];
										
					// If an opening and closing (CLOSE or SPLIT) fill exists, find all the fills inclusive and inbetween the opening and closing fill
					if (intval($StartingFill) < intval($EndingFill)) {
						array_push($DistinctContracts, $Contract);
						
						$GetFillsQuery = "SELECT FillID, OrderID, Contract, Side, Quant, TimestampUTC, Price, ContractsHeld, TypeOfTrade, HighSince, LowSince FROM fills_" . $Broker . " WHERE Account='$Account' AND Contract='$Contract' AND FillID >= '$StartingFill' AND FillID <= '$EndingFill' ORDER BY FillID ASC";
												
						$FillResults = $mysqliConn->query($GetFillsQuery);
						if ($FillResults != NULL && $FillResults->num_rows > 0) {
							while ($fill = $FillResults->fetch_assoc()) {
								$fill['FillID'] = intval($fill['FillID']);
								$fill['Quant'] = intval($fill['Quant']);
								$fill['ContractsHeld'] = intval($fill['ContractsHeld']);								
								$fill['Price'] = floatval($fill['Price']);
								$fill['HighSince'] = floatval($fill['HighSince']);
								$fill['LowSince'] = floatval($fill['LowSince']);
								
								array_push($FillsArray, $fill);
								array_push($AllFills, $fill);
							}
							$FillResults->free();
							
							//$output["Fills-$Contract"] = $FillsArray;
							
							// From fills create Trades
							$TradesArray = FillsToTrades($FillsArray);
													
							$AllDataArray = array_merge($AllDataArray, $TradesArray);
						}
					}
				}
			}			
		}
		
		// Operations
		usort($AllDataArray, "SortTradesByFirstFillASC");			// Sort the trades by opening trade date/time ASC
		$output['Data'] = $AllDataArray;							// Assign to Data
		
		

		// This section gets the contract details from the "admin_contracts" table and sends to user (info such as TickSize, ContractSize, etc.)
		$ContractDetailsArray = array();

		// Determine Distinct Base Contracts
		$DistinctBASEContracts = array();

		if (count($DistinctContracts) > 0) {

			foreach ($DistinctContracts as $cont) {
				$baseContract = substr($cont, 0, strlen($cont)-2);
				if (!in_array($baseContract, $DistinctBASEContracts)) {
					// if the base contract does not exist in $dDistinctBASEContracts then add it
					array_push($DistinctBASEContracts, $baseContract);
				}
			}

			if (count($DistinctBASEContracts) > 0) {
				$ContractDetailsQuery = "SELECT Contract, BaseContract, TickSize, ContractSize FROM admin_contracts WHERE BaseContract in ('" . implode("', '", $DistinctBASEContracts) . "')";
						
				$ContractQueryResults = $mysqliConn->query($ContractDetailsQuery);
				if ($ContractQueryResults != NULL && $ContractQueryResults->num_rows > 0) {
					while ($row = $ContractQueryResults->fetch_assoc()) {
						if ($row['TickSize'] != NULL) $row['TickSize'] = floatval($row['TickSize']);
						if ($row['ContractSize'] != NULL) $row['ContractSize'] = floatval($row['ContractSize']);
						array_push($ContractDetailsArray, $row);
					}
				}
				$output['ContractDetails'] = $ContractDetailsArray;
				//$output['query']		   = $ContractDetailsQuery;
			}
		}
		
		// Close DB Connection
		$mysqliConn->close();
		
		
		// Calculate day metrics from all fills, sort fills by date ascending
		usort($AllFills, "SoftFillsByTimestampUTCASC");

		// Trade Metrics
		$FirstFillDateTimeUTC = NULL;		// for date range
		$LastFillDateTimeUTC = NULL;		// for date range
		
		$DaysTradedArray = array();		// a temp array which holds arrays of array("Date"=>YY-MM-DD, array of products traded)  -> this will also feed "Max Products Traded Per Day" and "Days Traded"
				
		if (count($AllFills) > 0) {
			foreach ($AllFills as $key=>$fill) {
				$fillTimestampUTC = DateTime::createFromFormat("Y-m-d H:i:s e", ($fill['TimestampUTC']) . " UTC");
				$fillTimestampEST = DateTime::createFromFormat("Y-m-d H:i:s e", ($fill['TimestampUTC']) . " UTC")->setTimezone(new DateTimeZone('America/New_York'));
				$fillDate = $fillTimestampEST->format('Y-m-d');
				
				// Determine first / last fill dates
				if ($FirstFillDateTimeUTC == NULL) {
					$FirstFillDateTimeUTC = $fillTimestampUTC;
					$LastFillDateTimeUTC = $fillTimestampUTC;
				} else {
					if ($fillTimestampUTC > $LastFillDateTimeUTC) $LastFillDateTimeUTC = $fillTimestampUTC;
					if ($fillTimestampUTC < $FirstFillDateTimeUTC) $FirstFillDateTimeUTC = $fillTimestampUTC;
				}
				
				if (count($DaysTradedArray) == 0) {
					array_push( $DaysTradedArray, array("Date"=>$fillDate, "Products"=>array($fill['Contract'])) );	
				} else {
					$LastDateTradedObject = $DaysTradedArray[ count($DaysTradedArray)-1 ];
					if ($LastDateTradedObject['Date'] == $fillDate)
						array_push($LastDateTradedObject['Products'], $fill['Contract']);
					else
						array_push($DaysTradedArray, array("Date"=>$fillDate, "Products"=>array($fill['Contract'])) );							
				}
			}
			
			foreach ($DaysTradedArray as $array) {
				$array['Products'] = array_unique($array['Products']);
			}
			
			$output['FirstFillTimestampUTC'] = $FirstFillDateTimeUTC->format("Y-m-d H:i:s");
			$output['LastFillTimestampUTC'] = $LastFillDateTimeUTC->format("Y-m-d H:i:s");
			$output['DaysAndProductsTradedArray'] = $DaysTradedArray;
		}
		
		
	}
} else {

	$output['OutputType'] = "ERROR";
	$output['Data'] = "Post metrics not correctly set.";

}

// Code returns here
echo json_encode($output);

exit();
/*
** No more code executed beyond this point
*/


// Functions
function SortTradesByFirstFillASC($TradeA, $TradeB) {
	// Sort trades by date of the first fill ASC
	if (count($TradeA) > 0 && count($TradeB) > 0) {
		return strtotime($TradeA[0]['TimestampUTC']) > strtotime($TradeB[0]['TimestampUTC']);
	} else {
		return 0;
	}
}

function SoftFillsByTimestampUTCASC ($FillA, $FillB) {
	if ($FillA != NULL && $FillB != NULL)
		return strtotime($FillA['TimestampUTC']) > strtotime($FillB['TimestampUTC']);
	else 
		return 0;
}

function FillsToTrades ($ArrayOfFills) {	
	// Create array of trades from array of fills
	$outputArray = array();
	
	// Iterate fills to create trades
	$AllTrades = array();
	$singleTrade = array();
	$position = 0;
	$count = count($ArrayOfFills);
	foreach ($ArrayOfFills as $index=>$fill) {
		$TypeOfTrade = strtoupper($fill['TypeOfTrade']);
		$Quant = ($fill['Side'] == "Buy") ? intval($fill['Quant']) : -intval($fill['Quant']);
		$ContractsHeld = intval($fill['ContractsHeld']);
		
		if ($TypeOfTrade == 'O') {
			// Opening trade
			$singleTrade = array($fill);	
		} else if ($TypeOfTrade == '-') {
			// Continuation Trade
			array_push($singleTrade, $fill);
		} else if ($TypeOfTrade == 'C') {
			// Closing trade -> flat
			array_push($singleTrade, $fill);
			array_push($AllTrades, $singleTrade);
		} else if ($TypeOfTrade == 'S') {
			// Closing trade -> split trade flips the position
			
			// Find out if the SPLIT is the first trade, last trade, or in the middle.
			// If first, the initial part of the split should be ignored
			// If last, the last part of the split should be ignored
			
			if ($index == 0) {
				// SPLIT is first trade
				$fill['Quant'] = abs($ContractsHeld);
				$singleTrade = array($fill);
				array_push($singleTrade, $fill);
												
			} else if ($index >= $count-1) {
				// SPLIT is last trade
				$oldPosition = $ContractsHeld - $Quant;
				$fill['Quant'] = abs($oldPosition);
				array_push($singleTrade, $fill);
				array_push($AllTrades, $singleTrade);			
				
			} else {
				// SPLIT is in the middle
				$oldPosition = $ContractsHeld - $Quant;
				$fill['Quant'] = abs($oldPosition);
				array_push($singleTrade, $fill);
				array_push($AllTrades, $singleTrade);
				
				$fill['Quant'] = abs($ContractsHeld);
				$singleTrade = array($fill);
			}
			
		} else {
			// Errors
		}
	}
	
	return $AllTrades;
}


?>