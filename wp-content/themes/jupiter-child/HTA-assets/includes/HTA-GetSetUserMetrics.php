<?php 
date_default_timezone_set('UTC');

$output = array();

require ("Constants.php");

if (isset($_POST) && isset($_POST['Operation'])) {
	if ($_POST['Operation'] == "GET") {
		// Get user set trade metrics
		if (isset($_POST['Username']) && isset($_POST['FillIDArray'])) {
		
			$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
			
			if ($mysqliConn->connect_errno) {
				// Error in connection
				$output['OutputType'] = "ERROR";
				$output['Data'] = $mysqliConn->connect_error;	
			} else {
				// Connected
						
				$Username = $mysqliConn->real_escape_string($_POST['Username']);
				$FillIDArray = $_POST['FillIDArray'];
				
				if (is_array($FillIDArray)) {	
					$StringOfFillIDs = implode(",", $FillIDArray);			
					$query = "SELECT * FROM user_trade_metrics WHERE FillID in ($StringOfFillIDs) Order By Contract ASC";
				
					$outputArray = array();
					$results = $mysqliConn->query($query);
					if ($results != NULL && $results->num_rows > 0) {
						while ($row = $results->fetch_assoc()) {
							array_push($outputArray,
								array(
									"FillID" => $row['FillID'],
									"Contract" => $row['Contract'],
									"TradeGrade" => $row['TradeGrade'],
									"TradeType" => $row['TradeType'],
									"TradeError" => $row['TradeError']
								)
							);


							/* this only allows the trade metrics to show if the username looking at the account is the user that set the metric					
							if ($row['Username'] == $Username) {
								array_push($outputArray,
									array(
										"FillID" => $row['FillID'],
										"Contract" => $row['Contract'],
										"TradeGrade" => $row['TradeGrade'],
										"TradeType" => $row['TradeType'],
										"TradeEmoticon" => $row['TradeEmoticon']
									)
								);
							}
							*/
						}
						
						$output['OutputType'] = "Data";
						$output['Data'] = $outputArray;
						
					}
					$results->free();
				} else {
					$output['OutputType'] = "ERROR";
					$output['Data'] = "FillIDArray was not in array format.";	
				}
			}
			
			$mysqliConn->close();
		} else {			
			$output['OutputType'] = "ERROR";
			$output['Data'] = "GET operation post metrics not correctly set.";	
		}
	} else if ($_POST['Operation'] == "SET") {
		// Set operation
		if (isset($_POST['Username']) && isset($_POST['Account'])  && isset($_POST['Contract']) && isset($_POST['FillID']) && isset($_POST['Metric']) && isset($_POST['Value'])) {
		
			$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
			
			if ($mysqliConn->connect_errno) {
				// Error in connection
				$output['OutputType'] = "ERROR";
				$output['Data'] = $mysqliConn->connect_error;	
			} else {
				// Connected

				$Username =  $mysqliConn->real_escape_string($_POST['Username']);
				$Account = strtoupper( $mysqliConn->real_escape_string($_POST['Account']) );
				$Contract = strtoupper( $mysqliConn->real_escape_string($_POST['Contract']) );
				$FillID = $mysqliConn->real_escape_string($_POST['FillID']);
				$Metric = $mysqliConn->real_escape_string($_POST['Metric']);
				$Value = $mysqliConn->real_escape_string($_POST['Value']);


				switch ($Metric) {
				    case "TradeGrade":

						$query = "INSERT INTO user_trade_metrics (FillID, Username, Account, Contract, TradeGrade) VALUES ('$FillID', '$Username', '$Account', '$Contract', '$Value') ON DUPLICATE KEY UPDATE TradeGrade = VALUES(TradeGrade)";

				        break;

				    case "TradeType":
					
					    $query = "INSERT INTO user_trade_metrics (FillID, Username, Account, Contract, TradeType) VALUES ('$FillID', '$Username', '$Account', '$Contract', '$Value') ON DUPLICATE KEY UPDATE TradeType = VALUES(TradeType)";

				        break;

				    case "TradeError":
					
					    $query = "INSERT INTO user_trade_metrics (FillID, Username, Account, Contract, TradeError) VALUES ('$FillID', '$Username', '$Account', '$Contract', '$Value') ON DUPLICATE KEY UPDATE TradeError = VALUES(TradeError)";

				        break;
				}

				//$output['query'] = $query;

				$results = $mysqliConn->real_query($query);
				
				if (!$results) {
					$output['OutputType'] = "ERROR";
					$output['Data'] = "ERROR updating $Metric to DB.  Please contact admin.";
				} else {
					$output['OutputType'] = "DATA";
					$output['Data'] = "OK";
				}
				
			}
			
			$mysqliConn->close();
			
		} else {
			$output['OutputType'] = "ERROR";
			$output['Data'] = "SET operation post metrics not correctly set.";				
		}

	} else if ($_POST['Operation'] == "SET_SETTINGS") {
		try {
			if (isset($_POST['Username']) && isset($_POST['MetricName']) && isset($_POST['MetricValue']) ) {
				$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
				
				if ($mysqliConn->connect_errno) {
					// Error in connection
					$output['OutputType'] = "ERROR";
					$output['Data'] = $mysqliConn->connect_error;	
				} else {
					// Connected
					$Username = $mysqliConn->real_escape_string($_POST["Username"]);
					$metricName = $mysqliConn->real_escape_string($_POST["MetricName"]);
					$metricValue = $mysqliConn->real_escape_string($_POST["MetricValue"]);

					$set_query = "INSERT INTO user_settings (Username, $metricName) VALUES ('$Username', '$metricValue') ON DUPLICATE KEY UPDATE $metricName='$metricValue'";



					// $output['OutputType'] = "DATA";
					// $output['Data'] = "					$set_query;";

					$set_results = $mysqliConn->real_query($set_query);

					if ($set_query) {
						$output['OutputType'] = "DATA";
						$output['Data'] = "OK";
					} else {
						$output['OutputType'] = "ERROR";
						$output['Data'] = "Could not save settings due to an error on the database side.  Msg: " . $set_query;
					}
				}
			} else {
				$output['OutputType'] = "ERROR";
				$output['Data'] = "Incorrect metrics set for settings operation.";
			}
		} catch (Exception $ex) {
			$output['OutputType'] = "ERROR";
			$output['Data'] = "Error occured during setting save function.1";
		}
	} else {
		$output['OutputType'] = "ERROR";
		$output['Data'] = "'Operation' tag was set improperly.";
	}
} else {
	$output['OutputType'] = "ERROR";
	$output['Data'] = "Initial post metrics not correctly set.";
}

// Code returns here
echo json_encode($output);

exit();


?>