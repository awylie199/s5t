<?php 
/*
Template Name: ADMIN - User Permissions
*
*	This website created by Reza Khanahmadi
*	Trading Algo X
*	Created on June 16, 2016
*	Updated on: June 16, 2016
*/ 

function PHPErrorHandler ($errno, $errmsg, $errfile, $errline)
{
	echo "<error>PHP ERROR OCCURED<br /><br />Error Number: $errno<br />Line: $errline<br />Message: $errmsg</error>";
	echo "<style type='text/css'>";
	echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
	echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; padding: 10px 0px; border: 2px solid red; background-color: red; }";
	echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
	echo "</style>";
	echo "<a href='" . home_url() . "'>Redirect to Home</a>";
	exit();
}

// Shut off Error reporting and handle errors and Fatal errors internally via a message to the screen
error_reporting(0);
set_error_handler("PHPErrorHandler");

/*
$args = array(
	'sort_order' => 'asc',
	'sort_column' => 'post_title',
	'hierarchical' => 1,
	'exclude' => '',
	'include' => '',
	'meta_key' => '',
	'meta_value' => '',
	'authors' => '',
	'child_of' => 0,
	'parent' => -1,
	'exclude_tree' => '',
	'number' => '',
	'offset' => 0,
	'post_type' => 'page',
	'post_status' => 'publish'
); 
$pages = get_pages($args); 

$AdminLinksHTML = "";
$this_page_post_name = get_post()->post_name;
foreach ($pages as $page)
{
	$post_name = $page->post_name;
	$post_title = $page->post_title;
	if (strpos($post_name, "admin-") !== FALSE && $post_name != $this_page_post_name) {
		$AdminLinksHTML .= "<a href='" . get_template_directory_uri() . "/$post_name/' class='admin-link' title='$post_title'>$post_title</a>";
	}
}
if ($AdminLinksHTML != "") $AdminLinksHTML = "Other Admin Pages: " . $AdminLinksHTML;
*/

/* old WP login system 
if (!is_user_logged_in()) {
	// If user is not logged in redirect to login page
	wp_redirect( home_url() . '/login/' );
	exit();
} else if (! (current_user_can('administrator') || current_user_can('editor'))) {
	// This user is not ADMIN or EDITOR
	echo "Permission denied.  You are not an admin.";	
	exit();
}
*/

/* aMember login authenticator */
if (strpos($_SERVER['SERVER_NAME'], "localhost") === false) {
	    // production
    require_once '/home/s5trading/public_html/s5-members/library/Am/Lite.php';

} else {
    // localhost
    require_once  $_SERVER['DOCUMENT_ROOT'] . '/stage5/s5-members/library/Am/Lite.php';
}


if (! Am_Lite::getInstance()->isLoggedIn() ) {
    // If user is not logged in redirect to login page
    header("Location: " . Am_Lite::getInstance()->getLoginURL());
    exit();
}

$username = Am_Lite::getInstance()->getUsername();
$name = Am_Lite::getInstance()->getName();


// find out if the subscriptions include a product name called "ta_admin_access", if yes, then give access
$TheUserHasAccessToThis = false;

try {
	// try to iterate all the products this user has access to, then match that with the products Array to see what that subscription ID is called
	$ArrayOfProductsTheUserHasAccessTo = Am_Lite::getInstance()->getAccess();
	$ArrayOfAllProductsMappedToName = Am_Lite::getInstance()->getProducts();
	foreach ($ArrayOfProductsTheUserHasAccessTo as $productArray) {
		if ($ArrayOfAllProductsMappedToName [ $productArray['product_id'] ] == "ta_admin_access") {
			$TheUserHasAccessToThis = true;
		}
	}
} catch (Exception $ex) {}


if (!$TheUserHasAccessToThis) {
	echo "You do not have admin access. You will be directed in 5 seconds...";
	header("Location: " . Am_Lite::getInstance()->getLoginURL());
	exit();
}

//echo "Username: " . $username . "<br />";
//echo "We are perfoming critial maintenance on the site.  Please check back in a few hours.";
//exit();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stage 5 Trading - Real-Time Analyzer</title>
<base href="<?php echo get_stylesheet_directory_uri() . "/HTA-assets/" ; ?>" />

<script type="text/javascript">
	const username = "<?php echo $username; ?>";
</script>

<!-- CDNs -->
<script src="https://code.jquery.com/jquery-1.12.2.min.js" integrity="sha256-lZFHibXzMHo3GGeehn1hudTAP3Sc0uKXBXAzHX1sjtk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />


<link href='//fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>

<!-- Local JS & CSS -->
<script type="text/javascript" src="libraries/split-pane-management/split-pane.js"></script>
<link rel="stylesheet" href="libraries/split-pane-management/split-pane.css" />

<script type="text/javascript" src="libraries/sortable-table/tablesort.js"></script>
<script type="text/javascript" src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="js/NotificationClient.js"></script>

<link rel="stylesheet" href="libraries/sortable-table/css/tablesort.css" />
<link rel="stylesheet" href="css/UniversalElementStyles.css" />
<link rel="stylesheet" href="css/AdminStyles.css" />


<script type="text/javascript">

	$( init );
	function init() {
		// Split pane		
		$('div.split-pane').splitPane();
	}

	// Handler class
	var PermissionHandler = function () {
		this.Users = [];
		this.Accounts = {};
		this.CurrentEditableUser = "";
		this.SuperAdminUsername = "ta-admin";

		this.LoadUsers();
	}

	PermissionHandler.prototype.LoadUsers = function() {
		var ThisClass = this;		
		$.ajax({
			url: "includes/admin-permissions.php",
			method: "post",
			type: "post",
			data: { Operation: "GetUsers" },
			error: function (errData) {
				console.log(errData);
				NotyTopRight("Error processing script request. Message: " + errData.statusText, "error", 0);

				Handler.LoadAccounts();
			},
			success: function (data) {
				var parsedData = $.parseJSON(data);
				var LocalUsersArray = [];

				if (parsedData != null && parsedData.OutputType != null) {
					if (parsedData.OutputType == "Data") {
						$.each(parsedData.Data, function (i, user) {
							LocalUsersArray.push(user);
						});					
					}
				}

				ThisClass.Users = LocalUsersArray;
				ThisClass.DisplayUsers();

				Handler.LoadAccounts();
			}
		});
	};

	PermissionHandler.prototype.LoadAccounts = function() {
		//loading(true);
		var ThisClass = this;

		$.ajax({
			url: "includes/admin-permissions.php",
			method: "post",
			type: "post",
			data: { Operation: "GetAccounts" },
			error: function (errData) {
				console.log(errData);
				NotyTopRight("Error processing script request. Message: " + errData.statusText, "error", 0);
				loading(false);
			},
			success: function (data) {
				var parsedData = $.parseJSON(data);
				var LocalAccountsArray = {};

				var count = 0;
				if (parsedData != null && parsedData.OutputType != null) {
					if (parsedData.OutputType == "Data") {
						$.each(parsedData.Data, function (i, account) {
							//LocalAccountsArray.push(account);
							LocalAccountsArray[account.Account] = account;
							count++;
						});					
					}
				}

				NotyTopRight(count + " accounts loaded in total.", "alert", 5000);
				ThisClass.Accounts = LocalAccountsArray;
				ThisClass.DisplayAccounts();
			}
		});
	};

	PermissionHandler.prototype.DisplayUsers = function() {
		var UserHtmlBuilder = "<table id='users-table' width=100% class='table-sort table-sort-search'><thead><th>#</th><th class='left'>Username</th><th class='left'>Name</th><th class='center'>RTA<br />Access</th><th class='center'>HTA<br />Access</th><th class='center'>Commissions<br />Enabled</th><th class='left'>Linked Accounts</th><tbody>";

		var ThisClass = this;

		$.each(this.Users, function (i, user) {
			var UserID = user.user_login;

			var LinkedAccountsHTML = "",
				GAIN_LIVE_STR = $.trim(user.GAIN_LIVE).replace(' ', ''),
				GAIN_LIVE_ARRAY = [],
				GAIN_DEMO_STR = $.trim(user.GAIN_DEMO).replace(' ', ''),
				GAIN_DEMO_ARRAY = [],
				CQG_LIVE_STR = $.trim(user.CQG_LIVE).replace(' ', ''),
				CQG_LIVE_ARRAY = [];

			if (GAIN_LIVE_STR != "") GAIN_LIVE_ARRAY = GAIN_LIVE_STR.split(',');
			if (GAIN_DEMO_STR != "") GAIN_DEMO_ARRAY = GAIN_DEMO_STR.split(',');
			if (CQG_LIVE_STR != "") CQG_LIVE_ARRAY = CQG_LIVE_STR.split(',');

			user["GAIN_LIVE_ARRAY"] = GAIN_LIVE_ARRAY;
			user["GAIN_DEMO_ARRAY"] = GAIN_DEMO_ARRAY;
			user["CQG_LIVE_ARRAY"] = CQG_LIVE_ARRAY;

			if (UserID == ThisClass.SuperAdminUsername) {
				LinkedAccountsHTML = "<strong>ADMIN:</strong> You have access to all accounts.";
			} else {
				if (GAIN_LIVE_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>GAIN LIVE: </strong> " + GAIN_LIVE_ARRAY.join(", ") + "</span>";
				if (GAIN_DEMO_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>GAIN DEMO: </strong> " + GAIN_DEMO_ARRAY.join(", ") + "</span>";
				if (CQG_LIVE_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>CQG LIVE: </strong>  " + CQG_LIVE_ARRAY.join(", ") + "</span>";				
			}

			UserHtmlBuilder += "<tr id='" + UserID + "'>" + 
								"<td>" + (i + 1) + "</td>" +
								//"<td>" + UserRole + "</td>" +
								"<td><a title='Double click me to edit user in the edit box.' id='" + UserID + "' href='javascript: void(0);' class='username'>" + UserID + "</a></td>" + 
								"<td title='UserID: " + UserID + "'>" + user.display_name + (UserID == ThisClass.SuperAdminUsername ? "<img src='images/super-admin-icon.png'" : "") + "</td>" +
								"<td class='center'><input type='checkbox' class='user-checkbox' name='CanAccessRTA' id='" + user.user_login + "' " + (user.CanAccessRTA == "1" ? " Checked='Checked' " : "") + " /></td>" +
								"<td class='center'><input type='checkbox' class='user-checkbox' name='CanAccessHTA' id='" + user.user_login + "' " + (user.CanAccessHTA == "1" ? " Checked='Checked' " : "") + " /></td>" +
								"<td class='center'><input type='checkbox' class='user-checkbox' name='CommissionsEnabled' id='" + user.user_login + "' " + (user.CommissionsEnabled == "1" ? " Checked='Checked' " : "") + " /></td>" +

								"<td style='word-wrap: break-word; white-space: normal;' id='" + UserID + "'>" + LinkedAccountsHTML + "</td>" +
								"</tr>";
		});

		UserHtmlBuilder += "</tbody></table>";

		$("#users-container").html(UserHtmlBuilder);
	};

	PermissionHandler.prototype.DisplayAccounts = function() {
		loading(true);

		setTimeout(function () {

			/*
			 * Determine if the "allow archiving of Accounts" checkbox is checked, if yes, present the option to archive this account
			 */
			var Broker = $("#broker-combobox").val();
			var DisplayArchiveMetrics = $("#allow-archiving-accounts").length > 0 && $("#allow-archiving-accounts").is(":checked"),
				IsDemoAccount = Broker.includes("DEMO");

			console.log("Display DisplayArchiveMetrics", DisplayArchiveMetrics);

			var AccountsHtmlBuilder = "<table id='accounts-table' class='table-sort table-sort-search'><thead><th class='left'>Account #</th><th class='left'>Account Holder</th>"
									+ (DisplayArchiveMetrics? "<th class='left'>Archive</th>" : "")
									+ (IsDemoAccount? "<th class='left'>IsTemp</th>" : "")
									+ "<th class='left'>Established</th></thead><tbody>";
				
			$.each(Handler.Accounts, function (i, account) {

				if (account.Broker == Broker) {
					AccountsHtmlBuilder += "<tr><td><a href='javascript: void(0);' class='movable-account' id='" + account.Account + "' title='Double click me to add to user.'>" + account.Account + "</a></td><td>" + account.Name + "</td>" 

						+ (DisplayArchiveMetrics ? "<td><img src='images/archive-icon.png' id='" + account.Account + "' class='archive-icon' title='Archive account (" + account.Account + ") and its fills.' /></td>" : "")

						+ (IsDemoAccount ? "<td><input type='checkbox' class='tmp-account-checkbox' id='" + account.Account + "' " + (account.IsTemp == '1' ? 'checked' : '') + " /></td>" : "")
						+ "<td>" + moment.utc(account.Updated, "YYYY-MM-DD HH:mm:ss").format("MMM/DD/YY") 
						+ "</td></tr>";
				}

			});

			AccountsHtmlBuilder += "</tbody></table>";

			$("#accounts-selector").html(AccountsHtmlBuilder);
			$("table.table-sort,table.table-sort-search").tablesort();

			loading(false);

		}, 500);
	};

	PermissionHandler.prototype.UsernameToEdit = function (username) {
		var ThisClass = this;

		// check if the username selected is the SUPER ADMIN, if so, don't allow accounts to be added to it as it already has GOD MODE access
		if (username == this.SuperAdminUsername) {
			NotyTopRight("The user <strong>'" + username + "'</strong> is designated as Super Admin, and has <strong>GOD MODE</strong>, allowing it to look at all accounts in the RTA and HTA.<br /><br />No accounts can be assigned to this username.<br />", "warning", 10000);

			return;
		}


		this.CurrentEditableUser = username;
		$("#drag-and-drop-container #username").html("Username: <span style='color: #6666ff'>" + username + "</span>");

		var TheseAccountsAreAssignedButNotInAccountsList = [];
		$.each(this.Users, function (i, user) {
			if (user.user_login == username) {

				var LinkedAccountsHTML = "";

				// GAIN LIVE ARRAY
				if (user.GAIN_LIVE_ARRAY.length > 0) {
					$.each(user.GAIN_LIVE_ARRAY, function(j, acct) {
						// try to find the account in the list of accounts loaded
						var AccountObject = ThisClass.Accounts[acct];
						if (AccountObject == null) TheseAccountsAreAssignedButNotInAccountsList.push(acct);

						LinkedAccountsHTML += "<div class='drag-and-drop-account " + (AccountObject == null ? "error" : "") + "'><div class='drag-and-drop-left'><img src='images/icon-delete.png' data-broker='GAIN_LIVE' data-username='" + user.user_login + "' data-account='" + acct + "'></div><div class='drag-and-drop-right'><span class='drag-and-drop-top'>GAIN LIVE " + (AccountObject == null ? "(ERR)" : "") + "</span><span class='drag-and-drop-bottom'>" + acct + "</span></div></div>";
					}); 
				}

				// GAIN DEMO ARRAY
				if (user.GAIN_DEMO_ARRAY.length > 0) {
					$.each(user.GAIN_DEMO_ARRAY, function(j, acct) {
						// try to find the account in the list of accounts loaded
						var AccountObject = ThisClass.Accounts[acct];

						if (AccountObject == null) {
							// not found
							TheseAccountsAreAssignedButNotInAccountsList.push(acct);
							LinkedAccountsHTML += "<div class='drag-and-drop-account error' title='Account established on " + UpdatedTime + "'><div class='drag-and-drop-left'><img src='images/icon-delete.png' data-broker='GAIN_DEMO' data-username='" + user.user_login + "' data-account='" + acct + "'></div><div class='drag-and-drop-right'><span class='drag-and-drop-top'>GAIN DEMO <small>(ERR)</small></span><span class='drag-and-drop-bottom'>" + acct + "</span></div></div>";

						} else {
							// found, display	
							var IsTemp = AccountObject.IsTemp == '1',
								UpdatedTime = AccountObject.Updated;
							LinkedAccountsHTML += "<div class='drag-and-drop-account " + (IsTemp?"IsTempAcct":"") + "' title='Account established on " + UpdatedTime + "'><div class='drag-and-drop-left'><img src='images/icon-delete.png' data-broker='GAIN_DEMO' data-username='" + user.user_login + "' data-account='" + acct + "'></div><div class='drag-and-drop-right'><span class='drag-and-drop-top'>GAIN DEMO" + (IsTemp?" <small>(TEMP)</small>":"") + "</span><span class='drag-and-drop-bottom'>" + acct + "</span></div></div>";
						}
					}); 
				}

				// GQG LIVE ARRAY
				if (user.CQG_LIVE_ARRAY.length > 0) {
					$.each(user.CQG_LIVE_ARRAY, function(j, acct) {
						// try to find the account in the list of accounts loaded
						var AccountObject = ThisClass.Accounts[acct];
						if (AccountObject == null) TheseAccountsAreAssignedButNotInAccountsList.push(acct);

						LinkedAccountsHTML += "<div class='drag-and-drop-account'><div class='drag-and-drop-left'><img src='images/icon-delete.png' data-broker='CQG_LIVE' data-username='" + user.user_login + "' data-account='" + acct + "'></div><div class='drag-and-drop-right'><span class='drag-and-drop-top'>CQG LIVE</span><span class='drag-and-drop-bottom'>" + acct + "</span></div></div>";
					}); 
				}

				$("#drag-and-drop-linked-accounts").html(LinkedAccountsHTML);
				return false;
			}
		});

		if (TheseAccountsAreAssignedButNotInAccountsList.length > 0) {
			NotyTopRight("Error occured, the following accounts were not found in the list of accounts, they could have expired, please remove them:<br /><br />" + TheseAccountsAreAssignedButNotInAccountsList.join(", "), "error", 0);	
		}
		
	};

	PermissionHandler.prototype.AddAccountToUser = function (Account) {
		var Username = this.CurrentEditableUser,
			Broker = $("#broker-combobox").val();
			Account = $.trim(Account);
		var ThisClass = this;

		if (Username == "")
		{
			NotyTopRight("User not selected for editting.  Please double click a username on the left panel.", "warning", 10000);
			return false;
		}

		if (Account == "")
		{
			NotyTopRight("There was a technical issue with the account ('" + Account + "') being chosen.", "warning", 10000);
			return false;
		}

		$.each(this.Users, function (i, user) {
			if (user.user_login == Username) {

				if (user.UserRole == "admin") {
					return false;
				}

				if ($.inArray(Account, user[Broker + "_ARRAY"]) < 0) {	// value not found in array
					user[Broker + "_ARRAY"].push(Account);
					user[Broker + "_ARRAY"].sort();

					var AccountsString = user[Broker + "_ARRAY"].join(",");

					$.ajax({
						url: "includes/admin-permissions.php",
						method: "post",
						type: "post",
						data: { Operation: "AddAccountToUser", Broker: Broker, Account: AccountsString, Username: Username },
						error: function (errData) {
							console.log(errData);
							var errorString = "Error occured while trying to remove account '" + Account + "' from user '" + Username + "'.<br /><br />Message: " + errData + ".";
							NotyTopRight(errorString, "error", 0);							
						},
						success: function (data) {
							var parsedData = $.parseJSON(data);
							console.log(parsedData);
							if (parsedData.OutputType == "Data")
							{				
								ThisClass.UsernameToEdit(Username);
								NotyTopRight("Access '" + Account + "' successfully added to user '" + Username + "'.", "success", 2500);

							} else {
								var errorString = "Error occured while trying to remove account '" + Account + "' from user '" + Username + "'.<br /><br />Message: " + parsedData.Data + ".";
								NotyTopRight(errorString, "error", 0);							
							}
						}
					});

					ThisClass.UpdateUserAccountsInUserTable(Username);

				} else {	// value found already in the array
					NotyTopRight("Warning - account '" + Account + "' was already assigned to user '" + Username + "'.", "warning", 15000);
				}
				return false;
			}
		});
	};

	PermissionHandler.prototype.DeleteAccountFromUser = function (Broker, Account, Username) {
		var Broker = $.trim(Broker),
			Account = $.trim(Account),
			Username = $.trim(Username);
		var ThisClass = this;

		if (Broker != "" && Account != "" && Username != "") {
			$.each(this.Users, function (i, user) {
				if (user.user_login == Username) {
					var CurrentAccountArray = user[Broker + "_ARRAY"],
						NewAccountArray = user[Broker + "_ARRAY"].slice();

					NewAccountArray.remove(Account, true),
					NewAccountArraySTR = NewAccountArray.join(',');

					// Make AJAX call to remove from DB
					$.ajax({
						url: "includes/admin-permissions.php",
						method: "post",
						type: "post",
						data: { Operation: "DeleteAccountFromUser", Broker: Broker, Account: NewAccountArraySTR, Username: Username },
						error: function (errData) {
							console.log(errData);
							var errorString = "Error occured while trying to remove account '" + Account + "' from user '" + Username + "'.<br /><br />Message: " + errData + ".";
							NotyTopRight(errorString, "error", 0);							
						},
						success: function (data) {
							var parsedData = $.parseJSON(data);
							console.log(data);

							if (parsedData.OutputType == "Data")
							{				
								user[Broker + "_ARRAY"] = NewAccountArray;
								ThisClass.UsernameToEdit(Username);
								ThisClass.UpdateUserAccountsInUserTable(Username);
								NotyTopRight("Access to '" + Account + "' successfully removed from user '" + Username + "'.", "success", 2500);

							} else {
								var errorString = "Error occured while trying to remove account '" + Account + "' from user '" + Username + "'.<br /><br />Message: " + parsedData.Data + ".";
								NotyTopRight(errorString, "error", 0);
							}
						}
					});

					return false;
				}
			});
		}
	};

	PermissionHandler.prototype.UpdateUserAccountsInUserTable = function (Username) {
		var ThisClass = this;
		$.each(this.Users, function (i, user) {
			if (user.user_login == Username) {
				var LinkedAccountObj = $("#users-table").find("td[id='" + Username + "']");

				var LinkedAccountsHTML = "";
				if (user.UserRole == "admin") {
					LinkedAccountsHTML = "<strong>ADMIN:</strong> You have access to all accounts.";
				} else {
					if (user.GAIN_LIVE_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>GAIN LIVE: </strong> " + user.GAIN_LIVE_ARRAY.join(", ") + "</span>";
					if (user.GAIN_DEMO_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>GAIN DEMO: </strong> " + user.GAIN_DEMO_ARRAY.join(", ") + "</span>";
					if (user.CQG_LIVE_ARRAY.length > 0) LinkedAccountsHTML += "<span class='linked-account'><strong>CQG LIVE: </strong>  " + user.CQG_LIVE_ARRAY.join(", ") + "</span>";
				}

				// Flash the user accounts line
				var bgColor = LinkedAccountObj.parent().css("background-color");
				LinkedAccountObj.stop().css("background-color", "#FFFF9C").animate({ backgroundColor: bgColor }, 1500);
				LinkedAccountObj.html(LinkedAccountsHTML);

				ThisClass.UsernameToEdit(Username);
				return false;
			}
		});		
	};

	PermissionHandler.prototype.CheckAcocuntsListForDuplicates = function () {
		$.ajax({
			url: "includes/admin-permissions.php",
			method: "post",
			type: "post",
			data: { Operation: "FindDoublyAssignedAccounts" },
			error: function (errData) {
				console.log("ERROR", errData);
			},
			success: function (data) {
				var parsedData = $.parseJSON(data);
				if (parsedData != null && parsedData.OutputType != null) {
					if (parsedData.OutputType == "Data") {
						var parsedResponseArray = $.parseJSON(parsedData.Data);
						if ($.isArray(parsedResponseArray)) {
							if (parsedResponseArray.length > 0) {
								var ResponseString = "The following accounts were assigned more than once:<br /><br />";
								$.each(parsedResponseArray, function (i, val) {
									ResponseString += "<strong>" + val["Account"] + "</strong> assigned to " + val["Usernames"] + ".<br />";
								});

								NotyTopRight(ResponseString, "warning", 0);

							} else {
								NotyTopRight("Great news!<br />There were no double assignments.", "success", 5000);
							}
						} else {
							NotyTopRight("Error! The response from PHP script was not an array.", "error", 0);	
						}
					} else {
						NotyTopRight("There was an error in processing the request.  Message: " + parsedData.Data, "error", 0);
					}
				}
			}
		});
	};

	PermissionHandler.prototype.ManuallyRefreshPusherDataFromDB = function () {
		$.ajax({
			url: "includes/admin-permissions.php",
			method: "post",
			type: "post",
			data: { Operation: "ManuallyRefreshPusherDataFromDB" },
			error: function (errData) {
				console.log(errData);
			},
			success: function (data) {
				console.log(data);
				var parsedData = $.parseJSON(data);
				if (parsedData != null && parsedData.OutputType != null) {
					if (parsedData.OutputType == "Data") {
						if (parsedData.Data == "OK")
							NotyTopRight("Request successfully sent to Pusher C# Program to refresh user access from DB.", "success", 5000);
						else 
							NotyTopRight("Error response gotten from C# progress.  Message: " + parsedData.Data, "error", 0);
					} else {
						NotyTopRight("Error response gotten from C# progress.  Message: " + parsedData.Data, "error", 0);
					}
				} else {
					NotyTopRight("There was an error in processing the request.", "error", 0);
				}
			}
		});
	};

	PermissionHandler.prototype.ChangeTempAccountStatus = function (AccountName, changeToTempAccount) {
		// change account to temp
		var AccountObject = this.Accounts[AccountName];

		if (AccountObject != null) {
			// Account was found in the list of Accounts, proceed to changes
			AccountObject.IsTemp = (changeToTempAccount? '1' : '0');

			$.ajax({
				url: "includes/admin-permissions.php",
				method: "post",
				type: "post",
				data: { Operation: "SetDEMOAccountToTempSetting", Account: AccountName, IsTemp: (changeToTempAccount ? '1':'0') },
				error: function (errData) {
					console.log(errData);
					NotyTopRight("Error processing script request. Message: " + errData.statusText, "error", 0);
				},
				success: function (data) {
					console.log(data);
					var parsedData = $.parseJSON(data);
					if (parsedData != null && parsedData.OutputType != null) {
						if (parsedData.OutputType != "Data") {
							// ERROR
							NotyTopRight("Error occured while setting the temp status of the account (" + AccountName + ") to " + (changeToTempAccount?"True":"False") + "!", "error", 0);
						}
					}
				}
			});

			this.UsernameToEdit(this.CurrentEditableUser);

		} else {
			// Account not found in the list of accounts, produce error
			NotyTopRight("Account '" + AccountName + "' not found in accounts list.  Please contact your developer.", "error", 0);
		}
	};

	// Prototype for removing items from an array
	if (!Array.prototype.remove) {
	  Array.prototype.remove = function(vals, all) {
	    var i, removedItems = [];
	    if (!Array.isArray(vals)) vals = [vals];
	    for (var j=0;j<vals.length; j++) {
	      if (all) {
	        for(i = this.length; i--;){
	          if (this[i] === vals[j]) removedItems.push(this.splice(i, 1));
	        }
	      }
	      else {
	        i = this.indexOf(vals[j]);
	        if(i>-1) removedItems.push(this.splice(i, 1));
	      }
	    }
	    return removedItems;
	  };
	}

	function loading(showLoading) {
		if (showLoading) {
			$("#broker-combobox").css("display", "inline-block").css("width", "70%");
			$("#loading-icon").show();
		} else {
			$("#broker-combobox").css("display", "block").css("width", "90%");
			$("#loading-icon").hide();
		}
	}
</script>

<style type="text/css">

</style>

</head>

<body>
	<div class="split-pane fixed-top">
		<div class="split-pane-component" id="top-component">
        	<!-- HEADER -->
        	<table border=0 cellpadding="0" cellspacing="0">
        		<tr>
	        		<td width="10%"><img src="images/top-logo.png" style="display: inline-block; margin: 3px 15px; vertical-align: middle;"></td>
	        		<td>
	        			<input id="refresh-user-accounts" type="button" value="Manually refresh user account permissions in the C# Program" style="display: inline-block; margin: 5px 10px;" />
	        			<input id="run-manual-archiver" type="button" value="Manually run account archiver" style="display: inline-block; margin: 5px 10px;" />
	        			<input id="check-double-accounts" type="button" value="Check if any accounts have been doubly assigned" style="display: block; margin: 5px 10px;" />
	        		</td>
	        		<td><span style="float: right;"><a class="icon" id="logout" href="<?php echo Am_Lite::getInstance()->getLogoutURL(); ?>" title="Logout"></a></span></td>
    			</tr>
    		</table>

		</div>
		<div class="split-pane-divider" id="horizontal-divider"></div>
        
		<div class="split-pane-component" id="bottom-component">
        	<!-- bottom part -->
            <div id="split-pane-1" class="split-pane fixed-left">
                <div class="split-pane-component" id="left-component">
                    <!-- USERS AREA -->
                    <div id="users-container">
                    Users go here....
                    </div>
                </div>
                <div class="split-pane-divider" id="vertical-divider"></div>
                <div class="split-pane-component" id="right-component">
                		<!-- splot -->
                        <div class="split-pane fixed-top">
                            <div class="split-pane-component" id="top-component2">
                            	<!-- Accounts -->
                            	<div id="accounts-container">
                            		<img id="loading-icon" class="spin" src="images/loading-static-icon.png" />

									<select id="broker-combobox">
										<option value="GAIN_LIVE">Broker Select --> GAIN LIVE</option>
										<option value="GAIN_DEMO">Broker Select --> GAIN DEMO</option>
										<option value="CQG_LIVE">Broker Select --> CQG LIVE</option>
									</select>
									<label style="display: block; text-align: left; width: 90%; margin: 3px auto; padding: 0; font-size: 0.8em; vertical-align: middle;" title="Clicking this makes the archive option appear on the accounts display, and the user can select to send the account and its fills to the archive tables."><input type="checkbox" id="allow-archiving-accounts" style="vertical-align: top;" />Allow user to archive accounts and fills</label>

									<div id="accounts-selector">
										Account data goes here...
									</div>


                            	</div>
                            </div>
                            <div class="split-pane-divider" id="horizontal-divider2"></div>
                            <div class="split-pane-component" id="bottom-component2">
                            	<!-- Drag & Drop area -->
                            	<div id="drag-and-drop-container" style="padding: 5px 10px;">
                            		<h2 id="username">Username: </h2>
                            		<h2>Linked Accounts: </h2>
                            		<div id="drag-and-drop-linked-accounts">
                            			
                            			
                            		</div>
                            	</div>
                            </div>
                        </div>
                
                </div>
            </div>
		</div>
	</div>



	<script type="text/javascript">
		// RUN SCRIPTS

		window.Handler = new PermissionHandler();

		$(document).ready(function (e) {
			//window.Handler.LoadAccounts( $("#broker-combobox").val() );
		});

		$("#broker-combobox").change(function () {
			window.Handler.DisplayAccounts();
		});

		$(document).on("dblclick", "a.username", function () {
			var id = $.trim(this.id);
			if (id != "" && window.Handler != null) window.Handler.UsernameToEdit(this.id);
		});

		$(document).on("click", ".drag-and-drop-account img", function() {
			var Broker = $(this).data("broker"),
				Account = $(this).data("account"),
				Username = $(this).data("username");

			if (window.Handler != null) window.Handler.DeleteAccountFromUser(Broker, Account, Username);

		});

		$(document).on("dblclick", ".movable-account", function () {
			console.log("click moveable account");
			if (window.Handler != null) {
				var bgColor = $(this).parent().parent().css("background-color");
				$(this).parent().parent().stop().css("background-color", "#FFFF9C").animate({ backgroundColor: bgColor}, 1500);
				window.Handler.AddAccountToUser(this.id);
			}
		});


		$(document).on("change", ".user-checkbox", function () {
			var Type = this.name,
				Value = $(this).is(":checked"),
				Username = this.id;

			if (Type != "" && Username != "" && (Value == true || Value == false)) {
				$.ajax({
					url: "includes/admin-permissions.php",
					method: "post",
					type: "post",
					data: { Operation: "EditCheckboxPermissions", Type: Type, Value: Value, Username: Username },
					error: function (errData) {
						var errorString = "Error occured while trying to change " + Type + "' for user '" + Username + "'.<br /><br />Message: " + errData + ".";
						NotyTopRight(errorString, "error", 0);							
					},
					success: function (data) {
						var parsedData = $.parseJSON(data);
						if (parsedData.OutputType == "Data")
						{				
							NotyTopRight(Type + " successfully changed to " + Value, "success", 2500);

						} else {
							var errorString = "Error occured while trying to change " + Type + "' for user '" + Username + "'.<br /><br />Message: " + parsedData.Data + ".";
							NotyTopRight(errorString, "error", 0);							
						}
					}
				});
			}
			
		});

		$(document).on("change", ".tmp-account-checkbox", function () {
			// the "Is Temp" checkbox of the DEMO account (in the accounts panel) was clicked
			window.Handler.ChangeTempAccountStatus(this.id, $(this).is(":checked"));
		});

		$("#refresh-user-accounts").click(function () {
			// click to manullay refresh the C# user data from DB
			window.Handler.ManuallyRefreshPusherDataFromDB();

		});
		
		$("#check-double-accounts").click(function () {
			// click to run an accounts double check to find duplicates (single account assigned to multiple users)
			window.Handler.CheckAcocuntsListForDuplicates();
		});

		$("#run-manual-archiver").click(function () {
			// OK button is pressed
			$("#run-manual-archiver").css("background-color", "darkgrey").val("Working (May take a few mins)... Please wait...");
		    $.ajax({
				url: "includes/admin-account-archive.php?Operation=ArchiveAccountsCron",
				method: "get",
				type: "get",
				error: function (errData) {
					console.log(errData);
					var errorString = "Error occured while trying to running Account Archiver Cron.<br /><br />Message: " + errData + ".";
					NotyTopRight(errorString, "error", 0);	
					$("#run-manual-archiver").css("background-color", "#0066ff").val("Manually run account archiver");
				},
				success: function (data) {
					//console.log("outout", data);
					NotyTopRight(data, "alert", 0);
					$("#run-manual-archiver").css("background-color", "#0066ff").val("Manually run account archiver");
				}
			});
		});

		$("#allow-archiving-accounts").change(function () {
			console.log("archive clicked");
			window.Handler.DisplayAccounts();
		});

		$(document).on("click", ".archive-icon", function () {
			// Archive icon clicked for an account
			var Broker = $("#broker-combobox").val(),
				Account = this.id;

			noty({
				text: 'ALERT<br /><br />You are looking to archive account <strong>' + Account + '</strong>.<br /><br />Are you sure you want to send this account and its fills to the archives?<br />',
				layout: 'topRight',
				type: 'alert',
				buttons: [
				{addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
				    // this = button element
				    // $noty = $noty element
				    $noty.close();

				    // OK button is pressed
				    $.ajax({
						url: "includes/admin-permissions.php",
						method: "post",
						type: "post",
						data: { Operation: "ArchiveAccount", Broker: Broker, Account: Account },
						error: function (errData) {
							console.log(errData);
							var errorString = "Error occured while trying to archive fills for account (" + Account + ").<br /><br />Message: " + errData + ".";
							NotyTopRight(errorString, "error", 0);							
						},
						success: function (data) {
							console.log(data);

							var parsedData = $.parseJSON(data);
							if (parsedData.OutputType == "Data")
							{				
								NotyTopRight(parsedData.Data, "success", 2500);

							} else {
								NotyTopRight(parsedData.Data, "error", 0);							
							}
						}
					});

				  }
				},
				{addClass: 'btn btn-cancel', text: 'Cancel', onClick: function($noty) {
				    $noty.close();

				    // Cancel button is pressed
				     
				  }
				}]
			});
		});
	</script>
</body>
</html>
