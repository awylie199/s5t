<?php
namespace s5t;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Setup Child Jupiter Theme
 */
class Setup
{
    /**
     * Reference to self
     * @var s5t\Setup
     */
    private static $instance;

    private function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'registerAssets']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueAssets']);
        add_action('wp_print_scripts', [$this, 'dequeueAssets']);
    }

    /**
     * Register JS and SCSS Assets
     * @return void
     */
    public function registerAssets()
    {
        $dir = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'dist/';
        $libDir = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'assets/js/lib/';

        wp_register_script(
            's5t-js',
            $dir . 'main.js',
            ['jquery'],
            '1.0.0',
            true
        );

        wp_register_script(
            's5t-core-scripts',
            $libDir . 'core-scripts.js',
            ['jquery'],
            '1.0.0',
            true
        );

        wp_register_style(
            's5t-styles',
            $dir . 'main.css',
            ['theme-styles'],
            '1.0.0',
            'all'
        );
    }

    /**
     * Enqueue JS and CSS Assets
     * @return void
     */
    public function enqueueAssets()
    {
        wp_enqueue_script('s5t-js');
        wp_enqueue_style('s5t-styles');
        wp_enqueue_script('s5t-core-scripts');
    }

    /**
     * Dequeue Assets
     * @return void
     */
    public function dequeueAssets()
    {
        wp_dequeue_script('core-scripts');
    }

    /**
     * Primary Entry Point for Si24 Theme
     */
    public static function instance()
    {
        if (! isset(self::$instance) || empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Singleton Empty __clone Function
     * @return void
     */
    protected function __clone()
    {
    }

    /**
     * Singleton Empty __clone Function
     * @return void
     */
    protected function __serialize()
    {
    }
}
