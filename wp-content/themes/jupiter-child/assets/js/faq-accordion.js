import jQuery from 'jquery';

(function($) {
    $(function() {
        $('.mk-accordion-tab').on('click', function(ev) {
            let $target = $(ev.target).closest('.mk-accordion-single'),
                index = $target.index(),
                $accordion = $(ev.currentTarget).closest('.vc_tta-panel'),
                accordionID = $accordion[0].id,
                hash = `${accordionID}-item-${index}`;

            window.location.hash = hash;
        });

        if (window.location.hash && window.location.hash.indexOf('-item-') !== -1) {
            let [accordionID, itemIndex] = window.location.hash.split('-item-'),
                $accordionItem = $(accordionID).find('.mk-accordion-tab').eq(+itemIndex);

            window.setTimeout(function() {
                $accordionItem.parent('.mk-accordion-single').addClass('current');
                $accordionItem.siblings('.mk-accordion-pane').show();
                $accordionItem.closest('.vc_tta-panel').addClass('vc_active');

                $('html, body').animate({
                    scrollTop: $accordionItem.offset().top - 200
                }, 1000);
            }, 500);

            window.MK.utils.scrollTo = () => {};
            window.MK.utils.scrollToAnchor = () => {};
            window.MK.utils.scrollToURLHash = () => {};
            window.MK.utils.detectAnchor = () => false;
        }

        $('.instructions-link').on('click', function(ev) {
            ev.preventDefault();

            let $downloadTab = $('#s5t-gain-account-faq').find('.mk-accordion-single').eq(1);

            $('#s5t-gain-account-faq').addClass('vc_active');

            window.setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $downloadTab.offset().top - 200
                }, 1000);

                $downloadTab.addClass('current');
                $downloadTab.children('.mk-accordion-pane').show();
            }, 200);
        });

        $('#gain-1099').on('click', function(ev) {
            ev.preventDefault();

            let $statementsTab = $('#s5t-gain-account-faq').find('.mk-accordion-single').first();

            window.setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $statementsTab.offset().top - 200
                }, 1000);

                $statementsTab.addClass('current');
                $statementsTab.children('.mk-accordion-pane').show();
            }, 200);
        });
    });
}(jQuery));
