import './plugins/slider';
import './plugins/sticky';
import './plugins/truncate';
import './libs/jquery.easing.js';
import './libs/jquery.truncate.js';
import './libs/lodash.min.js';
import './libs/roll.min.js';
import './libs/unslider.min.js';
import './libs/velocity.min.js';

(function ($) {
    $(function () {
        var container = $('#mk-theme-container');
        var footer = $('#mk-footer');
        var footerTimer;

        var footerResize = function () {
            container.css({paddingBottom: footer.outerHeight()});
        };

        footerResize();

        $(window).on('resize', function () {
            clearTimeout(footerTimer);
            footerTimer = setTimeout(footerResize, 200);
        });

        /* Secondary Menu Toggle */
        var toggle = $('.secondary-menu-toggle');

        toggle.on('click', function (event) {
            event.preventDefault();

            var element = $(this);

            if (element.hasClass('is-open')) {
                element.removeClass('is-open');
                element.parent().removeClass('is-open');
            } else {
                element.addClass('is-open');
                element.parent().addClass('is-open');
            }
        });

        /* Scroll Down Arrow on Landing Page */
        var $document = $(document);
        var $window = $(window);
        var scrollDown = $('.scroll-down-arrow');

        scrollDown.on('click', function (event) {
            event.preventDefault();

            var top = $window.scrollTop();
            var height = $window.height();

            $('body, html').stop().animate({
                scrollTop: top + (height * 0.8),
                easing: 'easeOutQuart'
            }, 700);
        });

        window.addEventListener('scroll', function () {
            var position = $window.scrollTop() + $window.height();

            if (position >= $document.height()) {
                scrollDown.hide();
            } else {
                scrollDown.show();
            }
        }, false);

        /**
         * Roll.js Landing Page Scenes
         */
        var scene = $('[data-type="cm-scenes"]');

        if (scene.length) {
            var roll = Roll.DOM('#wrapper', '#pane', '#steps', '.step', 100);
            var views = document.querySelectorAll('.step');

            views[0].className = 'step curr';

            roll.on('step', Roll.stepHandler(roll, views, 'prev', 'next', 'curr', true));
        }
    });
})(jQuery);
