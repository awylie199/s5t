(function ($) {
    $(function () {
        var options = {
            slider: {
                animation: 'fade',
                autoplay: true,
                delay: 12000
            }
        };
        var testimonialSlider = $('.testimonial-slider');

        testimonialSlider.unslider({
            autoplay: true,
            delay: 12000
        });

        var landingMenuItems = $('#menu-landing-page').find('li');
        var landingSlider = $('[data-type~="landing-slider"]');
        var landingSlides = $.map(landingSlider.find('li'), function (element) {
            var slide = $(element);

            if (slide.hasClass('slide')) {
                var top = slide.find('.content-block').first();
                var bottom = slide.find('.content-block').last();

                return {
                    infographic: slide.find('.landing-infographic'),
                    top: {
                        left: top.find('.c2 .c1'),
                        right: top.find('.c2 .c2')
                    },
                    bottom: {
                        left: bottom.find('.c1'),
                        right: bottom.find('.c2')
                    }
                };
            }
        });

        var hideSlides = function (exclude) {
            $.each(landingSlides, function (index, slide) {
                slide.infographic.removeClass('is-active');

                if (index !== exclude) {
                    slide.top.left.hide();
                    slide.top.right.hide();
                    slide.bottom.left.hide();
                    slide.bottom.right.hide();
                }
            });
        };

        var animateHeight = function (element, duration) {
            var height = element.outerHeight();

            element.css({
                overflow: 'hidden',
                marginTop: height,
                height: 0
            }).show();

            element.animate({
                marginTop: 0,
                height: height
            }, {
                duration: duration,
                complete: function () {
                    element.css({
                        overflow: '',
                        marginTop: '',
                        height: ''
                    });
                }
            });
        };

        var showSlide = function (index) {
            var slide = landingSlides[index];

            slide.infographic.addClass('is-active');

            animateHeight(slide.top.left, 1000);
            animateHeight(slide.top.right, 1500);

            slide.bottom.left.show();
            slide.bottom.right.show();
        };

        var highlightMenu = function (index) {
            landingMenuItems.removeClass('is-active');
            $(landingMenuItems[index]).addClass('is-active');
        };

        landingSlider.on('unslider.ready', function () {
            highlightMenu(0);
        });

        landingSlider.on('unslider.change', function (event, index) {
            hideSlides(index);
            showSlide(index);
            highlightMenu(index);
        });

        landingSlider.unslider(options.slider);


        /**
         * Parallax Implementation
         */

        var parallax = $('.parallax-image');

        var updatePosition = function() {
            var y = window.pageYOffset;
            var r = 0.6;
            var translate = 'translateY(' + (y * r) + 'px)';

            _.each(parallax, function (item) {
                item.style['-webkit-transform'] = translate;
                item.style['-moz-transform'] = translate;
                item.style['-ms-transform'] = translate;
                item.style['-o-transform'] = translate;
                item.style.transform = translate;
            });
        };

        window.addEventListener('scroll', updatePosition, false);


        /**
         * Smooth Scrolling Behavior
         */

        var time = 400;
        var distance = 250;
        var target = $('body, html');

        var smoothScroll = function (event) {
            var sidenav = $('.mk-side-dashboard')[0];

            if (!$.contains(sidenav, event.target)) {
                event.preventDefault();

                var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
                var top = $(window).scrollTop();

                target.stop().animate({
                    scrollTop: top - parseInt(delta * distance)
                }, time);
            }

        };

        $(document.body).on('mousewheel DOMMouseScroll', smoothScroll);
    });
})(jQuery);
