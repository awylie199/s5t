(function ($) {
    $(function () {

        var top = 0;
        var zIndex = 1000;
        var menus = {
            toolbar: $('.the-toolbar'),
            header: $('.mk-header'),
            primary: $('.menu-landing-page-container'),
            secondary: $('.secondary-menu-toggle').parent()
        };

        $.each(menus, function (key, item) {
            item.css({
                position: 'fixed',
                top: top,
                zIndex: zIndex
            });

            top += item.height();
            zIndex--;
        });

        var pageWrapper = $('.internal-page-wrapper');

        if (pageWrapper.length) {
            pageWrapper.css('paddingTop', menus.primary.height() + menus.secondary.height());
        }

        var $window = $(window);
        var positions = {
            toolbar: menus.toolbar.position(),
            header: menus.header.position(),
            primary: menus.primary.position(),
            secondary: menus.secondary.position()
        };

        $window.on('scroll', function () {
            var distance = $window.scrollTop();

            if (positions.primary) {
                menus.primary.css({
                    top: positions.primary.top - distance
                });
            }

            if (positions.secondary) {
                menus.secondary.css({
                    top: positions.secondary.top - distance
                });
            }
        });
    });
})(jQuery);
