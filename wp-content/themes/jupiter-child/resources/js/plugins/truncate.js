(function ($) {
    $(function () {

        var mediumColumns = $('.home .vc_col-sm-8 .the-excerpt p');
        var smallColumns = $('.home .vc_col-sm-4 .the-excerpt p');

        $.each(mediumColumns, function (key, item) {
            var section = $(item);

            section.truncate({
                length: 625,
                words: true
            });
        });

        $.each(smallColumns, function (key, item) {
            var section = $(item);

            section.truncate({
                length: 150,
                words: true
            });
        });
    });
})(jQuery);
