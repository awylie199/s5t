<?php 
/*
Template Name:  Trade Analyzer Popout Trades For RTA
*
*	This website created by Reza Khanahmadi
*	Trading Algo X
*	Created on May 1, 2017
*/ 
$BaseLocation = get_stylesheet_directory_uri();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contract Summary</title>

<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
<base href="<?php echo "$BaseLocation/RTA-assets/"; ?>" />
<link rel="icon" href="images/S5TA.favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/ModuleStyles.css" />

<style type="text/css">
	html, body {
		height: 100%;
		min-height: 100%;
		margin: 0;
		padding: 0;
		background-color: #32363b;
		color: #f2f2f2;
		font-family: 'Ubuntu', sans-serif;
	}
</style>

</head>

<body onblur="self.focus();"> 
<div id="popup-contract-summary-container"></div>
</body>
</html>