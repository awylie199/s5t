
$(document).ready(function(e) {

	if (!isAdmin) {
		selectedAccount = $("#account-selector").val().trim();
		//newAccountSelected(selectedAccount);
	}
		
	$("#account-selector").change(function(e) {
		selectedAccount = $("#account-selector").val().trim();
		newAccountSelected(selectedAccount);
	});
	
	$("#contract-selector").change(function(e) {
        selectedContract = $("#contract-selector").val().trim();
		newContractSelected(selectedContract);
    });
	
	$("#closed-positions-display").change(function(e) {
		if (Trades != null) Trades.UpdateSummaryBarDetails();
    });
	
	$("a#popout").click(function(e) {
		if (window.PopOutSummaryBar != null && window.PopOutSummaryBar.closed != null && window.PopOutSummaryBar.closed == false) {
			window.PopOutSummaryBar.focus();
		} else {							
			window.PopOutSummaryBar = window.open("popoutWindow.php", "Summary Window", "menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no,height=150,width=850", "YES");
			window.PopOutSummaryBar.onload = function () {
				window.PopOutSummaryBar.focus();
				updateContractSummaryBar();
			}
		}
    });

    $("#expected-value-per-contract").change(function () {
    	var isChecked = $("#expected-value-per-contract").is(":checked");
    	if (isChecked) {
    		if (typeof popupNotification == "function") popupNotification ("Expected value and Expectancy are now calculated per contract.", "alert", 5000);
    	} else {
			if (typeof popupNotification == "function") popupNotification ("Expected value and Expectancy are now calculated per trade.", "alert", 5000);
    	}

    	if (window.Trades != null) window.Trades.DisplayTrades();
    });
	
	
	/* Bug report tool bar UI interactions */
	$(".icon#bug-report").click(function(e) {
		if ($("#bug-report-toolbar").is(":hidden")) {
			// toolbar is shown
			$(".content").css("top", "25em");
			$("#settings-toolbar").slideUp();
			$("#bug-report-toolbar").slideDown("slow");
		} else {
			// toolbar is hidden
			$("#bug-report-toolbar").slideUp("slow", function() { $(".content").css("top", "7em"); });
		}
    });

	$("#bug-report-toolbar #bug-report-submit").click(function(e) {
		var messageText = $("#bug-report-text").val(),
			watchedAccount = selectedAccount,
			watchedContracts = selectedContract;

		
		if (messageText.trim() == "") {
			if (typeof popupNotification == "function") popupNotification ("Body of report cannot be empty. Please write something before submitting report.", "warning", 5000);
		} else {
			// Submit bug report via email
			$.ajax({
				url: "includes/send-bug-report.php",
				type: "post",
				method: "post",
				data: { username: username, account: watchedAccount, contracts: watchedContracts, reportbody: messageText },
				error: function (err) {
					console.log(err);
					if (typeof popupNotification == "function") popupNotification ("There was an error processing your bug submit.", "error", 15000);
				},
				success: function (data) {
					console.log(data);
					if (data.Status == "OK") {
						if (typeof popupNotification == "function") popupNotification ("Bug report successfuly sent.", "information", 5000);
						$("#bug-report-text").val("");
						$("#bug-report-toolbar").slideUp("slow", function() { $(".content").css("top", "7em"); });	
					} else {
						if (typeof popupNotification == "function") popupNotification ("There was an error sending your bug report.  Message: " + data.Data, "error", 15000);
					}
				}
			});
		}
	});
	
	$("#bug-report-toolbar #bug-report-cancel").click(function(e) {
		$("#bug-report-toolbar").slideUp("slow", function() { $(".content").css("top", "7em"); });
	});
	
	
	/* settings tool bar UI interactions */
	$(".icon#settings").click(function(e) {
		if ($("#settings-toolbar").is(":hidden")) {
			// toolbar is shown
			$(".content").css("top", "25em");
			$("#bug-report-toolbar").slideUp();
			$("#settings-toolbar").slideDown("slow");
		} else {
			// toolbar is hidden
			$("#settings-toolbar").slideUp("slow", function() { $(".content").css("top", "7em"); });
		}						

    });
	
	$("#settings-display-results").change(function(e) {
        var value = $(this).val();
		if (value == "Points" || value == "Ticks") {
			$("#settings-warning").show('fast');
			$("#settings-commission-fee").val(0);
			CommissionFee = 0;
		} else {
			$("#settings-warning").hide('fast');
		}
    });
	
	$("#settings-commission-fee").change(function(e) {
		var DisplayResultsToBeSet = $("#settings-display-results").val();
		if ((DisplayResultsToBeSet == "Points" || DisplayResultsToBeSet == "Ticks") && $(this).val() > 0) {
			$(this).val(0);
			$("#settings-warning").stop().css("color", "yellow").animate({ color: "#FFFFFF"}, 1500);
		}
	});
	
	$("#settings-toolbar #settings-save").click(function(e) {
		// Settings save button pressed
		
		$("#settings-toolbar").slideUp("slow", function() { $(".content").css("top", "7em"); });
		ResultsDisplayType = $("#settings-display-results").val();
		CommissionFee = Number($("#settings-commission-fee").val());
		
		$("#results-type-display").html("Display: <b>" + ResultsDisplayType + "</b>");
		$("#commissions-display").html("Commissions: $<b>" + CommissionFee + "/contract</b>");
		
		// Display type of result in titlebar of The "trades window"
		$("#trades-window .titlebar span").text("Completed Trades (" + ResultsDisplayType + ")");

		// Save results to DB, if successful show save success message
		if (typeof popupNotification == "function") {
			popupNotification ("Settings successfully reflected.", "information", 5000);
		}
		
		if (Trades != null) { Trades.DisplayTrades(); }
    });

	$("#settings-toolbar #settings-cancel").click(function(e) {
		// Settings cancel button pressed
		
		$("#settings-toolbar").slideToggle("slow", 0 , function (e) {
			$(".content").css("top", "7em");
			// revert back to old values
			
			// Revert Display Type
			$("#settings-display-results").val(ResultsDisplayType);
			if ( $("#settings-display-results").val() == null) {
				if (typeof popupNotification == "function") popupNotification ("Results Display Type change not successful.  Default set to 'Points'.", "error", 5000);
				ResultsDisplayType = "Points";
			}
			
			
			// Revert Commissions
			$("#settings-commission-fee").val(CommissionFee);
			if ( $("#settings-commission-fee").val() == null) {
				if (typeof popupNotification == "function") popupNotification ("Commissions change not successful.  Commissions set to zero.", "error", 5000);
				CommissionFee = 0;
			}
		});
		
    });
	
	$(".chart-change").click(function(e) {
        $(".chart-container").hide();
		$(".chart-container#" + this.id).show();
		if (typeof reflowChart == "function") reflowChart();
    });
	
	
	$("body").on('change', '.trade-error-selector', function () {
		var tradeError = $(this).val(),
			tradeID = $(this).closest('tr').attr('id');
			
		if (Trades != null) Trades.UpdateTradeError(tradeID, tradeError);	
	});

	$("body").on('change', '.trade-grade-selector', function () {
		var tradeGrade = $(this).val(),
			tradeID = $(this).closest('tr').attr('id');

		if (Trades != null) Trades.UpdateTradeGrade(tradeID, tradeGrade);				
	});

	$("body").on('change', '.trade-type-selector', function () {
		var tradeType = $(this).val(),
			tradeID = $(this).closest('tr').attr('id');

		if (Trades != null) Trades.UpdateTradeType(tradeID, tradeType);				
	});
	
	$("#export-trades-to-csv").click(function (e) {
		var AccountName = selectedAccount;
		try {
			AccountName = selectedAccount.split("|")[1];
		} catch (err) {
			AccountName = selectedAccount.replace('|', '');
		}

		if (selectedContract == null || selectedContract == "") {
			if (typeof popupNotification == "function") popupNotification ("Please select a contract for export.", "warning", 15000);
		} else {
			var filename = "Stage5-RTA-" + AccountName + "-" + selectedContract + ".csv";
			exportTableToCSV.apply(this, [$("#trades-table"), filename]);			
		}

	});

	$("a.font-control").click(function () {
		var fontSizeStr = "0.95em";
		switch(this.id) {
		    case "large":
		        fontSizeStr = "1.15em";
		        break;

		    case "small":
				fontSizeStr = "0.8em";
		    	break;
		}

		$("table, titlebar, input, select").css("font-size", fontSizeStr);
	});
});


// resize visible chart on resizing the window
$(window).resize(function(e) {
	if (typeof reflowChart == "function") reflowChart();
});

// Display fills in fills window
$(document).on("click", ".contract-symbol", function () {
	if ($.trim(this.id) != "" && Trades != null) Trades.DisplayFillsForTrade(this.id, true);	
});

$(document).on("click", ".TradeFillID", function () {
	if ($.trim(this.id) != "" && Trades != null) Trades.DisplayFillsForTrade(this.id, false);	
});

function exportTableToCSV_OLD($table, filename) {
	try {		
		var $rows = $table.find('tr:has(th),tr:has(td)'),
	
			// Temporary delimiter characters unlikely to be typed by keyboard
			// This is to avoid accidentally splitting the actual contents
			tmpColDelim = String.fromCharCode(11), // vertical tab character
			tmpRowDelim = String.fromCharCode(0), // null character
	
			// actual delimiter characters for CSV format
			colDelim = '","',
			rowDelim = '"\r\n"',
	
			// Grab text from table into CSV formatted string
			csv = '"' + $rows.map(function (i, row) {
				var $row = $(row),
					$cols = $row.find('th,td');
	
				return $cols.map(function (j, col) {
					try {
						var $col = $(col);
						
						// This changes the text of the 'internal' text of the TD or TH for the TradeGrade, TradeEmotion, and TradeType 
						var text = $col.text();

						if (i != 0) {

							if (j == 27) {
								var obj = $col.find(".trade-grade-selector");
								if (obj.length > 0) text = obj.val();
							} else if (j == 28) {
								var obj = $col.find(".trade-emoticon.chosen");
								if (obj.length > 0) text = obj.attr("id");
							} else if (j == 29) {
								var obj = $col.find(".trade-type-selector");
								if (obj.length > 0) text = obj.val();
							} 
						}

						console.log(text);
		
						return text.replace(/"/g, '""'); // escape double quotes					
						
					} catch (err) {
						console.log(err);	
					}
				}).get().join(tmpColDelim);
	
			}).get().join(tmpRowDelim)
				.split(tmpRowDelim).join(rowDelim)
				.split(tmpColDelim).join(colDelim) + '"',
	
			// Data URI
			csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

		$(this)
			.attr({
			'download': filename,
				'href': csvData,
				'target': '_blank'
		});	

	} catch (error) {
		if (typeof popupNotification == "function") popupNotification ("There was an error exporting to CSV.<br /><br />" + error, "error", 30000);
	}
	//if (typeof popupNotification == "function") popupNotification ("Exporting to filename:<br /><strong>'" + filename + "'</strong>", "alert", 5000);
};

function exportTableToCSV($table, filename) {
	try {		
		var $rows = $table.find('tr:has(th),tr:has(td)'),
	
			// Temporary delimiter characters unlikely to be typed by keyboard
			// This is to avoid accidentally splitting the actual contents
			tmpColDelim = String.fromCharCode(11), // vertical tab character
			tmpRowDelim = String.fromCharCode(0), // null character
	
			// actual delimiter characters for CSV format
			colDelim = '","',
			rowDelim = '"\r\n"',
	
			// Grab text from table into CSV formatted string
			csv = '"' + $rows.map(function (i, row) {
				var $row = $(row),
					$cols = $row.find('th,td');
	
				return $cols.map(function (j, col) {
					try {
						var $col = $(col);
						
						// This changes the text of the 'internal' text of the TD or TH for the TradeGrade, TradeEmotion, and TradeType 
						var text = $col.text();

						if (i != 0) {

							if (j == 27) {
								var obj = $col.find(".trade-grade-selector");
								if (obj.length > 0) text = obj.val();
							} else if (j == 28) {
								var obj = $col.find(".trade-emoticon.chosen");
								if (obj.length > 0) text = obj.attr("id");
							} else if (j == 29) {
								var obj = $col.find(".trade-type-selector");
								if (obj.length > 0) text = obj.val();
							} 
						}

						return text.replace(/"/g, '""'); // escape double quotes					
						
					} catch (err) {
						console.log(err);	
					}
				}).get().join(tmpColDelim);
	
			}).get().join(tmpRowDelim)
				.split(tmpRowDelim).join(rowDelim)
				.split(tmpColDelim).join(colDelim) + '"';
	
			if(msieversion()){
		        var IEwindow = window.open();
		        IEwindow.document.write('sep=,\r\n' + csv);
		        IEwindow.document.close();
		        IEwindow.document.execCommand('SaveAs', true, filename);
		        IEwindow.close();
		    } else {
		        var uri = 'data:application/csv;charset=utf-8,' + escape(csv);
		        var link = document.createElement("a");
		        link.href = uri;
		        link.style = "visibility:hidden";
		        link.download = filename;
		        document.body.appendChild(link);
		        link.click();
		        document.body.removeChild(link);
		    }

	} catch (error) {
		if (typeof popupNotification == "function") popupNotification ("There was an error exporting to CSV.<br /><br />" + error, "error", 30000);
	}
};

function msieversion() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return true
  {
    return true;
  } else { // If another browser,
  return false;
  }
  return false;
};

