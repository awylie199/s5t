// Charts JS (Options and Operations)

// Charts
var win_loss_percentage_chart = null, equity_curve_chart = null, expectancy_chart = null, trade_distribution_chart = null;

// Gauges
var percent_win_dial = null, percent_loss_dial = null, pnl_dial = null;

var chartHeight = 230, chartWidth = 350;

// Options for different charts
var win_loss_percentage_options = {
	chart: {
		type: 'line'
	},
	credits: { enabled: true, text: "Stage 5 Trading Corp." },
	title: {
		text: 'Win / Loss Percentage'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y + " %";
		}
	},
	yAxis: [{
		id: 'main-y-axis',
		min: 0,
		max: 100,
		title: { text: "" }
	}],
	xAxis: {
		minTickInterval: 1,
		min: 1
	},			
	series : [{
		id: 'PercentWin',
		name : "Percent Win",
		type: 'line',
		color: '#347bb2',
		pointStart: 1,
		data : []
	}, {
		id: 'PercentLoss',
		name : "Percent Loss",
		type: 'line',
		color: '#da200b',
		pointStart: 1,		
		data : []
	}]

}

var equity_curve_options = {
	chart: {
		type: 'line'
	},
	credits: { enabled: true, text: "Stage 5 Trading Corp." },	
	title: {
		text: 'Equity Curve'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y;
		}
	},	
	xAxis: {
		tickmarkPlacement: 'on',
		minTickInterval: 1	
	},
	yAxis: [{
		title: { text: "" }
	}],
	series : [
	{
		id: 'EquityCurve',
		name : "Equity Curve",
		type: 'line',
		pointStart: 1,
		index: 9,		
		data : []
	}, {
		id: 'PnL',
		name : "P&L",
		type: 'line',
		pointInerval: 1,
		pointStart: 1,
		index: 19,
		data : []
	}, {
		id: 'MFE',
		name : "MFE",
		type: 'column',
		pointStart: 1,
		stacking: 'normal',
		pointInerval: 1,
		pointPlacement: 'on',
		data : []
	}, {
		id: 'MAE',
		name : "MAE",
		type: 'column',
		pointStart: 1,
		stacking: 'normal',
		pointInerval: 1,
		pointPlacement: 'on',		
		data : []
	}]				
}

var expectancy_options = {
	chart: {
		type: 'line'
	},
	credits: { enabled: true, text: "Stage 5 Trading Corp." },	
	title: {
		text: 'Expectancy'
	},
	tooltip: {
		formatter: function () {
			return "<b>Trade #" + this.x + "</b><br />" + this.series.name + " " + this.y;
		}
	},	
	yAxis: [{
		id: 'main-y-axis',
		title: { text: "" }
	}],
	xAxis: {
		minTickInterval: 1,
		min: 1
	},			
	series : [{
		id: 'Expectancy',
		name : "Expectancy",
		type: 'line',
		pointStart: 1,		
		data : []
	}]
}

// Trade Distribution Chart
var trade_distribution_options = {
	chart: {
		type: 'column',
		zoomType: 'xy'			
	},
	credits: { enabled: false }, 
	title: {
		text: 'Trade Distribution'
	},
	xAxis: {
		title: {
			text: 'Ticks'
		},
		categories: [],
		minTickInterval: 1
	},
	yAxis: {
		title: {
			text: 'Number of Trades'
		},
		min: 0
	},
	legend: {
		enabled: false
	},
	tooltip: {
		formatter: function () {
			if (this.series != null && this.series.xAxis != null && this.series.xAxis.tickInterval != null) {
				if (this.x == 0)
					return "<b>Zero Return</b><br />" + this.series.name + " " + this.y;
				else 
					return (this.y + " returns between " + this.x + " and " + (this.x + this.series.xAxis.tickInterval) + ".");
			} else {
				return "<b>Return distribution " + this.x + "</b><br />" + this.series.name + " " + this.y;
			}
		}
	},
	plotOptions: {
		column: {
			groupPadding: 0.1,
			pointPadding: 0,
			borderWidth: 0
		}			
	},
	series: [{
		id: 'main',
		name: 'Trade Distribution',
		data: []
	}]		
}

var gaugeOptions = {
	chart: {
		type: 'solidgauge',
		backgroundColor: 'transparent',
		height: 150
	},
	credits: {
		enabled: false
	},
	title: {
		align: 'center',
		useHTML: true,
		style: { fontSize: '.9em' }
	},
	pane: {
		center: ['50%', '55%'],
		size: '110%',
		startAngle: -100,
		endAngle: 100,
		background: {
			backgroundColor: '#EEE',
			innerRadius: '60%',
			outerRadius: '90%',
			shape: 'arc'
		}
	},

	tooltip: {
		enabled: false
	},

	// the value axis
	yAxis: {
		min: 0,
		max: 100,
		lineWidth: 0,
		minorTickInterval: null,
		tickPixelInterval: 50,
		tickWidth: 2,
		labels: {
			y: -2,
			padding: 15,
			distance: 2
		}
	},
	series: [{
		data: [{
			radius: 90,
			innerRadius: 60,
			y: 0
		}]
	}],
	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: -15,
				borderWidth: 0,
				useHTML: true,
				style: { fontSize: '1em' }
			}
		}
	}
};

var sparkline_options = {
	chart: {
		//height: 69
	},
	credits: { enabled: false },
	legend: { enabled: false },
	title: { text: '' },
	tooltip: { enabled: false },
	yAxis: {
		title: { enabled: false },
		labels: { enabled: false },
		lineWidth: 0,
		gridLineWidth: 0,
		minorGridLineWidth: 0,
		minorTickLength: 0,
		lineColor: 'transparent',
		tickLength: 0
  	},
	xAxis: {
		visible: false,
		lineColor: 'transparent'
	},
	series: [{
		id: 'MFE',
		name: 'MFE',
		type: 'line',
		marker: { enabled: false },
		data: []
	}, {
		id: 'MAE',
		name: 'MAE',
		type: 'line',
		marker: { enabled: false },		
		data: []	
	}],
};

$(document).ready(function() {
	// Hide last 3 charts

	Highcharts.setOptions(Highcharts.theme);

	Highcharts.setOptions({
		global: {
			useUTC: true
		}
	});


	// Create the chart
	win_loss_percentage_chart = $(".chart-container#win-loss-percentage-chart").highcharts(win_loss_percentage_options);
	equity_curve_chart = $(".chart-container#equity-curve-chart").highcharts(equity_curve_options);
	expectancy_chart = $(".chart-container#expectancy-chart").highcharts(expectancy_options);
	trade_distribution_chart = $(".chart-container#trade-distribution-chart").highcharts(trade_distribution_options);	
	
	$(".chart-container#equity-curve-chart").hide();
	$(".chart-container#expectancy-chart").hide();
	$(".chart-container#trade-distribution-chart").hide();

	win_loss_percentage_chart = win_loss_percentage_chart.highcharts();
	expectancy_chart = expectancy_chart.highcharts();
	equity_curve_chart = equity_curve_chart.highcharts();
	trade_distribution_chart = trade_distribution_chart.highcharts();


	// Gauges
	percent_win_dial = $('#percent-win-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Win %' },
		series: [{
			data: [{
				radius: 90,
				innerRadius: 60,
				y: 0,
				color: 'rgba(20, 47, 68, .9)'	// stage 5 blue
			}]
		}]
	}));
	
	percent_loss_dial = $('#percent-loss-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'Loss %' },
		series: [{
			data: [{
				radius: 90,
				innerRadius: 60,
				y: 0,
				color: 'rgba(97, 14, 5, .9)'	// stage 5 red
			}]
		}]
	}));
	
	pnl_dial = $('#pnl-dial').highcharts(Highcharts.merge(gaugeOptions, {
		title: { text: 'P&L' },
		yAxis: {
			min: 0,
			max: 0,
			startOnTick: true,
			endOnTick: true,
			stops: [
				[0.49999,  'rgba(97, 14, 5, .9)'],			// stage 5 red
				[0.50000,  '#333'				],			// grey (neutral)
				[0.50001, 'rgba(20, 47, 68, .9)']			// stage 5 blue
			]
		}
	}));

});

function ClearAllCharts() {
	var ChartsObjectArray = [ win_loss_percentage_chart, expectancy_chart, equity_curve_chart, trade_distribution_chart ];
	var ChartsTitleArray = [ "Win/Loss Percentage", "Equity Curve", "Expectancy", "Trade Distribution" ];
		
	$.each(ChartsObjectArray, function (i, chart) {
		var seriesLength = chart.series.length;
		for(var j = seriesLength - 1; j > -1; j--)
			chart.series[j].setData([], false, false, false);
		
		chart.redraw();
	});

	percent_win_dial = $('#percent-win-dial').highcharts();
	percent_loss_dial = $('#percent-loss-dial').highcharts();
	pnl_dial = $('#pnl-dial').highcharts();
	
	// reset ranges
	percent_win_dial.yAxis[0].setExtremes(0, 100);
	percent_loss_dial.yAxis[0].setExtremes(0, 100);
	pnl_dial.yAxis[0].setExtremes(0, 0);

	// reset values
	percent_win_dial.series[0].points[0].update(0);
	percent_loss_dial.series[0].points[0].update(0);
	pnl_dial.series[0].points[0].update(0);
}


function reflowChart() {
	// resize visible chart
	var hc = $(".chart-container:visible").highcharts();
	hc.reflow();
}