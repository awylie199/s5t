/*
**  This is the RTA main PUSHER javascript file
*/

var Trades = new TradeHandler(null);

// Pusher connection variables
var PusherCloud_Connected 				= false, 
	PusherDedicatedChannel_Connected 	= false,
	PusherSharedChannel_Connected 		= false,
	PusherWorker_Ping_Last				= null,
	LastPusherStatus_Connected			= false;

// Pusher Handler
$(document).ready(function(e) {
	
    console.log("Pusher JS loaded.");
	
	window.pusher = null;
	const pusher_sharedChannel_name = "private-shared";
	var pusher_dedicatedChannel_template = "private-dedicated-";
	
	var isPusherSandbox = (typeof _PUSHER_SANDBOX == 'undefined' || _PUSHER_SANDBOX == null || _PUSHER_SANDBOX == false) ? false : true;
	if (isPusherSandbox) {
		if (typeof popupNotification == "function") popupNotification("You are operating in a PUSHER SANDBOX environment.", "alert", 0);
		$(".toolbar-left-section").append("<span style='font-weight: bold;'>This is a PUSHER SANDBOX environment.</span>");
	}

	var locationOfAuthenticator = (window.location.hostname == "localhost") ? "/stage5/" : "/";

	if (window.pusher == null) {
		// Pusher not initialized yet

		// INIT PUSHER
		if (isPusherSandbox) {

			window.pusher = new Pusher("295bf81725f5f6a203da", {
				encrypted: true,
				authEndpoint: locationOfAuthenticator + 'pusher-test/pusher_auth.php',
				auth: {
				  params: {
					  'username' : username
				  }
				}		
			});

		} else {
			
			window.pusher = new Pusher(PUSHER_APP_KEY, {
				encrypted: true,
				authEndpoint: locationOfAuthenticator + 'pusher-auth/pusher_auth.php',
				auth: {
				  params: {
					  'username' : username
				  }
				}		
			});
		}


		// bind connection metrics
		window.pusher.connection.bind('connected', function () {
			console.log("pusher connected");
			PusherCloud_Connected = true;
		});

		window.pusher.connection.bind('failed', function () {
			if (typeof popupNotification == "function") popupNotification("FATAL ERROR<br />Your browser does not support WebSockets or HTTP based transports.<br />Please upgrade your browser.", "error", 0);	
		});

		window.pusher.connection.bind('unavailable', function () {
			if (typeof popupNotification == "function") popupNotification("Connection to real-time protocol has become unavailable.", "error", 0);	
		});

		window.pusher.connection.bind('connecting_in', function(delay) {
  			if (typeof popupNotification == "function") popupNotification("Retrying connection to real-time protocol in " + delay + " seconds.", "warning", 2500);	
		});

		// When succssfully connected to pusher then if the data channel is not initialized subscribe to it
		if (window.sharedChannel == null) {
			window.sharedChannel = window.pusher.subscribe(pusher_sharedChannel_name);

			// set channel bindings
			window.sharedChannel.bind('pusher:subscription_succeeded', function() {
				//if (typeof popupNotification == "function") popupNotification("Success connecting to shared data channel.", "success", 2500);
				//$("#connection-status-shared").css("background-color", "green").attr("title", "Connected");
				PusherSharedChannel_Connected = true;
				if (PusherSharedChannel_Connected && PusherDedicatedChannel_Connected)
				{
					$("#connection-status-pusher").css("background-color", "green").attr("title", "Connected");
				}
			});

			window.sharedChannel.bind('pusher:subscription_error', function() {
				$("#connection-status-pusher").css("background-color", "darkred").attr("title", "Disconnected");
				if (typeof popupNotification == "function") popupNotification("Error connecting to shared data channel.", "error", 0);
			});
		}

		if (window.dedicatedChannel == null) {
			if (username.trim() != "") {
				window.dedicatedChannel = window.pusher.subscribe(pusher_dedicatedChannel_template + username);
			} else {
				if (typeof popupNotification == 'function') popupNotification ("There was an error connecting to dedicated data channel.  Username was set to '" + username + "'." , 'error', 0);
			}


			// set channel bindings
			window.dedicatedChannel.bind('pusher:subscription_succeeded', function() {
				//var triggered = window.dataChannel.trigger('client-NewSubscriber', { 'Username': username , 'UUID': UUID });
				//if (typeof popupNotification == "function") popupNotification("Success connecting to dedicated data channel.", "success", 2500);
				PusherDedicatedChannel_Connected = true;
				if (PusherSharedChannel_Connected && PusherDedicatedChannel_Connected)
				{
					$("#connection-status-pusher").css("background-color", "green").attr("title", "Connected");
				}

				// When dedicated data channel is established, send the server a message telling it what account you're looking at
				LookingAtAccount(selectedAccount);
			});

			window.dedicatedChannel.bind('pusher:subscription_error', function() {
				if (typeof popupNotification == "function") popupNotification("Error connecting to dedicated data channel.", "error", 0);
				$("#connection-status-pusher").css("background-color", "darkred").attr("title", "Disconnected");
			});
		}
		
		var BindingAndFunctionMapping = {
			"MarketData"				: received_MarketData,
			"InitialTrades"				: received_InitialTrades,
			"NewTrade"					: received_NewTrade,
			"Shutdown"					: received_Shutdown,
			"FillANDPositionConsistency": received_FillANDPositionConsistency,
			"AdminOperations"			: received_AdminOperations,
			"PingByServer"				: received_PingedByServer,
			"PongFromServer"			: received_PongFromServer,
			"NewMessage"				: received_NewMessage,
			"BrokerDisconnected"		: received_BrokerDisconnected
		};


		// main pusher event bindings
		$.each(BindingAndFunctionMapping, function (key, funct) {
			//console.log("trying to bind... ", window.pusher.global_emitter.callbacks._callbacks.hasOwnProperty("_" + key) );
			if (!window.pusher.global_emitter.callbacks._callbacks.hasOwnProperty("_" + key)) {
				//console.log("binding to key " + key);
				window.pusher.bind(key, funct);	
			}
		});

		// Check every 10 seconds for when the last PING/msg from Pusher worker was
		setTimeout(checkForConnectionToStage5, 2500);
		setInterval(checkForConnectionToStage5, 8000);
	}

	function checkForConnectionToStage5 () {
		if (PusherWorker_Ping_Last == null  || moment() - PusherWorker_Ping_Last > 10000) {

			if (LastPusherStatus_Connected == true) {			
				$("#connection-status-worker").css("background-color", "darkred").attr("title", "Disconnected");
				
				//if (typeof popupNotification == 'function')	popupNotification ("Real-time connection to Stage 5 server has dropped." , 'error', 0);
			}

			LastPusherStatus_Connected = false;

		} else {
			$("#connection-status-worker").css("background-color", "green").attr("title", "Connected");
		}
	}

	
	/* -------------------------------------
	 * Binding function implementarions 
	 * -------------------------------------
	*/
	var WriteFunctionsOutputToConsole = false;


	// Fill Consistency received
	function received_FillANDPositionConsistency (msg) {
		console.log("Function " + arguments.callee.name + " called.", msg);

		var HasError = false;

		try {
			if (msg != null && msg.Username != null) {
				if (msg.UUID == UUID || msg.UUID == "---") {	
					if (msg.Data != null) {
						var parsedMsg = $.parseJSON(msg.Data);
						if ($.isArray(parsedMsg) && Trades != null) {

							// Iterate List of Contracts
							$.each(Trades.ContractsList, function (cIndex, contract) {
								

								// create a list of FillIDs for contract
								var ListOfFillIDs = [];
								$.each(contract.FillsArray, function (fIndex, fill) {
									ListOfFillIDs.push(fill.FillID);
								});

								$.each(parsedMsg, function(i, val) {
									// Find the matching symbol
									if (val.Symbol == contract.Symbol) {

										if (contract.TradesArray != null && contract.TradesArray.length > 0) {
											var LastTrade = contract.TradesArray[contract.TradesArray.length-1];
											if (LastTrade != null) {
												if (val.Position != LastTrade.Position) { HasError = true; }
											}
										}

										$.each(val.ListFillIDs.split(','), function (ffIndex, fillID) {
											// traverse the fillIDs from the check program to find differences to the ListofFillID at the client browser
											try {
												if (fillID != contract.FillsArray[ffIndex].FillID) { HasError = true; }	
											} catch (err) {
												HasError = true;
											}
										});
									}
								});
							});
						}
					}
				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}	


			if (HasError) {
				LookingAtAccount(selectedAccount);
			}

		} catch (err) {
			console.log(err);
		}
	};


	// Market Data received 
	function received_MarketData (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		PusherWorker_Ping_Last = moment();
		LastPusherStatus_Connected = true;
		if (Trades != null && msg != null) Trades.PriceUpdate(msg);
	};

	function received_InitialTrades (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		
		// Make sure the connection to Worker Program is set as connected
		PusherWorker_Ping_Last = moment();
		$("#connection-status-worker").css("background-color", "green").attr("title", "Connected");

		if (msg != null && msg.Username != null) {
			if (msg.UUID == UUID || msg.UUID == "---") {	
				if (msg.Data != null) {
					var parsedMsg = $.parseJSON(msg.Data);

					// Check is Account is the same, if not don't show
					if (parsedMsg.Account != null && parsedMsg.Page != null && parsedMsg.Total != null && parsedMsg.Data != null) {
						if (parsedMsg.Account == selectedAccount) {
							if (typeof loadingWindow == 'function') loadingWindow(true);
							if (Trades != null) Trades.InitFills(parsedMsg.Data, (parsedMsg.Page == parsedMsg.Total));	
						}
					}
				}
			} else {
				if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
			}
		}
	};

	function received_NewTrade (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		if (msg.Username == username) {
			if (msg.UUID == UUID || msg.UUID == "---") {
				Trades.InsertFill( $.parseJSON(msg.Data) , true );
			} else {
				if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
			}
		}
	};

	function received_Shutdown(msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		if (typeof popupNotification == "function") popupNotification("Real-time data broker has shutdown. This could be for maintenance.<br />Your page will refresh when the data broker comes back online.", "information", 0);
	};

	function received_AdminOperations (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		try {
			if (msg != null) {
				var Message = $.parseJSON(msg.Data),
				    Operation = Message.Operation;
				
				if (msg.Username == "") {
					// This was a broadcast
					try {
					    var Broker = Message.Broker;
						if(!(Broker == "-- ALL BROKERS --" || Broker == selectedAccount.split("|")[0])) return;						
					} catch (err) {}
				}

				if (!(Broker == "-- ALL BROKERS --" || Broker == selectedAccount.split("|")[0])) {
					return;
				}


				if (Operation == "FillCheck") {
					if (Trades != null) Trades.CheckFillConsistency();
				} else if (Operation == "ClearMessages") {
					clearPopupMessages();
				
				} else if (Operation == "PingFromServer") {
					// Ping is coming in from server, return a pong
					ReturnPingToServer();
				
				} else if (Operation == "Reset") {
					// A force reset is coming in, reset the user within a 20 second window (but do not refresh)
					var resetTimeInMilliseconds = Math.round(Math.random() * 15000) + 5000;
					//if (typeof popupNotification == "function") popupNotification("Server is resetting your connection.  Your data will reload within a 20 second window.", "information", 20000);
					setTimeout(function () {
						LookingAtAccount(selectedAccount);
					}, resetTimeInMilliseconds);

				} else if (Operation  == "RefreshDashboard") {
					// Reload the page in a random time between 5 to 20 seconds
					var refreshTimeInMilliseconds = Math.round(Math.random() * 15000) + 5000;
					if (typeof popupNotification == "function") popupNotification("Admin requested page refresh.  Your page will refresh in " + Math.round(refreshTimeInMilliseconds/1000) + " seconds...", "information", 0);
					setTimeout(function () {
						location.reload();
					}, refreshTimeInMilliseconds);

				}
			}

		} catch (err) {
			console.error("There was an error in the admin function force.  Message: " + err);
		}
	};

	function received_PingedByServer (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		PusherWorker_Ping_Last = moment();
		if (typeof popupNotification == "function") popupNotification("User pinged.", "alert", 2500);
		if (window.dedicatedChannel != null) 
			window.dedicatedChannel.trigger('client-PongByUser', { 'Username': username });
	};

	function received_PongFromServer (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		PusherWorker_Ping_Last = moment();
		if (typeof popupNotification == "function") popupNotification("Server returned pong.", "alert", 2500);
	};

	function received_NewMessage (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		PusherWorker_Ping_Last = moment();

		if (msg.Username == username || msg.Username == "") {
			if (msg.UUID == UUID || msg.UUID == "---" || msg.UUID == "") {
				if (msg.Data != null) {
					var parsed = $.parseJSON(msg.Data);
					
					if (msg.Username == "") {
						// This was a broadcast
						try {
							Broker = parsed.Broker;
							if(!(Broker == "-- ALL BROKERS --" || Broker == selectedAccount.split("|")[0])) return;						
						} catch (err) {}
					}


					if (parsed.MessageType == "information") {
						if (typeof popupNotification == 'function') popupNotification (parsed.Message, parsed.MessageType, 0);
					}
					else {
						if (typeof popupNotification == 'function') popupNotification (parsed.Message, parsed.MessageType, 15000);
					}
				}
			} else {
				if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
			}
		}
	};

	function received_BrokerDisconnected (msg) {
		if (WriteFunctionsOutputToConsole) console.log("Function " + arguments.callee.name + " called.", msg);
		if (typeof popupNotification == 'function')
			popupNotification ("Account (" + msg.Data + ") disconnected, connection with API lost.<br />This could be an internal or server error, please try another account or refresh in a few minutes." , "error", 30000);
	};

});


var serverLastPinged = new Date();
var reponsePongByServerAt = new Date();
function PingServer () {
	if (new Date() - serverLastPinged > 30000) {
		if (window.dedicatedChannel != null)
			window.dedicatedChannel.trigger("client-PingFromClient", { 'Username': username, 'UUID': UUID });
		serverLastPinged = new Date();
		setTimeout( function () {
			// Check for pong response
			if (reponsePongByServerAt > serverLastPinged) {
				if (typeof popupNotification == 'function') popupNotification ("Pong received in " + (reponsePongByServerAt - serverLastPinged) + " milliseconds." , 'alert', 2500);
			} else {
				if (typeof popupNotification == 'function') popupNotification ("Pong not received." , 'warning', 2500);
			}
		}, 2500);
	}
}

function ReturnPingToServer () {
	if (window.dedicatedChannel != null)
		window.dedicatedChannel.trigger("client-ReturnPingToServer", { 'Username': username, 'Account': selectedAccount ,'UUID': UUID });
	else
		if (typeof popupNotification == 'function') popupNotification ("Error occured!<br />Dedicated data channel was null. Please contact admin.", 'error', 0);
}


function LookingAtAccount (account) {

	if (typeof account == 'undefined' || account == null || account == "") {
		if ($("#account-selector").length > 0) selectedAccount = $("#account-selector").val().trim();
		else return false;
	} else {
		selectedAccount = account;
	}

	if (selectedAccount != "") {
		if (window.dedicatedChannel != null) {
			window.dedicatedChannel.trigger('client-LookingAtAccount', { 'Username': username , 'UUID': UUID, 'Account': selectedAccount });
		} else {
			if (typeof popupNotification == 'function') popupNotification ("Error occured!<br />Dedicated data channel was null. Please contact admin.", 'error', 0);	
		}
	} else {
		if (typeof popupNotification == 'function') popupNotification ("Error occured!<br />Account name is null or empty. Please contact admin.", 'error', 0);	
	}
}
