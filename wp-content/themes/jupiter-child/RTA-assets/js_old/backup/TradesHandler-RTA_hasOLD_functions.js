
var TradeHandler = function (accountName) {
	this.Account = accountName;
	this.ContractsList = [];
};

TradeHandler.prototype.AdminDataDump = function () {
	// This will dump all inside the contractlist into a string and send it to server for storage
	$.ajax({
		url: 'includes/Admin-data-dump.php',
		method: 'post',
		data: { Username: username, Data: this.Account + "-" + JSON.stringify(this.ContractsList) },
		error: function (err) { 
			if (typeof popupNotification == "function") popupNotification (err, "error", 0);		
		},
		success: function (data) { 
			if (typeof popupNotification == "function") popupNotification (data, "alert", 5000);
		}
	});
		
};

TradeHandler.prototype.InitFills = function (fills) {
	console.log(fills);
	if (typeof loadingWindow == 'function') loadingWindow(true);
	this.Account = "";
	this.ContractsList = [];
	if (typeof ClearAllCharts == 'function') ClearAllCharts();

	$.each(fills, function (i, fill) {
		if (fill.Account != null && fill.Account == selectedAccount) Trades.InsertFill(fill, false);
	});
	
	Trades.ReOrgContractsDropDownList();
	Trades.DisplayTrades();
	if (typeof loadingWindow == 'function') loadingWindow(false);	
};

TradeHandler.prototype.InsertFill = function (fill, isLive) {
	if (isLive == null) isLive = false;
	
	if (fill.Account != null && fill.Account == selectedAccount) {
		
		// If this is a live fill, then show notification of the fill
		var popupString = "<b>" + fill.Contract + "</b> " + (fill.Side == "Buy" ? "Bought " : "Sold ") + fill.Quant + " " + " @ " + fill.Price + "<br /><span style='font-size: .8em'>" + moment.utc(fill.TimestampUTC).local().format('MMM D h:mma') + "</span>";
		if (isLive == true && typeof popupNotification == "function") popupNotification (popupString, "alert", 5000);
		
		// Search for the contract that this fill belongs to, call the contract's AddFill function
		var currentContract = null;
		$.each(this.ContractsList, function (i, contract) {
			if (contract.Symbol == fill.Contract) {
				contract.AddFill(fill, isLive);
				currentContract = contract;
			}
		});
		
		
		// If no contracts exist, this is the initiating fill of a new contract
		// Create contract and add to list
		// Call the ReOrg function to add this to the Contract DropdownList
		if (currentContract == null) {
			if (selectedContract == null || selectedContract == "") selectedContract = fill.Contract;

			currentContract = new Contract(fill.Contract, fill.TickSize, fill.ContractSize, fill.StopTime);
			currentContract.AddFill(fill, isLive);
			this.ContractsList.push(currentContract);

			this.ReOrgContractsDropDownList();			
		}
		
		if (isLive) updateContractSummaryBar(currentContract);
	}
};

TradeHandler.prototype.PriceUpdate = function (priceArray) {
	// Update the contracts with prices
	if (priceArray != null) {
		$.each(priceArray, function (i, priceObject) {
			if (priceObject.Symbol != null && priceObject.Price != null) {
				$.each(Trades.ContractsList, function (i, contract) {
					if (contract.Symbol == priceObject.Symbol) {
						
						// Call the Contract's UpdatePrice with the latest price
						contract.UpdatePrice(priceObject.Price);
						return false;
					}
				});
			}
		});
	}
};

TradeHandler.prototype.GetUserTradeMetricsFromDB = function () {
	if (selectedAccount != null && selectedAccount != "" && selectedContract != null && selectedContract != "") {
		var FillIDArray = [];

		$.each(this.ContractsList, function (i, contract) {
			if (contract.Symbol == selectedContract) {
				contract.GetUserTradeMetricsFromDB();
				return false;
			}
		});
	}
};

TradeHandler.prototype.UpdateTradeType = function (tradeFillID, newType) {
	$.each(this.ContractsList, function (i, contract) {
		if (selectedContract != null &contract.Symbol == selectedContract) {
			contract.UpdateTradeType(tradeFillID, newType);	
		}
	});	
};

TradeHandler.prototype.UpdateTradeGrade = function (tradeFillID, newGrade) {
	$.each(this.ContractsList, function (i, contract) {
		if (selectedContract != null &contract.Symbol == selectedContract) {
			contract.UpdateTradeGrade(tradeFillID, newGrade);	
		}
	});	
};

TradeHandler.prototype.UpdateTradeEmoticon = function (tradeFillID, newEmoticon) {
	$.each(this.ContractsList, function (i, contract) {
		if (selectedContract != null &contract.Symbol == selectedContract) {
			contract.UpdateTradeEmoticon(tradeFillID, newEmoticon);	
		}
	});	
};

TradeHandler.prototype.ReOrgContractsDropDownList = function () {
	if (this.ContractsList.length == 0) {
		$("#contract-selector").empty();
		$("<option />", { val: "--", text: "--" }).appendTo($("#contract-selector"));
	} else {
		var contractsList = [];
		$.each(this.ContractsList, function (i, cont) { contractsList.push( cont.Symbol ) });
		contractsList.sort();
		$("#contract-selector").empty();
		$.each(contractsList, function (i, cont) { $("<option />", { val: cont, text: cont }).appendTo( $("#contract-selector") ) });

		if (selectedContract != null && selectedContract != "") $("#contract-selector").val(selectedContract);
	}
}

TradeHandler.prototype.DisplayTrades = function () {
	// this function will display the trades' metrics to
	//		1) Stats toolbar,
	//		2) Charts
	//		3) Completed Trades table
	
	console.log('TradeHandler -> DisplayTradesInTable');
	$.each(this.ContractsList, function(i, contract) {
		// This displays everything to screen according to which contract is the Selected Contract
		contract.DisplayToScreen();
		updateContractSummaryBar(contract);
	});
};

/*
TradeHandler.prototype.RecalcContractMetricsFromTrades = function() {
	$.each(this.ContractsList, function(i, contract) {
		contract.RecalcContractMetricsFromTrades();
	});
	this.DisplayTradesInTable();
};
*/


var Trade = function() {
	this.TradeNumber = 0;
	this.TradeFillID = "";
	this.Fills = [];
	
	// Metrics
	this.EntryDateTime = 0;
	this.ExitDateTime = 0;
	this.EntryPrice = 0;
	this.ExitPrice = 0;
	this.isFlat = true
	this.isSplit = false;
	this.isInitalLong = null;
	
	this.Position = 0;
	this.AveragePrice = 0;
	this.TheoreticalAverage = 0;
	this.TheoreticalAverageDelta = 0;

	this.ContractsTraded = 0;
	this.ContractsLong = 0;
	this.ContractsShort = 0;
	
	this.TotalBuyContracts = 0;
	this.TotalSellContracts = 0;
	this.TotalBuyPoints = 0;
	this.TotalSellPoints = 0;
	this.MaxPosition = 0;

	this.Commissions = 0;
	this.PnLRealizedPts = 0;
	this.PnLUnrealizedPts = 0;
	this.PnLRealizedDollars = 0;
	this.PnLUnrealizedDollars = 0;
	
	this.MAE = 0;
	this.MFE = 0;
	this.BSO = 0;
	this.TIT = 0;
	this.TSB = 0;
	
	// Cummulative Metrics
	this.RunningPnLPts = 0;
	this.RunningPnLDollars = 0;
	this.PercentWin = 0;
	this.PercentLoss = 0;
	this.AverageWin = 0;
	this.AverageLoss = 0;
	this.Expectancy = 0;
	this.Efficiancy = 0;
	this.PeakGain = 0;
	this.MaxDrawdown = 0;
	this.Efficiancy = 0;
	this.AverageWinDuration = 0;
	this.AverageLossDuration = 0;
	this.PercentFullStop = 0;
	this.PercentBSO = 0;
	
	// Database Stored individual Metrics
	this.TradeGrade = "";
	this.TradeEmoticon = "";
	this.TradeType = "";
};


var Contract = function(Symbol, TickSize, ContractValue, ClosingTime) {
	// Contract Details
	this.Symbol = Symbol;
	this.TickSize = TickSize;
	this.ContractValue = ContractValue;
	this.ClosingTime = ClosingTime;

	this.FillsArray = [];
	this.TradesArray = [];
	
	// Total Trades Details
	this.NumberOfTrades = 0;
	this.NumberOfWins = 0;
	this.NumberOfLosses = 0;
	this.TotalWinsPts = 0;
	this.TotalWinsDollars = 0;
	this.TotalLossesPts = 0;
	this.TotalLossesDollars = 0;
	this.TotalWinDuration = 0;
	this.TotalLossDuration = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	this.TotalFullStopTrades = 0;

	this.AverageWinDuration = 0;
	this.AverageLossDuration = 0;
	this.PercentWin = 0;
	this.PercentLoss = 0;
	
	this.MaxPosition = 0;
	this.ContractsTraded = 0;
	this.Expectancy = 0;
	this.PeakGain = 0;
	this.MaxDrawdown = 0;
	this.PercentFullStop = 0;
	this.PercentBSO = 0;
	
	this.isFlat = true;
	this.Position = 0;
	
	// References
	this.LastTrade = null;
	this.LastFill = null;
	
	// Metrics
	this.AveragePrice = 0;
	this.TheoreticalPrice = 0;
	this.TheoreticalPriceDelta = 0;
	this.MAE = 0;
	this.MFE = 0;
	this.BSO = 0;
	this.TIT = 0;
	this.TSB = 0;
	this.Commissions = 0;
	this.PnLRealizedPts = 0;
	this.PnLUnrealizedPts = 0;
	this.PnLRealizedDollars = 0;
	this.PnLUnrealizedDollars = 0;
};


Contract.prototype.UpdateTradeType = function (tradeFillID, newType) {
	console.log("UpdateTradeType ---> " + this.Symbol + " TradeID: " + tradeFillID + " newType: " + newType);
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeType = newType;
			return false;
		}
	});
};

Contract.prototype.UpdateTradeGrade = function (tradeFillID, newGrade) {
	console.log("UpdateTradeGrade ---> " + this.Symbol + " TradeID: " + tradeFillID + " newType: " + newGrade);
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeGrade = newGrade;
			return false;
		}
	});
};

Contract.prototype.UpdateTradeEmoticon = function (tradeFillID, newEmoticon) {
	console.log("UpdateTradeEmoticon ---> " + this.Symbol + " TradeID: " + tradeFillID + " newType: " + newEmoticon);
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeEmoticon = newEmoticon;
			return false;
		}
	});	
};

Contract.prototype.SortFills = function () {
	this.FillsArray.sort(function (a, b) { return a.FillID - b.FillID; });
};

Contract.prototype.AddFill = function (fill, isLive) {
	isLive = (isLive != null && isLive == true) ? true : false;
	this.LastFill = fill;

	// Push the fill into the Contract's FillsArray
	this.FillsArray.push(fill);
	
	// Find out if this fill will fit into an already open trade or if it starts opening a new trade
	if (this.LastTrade == null) {
		// An opening for a new trade	
		
		// Check the trade's TypeOfTrade metric, it should be 'O'
		if (fill.TypeOfTrade != "O") {
			if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " fill was meant to be an opening of trade, but TypeOfTrade was " + fill.TypeOfTrade + "!<br /><br />" + FillToString(fill), "error", 0);
		}
		
		// Set the initial trade metrics
		var trade = new Trade();
		trade.TradeNumber = this.TradesArray.length + 1;
		trade.TradeFillID = fill.FillID;
		trade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
		trade.EntryPrice = fill.Price;
		trade.isFlat = false;
		
		if (fill.Side == "Buy") {
			trade.isInitalLong = true;
			trade.Position = fill.Quant;
			trade.TotalBuyContracts = fill.Quant;
			trade.TotalBuyPoints = fill.Quant * fill.Price;
		} else {
			trade.isInitalLong = false;
			trade.Position = -fill.Quant;
			trade.TotalSoldContracts = fill.Quant;
			trade.TotalSellPoints = fill.Quant * fill.Price;
		}

		trade.AveragePrice = fill.Price;
		trade.TheoreticalAverage = fill.Price;
		trade.TheoreticalAverageDelta = 0;
		trade.ContractsTraded = fill.Quant;
		trade.MaxPosition = fill.Quant;

		trade.Commissions += fill.Quant * CommissionFee;
		
		trade.Fills.push(fill);
		
		if (!isLive) {
			// Calculate MFE/MAE	
			if (trade.isInitalLong) {
				trade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
			} else {
				trade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
			}
		}

		// Add trade to Contract's TradesArray
		this.TradesArray.push(trade);
		this.LastTrade = trade;
		
	} else {
		// This is part of an already open trade
		var trade = this.LastTrade;
		
		// Push fill into the Trade's Fills
		trade.Fills.push(fill);
		
		if (trade.isInitalLong) {
			// The trade is a long trade
			if (fill.Side == "Buy") {
				// Long & Buy -> Continuation Trade
				trade.ContractsTraded += fill.Quant;
				trade.TotalBuyContracts += fill.Quant;
				trade.TotalBuyPoints += fill.Quant * fill.Price;
				
				trade.Position += fill.Quant;
				trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
				trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);

				trade.Commissions += (fill.Quant * CommissionFee);
				
				/*
				if (isLive) {
					var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
				} else {
					var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts;					
				}
				
				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				*/
				
				//trade.BSO = ?;  NO BSO calculation here
				trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local() - trade.EntryDateTime;

				var newTheoreticalAverage = trade.AveragePrice;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					
				
			} else {
				// Long & Sell -> Closing, Reduction, or Split Trade
				var newPosition = trade.Position - fill.Quant;
				
				if (newPosition == 0) {
					// Flatting fill
					trade.isFlat = true;
					
					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local()
					trade.ExitPrice = fill.Price;
	
					trade.ContractsTraded += fill.Quant;
					trade.TotalSellContracts += fill.Quant;
					trade.TotalSellPoints += (fill.Quant * fill.Price);
					
					trade.Position = 0;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - trade.TotalBuyPoints);

					//trade.PnLUnrealized = 0;					
					//trade.BSO = 0;
					//trade.MAE = 0;
					//trade.MFE = 0;
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
					
					this.LastTrade = null;
				
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);

				} else if (newPosition > 0) {
					// Reduction trade
					trade.ContractsTraded += fill.Quant;
					trade.TotalSellContracts += fill.Quant;
					trade.TotalSellPoints += (fill.Quant * fill.Price);
					
					trade.Position = newPosition;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - ((trade.TotalBuyPoints / trade.TotalBuyContracts) * trade.TotalSellContracts));
					
					//trade.PnLUnrealized = 0;
					//trade.BSO = 0;
					//trade.MAE = 0;
					//trade.MFE = 0;
					trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().diff(trade.EntryDateTime);
										
					
				} else if (newPosition < 0) {
					// Split trade!

					// Check the trade's TypeOfTrade metric, it should be 'O'
					if (fill.TypeOfTrade != "S") {
						if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " fill was meant to be a split trade, but TypeOfTrade was " + fill.TypeOfTrade + "!<br /><br />" + FillToString(fill), "error", 30000);
					}
					
					var contractsToClosePosition = Math.abs(trade.Position);
					trade.isFlat = true;
					trade.isSplit = true;
					
					trade.ContractsTraded += contractsToClosePosition;
					trade.TotalSellContracts += contractsToClosePosition;
					trade.TotalSellPoints += (contractsToClosePosition * fill.Price);

					trade.Position = 0;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = trade.TotalSellPoints - trade.TotalBuyPoints;
					trade.PnLUnrealizedPts = 0;
					
					//trade.BSO = 0;
					//trade.MFE = 0;
					//trade.MAE = 0;
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
				
				
					// SPLIT TRADE INIT					
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesList.length;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.isSplit = true;

					// Set the initial trade metrics
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					
					splitTrade.isInitalLong = false;
					splitTrade.Position = newPosition;
					splitTrade.TotalSoldContracts = fill.Quant;
					splitTrade.TotalSellPoints = fill.Quant * fill.Price;
			
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;
					splitTrade.ContractsTraded = fill.Quant;
					splitTrade.MaxPosition = fill.Quant;
			
					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					splitTrade.Fills.push(fill);
					
					if (!isLive) {
						// Calculate MFE/MAE	
						if (splitTrade.isInitalLong) {
							splitTrade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
						} else {
							splitTrade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
						}
					}
			
					// Add trade to Contract's TradesArray
					this.TradesArray.push(splitTrade);
					this.LastTrade = splitTrade;
					
					
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
			

				}				
			}
		} else {
			// this is a short trade
			if (fill.Side == "Buy") {
				// Short & Buy -> Closing, Reduction, or Split Trade
				var newPosition = trade.Position - fill.Quant;
				
				if (newPosition == 0) {
					// Flatting fill
					// Flatting fill
					trade.isFlat = true;
					
					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local()
					trade.ExitPrice = fill.Price;
	
					trade.ContractsTraded += fill.Quant;
					trade.TotalBuyContracts += fill.Quant;
					trade.TotalBuyPoints += (fill.Quant * fill.Price);
					
					trade.Position = 0;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalBuyPoints - trade.TotalSellPoints);
					trade.PnLUnrealized = 0;
					
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
					
					this.LastTrade = null;
					
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
					
				} else if (newPosition < 0) {
					// Reduction trade
					trade.ContractsTraded += fill.Quant;
					trade.TotalBuyContracts += fill.Quant;
					trade.TotalBuyPoints += (fill.Quant * fill.Price);
					
					trade.Position = newPosition;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalBuyPoints - ((trade.TotalSellPoints / trade.TotalSellContracts) * trade.TotalBuyContracts));
					
					//trade.PnLUnrealized = 0;
					//trade.BSO = 0;
					//trade.MAE = 0;
					//trade.MFE = 0;
					trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().diff(trade.EntryDateTime);

					
					
				} else if (newPosition > 0) {
					// Split trade!
					
					// Check the trade's TypeOfTrade metric, it should be 'O'
					if (fill.TypeOfTrade != "S") {
						if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " fill was meant to be a split trade, but TypeOfTrade was " + fill.TypeOfTrade + "!<br /><br />" + FillToString(fill), "error", 30000);
					}
					
					var contractsToClosePosition = Math.abs(trade.Position);
					trade.isFlat = true;
					trade.isSplit = true;
					
					trade.ContractsTraded += contractsToClosePosition;
					trade.TotalBuyContracts += contractsToClosePosition;
					trade.TotalBuyPoints += (contractsToClosePosition * fill.Price);

					trade.Position = 0;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = trade.TotalBuyPoints - trade.TotalSellPoints;
					trade.PnLUnrealizedPts = 0;
					
					//trade.BSO = 0;
					//trade.MFE = 0;
					//trade.MAE = 0;
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
				
				
					// SPLIT TRADE INIT					
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesList.length;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.isSplit = true;

					// Set the initial trade metrics
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					
					splitTrade.isInitalLong = false;
					splitTrade.Position = newPosition;
					splitTrade.TotalSoldContracts = fill.Quant;
					splitTrade.TotalSellPoints = fill.Quant * fill.Price;
			
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;
					splitTrade.ContractsTraded = fill.Quant;
					splitTrade.MaxPosition = fill.Quant;
			
					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					splitTrade.Fills.push(fill);
					
					if (!isLive) {
						// Calculate MFE/MAE	
						if (splitTrade.isInitalLong) {
							splitTrade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
						} else {
							splitTrade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
						}
					}
			
					// Add trade to Contract's TradesArray
					this.TradesArray.push(splitTrade);
					this.LastTrade = splitTrade;
					
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
							
				}
			} else {
				// Short & Sell -> This is a continuation trade
				trade.ContractsTraded += fill.Quant;
				trade.TotalSellContracts += fill.Quant;
				trade.TotalSellPoints += fill.Quant * fill.Price;
				
				trade.Position += -fill.Quant;
				trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
				trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);

				trade.Commissions += (fill.Quant * CommissionFee);
				
				/*
				if (isLive) {
					var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
				} else {
					var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts;					
				}
				
				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				*/
				
				//trade.BSO = ?;  NO BSO calculation here
				trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local() - trade.EntryDateTime;

				var newTheoreticalAverage = trade.AveragePrice;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					
				
				
			}
			
		}
	}
	
};

Contract.prototype.AddFill2 = function (fill, recalculate) {
	var isLive = (recalculate != null && recalculate == true) ? true : false;
	this.LastFill = fill;

	console.log("adding live fill ID: " + fill.FillID);
	this.FillsArray.push(fill);
	
	if (this.LastTrade == null) {
		// This fill will form a new trade
		var trade = new Trade();
		trade.TradeNumber = this.TradesArray.length + 1;
		trade.TradeFillID = fill.FillID;
		trade.EntryDateTime = moment.utc(fill.TimestampUTC);
		trade.EntryPrice = fill.Price;
		trade.isFlat = false;
		trade.isInitalLong = fill.Side == "Buy" ? true : false;

		trade.Position = fill.Side == "Buy" ? fill.Quant : -fill.Quant;
		trade.AveragePrice = fill.Price;
		trade.TheoreticalAverage = fill.Price;
		trade.TheoreticalAverageDelta = 0;
		trade.ContractsTraded += fill.Quant;
		trade.ContractsLong += fill.Side == "Buy" ? fill.Quant : 0;
		trade.ContractsShort += fill.Side == "Buy" ? 0 : fill.Quant;
		trade.MaxPosition = fill.Quant;

		trade.Commissions += fill.Quant * CommissionFee;
		
		this.TradesArray.push(trade);
		this.LastTrade = trade;
		
		if (!isLive) {
			// Calculate MFE/MAE	
			if (trade.isInitalLong) {
				trade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
			} else {
				trade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
			}
		}

		if (isLive) updateContractSummaryBar(this);

	} else {
		// This fill will continue a trade
		trade = this.LastTrade;
		
		if (trade.isInitalLong) {
			if (fill.Side == "Buy") {
				console.log("LONG & BUY");
				
				trade.isFlat = false;
				
				trade.Position += fill.Quant;
				trade.AveragePrice = ((trade.AveragePrice * trade.ContractsTraded) + (fill.Quant * fill.Price)) / (trade.ContractsTraded + fill.Quant);
				trade.MaxPosition = Math.max(trade.Position, trade.MaxPosition);
				
				trade.ContractsTraded += fill.Quant;
				trade.ContractsLong += fill.Quant;

				trade.Commissions += fill.Quant * CommissionFee;
				
				if (isLive) {
					var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
				} else {
					var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts;					
				}
				
				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				
				trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
				trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
				
				//trade.BSO = ?;  NO BSO calculation here
				trade.TIT = (fill.TimestampUTC - trade.EntryDateTime);

				var newTheoreticalAverage = trade.AveragePrice;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					
				
				if (isLive) {
					updateContractSummaryBar(this);
				}
									
			} else {
				console.log("LONG & SELL");

				// Update PnL
				var newPosition = trade.Position - fill.Quant;

				if (newPosition == 0) {
					// is Flat Now					
					trade.isFlat = true;						

					trade.ExitDateTime = moment.utc(fill.TimestampUTC);
					trade.ExitPrice = fill.Price;
								
					trade.ContractsShort += fill.Quant;
					trade.ContractsTraded += fill.Quant;

					trade.Commissions += (fill.Quant * CommissionFee);
					
					trade.PnLRealizedPts += (fill.Quant * (fill.Price - trade.AveragePrice));
					trade.PnLRealizedDollars += (fill.Quant * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * fill.Quant);
					trade.PnLUnrealizedPts = 0;
					trade.PnLUnrealizedDollars = 0;

					var MFEonThisFill = trade.PnLRealizedPts > 0 ? Math.abs(trade.PnLRealizedPts) : 0,
						MAEonThisFill = trade.PnLRealizedPts < 0 ? Math.abs(trade.PnLRealizedPts) : 0;
				
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ??	NO BSO calculation here
					trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);
					trade.Position = 0;
					
					this.LastTrade = null;
					
					/* TRADE CLOSED -> This is for contract updating */
					this.UpdateMetricsWithTrade(trade);

					if (isLive) {
						updateContractSummaryBar(this);
						this.DisplayToScreen();
					}
					

				} else if (newPosition > 0) {
					// Is still in trade
					trade.isFlat = false;
					
					trade.Position = newPosition;
					trade.ContractsTraded += fill.Quant;
					trade.ContractsShort += fill.Quant;
					
					trade.Commissions += fill.Quant * CommissionFee;
					trade.PnLRealizedPts += (fill.Quant * (fill.Price - trade.AveragePrice));
					trade.PnLRealizedDollars += (fill.Quant * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * fill.Quant);

					if (isLive) {
						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts;					
					}
				
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					trade.BSO = Math.max(trade.BSO, (fill.Price - trade.AveragePrice));
					trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

					var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					

					if (isLive) {
						updateContractSummaryBar(this);
					}
										
					
				} else {
					// Is a split trade
					// Update current Trade
					trade.isFlat = true;
					trade.isSplit = true;

					trade.ExitDateTime = moment.utc(fill.TimestampUTC);
					trade.ExitPrice = fill.Price;

					trade.ContractsTraded += Math.abs(trade.Position);
					trade.ContractsShort += Math.abs(trade.Position);

					trade.Commissions += Math.abs(trade.Position) * CommissionFee;


					trade.PnLRealizedPts += (Math.abs(trade.Position) * (fill.Price - trade.AveragePrice));
					trade.PnLRealizedDollars += (Math.abs(trade.Position) * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * Math.abs(trade.Position));
					trade.PnLUnrealizedPts = 0;
					trade.PnLUnrealizedDollars = 0;

					var MFEonThisFill = trade.PnLRealizedPts > 0 ? Math.abs(trade.PnLRealizedPts) : 0,
						MAEonThisFill = trade.PnLRealizedPts < 0 ? Math.abs(trade.PnLRealizedPts) : 0;
				
					trade.MFE = Math.max( trade.MFE , MFEonThisFill);
					trade.MAE = Math.max( trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ??  NO BSO calculation on closing trade
					trade.TIT = trade.ExitDateTime - trade.EntryDateTime;
					
					var newTheoreticalAverage = trade.AveragePrice + trade.PnLRealizedPts;
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					

					trade.Position = 0;
					
					/* TRADE CLOSED -> This is for contract updating */
					this.UpdateMetricsWithTrade(trade);
					if (isLive) {
						updateContractSummaryBar(this);
						this.DisplayToScreen();
					}
								
					// Create new Trade
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;						
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC);
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					splitTrade.isInitalLong = newPosition < 0 ? false : true;
											
					splitTrade.Position = newPosition;
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;						
					splitTrade.ContractsTraded += Math.abs(newPosition);
					splitTrade.ContractsShort += Math.abs(newPosition);						
					splitTrade.MaxPosition = Math.abs(newPosition);

					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					if (isLive) {
						var MFEonThisFill = 0;
							MAEonThisFill = 0;
					} else {
						var MFEonThisFill = splitTrade.isInitalLong ? ((fill.HighSince - fill.Price) * fill.Quant) : ((fill.Price - fill.LowSince) * fill.Quant);
							MAEonThisFill = splitTrade.isInitalLong ? ((fill.Price - fill.LowSince) * fill.Quant) : ((fill.HighSince - fill.Price) * fill.Quant);
					}

					splitTrade.MFE = MFEonThisFill;
					splitTrade.MAE = MAEonThisFill;
					splitTrade.PeakGain = splitTrade.MFE * this.ContractValue - trade.Commissions;
					splitTrade.MaxDrawdown = splitTrade.MAE * this.ContractValue + trade.Commissions;

					splitTrade.Fills.push(fill);
					
					this.TradesArray.push(splitTrade);
					this.LastTrade = splitTrade;
					
					if (isLive) {
						updateContractSummaryBar(this);
						this.DisplayToScreen();
					}
					

					
				}
				
				
			}
		} else {
			
			if (fill.Side == "Buy") {
				console.log("SHORT & BUY");
				
				// Update PnL
				var newPosition = trade.Position + fill.Quant;

				if (newPosition == 0) {
					// is Flat Now
					trade.isFlat = true;						

					trade.ExitDateTime = moment.utc(fill.TimestampUTC);
					trade.ExitPrice = fill.Price;

					trade.ContractsLong += fill.Quant;
					trade.ContractsTraded += fill.Quant;

					trade.Commissions += (fill.Quant * CommissionFee);
					
					trade.PnLRealizedPts += (fill.Quant * (trade.AveragePrice - fill.Price));
					trade.PnLRealizedDollars += (fill.Quant * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * fill.Quant);
					trade.PnLUnrealizedPts = 0;
					trade.PnLUnrealizedDollars = 0;

					var MFEonThisFill = trade.PnLRealizedPts > 0 ? Math.abs(trade.PnLRealizedPts) : 0,
						MAEonThisFill = trade.PnLRealizedPts < 0 ? Math.abs(trade.PnLRealizedPts) : 0;
				
					trade.MFE = Math.max( trade.MFE , MFEonThisFill);
					trade.MAE = Math.max( trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ??  NO BSO calculation on closing trade
					trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

					trade.Position = 0;	
					this.LastTrade = null;

					/* TRADE CLOSED -> This is for contract updating */
					this.UpdateMetricsWithTrade(trade);
					if (isLive) {
						updateContractSummaryBar(this);
						this.DisplayToScreen();
					}
					
					
					
				} else if (newPosition < 0) {
					// Is still in trade
					trade.isFlat = false;
					
					trade.Position = newPosition;
					trade.ContractsTraded += fill.Quant;
					trade.ContractsLong += fill.Quant;
					
					trade.Commissions += fill.Quant * CommissionFee;
					trade.PnLRealizedPts += (fill.Quant * (trade.AveragePrice - fill.Price));
					trade.PnLRealizedDollars += (fill.Quant * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * fill.Quant);

					if (isLive) {
						var MAEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MFEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
					} else {
						var MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts;					
					}
				
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					trade.BSO = Math.max(trade.BSO, (trade.AveragePrice - fill.Price));
					trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

					var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					
					
					if (isLive) {
						updateContractSummaryBar(this);
					}
					

				} else {
					// Is a split trade
					// Update current Trade
					trade.isFlat = true;
					trade.isSplit = true;

					trade.ExitDateTime = moment.utc(fill.TimestampUTC);
					trade.ExitPrice = fill.Price;

					trade.ContractsTraded += Math.abs(trade.Position);
					trade.ContractsLong += Math.abs(trade.Position);

					trade.Commissions += Math.abs(trade.Position) * CommissionFee;


					trade.PnLRealizedPts += (Math.abs(trade.Position) * (trade.AveragePrice - fill.Price));
					trade.PnLRealizedDollars += (Math.abs(trade.Position) * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * Math.abs(trade.Position));
					trade.PnLUnrealizedPts = 0;
					trade.PnLUnrealizedDollars = 0;

					var MFEonThisFill = trade.PnLRealizedPts > 0 ? Math.abs(trade.PnLRealizedPts) : 0,
						MAEonThisFill = trade.PnLRealizedPts < 0 ? Math.abs(trade.PnLRealizedPts) : 0;
				
					trade.MFE = Math.max( trade.MFE , MFEonThisFill);
					trade.MAE = Math.max( trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ??  NO BSO on closing trade
					trade.TIT = trade.ExitDateTime - trade.EntryDateTime;

					var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					
					trade.Position = 0;
					
					/* TRADE CLOSED -> This is for contract updating */
					this.UpdateMetricsWithTrade(trade);
					if (isLive) {
						updateContractSummaryBar(this);
						this.DisplayToScreen();
					}
					
				
					// Create new Trade
					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;						
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC);
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					splitTrade.isInitalLong = newPosition < 0 ? false : true;
											
					splitTrade.Position = newPosition;
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;
					splitTrade.ContractsTraded += Math.abs(newPosition);
					splitTrade.ContractsShort += Math.abs(newPosition);						
					splitTrade.MaxPosition = Math.abs(newPosition);

					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					if (isLive) {
						var MFEonThisFill = 0;
							MAEonThisFill = 0;
					} else {
						var MAEonThisFill = splitTrade.isInitalLong ? ((fill.HighSince - fill.Price) * fill.Quant) : ((fill.Price - fill.LowSince) * fill.Quant);
							MFEonThisFill = splitTrade.isInitalLong ? ((fill.Price - fill.LowSince) * fill.Quant) : ((fill.HighSince - fill.Price) * fill.Quant);
					}

					splitTrade.MFE = MFEonThisFill;
					splitTrade.MAE = MAEonThisFill;
					splitTrade.PeakGain = splitTrade.MFE * this.ContractValue - trade.Commissions;
					splitTrade.MaxDrawdown = splitTrade.MAE * this.ContractValue + trade.Commissions;

					splitTrade.Fills.push(fill);
					
					this.TradesArray.push(splitTrade);
					this.LastTrade = splitTrade;

					if (isLive) {
						updateContractSummaryBar(this);
					}
					

				}
								
			} else {
				console.log("SHORT & SELL");
				trade.isFlat = false;
				
				trade.Position -= fill.Quant;
				trade.AveragePrice = ((trade.AveragePrice * trade.ContractsTraded) + (fill.Quant * fill.Price)) / (trade.ContractsTraded + fill.Quant);
				trade.MaxPosition = Math.min(trade.Position, trade.MaxPosition);
				
				trade.ContractsTraded += fill.Quant;
				trade.ContractsShort += fill.Quant;

				trade.Commissions += fill.Quant * CommissionFee;
				
				if (isLive) {
					var MAEonThisFill = ((fill.Price - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
						MFEonThisFill = ((trade.AveragePrice - fill.Price) * trade.Position) + trade.PnLRealizedPts;
				} else {
					var MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
						MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * trade.Position) + trade.PnLRealizedPts;					
				}
			
				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				
				trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
				trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
				//trade.BSO = ?;  NO BSO on continuation trades
				trade.TIT = (fill.TimestampUTC - trade.EntryDateTime);
				
				var newTheoreticalAverage = trade.AveragePrice;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					

				if (isLive) {
					updateContractSummaryBar(this);
				}

			}
		}

		trade.Fills.push(fill);
	}
};

Contract.prototype.UpdateMetricsWithTrade = function (trade) {
	// This msg now let's Display To Screen recalculate everything on the fly
	this.DisplayToScreen();
	return;
	
	this.NumberOfTrades++;	
	if (trade.PnLRealizedDollars > 0) {
		// WIN
		this.NumberOfWins++;
		this.TotalWinsPts += trade.PnLRealizedPts;
		this.TotalWinsDollars += trade.PnLRealizedDollars;
		this.TotalWinDuration += trade.TIT;
		
	} else if (trade.PnLRealizedDollars < 0) {
		// LOSS
		this.NumberOfLosses++;
		this.TotalLossesPts += trade.PnLRealizedPts;
		this.TotalLossesDollars += trade.PnLRealizedDollars;
		this.TotalLossDuration += trade.TIT;
		
	} else {
		// FLAT		
	}
	
	// Time Stand By (TSB) calculation
	if (this.TradesArray.length > 1) trade.TSB = trade.EntryDateTime - this.TradesArray[this.TradesArray.length-2].ExitDateTime;
	
	trade.AverageWinDuration = this.TotalWinDuration / this.NumberOfWins;
	this.AverageWinDuration = this.TotalWinDuration / this.NumberOfWins;
	trade.AverageLossDuration = this.TotalLossDuration / this.NumberOfLosses;
	this.AverageLossDuration = this.TotalLossDuration / this.NumberOfLosses;
	trade.PercentWin = this.PercentWin = this.NumberOfWins / this.NumberOfTrades;
	trade.PercentLoss = this.PercentLoss = this.NumberOfLosses / this.NumberOfTrades;
	trade.Efficiancy = (trade.PnLRealizedPts / trade.MFE);
	
	if (ResultsDisplayType != null && (ResultsDisplayType == "Points" || ResultsDisplayType == "Ticks")) {
		trade.AverageWin = this.TotalWinsPts / this.NumberOfWins;
		trade.AverageLoss = this.TotalLossesPts / this.NumberOfLosses;
		trade.Expectancy = (this.TotalWinsPts + this.TotalLossesPts) / this.NumberOfTrades;
	} else {
		trade.AverageWin = this.TotalWinsDollars / this.NumberOfWins;
		trade.AverageLoss = this.TotalLossDollars / this.NumberOfLosses;	
		trade.Expectancy = (this.TotalWinsDollars + this.TotalLossesDollars) / this.NumberOfTrades;		
	}


	this.MaxPosition = ( trade.MaxPosition > this.MaxPosition ? trade.MaxPosition : this.MaxPosition );
	this.ContractsTraded += trade.ContractsTraded;
	
	this.PnLRealizedPts += trade.PnLRealizedPts;
	this.PnLRealizedDollars += trade.PnLRealizedDollars;
	
	trade.RunningPnLPts = this.PnLRealizedPts;
	trade.RunningPnLDollars = this.PnLRealizedDollars;
	
	this.Expectancy = trade.Expectancy;
	//trade.PercentFullStop = this.PercentFullStop = 0;
	//trade.PercentBSO = this.PercentBSO = 0;
	
	this.isFlat = trade.isFlat;
	this.Position = trade.Position;
	
	// Display the trade metrics to screen IFF (this contract is the selectedContract)
	this.DisplayToScreen();

};

Contract.prototype.UpdatePrice = function (MarketPrice) {
	var trade = this.LastTrade;
	if (trade != null) {
		var PnLUnrealized = trade.Position * (MarketPrice - trade.AveragePrice);	
		trade.PnLUnrealized = PnLUnrealized;
		this.PnLUnrealized = PnLUnrealized;
		
		if (trade.isInitalLong) {
			var MFEonPriceUpdate = ((MarketPrice - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
				MAEonPriceUpdate = ((trade.AveragePrice - MarketPrice) * trade.Position) + trade.PnLRealizedPts;
				
			trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
			trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);
			
			trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
			trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;

		} else {
			var MAEonPriceUpdate = ((MarketPrice - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
				MFEonPriceUpdate = ((trade.AveragePrice - MarketPrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
				
			trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
			trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);
			
			trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
			trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
		}
		
		trade.TIT = moment.utc() - trade.EntryDateTime;

		// Update data bars		
		updateContractSummaryBar (this);
	}
	
	if (this.LastFill != null) {
		if (MarketPrice > this.LastFill.HighSince) this.LastFill.HighSince = MarketPrice;
		if (MarketPrice < this.LastFill.LowSince) this.LastFill.LowSince = MarketPrice;	
	}
};

Contract.prototype.RecalcContractMetricsFromTrades = function () {
	console.log('DEPRICATED calling contract recalc from trades');
	// Depricated
	return;

	// Reset metrics
	this.NumberOfTrades = 0;
	this.NumberOfWins = 0;
	this.NumberOfLosses = 0;
	this.TotalWinsPts = 0;
	this.TotalWinsDollars = 0;
	this.TotalLossesPts = 0;
	this.TotalLossesDollars = 0;
	this.TotalWinDuration = 0;
	this.TotalLossDuration = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	this.TotalFullStopTrades = 0;
	this.AverageWinDuration = 0;
	this.AverageLossDuration = 0;
	this.PercentWin = 0;
	this.PercentLoss = 0;
	
	this.MaxPosition = 0;
	this.ContractsTraded = 0;
	this.Expectancy = 0;
	this.PeakGain = 0;
	this.MaxDrawdown = 0;
	this.PercentFullStop = 0;
	this.PercentBSO = 0;
	
	this.isFlat = true;
	this.Position = 0;

	this.AveragePrice = 0;
	this.TheoreticalPrice = 0;
	this.TheoreticalPriceDelta = 0;
	this.MAE = 0;
	this.MFE = 0;
	this.BSO = 0;
	this.TIT = 0;
	this.TSB = 0;
	this.Commissions = 0;
	this.PnLRealized = 0;
	this.PnLUnrealized = 0;
//	this.PnLRealizedDollars = 0;
//	this.PnLUnrealizedDollars = 0;
	
	// Reset ALL Trade Metrics
	for (var i = 0; i < this.TradesArray.length; i++) {
		var trade = this.TradesArray[i];
		// Only update data if the trade is complete
		if (trade.isFlat) {
			
			/* Recalc Dollar values based on commissions */
			if (ResultsDisplayType == "Dollars") {
				var totalCommission = trade.ContractsTraded * CommissionFee;
				trade.PnLRealizedDollars = trade.PnLRealizedPts * this.ContractValue;
				trade.PnLRealizedDollars -= totalCommission;
				
				trade.PeakGain =  Math.max(trade.PeakGain - totalCommission, 0);
				trade.MaxDrawdown += totalCommission;
			}
			
			this.NumberOfTrades++;	
			if (trade.PnLRealizedDollars > 0) {
				// WIN
				this.NumberOfWins++;
				this.TotalWinsPts += trade.PnLRealizedPts;
				this.TotalWinsDollars += trade.PnLRealizedDollars;
				this.TotalWinDuration += trade.TIT;
				
			} else if (trade.PnLRealizedDollars < 0) {
				// LOSS
				this.NumberOfLosses++;
				this.TotalLossesPts += trade.PnLRealizedPts;
				this.TotalLossesDollars += trade.PnLRealizedDollars;
				this.TotalLossDuration += trade.TIT;
				
				if (trade.BSO == 0) {
					// FULL STOP calculation -> % Full Stop = trades that had no BSO and were exited at a loss / total number of trades
					this.TotalFullStopTrades++;
					this.PercentFullStop = this.TotalFullStopTrades / this.NumberOfTrades;
				}
				
			} else {
				// FLAT		
			}
			
			if (trade.MaxPosition > 1) this.TotalBSOEligibleTrades++;

			if (trade.BSO > 0) {
				// BSO calculation	-> % BSO = closed trades with more than one contract that were scaled out for a gain / total trades that could have been scaled out
				this.TotalBSOTrades++;
				this.PercentBSO = this.TotalBSOTrades / this.TotalBSOEligibleTrades;
			}
			
			
			// Time Stand By (TSB) calculation
			if (i > 0) trade.TSB = trade.EntryDateTime - this.TradesArray[i-1].ExitDateTime;
			
			trade.AverageWinDuration = this.AverageWinDuration = this.TotalWinDuration / this.NumberOfWins;
			trade.AverageWinDuration = this.AverageLossDuration = this.TotalLossDuration / this.NumberOfLosses;
			trade.PercentWin = this.PercentWin = this.NumberOfWins / this.NumberOfTrades;
			trade.PercentLoss = this.PercentLoss = this.NumberOfLosses / this.NumberOfTrades;
			trade.Efficiancy = (trade.PnLRealizedPts / trade.MFE);
			
			if (ResultsDisplayType != null && (ResultsDisplayType == "Points" || ResultsDisplayType == "Ticks")) {
				trade.AverageWin = this.TotalWinsPts / this.NumberOfWins;
				trade.AverageLoss = this.TotalLossesPts / this.NumberOfLosses;
				trade.Expectancy = (this.TotalWinsPts + this.TotalLossesPts) / this.NumberOfTrades;
			} else {
				trade.AverageWin = this.TotalWinsDollars / this.NumberOfWins;
				trade.AverageLoss = this.TotalLossDollars / this.NumberOfLosses;	
				trade.Expectancy = (this.TotalWinsDollars + this.TotalLossesDollars) / this.NumberOfTrades;		
			}
	
	
			this.MaxPosition = ( trade.MaxPosition > this.MaxPosition ? trade.MaxPosition : this.MaxPosition );
			this.ContractsTraded += trade.ContractsTraded;
			
			this.PnLRealizedPts += trade.PnLRealizedPts;
			this.PnLRealizedDollars += trade.PnLRealizedDollars;
			
			trade.RunningPnLPts = this.PnLRealizedPts;
			trade.RunningPnLDollars = this.PnLRealizedDollars;
			
			this.Expectancy = trade.Expectancy;
			//trade.PercentFullStop = this.PercentFullStop = 0;
			//trade.PercentBSO = this.PercentBSO = 0;
		}

		this.isFlat = trade.isFlat;
		this.Position = trade.Position;
	}

	updateContractSummaryBar(this);
	this.DisplayToScreen();
};


/*
Contract.prototype.RecalcFromFills = function () {
	this.SortFills();
	this.TradesArray = [];
	this.LastTrade = null;
	
	this.NumberOfTrades = 0;
	
	// New Metrics 
	for (var i = 0; i < this.FillsArray.length; i++) {
		var fill = this.FillsArray[i];
		if (this.LastTrade == null) {
			// first Trade
			var trade = new Trade();
			trade.TradeNumber = this.TradesArray.length + 1;
			trade.TradeFillID = fill.FillID;
			trade.EntryDateTime = moment.utc(fill.TimestampUTC);
			trade.EntryPrice = fill.Price;
			trade.isFlat = false;
			trade.isInitalLong = fill.Side == "Buy" ? true : false;

			trade.Position = fill.Side == "Buy" ? fill.Quant : -fill.Quant;
			trade.AveragePrice = fill.Price;
			trade.TheoreticalAverage = fill.Price;
			trade.TheoreticalAverageDelta = 0;
			trade.ContractsTraded = fill.Quant;
			trade.ContractsLong = fill.Side == "Buy" ? fill.Quant : 0;
			trade.ContractsShort = fill.Side == "Buy" ? 0 : fill.Quant;
			trade.ProceedsBought = fill.Side == "Buy" ? (fill.Quant * fill.Price) : 0;
			trade.ProceedsSold = fill.Side == "Buy" ? 0 : (fill.Quant * fill.Price);
			trade.MaxPosition = fill.Quant;

			trade.Commissions = fill.Quant * CommissionFee;

			trade.MFE = trade.isInitalLong ? ((fill.HighSince - fill.Price) * fill.Quant) : ((fill.Price - fill.LowSince) * fill.Quant);
			trade.MAE = trade.isInitalLong ? ((fill.Price - fill.LowSince) * fill.Quant) : ((fill.HighSince - fill.Price) * fill.Quant);
			trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
			trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;			
			
			this.TradesArray.push(trade);
			this.LastTrade = trade;

		} else {

			trade = this.LastTrade;

			if (trade.isInitalLong) {
				if (fill.Side == "Buy") {
					console.log("LONG & BUY");
					
					trade.isFlat = false;
					
					trade.Position += fill.Quant;
					trade.AveragePrice = ((trade.AveragePrice * trade.ContractsTraded) + (fill.Quant * fill.Price)) / (trade.ContractsTraded + fill.Quant);
					trade.MaxPosition = Math.max(trade.Position, trade.MaxPosition);
					
					trade.ContractsTraded += fill.Quant;
					trade.ContractsLong += fill.Quant;
					trade.ProceedsBought += (fill.Quant * fill.Price);

					trade.Commissions += fill.Quant * CommissionFee;
					
					var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * trade.Position) + trade.PnLRealizedPts;
				
					trade.MFE = Math.max( trade.MFE , MFEonThisFill);
					trade.MAE = Math.max( trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ?;
					trade.TIT = (fill.TimestampUTC - trade.EntryDateTime);

					var newTheoreticalAverage = trade.AveragePrice;
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					
										
				} else {
					console.log("LONG & SELL");

					// Update PnL
					var newPosition = trade.Position - fill.Quant;

					if (newPosition == 0) {
						// is Flat Now					
						trade.isFlat = true;						

						trade.ExitDateTime = moment.utc(fill.TimestampUTC);
						trade.ExitPrice = fill.Price;

						trade.Position = 0;											
						trade.ContractsShort += fill.Quant;
						trade.ContractsTraded += fill.Quant;
						trade.ProceedsSold += (fill.Quant * fill.Price);
						
						trade.Commissions += (fill.Quant * CommissionFee);
						
						trade.PnLRealizedPts += (fill.Quant * (fill.Price - trade.AveragePrice));
						trade.PnLRealizedDollars += (fill.Quant * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * fill.Quant);


						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);
					
						this.LastTrade = null;

					} else if (newPosition > 0) {
						// Is still in trade
						trade.isFlat = false;
						
						trade.Position = newPosition;
						trade.ContractsTraded += fill.Quant;
						trade.ContractsShort += fill.Quant;
						trade.ProceedsSold += (fill.Quant * fill.Price);
						
						trade.Commissions += fill.Quant * CommissionFee;
						trade.PnLRealizedPts += (fill.Quant * (fill.Price - trade.AveragePrice));
						trade.PnLRealizedDollars += (fill.Quant * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * fill.Quant);

						var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

						var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
						trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
						trade.TheoreticalAverage = newTheoreticalAverage;					
						

					} else {
						// Is a split trade
						// Update current Trade
						trade.isFlat = true;
						trade.isSplit = true;

						trade.ExitDateTime = moment.utc(fill.TimestampUTC);
						trade.ExitPrice = fill.Price;

						trade.Position = 0;
						trade.ContractsTraded += Math.abs(trade.Position);
						trade.ContractsShort += Math.abs(trade.Position);
						trade.ProceedsSold += (fill.Quant * fill.Price);
						
						trade.Commissions += Math.abs(trade.Position) * CommissionFee;


						trade.PnLRealizedPts += (Math.abs(trade.Position) * (fill.Price - trade.AveragePrice));
						trade.PnLRealizedDollars += (Math.abs(trade.Position) * (fill.Price - trade.AveragePrice)) * this.ContractValue - (CommissionFee * Math.abs(trade.Position));

						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = trade.ExitDateTime - trade.EntryDateTime;
						
						var newTheoreticalAverage = trade.AveragePrice + trade.PnLRealizedPts;
						trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
						trade.TheoreticalAverage = newTheoreticalAverage;					
						
						// Create new Trade
						var splitTrade = new Trade();
						splitTrade.TradeNumber = this.TradesArray.length + 1;
						splitTrade.TradeFillID = fill.FillID;						
						splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC);
						splitTrade.EntryPrice = fill.Price;
						splitTrade.isFlat = false;
						splitTrade.isInitalLong = newPosition < 0 ? false : true;
						splitTrade.ProceedsBought = (fill.Quant * fill.Price);
												
						splitTrade.Position = newPosition;
						splitTrade.AveragePrice = trade.AveragePrice;
						splitTrade.TheoreticalAverage = fill.Price;
						splitTrade.TheoreticalAverageDelta = 0;						
						splitTrade.ContractsTraded += Math.abs(newPosition);
						splitTrade.ContractsShort += Math.abs(newPosition);						
						splitTrade.MaxPosition = Math.abs(newPosition);

						splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;


						splitTrade.MFE = trade.isInitalLong ? ((fill.HighSince - fill.Price) * fill.Quant) : ((fill.Price - fill.LowSince) * fill.Quant);
						splitTrade.MAE = trade.isInitalLong ? ((fill.Price - fill.LowSince) * fill.Quant) : ((fill.HighSince - fill.Price) * fill.Quant);
						splitTrade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						splitTrade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;

						splitTrade.Fills.push(fill);
						
						this.TradesArray.push(splitTrade);
						this.LastTrade = splitTrade;
					}
					
					
				}
			} else {
				
				if (fill.Side == "Buy") {
					console.log("SHORT & BUY");
					
					// Update PnL
					var newPosition = trade.Position + fill.Quant;

					if (newPosition == 0) {
						// is Flat Now
						trade.isFlat = true;						

						trade.ExitDateTime = moment.utc(fill.TimestampUTC);
						trade.ExitPrice = fill.Price;

						trade.Position = 0;											
						trade.ContractsLong += fill.Quant;
						trade.ContractsTraded += fill.Quant;
						trade.ProceedsBought += (fill.Quant * fill.Price);
						
						trade.Commissions += (fill.Quant * CommissionFee);
						
						trade.PnLRealizedPts += (fill.Quant * (trade.AveragePrice - fill.Price));
						trade.PnLRealizedDollars += (fill.Quant * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * fill.Quant);

						var MAEonThisFill = ((fill.Price - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MFEonThisFill = ((trade.AveragePrice - fill.Price) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

						this.LastTrade = null;
						
					} else if (newPosition > 0) {
						// Is still in trade
						trade.isFlat = false;
						
						trade.Position = newPosition;
						trade.ContractsTraded += fill.Quant;
						trade.ContractsLong += fill.Quant;
						trade.ProceedsBought += (fill.Quant * fill.Price);						
						
						trade.Commissions += fill.Quant * CommissionFee;
						trade.PnLRealizedPts += (fill.Quant * (trade.AveragePrice - fill.Price));
						trade.PnLRealizedDollars += (fill.Quant * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * fill.Quant);

						var MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = moment.utc(fill.TimestampUTC).diff(trade.EntryDateTime);

						var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
						trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
						trade.TheoreticalAverage = newTheoreticalAverage;					
						

					} else {
						// Is a split trade
						// Update current Trade
						trade.isFlat = true;
						trade.isSplit = true;

						trade.ExitDateTime = moment.utc(fill.TimestampUTC);
						trade.ExitPrice = fill.Price;

						trade.ContractsTraded += Math.abs(trade.Position);
						trade.ContractsLong += Math.abs(trade.Position);
						trade.ProceedsBought += (Math.abs(trade.Position) * fill.Price);
												
						trade.Commissions += Math.abs(trade.Position) * CommissionFee;

						trade.PnLRealizedPts += (Math.abs(trade.Position) * (trade.AveragePrice - fill.Price));
						trade.PnLRealizedDollars += (Math.abs(trade.Position) * (trade.AveragePrice - fill.Price)) * this.ContractValue - (CommissionFee * Math.abs(trade.Position));

						var MAEonThisFill = ((fill.Price - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
							MFEonThisFill = ((trade.AveragePrice - fill.Price) * trade.Position) + trade.PnLRealizedPts;
					
						trade.MFE = Math.max( trade.MFE , MFEonThisFill);
						trade.MAE = Math.max( trade.MAE , MAEonThisFill);
						
						trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
						//trade.BSO = ??
						trade.TIT = trade.ExitDateTime - trade.EntryDateTime;

						var newTheoreticalAverage = trade.AveragePrice - (trade.PnLRealizedPts/newPosition);
						trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
						trade.TheoreticalAverage = newTheoreticalAverage;					

						trade.Position = 0;
						
						// Create new Trade
						var splitTrade = new Trade();
						splitTrade.TradeNumber = this.TradesArray.length + 1;
						splitTrade.TradeFillID = fill.FillID;						
						splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC);
						splitTrade.EntryPrice = fill.Price;
						splitTrade.isFlat = false;
						splitTrade.isInitalLong = newPosition < 0 ? false : true;
						splitTrade.ProceedsBought += (newPosition * fill.Price);
						
						splitTrade.Position = newPosition;
						splitTrade.AveragePrice = trade.AveragePrice;
						splitTrade.TheoreticalAverage = fill.Price;
						splitTrade.TheoreticalAverageDelta = 0;
						splitTrade.ContractsTraded += Math.abs(newPosition);
						splitTrade.ContractsShort += Math.abs(newPosition);						
						splitTrade.MaxPosition = Math.abs(newPosition);

						splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

						splitTrade.MFE = trade.isInitalLong ? ((fill.HighSince - fill.Price) * fill.Quant) : ((fill.Price - fill.LowSince) * fill.Quant);
						splitTrade.MAE = trade.isInitalLong ? ((fill.Price - fill.LowSince) * fill.Quant) : ((fill.HighSince - fill.Price) * fill.Quant);
						splitTrade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
						splitTrade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;

						splitTrade.Fills.push(fill);
						
						this.TradesArray.push(splitTrade);
						this.LastTrade = splitTrade;
					}
									
				} else {
					console.log("SHORT & SELL");
					trade.isFlat = false;
					
					trade.Position -= fill.Quant;
					trade.AveragePrice = ((trade.AveragePrice * trade.ContractsTraded) + (fill.Quant * fill.Price)) / (trade.ContractsTraded + fill.Quant);
					trade.MaxPosition = Math.min(trade.Position, trade.MaxPosition);
					
					trade.ContractsTraded += fill.Quant;
					trade.ContractsShort += fill.Quant;
					trade.ProceedsSold += (fill.Quant * fill.Price);
					
					trade.Commissions += fill.Quant * CommissionFee;
					
					var MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * trade.Position) + trade.PnLRealizedPts,
						MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * trade.Position) + trade.PnLRealizedPts;
				
					trade.MFE = Math.max( trade.MFE , MFEonThisFill);
					trade.MAE = Math.max( trade.MAE , MAEonThisFill);
					
					trade.PeakGain = trade.MFE * this.ContractValue - trade.Commissions;
					trade.MaxDrawdown = trade.MAE * this.ContractValue + trade.Commissions;
					//trade.BSO = ?;
					trade.TIT = (fill.TimestampUTC - trade.EntryDateTime);
					
					var newTheoreticalAverage = trade.AveragePrice;
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					


				}
			}
			
		}
		
		trade.Fills.push(fill);
	}
	
	this.RecalcContractMetricsFromTrades();
};
*/

Contract.prototype.DisplayToScreen = function () {
	// The default display Type is Points
	if (ResultsDisplayType == null) ResultsDisplayType = "Points";

	if (selectedContract == null || selectedContract != this.Symbol) return;	// Not current Contract
	
	console.log("display to screen '" + this.Symbol + "' in format of '" + ResultsDisplayType + "'");
	
	ClearAllCharts();
	
	win_loss_percentage_chart = $(".chart-container#win-loss-percentage-chart").highcharts();
	expectancy_chart = $(".chart-container#expectancy-chart").highcharts();
	equity_curve_chart = $(".chart-container#equity-curve-chart").highcharts();
	trade_distribution_chart = $(".chart-container#trade-distribution-chart").highcharts();

	var TickSize = this.TickSize,
		ContractValue = this.ContractValue,
		contract = this;
		
	var tradeEmoticonArray = [ "sad", "normal", "happy" ];
	var TradeTypeArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

	// Metrics that need to be calculated and displayed
	// Reset calculated metrics
	this.NumberOfWinningTrades = 0;
	this.NumberOfLosingTrades = 0;
	this.NumberOfTotalTrades = 0;
	this.NumberOfTotalContractsTraded = 0;
	this.PercentWinners = 0;
	this.PercentLosers = 0;
		
	this.Expectancy = 0;
	this.Efficiancy = 0;

	this.NetGain = 0;
	this.NetLoss = 0;
	this.GrossGain = 0;	
	this.GrossLoss = 0;

	this.AverageGain = 0;
	this.AverageDraw = 0;

	this.TotalTiTGain = 0;
	this.TotalTiTLoss = 0;
	this.TotalMFE = 0;
	this.TotalMAE = 0;

	this.TotalBSO = 0;
	this.PercentBSO = 0;
	this.NumberOfFullStops = 0;
	this.PercentFullStop = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	
	
	// Cummnulative metrics (change per trades)
	var RunningPnL = 0,
		PeakGain = 0,
		MaxDraw = 0;
	
	var	TradeDistributionArray = [];
	

	// Cumulative metrics
	// TO DO separate the metrics that need to be totaled and metrics that need to keep track of cummulative data (refer to notes)	
	
	var htmlBuilder = "<table id='trades-table' border=0 cellspacing=0 cellpadding=0><thead><th title='Trade Number'>#</th><th>L/S</th><th title='Max. Favorable Excursion'>MFE<br />(" + ResultsDisplayType + ")</th><th title='Max. Adverse Excursion'>MAE<br />(" + ResultsDisplayType + ")</th><th title='Best Scale Out'>BSO<br />(" + ResultsDisplayType + ")</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(" + ResultsDisplayType + ")</th><th>Running P&L<br />(" + ResultsDisplayType + ")</th><th>Entry Time</th><th>Exit Time</th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win</th><th>Avg. Loss</th><th>Expectancy</th><th>Peak Gain<br />(" + ResultsDisplayType + ")</th><th>Max Drawdown<br />(" + ResultsDisplayType + ")</th><th>Efficiency %</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Emotion</th><th>Trade Type</th></thead><tbody>";		

	if (this.TradesArray != null && this.TradesArray.length > 0) {
		var TradeCount = this.TradesArray.length;
		
		for (var i = 0; i < TradeCount; i++) {
			var trade = this.TradesArray[i];
			if (trade.isFlat) {
				var TSB = 0;
			
				// ITERATE TRADES
				var TotalCommissionsForTrade = trade.ContractsTraded * (CommissionFee != null ? CommissionFee : 0);
				var TradePnL = 0, TradePnL_Gross = 0, TradeMFE = 0, TradeMAE = 0;
				var bgColor = "";
				
				// Figure out how the PnL is defined, does commissions come into play?
				if (ResultsDisplayType == "Points") { TradePnL = trade.PnLRealizedPts; TradePnL_Gross = TradePnL; TradeMFE = trade.MFE, TradeMAE = trade.MAE; }
				else if (ResultsDisplayType == "Ticks") { TradePnL = trade.PnLRealizedPts / TickSize; TradePnL_Gross = TradePnL; TradeMFE = trade.MFE / TickSize; TradeMAE = trade.MAE / TickSize; }
				else if (ResultsDisplayType == "Dollars") { TradePnL = (trade.PnLRealizedPts * ContractValue) - TotalCommissionsForTrade; TradePnL_Gross = TradePnL; TradeMFE = trade.MFE * ContractValue; TradeMAE = trade.MAE * ContractValue; }
				
				//Calculate
				this.NumberOfTotalTrades++;
				this.NumberOfTotalContractsTraded += trade.ContractsTraded;
				RunningPnL += TradePnL;
				TradeDistributionArray.push(TradePnL);
				
				// Is Winning OR Losing OR Flat?
				if (TradePnL > 0) {
					// Winning
					this.NumberOfWinningTrades++;
					this.NetGain += TradePnL;
					this.GrossGain += TradePnL_Gross;
					this.TotalTiTGain += trade.TIT;
					bgColor = " style='background-color: rgba(25,47,68,0.3);' ";
					
				} else if (TradePnL < 0) {
					// Losing
					this.NumberOfLosingTrades++;
					this.NetLoss += TradePnL;
					this.GrossLoss += TradePnL_Gross;
					this.TotalTiTLoss += trade.TIT;	
					bgColor = " style='background-color: rgba(194,28,10,0.3);' ";	
					
				} else {
					// Flat
					
				}
				
				this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / this.NumberOfTotalTrades;
				this.PercentWinners = ToPercent(this.NumberOfWinningTrades / this.NumberOfTotalTrades);
				this.PercentLosers = ToPercent(this.NumberOfLosingTrades / this.NumberOfTotalTrades);
				this.AverageGain = this.NetGain / this.NumberOfTotalTrades;
				this.AverageDraw = this.NetLoss / this.NumberOfTotalTrades;
				
				

				// Display the metrics, or add to table / graphs
				if (ResultsDisplayType == "Points") {
					// Display in points
	
					htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
								+ "<td>" + (i+1) + "</td>"
								+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
								+ "<td>" + TradeMFE + "</td>"
								+ "<td>" + TradeMAE + "</td>"
								+ "<td>" + trade.BSO + "</td>"
								+ "<td>" + trade.MaxPosition + "</td>"
								+ "<td>" + trade.ContractsTraded + "</td>"
								+ "<td>" + MillisecondsToTime(trade.TIT) + "</td>"
								+ "<td>" +  MillisecondsToTime(TSB) + "</td>"
								+ "<td>" + TradePnL + "</td>"
								+ "<td>" + RunningPnL + "</td>"
								+ "<td>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.EntryPrice+ "</td>"
								+ "<td>" + trade.ExitPrice + "</td>"
								+ "<td>" + this.PercentWinners + "</td>"
								+ "<td>" + this.PercentLosers + "</td>"
								+ "<td>" + this.AverageGain + "</td>"
								+ "<td>" + this.AverageDraw	+ "</td>"
								+ "<td>" + this.Expectancy + "</td>"
								+ "<td>" + PeakGain + "</td>"
								+ "<td>" + MaxDraw + "</td>"
								+ "<td>" + this.Efficiancy + "</td>"
								+ "<td>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
								+ "</td><td>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
								+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
								+ "<td>" + ToPercent(this.PercentBSO) + "</td>";

				} else if (ResultsDisplayType == "Ticks") {
					// Display in Ticks

					htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
								+ "<td>" + (i+1) + "</td>"
								+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
								+ "<td>" + TradeMFE + "</td>"
								+ "<td>" + TradeMAE + "</td>"
								+ "<td>" + trade.BSO + "</td>"
								+ "<td>" + trade.MaxPosition + "</td>"
								+ "<td>" + trade.ContractsTraded + "</td>"
								+ "<td>" + MillisecondsToTime(trade.TIT) + "</td>"
								+ "<td>" +  MillisecondsToTime(TSB) + "</td>"
								+ "<td>" + TradePnL + "</td>"
								+ "<td>" + RunningPnL + "</td>"
								+ "<td>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.EntryPrice+ "</td>"
								+ "<td>" + trade.ExitPrice + "</td>"
								+ "<td>" + this.PercentWinners + "</td>"
								+ "<td>" + this.PercentLosers + "</td>"
								+ "<td>" + this.AverageGain + "</td>"
								+ "<td>" + this.AverageDraw	+ "</td>"
								+ "<td>" + this.Expectancy + "</td>"
								+ "<td>" + PeakGain + "</td>"
								+ "<td>" + MaxDraw + "</td>"
								+ "<td>" + this.Efficiancy + "</td>"
								+ "<td>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
								+ "</td><td>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
								+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
								+ "<td>" + ToPercent(this.PercentBSO) + "</td>";

				} else if (ResultsDisplayType == "Dollars") {
					// Display in Dollars

					htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
								+ "<td>" + (i+1) + "</td>"
								+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
								+ "<td>" + TradeMFE + "</td>"
								+ "<td>" + TradeMAE + "</td>"
								+ "<td>" + trade.BSO + "</td>"
								+ "<td>" + trade.MaxPosition + "</td>"
								+ "<td>" + trade.ContractsTraded + "</td>"
								+ "<td>" + MillisecondsToTime(trade.TIT) + "</td>"
								+ "<td>" +  MillisecondsToTime(TSB) + "</td>"
								+ "<td>" + TradePnL + "</td>"
								+ "<td>" + RunningPnL + "</td>"
								+ "<td>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
								+ "<td>" + trade.EntryPrice+ "</td>"
								+ "<td>" + trade.ExitPrice + "</td>"
								+ "<td>" + this.PercentWinners + "</td>"
								+ "<td>" + this.PercentLosers + "</td>"
								+ "<td>" + this.AverageGain + "</td>"
								+ "<td>" + this.AverageDraw	+ "</td>"
								+ "<td>" + this.Expectancy + "</td>"
								+ "<td>" + PeakGain + "</td>"
								+ "<td>" + MaxDraw + "</td>"
								+ "<td>" + this.Efficiancy + "</td>"
								+ "<td>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
								+ "</td><td>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
								+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
								+ "<td>" + ToPercent(this.PercentBSO) + "</td>";
					
				}		

							
				// Handle Trade Grade
				var TradeGradeHtmlBuilder = "<td><select class='trade-grade-selector'><option value='--'>--</option>";
				for (var jj = 5; jj > 0; jj--) {
					if (jj.toString() == trade.TradeGrade)
						TradeGradeHtmlBuilder += "<option value='" + jj + "' selected='selected'>" + jj + "</option>";
					else 
						TradeGradeHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";
				}
				TradeGradeHtmlBuilder += "</select></td>";					
				htmlBuilder += TradeGradeHtmlBuilder;

				// Handle Trade Emoticon
				var TradeEmoticonBuilder = "<td style='text-wrap: supress;'>";
				$.each(tradeEmoticonArray, function (kk, emoticon) {
					if (emoticon == trade.TradeEmoticon)
						TradeEmoticonBuilder += "<span class='trade-error chosen' id='" + emoticon + "'></span>";
					else
						TradeEmoticonBuilder += "<span class='trade-error' id='" + emoticon + "'></span>";
				});
				TradeEmoticonBuilder += "</td>";
				htmlBuilder += TradeEmoticonBuilder;
				
				var TradeTypeHtmlBuilder = "<td><select class='trade-type-selector'><option value='--'>--</option>";
				$.each(TradeTypeArray, function (hh, Type) {
					if (trade.TradeType == Type)
						TradeTypeHtmlBuilder += "<option selected='selected' value='" + Type + "'>" + Type + "</option>";
					else 
						TradeTypeHtmlBuilder += "<option value='" + Type + "'>" + Type + "</option>";
				});
				TradeTypeHtmlBuilder += "</select></td>";
				htmlBuilder += TradeTypeHtmlBuilder;
				 
				htmlBuilder += "</tr>";
	

				
				// Update win loss chart
				win_loss_percentage_chart.get("PercentWin").addPoint(this.PercentWinners, false, false, false);
				win_loss_percentage_chart.get("PercentLoss").addPoint(this.PercentLosers, false, false, false);
				expectancy_chart.get("Expectancy").addPoint(this.Expectancy, false, false, false);
				equity_curve_chart.get("MFE").addPoint(TradeMFE, false, false, false);
				equity_curve_chart.get("MAE").addPoint(-TradeMAE, false, false, false);
				equity_curve_chart.get("PnL").addPoint(TradePnL, false, false, false);
				//equity_curve_chart.get("EquityCurve").addPoint(Round(RunningPnL,2), false, false, false);
			
				
			}
		}
	}
	
	// Update outside of the iteration

	// Display Completed trades table
	htmlBuilder += "</tbody></table>";
	console.log(htmlBuilder);
	$("#trades-window .databar").html(htmlBuilder);

	if (ResultsDisplayType == "Points") {
		// Display in points
		$("#stats-current-pnl").text(PointsToRound(TradePnL, TickSize));
		$("#stats-current-expectancy").text(PointsToRound(this.Expectancy, TickSize));
		
	} else if (ResultsDisplayType == "Ticks") {
		// Display in Ticks
		$("#stats-current-pnl").text(Round(TradePnL, 2));
		$("#stats-current-expectancy").text(Round(this.Expectancy, 2));
		
	} else if (ResultsDisplayType == "Dollars") {
		// Display in Dollars
		$("#stats-current-pnl").text(Round(TradePnL, 2));
		$("#stats-current-expectancy").text(Round(this.Expectancy, 2));

	}		

	// Update Stats Toolbar
	$("#stats-num-of-trades").text(this.NumberOfTotalTrades);
	$("#stats-num-of-contracts-traded").text(this.NumberOfTotalContractsTraded);
	$("#stats-avg-win-duration").text(MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades));
	$("#stats-avg-loss-duration").text(MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades));

	
	// Intialize Dials
	percent_win_dial = $('#percent-win-dial').highcharts();
	percent_loss_dial = $('#percent-loss-dial').highcharts();
	pnl_dial = $('#pnl-dial').highcharts();


	// Update Win/Loss Dials
	percent_win_dial.series[0].points[0].update(ToPercent(this.NumberOfWinningTrades / this.NumberOfTotalTrades));
	percent_loss_dial.series[0].points[0].update(ToPercent(this.NumberOfLosingTrades / this.NumberOfTotalTrades));
	
	if (TradeMFE == 0 && TradeMAE == 0) {
		// MFE / MAE not set
		pnl_dial.yAxis[0].update({ min: TradePnL-10, max: TradePnL+10 }, false);
	} else {
		pnl_dial.yAxis[0].update({ min: -TradeMAE, max: TradeMFE }, false);
	}
	pnl_dial.series[0].points[0].update(TradePnL);	
	
	
	// Redraw charts
	win_loss_percentage_chart.redraw();
	expectancy_chart.redraw();
	equity_curve_chart.redraw();
}

Contract.prototype.DisplayToScreen2 = function () {
	// Depricated
	return;
	if (selectedContract == null || selectedContract != this.Symbol) return;	// Not current Contract
	
	console.log("display to screen " + this.Symbol);
	
	ClearAllCharts();
	
	win_loss_percentage_chart = $(".chart-container#win-loss-percentage-chart").highcharts();
	expectancy_chart = $(".chart-container#expectancy-chart").highcharts();
	equity_curve_chart = $(".chart-container#equity-curve-chart").highcharts();
	trade_distribution_chart = $(".chart-container#trade-distribution-chart").highcharts();

	var TickSize = this.TickSize,
		ContractValue = this.ContractValue,
		contract = this;
		
	var tradeEmoticonArray = [ "sad", "normal", "happy" ];
	var TradeTypeArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
		
	
	if (ResultsDisplayType == "Dollars") {
		// DOLLARS
		console.log("Display " + this.Symbol + " in Dollars");
		
		var htmlBuilder = "<table id='trades-table' border=0 cellspacing=0 cellpadding=0><thead><th>#</th><th>MFE<br />(Points)</th><th>MAE<br />(Points)</th><th>BSO</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(Dollars)</th><th>Running P&L<br />(Dollars)</th><th>Entry Time</th><th>Exit Time</th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win<br />(Dollars)</th><th>Avg. Loss<br />(Dollars)</th><th>Expectancy</th><th>Peak Gain<br />(Dollars)</th><th>Max Drawdown<br />(Dollars)</th><th>Efficiency</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Emotion</th><th>Trade Type</th></thead><tbody>";
		$.each(this.TradesArray, function (i, trade) {
			if (trade.isFlat) {
				var bgColor = (trade.PnLRealizedDollars > 0 ? " style='background-color: rgba(25,47,68,0.3);' " : (trade.PnLRealizedDollars < 0 ? " style='background-color: rgba(194,28,10,0.3);' " : ""));
				htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'><td>" + trade.TradeNumber + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.MFE, TickSize) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.MAE, TickSize) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.BSO, TickSize) + "</td>";
				htmlBuilder += "<td>" + trade.MaxPosition + "</td>";
				htmlBuilder += "<td>" + trade.ContractsTraded + "</td>";
				htmlBuilder += "<td>" + MillisecondsToTime(trade.TIT) + "</td>";
				htmlBuilder += "<td>" + MillisecondsToTime(trade.TSB) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.PnLRealizedDollars, 0.01) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.RunningPnLDollars, 0.01) + "</td>";
				htmlBuilder += "<td>" + trade.EntryDateTime.local().format('MMM D h:mma') + "</td>";
				htmlBuilder += "<td>" + trade.ExitDateTime.local().format('MMM D h:mma') + "</td>";
				htmlBuilder += "<td>" + trade.EntryPrice + "</td>";
				htmlBuilder += "<td>" + trade.ExitPrice + "</td>";
				htmlBuilder += "<td>" + ToPercent(trade.PercentWin) + "</td>";
				htmlBuilder += "<td>" + ToPercent(trade.PercentLoss) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.AverageWin, 0.01) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.AverageLoss, 0.01) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.Expectancy, 0.01) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.PeakGain, 0.01) + "</td>";
				htmlBuilder += "<td>" + PointsToRound(trade.MaxDrawdown, 0.01) + "</td>";
				htmlBuilder += "<td>" + ToPercent(trade.Efficiancy) + "</td>";
				htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageWinDuration) + "</td>";
				htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageLossDuration) + "</td>";
				htmlBuilder += "<td>" + trade.PercentFullStop + "</td>";						
				htmlBuilder += "<td>" + trade.PercentBSO + "</td>";						
				htmlBuilder += "<td>" + trade.TradeGrade + "</td>";						
				htmlBuilder += "<td>" + trade.TradeEmoticon + "</td>";						
				htmlBuilder += "<td>" + trade.TradeType + "</td>";
				htmlBuilder += "</tr>";
				
				// Win/Loss chart
				win_loss_percentage_chart.get('PercentWin').addPoint(ToPercent(this.PercentWin), false, false, false);
				win_loss_percentage_chart.get('PercentLoss').addPoint(ToPercent(this.PercentLoss), false, false, false);		
				
				// Expectancy chart
				expectancy_chart.get('Expectancy').addPoint((PointsToRound(this.Expectancy, 0.01)), false, false, false);
				
				// Equity Curve Chart
				equity_curve_chart.get('EquityCurve').addPoint([trade.TradeNumber, this.PnLRealizedPts], false, false, false);
				equity_curve_chart.get('PnL').addPoint([trade.TradeNumber, trade.PnLRealizedPts], false, false, false);
				equity_curve_chart.get('MFE').addPoint([trade.TradeNumber, trade.MFE], false, false, false);
				equity_curve_chart.get('MAE').addPoint([trade.TradeNumber, -trade.MAE], false, false, false);
			}					
		});
		htmlBuilder += "</tbody></table>";

		var lastTrade = (this.TradesArray != null && this.TradesArray.length > 0) ? this.TradesArray[ this.TradesArray.length-1 ] : null;
		var expectancy = lastTrade != null ? lastTrade.expectancy : "n.a.",
			tradeMAE = lastTrade != null ? lastTrade.MAE : 0,
			tradeMFE = lastTrade != null ? lastTrade.MFE : 0,
			tradePNL = lastTrade != null ? (lastTrade.PnLUnrealizedPts + lastTrade.PnLRealizedPts) : 0;

		$("#stats-current-pnl").text(tradePNL);
		$("#stats-num-of-trades").text(this.NumberOfTrades);
		$("#stats-num-of-contracts-traded").text(this.ContractsTraded);
		$("#stats-current-expectancy").text(PointsToRound(this.Expectancy, 0.01));
		$("#stats-avg-win-duration").text(MillisecondsToTime(this.AverageWinDuration));
		$("#stats-avg-loss-duration").text(MillisecondsToTime(this.AverageLossDuration));
		
		percent_win_dial = $('#percent-win-dial').highcharts();
		percent_loss_dial = $('#percent-loss-dial').highcharts();
		pnl_dial = $('#pnl-dial').highcharts();
		
		percent_win_dial.series[0].points[0].update(ToPercent(this.PercentWin));
		percent_loss_dial.series[0].points[0].update(ToPercent(this.PercentLoss));
		
		pnl_dial.yAxis[0].update({ min: -PointsToRound(tradeMAE, 0.01), max: PointsToRound(tradeMFE, 0.01) }, false);
		pnl_dial.series[0].points[0].update(PointsToRound(tradePNL, 0.01));	

	} else if (ResultsDisplayType == "Points") {
		// POINTS
		console.log("Display " + this.Symbol + " in points");
		var htmlBuilder = "<table id='trades-table' border=0 cellspacing=0 cellpadding=0><thead><th title='Trade Number'>#</th><th title='Max. Favorable Excursion'>MFE<br />(Points)</th><th title='Max. Adverse Excursion'>MAE<br />(Points)</th><th title='Best Scale Out'>BSO<br />(Points)</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(Points)</th><th>Running P&L<br />(Points)</th><th>Entry Time</th><th>Exit Time</th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win</th><th>Avg. Loss</th><th>Expectancy</th><th>Peak Gain<br />(Dollars)</th><th>Max Drawdown<br />(Dollars)</th><th>Efficiency %</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Emotion</th><th>Trade Type</th></thead><tbody>";
		if (TickSize != null && TickSize != 0) {						
			$.each(this.TradesArray, function (i, trade) {
				if (trade.isFlat) {
					var bgColor = (trade.PnLRealizedDollars > 0 ? " style='background-color: rgba(25,47,68,0.3);' " : (trade.PnLRealizedDollars < 0 ? " style='background-color: rgba(194,28,10,0.3);' " : ""));
					htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'><td>" + trade.TradeNumber + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.MFE, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.MAE, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.BSO, TickSize) + "</td>";
					htmlBuilder += "<td>" + trade.MaxPosition + "</td>";
					htmlBuilder += "<td>" + trade.ContractsTraded + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.TIT) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.TSB) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.PnLRealizedPts, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.RunningPnLPts, TickSize) + "</td>";
					htmlBuilder += "<td>" + trade.EntryDateTime.local().format('MMM D h:mma') + "</td>";
					htmlBuilder += "<td>" + trade.ExitDateTime.local().format('MMM D h:mma') + "</td>";
					htmlBuilder += "<td>" + trade.EntryPrice + "</td>";
					htmlBuilder += "<td>" + trade.ExitPrice + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.PercentWin) + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.PercentLoss) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.AverageWin, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.AverageLoss, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.Expectancy, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.PeakGain, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.MaxDrawdown, 0.01) + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.Efficiancy) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageWinDuration) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageLossDuration) + "</td>";
					htmlBuilder += "<td>" + trade.PercentFullStop + "</td>";						
					htmlBuilder += "<td>" + trade.PercentBSO + "</td>";
					
					// Handle Trade Grade
					var TradeGradeHtmlBuilder = "<td><select class='trade-grade-selector'><option value='--'>--</option>";
					for (var jj = 5; jj > 0; jj--) {
						if (jj.toString() == trade.TradeGrade)
							TradeGradeHtmlBuilder += "<option value='" + jj + "' selected='selected'>" + jj + "</option>";
						else 
							TradeGradeHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";
					}
					TradeGradeHtmlBuilder += "</select></td>";					
					htmlBuilder += TradeGradeHtmlBuilder;
					
					// Handle Trade Emoticon
					var TradeEmoticonBuilder = "<td>";
					$.each(tradeEmoticonArray, function (kk, emoticon) {
						if (emoticon == trade.TradeEmoticon)
							TradeEmoticonBuilder += "<span class='trade-error chosen' id='" + emoticon + "'></span>";
						else
							TradeEmoticonBuilder += "<span class='trade-error' id='" + emoticon + "'></span>";
					});
					TradeEmoticonBuilder += "</td>";
					htmlBuilder += TradeEmoticonBuilder;
					
					var TradeTypeHtmlBuilder = "<td><select class='trade-type-selector'><option value='--'>--</option>";
					$.each(TradeTypeArray, function (hh, Type) {
						if (trade.TradeType == Type)
							TradeTypeHtmlBuilder += "<option selected='selected' value='" + Type + "'>" + Type + "</option>";
						else 
							TradeTypeHtmlBuilder += "<option value='" + Type + "'>" + Type + "</option>";
					});
					TradeTypeHtmlBuilder += "</select></td>";
					htmlBuilder += TradeTypeHtmlBuilder;
					 
					htmlBuilder += "</tr>";
					
					// Win/Loss chart
					win_loss_percentage_chart.get('PercentWin').addPoint(ToPercent(this.PercentWin), false, false, false);
					win_loss_percentage_chart.get('PercentLoss').addPoint(ToPercent(this.PercentLoss), false, false, false);		
					
					// Expectancy chart
					expectancy_chart.get('Expectancy').addPoint((PointsToRound(this.Expectancy, 0.01)), false, false, false);
					
					// Equity Curve Chart
					equity_curve_chart.get('EquityCurve').addPoint([trade.TradeNumber, this.PnLRealizedPts], false, false, false);
					equity_curve_chart.get('PnL').addPoint([trade.TradeNumber, trade.PnLRealizedPts], false, false, false);
					equity_curve_chart.get('MFE').addPoint([trade.TradeNumber, trade.MFE], false, false, false);
					equity_curve_chart.get('MAE').addPoint([trade.TradeNumber, -trade.MAE], false, false, false);
				}
			});

			var lastTrade = (this.TradesArray != null && this.TradesArray.length > 0) ? this.TradesArray[ this.TradesArray.length-1 ] : null;
			var expectancy = lastTrade != null ? lastTrade.expectancy : "n.a.",
				tradeMAE = lastTrade != null ? lastTrade.MAE : 0,
				tradeMFE = lastTrade != null ? lastTrade.MFE : 0,
				tradePNL = lastTrade != null ? (lastTrade.PnLUnrealizedPts + lastTrade.PnLRealizedPts) : 0;
	

			$("#stats-current-pnl").text(tradePNL);
			$("#stats-num-of-trades").text(this.NumberOfTrades);
			$("#stats-num-of-contracts-traded").text(this.ContractsTraded);
			$("#stats-current-expectancy").text(PointsToRound(this.Expectancy, 0.01));
			$("#stats-avg-win-duration").text(MillisecondsToTime(this.AverageWinDuration));
			$("#stats-avg-loss-duration").text(MillisecondsToTime(this.AverageLossDuration));
			
			percent_win_dial = $('#percent-win-dial').highcharts();
			percent_loss_dial = $('#percent-loss-dial').highcharts();
			pnl_dial = $('#pnl-dial').highcharts();
			
			percent_win_dial.series[0].points[0].update(ToPercent(this.PercentWin));
			percent_loss_dial.series[0].points[0].update(ToPercent(this.PercentLoss));
			
			pnl_dial.yAxis[0].update({ min: -PointsToRound(tradeMAE, 0.01), max: PointsToRound(tradeMFE, 0.01) }, false);
			pnl_dial.series[0].points[0].update(PointsToRound(tradePNL, 0.01));						

		} else {
			if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " did not contain a correct TickSize (" + TickSize + ")", "error", 0);
		}					

		htmlBuilder += "</tbody></table>";
	} else {
		// TICKS is default
		console.log("Display " + this.Symbol + " in ticks");
		var htmlBuilder = "<table id='trades-table' border=0 cellspacing=0 cellpadding=0><thead><th title='Trade Number'>#</th><th title='Max. Favorable Excursion'>MFE<br />(Ticks)</th><th title='Max. Adverse Excursion'>MAE<br />(Ticks)</th><th title='Best Scale Out'>BSO<br />(Ticks)</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(Ticks)</th><th>Running P&L<br />(Ticks)</th><th>Entry Time</th><th>Exit Time</th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win<br />(Ticks)</th><th>Avg. Loss<br />(Ticks)</th><th>Expectancy</th><th>Peak Gain<br />(Dollars)</th><th>Max Drawdown<br />(Dollars)</th><th>Efficiency %</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Emotion</th><th>Trade Type</th></thead><tbody>";
		if (TickSize != null && TickSize != 0) {						
			$.each(this.TradesArray, function (i, trade) {
				if (trade.isFlat) {
					var bgColor = (trade.PnLRealizedDollars > 0 ? " style='background-color: rgba(25,47,68,0.3);' " : (trade.PnLRealizedDollars < 0 ? " style='background-color: rgba(194,28,10,0.3);' " : ""));
					htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'><td>" + trade.TradeNumber + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.MFE, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.MAE, TickSize) + "</td>";
					htmlBuilder += "<td>" + trade.BSO + "</td>";
					htmlBuilder += "<td>" + trade.MaxPosition + "</td>";
					htmlBuilder += "<td>" + trade.ContractsTraded + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.TIT) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.TSB) + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.PnLRealizedPts, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.RunningPnLPts, TickSize) + "</td>";
					htmlBuilder += "<td>" + trade.EntryDateTime.local().format('MMM D h:mma') + "</td>";
					htmlBuilder += "<td>" + trade.ExitDateTime.local().format('MMM D h:mma') + "</td>";
					htmlBuilder += "<td>" + trade.EntryPrice + "</td>";
					htmlBuilder += "<td>" + trade.ExitPrice + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.PercentWin) + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.PercentLoss) + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.AverageWin, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToTick(trade.AverageLoss, TickSize) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.Expectancy, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.PeakGain, 0.01) + "</td>";
					htmlBuilder += "<td>" + PointsToRound(trade.MaxDrawdown, 0.01) + "</td>";
					htmlBuilder += "<td>" + ToPercent(trade.Efficiancy) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageWinDuration) + "</td>";
					htmlBuilder += "<td>" + MillisecondsToTime(trade.AverageLossDuration) + "</td>";
					htmlBuilder += "<td>" + trade.PercentFullStop + "</td>";						
					htmlBuilder += "<td>" + trade.PercentBSO + "</td>";						
					htmlBuilder += "<td><select class='trade-grade-selector'><option value='--'>--</option><option value='5'>5</option><option value='4'>4</option><option value='3'>3</option><option value='2'>2</option><option value='1'>1</option></select></td>";
					htmlBuilder += "<td><span class='trade-error' id='sad' title='Unfavorable'></span><span class='trade-error' id='normal' title='OK'></span><span class='trade-error' id='happy' title='Favorable'></span></td>";
					htmlBuilder += "<td><select class='trade-type-selector'><option value='--'>--</option><option value='A'>A</option><option value='B'>B</option><option value='C'>C</option><option value='D'>D</option><option value='E'>E</option></select></td>";
					htmlBuilder += "</tr>";
					
					// Win/Loss chart
					win_loss_percentage_chart.get('PercentWin').addPoint(ToPercent(this.PercentWin), false, false, false);
					win_loss_percentage_chart.get('PercentLoss').addPoint(ToPercent(this.PercentLoss), false, false, false);		
					
					// Expectancy chart
					expectancy_chart.get('Expectancy').addPoint((PointsToRound(this.Expectancy, 0.01)), false, false, false);
					
					// Equity Curve Chart
					equity_curve_chart.get('EquityCurve').addPoint([trade.TradeNumber, PointsToTick(this.PnLRealizedPts, TickSize)], false, false, false);
					equity_curve_chart.get('PnL').addPoint([trade.TradeNumber, trade.PnLRealizedPts], false, false, false);
					equity_curve_chart.get('MFE').addPoint([trade.TradeNumber, PointsToTick(trade.MFE, TickSize)], false, false, false);
					equity_curve_chart.get('MAE').addPoint([trade.TradeNumber, -PointsToTick(trade.MAE, TickSize)], false, false, false);
				}
			});

			var lastTrade = (this.TradesArray != null && this.TradesArray.length > 0) ? this.TradesArray[ this.TradesArray.length-1 ] : null;
			var expectancy = lastTrade != null ? lastTrade.expectancy : "n.a.",
				tradeMAE = lastTrade != null ? lastTrade.MAE : 0,
				tradeMFE = lastTrade != null ? lastTrade.MFE : 0,
				tradePNL = lastTrade != null ? (lastTrade.PnLRealizedPts + lastTrade.PnLUnrealizedPts) : 0;
			

			$("#stats-current-pnl").text(PointsToTick(this.PnLRealizedPts + this.PnLUnrealizedPts));
			$("#stats-num-of-trades").text(this.NumberOfTrades);
			$("#stats-num-of-contracts-traded").text(this.ContractsTraded);
			$("#stats-current-expectancy").text(PointsToRound(this.Expectancy, 0.01));
			$("#stats-avg-win-duration").text(MillisecondsToTime(this.AverageWinDuration));
			$("#stats-avg-loss-duration").text(MillisecondsToTime(this.AverageLossDuration));

			percent_win_dial = $('#percent-win-dial').highcharts();
			percent_loss_dial = $('#percent-loss-dial').highcharts();
			pnl_dial = $('#pnl-dial').highcharts();
			
			percent_win_dial.series[0].points[0].update(ToPercent(this.PercentWin));
			percent_loss_dial.series[0].points[0].update(ToPercent(this.PercentLoss));

			pnl_dial.yAxis[0].update({ min: -PointsToTick(tradeMAE, TickSize), max: PointsToTick(tradeMFE, TickSize) }, false);
			pnl_dial.series[0].points[0].update(PointsToTick(tradePNL, TickSize));
			
		} else {
			if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " did not contain a correct TickSize (" + TickSize + ")", "error", 0);
		}
		htmlBuilder += "</tbody></table>";
	}
	
	$("#trades-window .databar").html(htmlBuilder);
	win_loss_percentage_chart.redraw();
	expectancy_chart.redraw();
	equity_curve_chart.redraw();
	
	updateContractSummaryBar(this);
};

Contract.prototype.GetUserTradeMetricsFromDB = function () {
	console.log("Loading User Trade Metrics for contract " + this.Symbol);
	// TODO
	if (this.TradesArray != null && this.TradesArray.length > 0) {
		var FillIDArray = [];
		$.each (this.TradesArray, function (i, trade) { FillIDArray.push(trade.TradeFillID); });
		
		if (FillIDArray.length > 0) {
			// There were some inital Fill IDs to use for getting trade metrics	
			$.ajax({
				url: "",
				method: "post",
				type: "post",
				data: { username: username, Account: selectedAccount, Contract: selectedContract, FillIDArray: FillIDArray },
				error: function (err) { console.log(err); },
				success: function (data) { console.log(data); }
			});
		}
	}
};


function MillisecondsToTime (ms) {
	if (isNaN(ms)) {
		return "n.a.";
	} else {
		var dur = moment.duration(ms),
			days = Math.floor(dur.days()),
			hours = Math.floor(dur.hours()),
			minutes = Math.floor(dur.minutes()),
			seconds = Math.floor(dur.seconds());
		if (ms == 0)
			return 0;
		else if (days > 0) 
			return days + "d " + hours + "h " + minutes + "m " + seconds + "s";
		else if (hours > 0)
			return hours + "h " + minutes + "m " + seconds + "s";
		else if (minutes > 0)
			return minutes + "m " + seconds + "s";
		else
			return seconds + "s";
	}
}

function PointsToTick (points, tickSize) {
	if (tickSize > 0) {			
		tickSize = tickSize.toString();
		var decPlace = (tickSize.split('.')[1] || []).length;
		var decimalMultiplier = 10 ^ decPlace;
		return (Math.round(points * decimalMultiplier / tickSize) / decimalMultiplier);
	} else {
		return -1;
	}
}

function PointsToRound (points, tickSize) {
	if (tickSize > 0) {
		tickSize = tickSize.toString();
		var decPlace = (tickSize.split('.')[1] || []).length;
		return Number(Number(points).toFixed(decPlace));
		//var decimalMultiplier = Math.pow(10,decPlace);
		//return (Math.round(points * decimalMultiplier) / decimalMultiplier);
	} else {
		return -1;
	}
}

function Round (Input, DecimalPlaces) {
	if (DecimalPlaces == null) DecimalPlaces = 0;
	if ($.isNumeric(Input) && $.isNumeric(DecimalPlaces)) {
		var m = Math.pow(10, Number(DecimalPlaces));
		return (Math.round(Number(Input) * m) / m);
	} else if (Input == "NaN") {
		return "NaN";
	} else {
		return "ERR";
	}
}

function ToPercent(num) {
	return (num == 0 ? 0 :(Math.round(num * 10000)/100));
}

function FillToString(fill) {
	var str = fill.Account + " -> " + fill.Contract + "<br />" + fill.FillID + " " + fill.Side + " " + fill.Quant + " " + fill.Price + "<br />" + fill.TimestampUTC;
	return str;
}
