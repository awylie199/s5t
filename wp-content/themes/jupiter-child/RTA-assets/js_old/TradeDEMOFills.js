// JavaScript Document
// Trades Handler Class
$(document).ready(function(e) {
    $("#submitMarketPrice").click(function(e) {
		console.log("sumbit market price");
		var symbol1 = $("#contract1Symbol").val();
		var price1 = $("#contract1Price").val();
		var obj1 = { Symbol: symbol1, TimestampUTC: moment().unix(), Price: price1 };

		var symbol2 = $("#contract2Symbol").val();
		var price2 = $("#contract2Price").val();
		var obj2 = { Symbol: symbol2, TimestampUTC: moment().unix(), Price: price2 };		
		
		var priceArray = [ obj1, obj2 ]
		
		Trades.PriceUpdate(priceArray);
	});

});

var fill1 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 1,
	HighSince: 2001,
	LowSince: 1999,
	OrderID: "1",
	Price: 2000,
	Quant: 2,
	Side: "Buy",
	TimestampUTC: 1452807407,
	TypeOfTrade: "O"
}

var fill2 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 2,
	HighSince: 2015,
	LowSince: 2009.25,
	OrderID: "2",
	Price: 2010,
	Quant: 2,
	Side: "Sell",
	TimestampUTC: 1452907407,
	TypeOfTrade: "C"	
}

var fill3 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 3,
	HighSince: 2000.25,
	LowSince: 1995.75,
	OrderID: "3",
	Price: 1998,
	Quant: 1,
	Side: "Sell",
	TimestampUTC: 1452950117,
	TypeOfTrade: "O"	
}

var fill4 = {
	TickSize: 0.1,
	ContractValue: 100,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "NQH7",
	FillID: 4,
	HighSince: 4010,
	LowSince: 4005,
	OrderID: "4",
	Price: 4007,
	Quant: 2,
	Side: "Buy",
	TimestampUTC: 1453950117,
	TypeOfTrade: "O"	
}


var fill5 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 5,
	HighSince: 2055,
	LowSince: 2045,
	OrderID: "5",
	Price: 2050,
	Quant: 1,
	Side: "Buy",
	TimestampUTC: 1453050117,
	TypeOfTrade: "C"	
}

var fill6 = {
	TickSize: 0.25,
	ContractValue: 50,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 6,
	HighSince: 2075,
	LowSince: 2040,
	OrderID: "6",
	Price: 2055,
	Quant: 5,
	Side: "Sell",
	TimestampUTC: 1453190117,
	TypeOfTrade: "C"	
}


var fill7 = {
	TickSize: 0.1,
	ContractValue: 100,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "NQH7",
	FillID: 7,
	HighSince: 4010,
	LowSince: 4005,
	OrderID: "7",
	Price: 4000,
	Quant: 2,
	Side: "Sell",
	TimestampUTC: 1454450117,
	TypeOfTrade: "C"	
}


var fill8 = {
	TickSize: 0.1,
	ContractValue: 100,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 8,
	HighSince: 2010,
	LowSince: 1988,
	OrderID: "8",
	Price: 1995,
	Quant: 2,
	Side: "Buy",
	TimestampUTC: 1454450117,
	TypeOfTrade: "O"	
}


var fill9 = {
	TickSize: 0.1,
	ContractValue: 100,
	ClosingTime: 6,	
	Account: "API002565",
	Broker: "OEC",
	Contract: "ESZ5",
	FillID: 9,
	HighSince: 2000,
	LowSince: 1990,
	OrderID: "9",
	Price: 1990,
	Quant: 1,
	Side: "Sell",
	TimestampUTC: 1454550117,
	TypeOfTrade: "C"	
}

var BackFills = [ fill1, fill2 ]; //, fill3, fill5, fill6, fill7, fill8, fill9 ];
var LiveFills = [ fill3, fill5 ];

var LiveFillsIndex = 0;



$(document).keydown(function(e) {
	//console.log(e.keyCode);
/*
	if (e.keyCode == 70) {	// F
		// insert single fill	
		console.log("insert single fill");
		var fill = LiveFills[LiveFillsIndex++];
		fill.TimestampUTC = moment().unix();
		Trades.InsertFill( fill, true );
	}
	
	if (e.keyCode == 81) {		// Q
		// insert init fills
		console.log("insert init fills");
		Trades.InitFills(BackFills);
	}
	
	if (e.keyCode == 68) {		// D
		// display fills	
		console.log("display fills");
		Trades.DisplayTradesInTable();
	}
*/
	if (e.keyCode == 84) {		// T
		// display Trades
		$.each(Trades.ContractsList, function (i, v) {
			console.log("--------" + v.Symbol + "--------");
			$.each(v.TradesArray, function (j, t) {
				console.log(t);
			});
		});
	}

	if (e.keyCode == 67) {		// C
		// display Contracts
		$.each(Trades.ContractsList, function (i, c) {
			console.log(c);
		});
	}
	
	if (e.keyCode == 82 && e.altKey) {		// R
		// 	SUPER DUMP ADMIN
		console.log("SUPER DUMP");
		Trades.AdminDataDump();
	}
	
	if (e.keyCode == 80) { // P
		console.log("Ping Server");
		PingServer();
	}
});

