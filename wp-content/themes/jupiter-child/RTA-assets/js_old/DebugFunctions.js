/* debug functions */
$(document).ready(function () {
	// This function will run the CHECK function of the RTA-TradeHandler every x minutes
	setInterval(function () {
		if (Trades != null)
			Trades.CheckFillConsistency();
	}, 10 * 60 * 1000);

	if (typeof popupNotification == "function")
		popupNotification("We are in the beta testing phase of the RTA.", "information", 30000);	


});


$(document).keydown(function(e) {
	// Make sure the ALT key is also pressed
	if (e.altKey)  {
		if (e.keyCode == 84) {		// T
			// display Trades
			$.each(Trades.ContractsList, function (i, v) {
				console.log("--------" + v.Symbol + "--------");
				$.each(v.TradesArray, function (j, t) {
					console.log(t);
				});
			});
		} else if (e.keyCode == 67) {		// C
			// display Contracts
			$.each(Trades.ContractsList, function (i, c) {
				console.log(c);
			});
		} else if (e.keyCode == 82) {		// R
			// 	SUPER DUMP ADMIN
			console.log("SUPER DUMP");
			if (Trades != null) Trades.AdminDataDump();
		} else if (e.keyCode == 80) { // P
			console.log("Ping Server");
			PingServer();
		} else if (e.keyCode == 65) { // A
			if (typeof popupNotification == "function") popupNotification ("Your current selected account is '" + selectedAccount + "'.", "alert", 5000);

		} else if (e.keyCode == 71) { // G
			Trades.CheckFillConsistency();
		}

	}
});	