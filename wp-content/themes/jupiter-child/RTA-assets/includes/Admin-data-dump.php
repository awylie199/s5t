<?php
if (isset($_POST['Username']) && isset($_POST['Data'])) {

	$Username = trim($_POST['Username']);
	$Data = $_POST['Data'];
	
	// Check to see if the user has access to RTA and which accounts
	require ("Constants.php");

	$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);
	
	if ($mysqliConn->connect_errno) {
		// Error in connection
		echo "ERROR -> DB: " + $mysqliConn->connect_error;	
	} else {
		// Connected
		$Data = $mysqliConn->real_escape_string($Data);
		$query = "INSERT INTO admin_data_dump (Username, Data) VALUES ('$Username', '$Data')";
		$results = $mysqliConn->real_query($query);
		echo ("Data dump results: " . ($results ? "SENT" : "ERROR"));
	}
} else {
	echo "ERROR -> Data input did not include the required POST metrics!";
}
?>