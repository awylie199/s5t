/*!

Split Pane v0.5.1

Copyright (c) 2014 Simon Hagström

Released under the MIT license
https://raw.github.com/shagstrom/split-pane/master/LICENSE

*/
!function(t){function e(){var e=t(this),i=e.children(".split-pane-component:first"),s=e.children(".split-pane-divider"),n=e.children(".split-pane-component:last")
e.is(".fixed-top, .fixed-bottom, .horizontal-percent")?e.css("min-height",u(i)+u(n)+s.height()+"px"):e.css("min-width",v(i)+v(n)+s.width()+"px")}function i(e){e.preventDefault()
var i=e.type.match(/^touch/),s=i?"touchmove":"mousemove",f=i?"touchend":"mouseup",o=t(this),r=o.parent(),a=o.siblings(".split-pane-resize-shim")
a.show(),o.addClass("dragged"),i&&o.addClass("touch")
var d=n(r,l(e),c(e))
t(document).on(s,d),t(document).one(f,function(e){t(document).unbind(s,d),o.removeClass("dragged touch"),a.hide()})}function s(t){var e=t[0],i=t.children(".split-pane-component:first")[0],s=t.children(".split-pane-divider")[0],n=t.children(".split-pane-component:last")[0]
return t.is(".fixed-top")?function(f){var o=u(n),r=e.offsetHeight-o-s.offsetHeight
i.offsetHeight>r&&x(i,s,n,r+"px"),t.resize()}:t.is(".fixed-bottom")?function(f){var o=u(i),r=e.offsetHeight-o-s.offsetHeight
n.offsetHeight>r&&H(i,s,n,r+"px"),t.resize()}:t.is(".horizontal-percent")?function(f){var o=u(n),r=u(i),a=e.offsetHeight-r-s.offsetHeight
n.offsetHeight>a?H(i,s,n,a/e.offsetHeight*100+"%"):e.offsetHeight-i.offsetHeight-s.offsetHeight<o&&H(i,s,n,o/e.offsetHeight*100+"%"),t.resize()}:t.is(".fixed-left")?function(f){var o=v(n),r=e.offsetWidth-o-s.offsetWidth
i.offsetWidth>r&&z(i,s,n,r+"px"),t.resize()}:t.is(".fixed-right")?function(f){var o=v(i),r=e.offsetWidth-o-s.offsetWidth
n.offsetWidth>r&&W(i,s,n,r+"px"),t.resize()}:t.is(".vertical-percent")?function(f){var o=v(n),r=v(i),a=e.offsetWidth-r-s.offsetWidth
n.offsetWidth>a?W(i,s,n,a/e.offsetWidth*100+"%"):e.offsetWidth-i.offsetWidth-s.offsetWidth<o&&W(i,s,n,o/e.offsetWidth*100+"%"),t.resize()}:void 0}function n(t,e,i){var s=p(t)
return t.is(".fixed-top")?f(s,i):t.is(".fixed-bottom")?o(s,i):t.is(".horizontal-percent")?r(s,i):t.is(".fixed-left")?a(s,e):t.is(".fixed-right")?d(s,e):t.is(".vertical-percent")?h(s,e):void 0}function f(e,i){var s=u(e.first),n=e.splitPane.offsetHeight-u(e.last)-e.divider.offsetHeight,f=e.divider.offsetTop-i
return function(i){var o=g(s,n,f+c(i))
x(e.first,e.divider,e.last,o+"px"),t(e.splitPane).resize()}}function o(e,i){var s=u(e.last),n=e.splitPane.offsetHeight-u(e.first)-e.divider.offsetHeight,f=e.last.offsetHeight+i
return function(i){var o=Math.min(Math.max(s,f-c(i)),n)
H(e.first,e.divider,e.last,o+"px"),t(e.splitPane).resize()}}function r(e,i){var s=e.splitPane.offsetHeight,n=u(e.last),f=s-u(e.first)-e.divider.offsetHeight,o=e.last.offsetHeight+i
return function(i){var r=Math.min(Math.max(n,o-c(i)),f)
H(e.first,e.divider,e.last,r/s*100+"%"),t(e.splitPane).resize()}}function a(e,i){var s=v(e.first),n=e.splitPane.offsetWidth-v(e.last)-e.divider.offsetWidth,f=e.divider.offsetLeft-i
return function(i){var o=m(s,n,f+l(i))
z(e.first,e.divider,e.last,o+"px"),t(e.splitPane).resize()}}function d(e,i){var s=v(e.last),n=e.splitPane.offsetWidth-v(e.first)-e.divider.offsetWidth,f=e.last.offsetWidth+i
return function(i){var o=Math.min(Math.max(s,f-l(i)),n)
W(e.first,e.divider,e.last,o+"px"),t(e.splitPane).resize()}}function h(e,i){var s=e.splitPane.offsetWidth,n=v(e.last),f=s-v(e.first)-e.divider.offsetWidth,o=e.last.offsetWidth+i
return function(i){var r=Math.min(Math.max(n,o-l(i)),f)
W(e.first,e.divider,e.last,r/s*100+"%"),t(e.splitPane).resize()}}function p(t){return{splitPane:t[0],first:t.children(".split-pane-component:first")[0],divider:t.children(".split-pane-divider")[0],last:t.children(".split-pane-component:last")[0]}}function l(t){return void 0!==t.pageX?t.pageX:t.originalEvent.pageX}function c(t){return void 0!==t.pageY?t.pageY:t.originalEvent.pageY}function u(e){return parseInt(t(e).css("min-height"))||0}function v(e){return parseInt(t(e).css("min-width"))||0}function g(t,e,i){return Math.min(Math.max(t,i),e)}function m(t,e,i){return Math.min(Math.max(t,i),e)}function x(t,e,i,s){t.style.height=s,e.style.top=s,i.style.top=s}function H(t,e,i,s){t.style.bottom=s,e.style.bottom=s,i.style.height=s}function z(t,e,i,s){t.style.width=s,e.style.left=s,i.style.left=s}function W(t,e,i,s){t.style.right=s,e.style.right=s,i.style.width=s}var y={}
y.init=function(){var n=this
n.each(e),n.append('<div class="split-pane-resize-shim">')
var f="ontouchstart"in document?"touchstart":"mousedown"
n.children(".split-pane-divider").html('<div class="split-pane-divider-inner"></div>'),n.children(".split-pane-divider").bind(f,i),setTimeout(function(){n.each(function(){t(this).bind("_splitpaneparentresize",s(t(this)))}),t(window).trigger("resize")},100)},y.setSize=function(e){this.each(function(){var i=t(this),s=p(i)
i.is(".fixed-top")?f(s,s.divider.offsetTop)({pageY:e}):i.is(".fixed-bottom")?o(s,-s.last.offsetHeight)({pageY:-e}):i.is(".horizontal-percent")?r(s,-s.last.offsetHeight)({pageY:-e}):i.is(".fixed-left")?a(s,s.divider.offsetLeft)({pageX:e}):i.is(".fixed-right")?d(s,-s.last.offsetWidth)({pageX:-e}):i.is(".vertical-percent")&&h(s,-s.last.offsetWidth)({pageX:-e})})},y.firstComponentSize=function(e){this.each(function(){var i=t(this),s=p(i)
i.is(".fixed-top")?f(s,s.divider.offsetTop)({pageY:e}):i.is(".fixed-bottom")?(e=s.splitPane.offsetHeight-s.divider.offsetHeight-e,o(s,-s.last.offsetHeight)({pageY:-e})):i.is(".horizontal-percent")?(e=s.splitPane.offsetHeight-s.divider.offsetHeight-e,r(s,-s.last.offsetHeight)({pageY:-e})):i.is(".fixed-left")?a(s,s.divider.offsetLeft)({pageX:e}):i.is(".fixed-right")?(e=s.splitPane.offsetWidth-s.divider.offsetWidth-e,d(s,-s.last.offsetWidth)({pageX:-e})):i.is(".vertical-percent")&&h(s,-s.last.offsetWidth)({pageX:-e})})},t.fn.splitPane=function(t){t||(t="init"),y[t].apply(this,Array.prototype.splice.call(arguments,1))}
var P="_splitpaneparentresizeHandler"
t.event.special._splitpaneparentresize={setup:function(e,i){var s=this,n=t(this).parent().closest(".split-pane")[0]||window
t(this).data(P,function(e){var i=e.target===document?window:e.target
i===n?(e.type="_splitpaneparentresize",t.event.dispatch.apply(s,arguments)):e.stopPropagation()}),t(n).bind("resize",t(this).data(P))},teardown:function(e){var i=t(this).parent().closest(".split-pane")[0]||window
t(i).unbind("resize",t(this).data(P)),t(this).removeData(P)}}}(jQuery)
