/* this is the RTA functions to be performed */
console.log("Functions loaded.");

$(document).ready(function(e) {
	init();
	
	function init() {
		// Initializing function
		// var LinkedAccounts_GAIN_LIVE = <?php echo json_encode($LinkedAccounts_GAIN_LIVE); ?>,
  //   		LinkedAccounts_GAIN_DEMO = <?php echo json_encode($LinkedAccounts_GAIN_DEMO); ?>,
  //   		LinkedAccounts_CQG_LIVE = <?php echo json_encode($LinkedAccounts_CQG_LIVE); ?>;

  //   		LinkedAccounts_GAIN_LIVE = $.map(LinkedAccounts_GAIN_LIVE, function (val, i) { return "GAIN_LIVE|" + val; });
  //   		LinkedAccounts_GAIN_DEMO = $.map(LinkedAccounts_GAIN_DEMO, function (val, i) { return "GAIN_DEMO|" + val; });
  //   		LinkedAccounts_CQG_LIVE = $.map(LinkedAccounts_CQG_LIVE, function (val, i) { return "CQG_LIVE|" + val; });

		// var TotalArray = $.merge( $.merge(LinkedAccounts_GAIN_LIVE, LinkedAccounts_GAIN_DEMO), LinkedAccounts_CQG_LIVE);
		var LinkedAccounts_GAIN_LIVE_Mapped = $.map(LinkedAccounts_GAIN_LIVE, function (val, i) { return "GAIN_LIVE|" + val; });
			LinkedAccounts_GAIN_DEMO_Mapped = $.map(LinkedAccounts_GAIN_DEMO, function (val, i) { return "GAIN_DEMO|" + val; });
			LinkedAccounts_CQG_LIVE_Mapped = $.map(LinkedAccounts_CQG_LIVE, function (val, i) { return "CQG_LIVE|" + val; });
		var TotalArray = $.merge( $.merge(LinkedAccounts_GAIN_LIVE_Mapped, LinkedAccounts_GAIN_DEMO_Mapped), LinkedAccounts_CQG_LIVE_Mapped);


		if (typeof isAdmin != "undefined" && isAdmin) {
			// if the user is ADMIN then show the auto complete box
			$("#admin-account-selector").autocomplete({
			  source: TotalArray,
			  select: function (event, ui) {
			  	    selectedAccount = ui.item.value.trim();
						var Broker = selectedAccount.split('|')[0],
					Account = selectedAccount.split('|')[1];

					newAccountSelected(selectedAccount);

			  	 setTimeout ( function () { 
			  	 		$("#admin-account-selector").val(Broker + ("*").repeat(Account.length-3) + selectedAccount.substr(selectedAccount.length-3, 3));
			  	 	}, 100 );
			  }
			});

			$("#admin-account-selector").click(function() { $(this).select(); } );
		}

		    	  
		// setting display results type on settings tab
		$("#results-type-display").html("Display: <b>" + ResultsDisplayType + "</b>");
		
		/*
		$("#settings-display-results").val(ResultsDisplayType);
		if ( $("#settings-display-results").val() == null) {
			if (typeof popupNotification == "function") popupNotification ("Results Display Type change not successful.  Default set to 'Points'.", "error", 5000);
			ResultsDisplayType = "Points";
			$("#settings-display-results").val(ResultsDisplayType);				
		}
		
		// setting commission fee on settings tab
		if (CommissionsEnabled) $("#commissions-display").html("Commissions: $<b>" + CommissionFee + "/contract</b>");
		$("#settings-commission-fee").val(CommissionFee);
		if ( $("#settings-commission-fee").val() == null) {
			if (typeof popupNotification == "function") popupNotification ("Commissions change not successful.  Commissions set to zero.", "error", 5000);
			CommissionFee = 0;
			$("#settings-commission-fee").val(CommissionFee);				
		}
		*/
	}
});


function updateContractSummaryBar (contract) {

	if (contract == null) return;
	
	try {
		// try to run function
		var ticker = contract.Symbol.toUpperCase();
		var ID = "A_" + ticker.trim().replace(/[.]/ig, "-");		// This prepends the "A_" before the ticker to create the ID for the element, as some tickers start with a number and jQuery does not allow IDs to start with a number

		// Set the border of selected contract to black
		//$(".contract-summary-box").css("border", "3px solid #333");
		//console.log("update", ID, selectedContract, ticker);
		if (selectedContract != null && selectedContract != "" && selectedContract == ticker) 
			$(".contract-summary-box#" + ID).css("border", "3px solid #DDDF0D");
		else
			$(".contract-summary-box#" + ID).css("border", "3px solid #333");

		var trade = contract.TradesArray.length > 0 ? contract.TradesArray[ contract.TradesArray.length - 1] : null;

		if (trade == null) return;
		
		var tickerObj_MainWindow = $(".contract-summary-box#" + ID);
		if (tickerObj_MainWindow.length > 0) {
			// Object found
			// Number of fills shows as user hovers over Symbol
			
			if (trade.Fills != null) tickerObj_MainWindow.find(".contract-symbol").attr("title", trade.Fills.length + " fills.");
			if (trade.TradeFillID != null && trade.TradeFillID != "") tickerObj_MainWindow.find(".contract-symbol").attr("id", trade.TradeFillID);
			
			// Calculate the trades Time In Trade
			trade.UpdateTimeInTrade();


			var sparkLineChart = $(".contract-summary-chart#" + ID).highcharts();
			 
			if (trade.Position == 0) {
				// Closed
				tickerObj_MainWindow.find(".average").text("n.a.");
				tickerObj_MainWindow.find(".delta").text("");
				tickerObj_MainWindow.find(".position").text(0);
				tickerObj_MainWindow.find(".MFE").text("n.a.");
				tickerObj_MainWindow.find(".MAE").text("n.a.");
				tickerObj_MainWindow.find(".BSO").text("n.a.");
				tickerObj_MainWindow.find(".TIT").text("n.a.");

				if ($("#closed-positions-display").is(":checked")) {
					tickerObj_MainWindow.find(".position-color").css("background-color", "#000");
				}				
				else {
					tickerObj_MainWindow.remove();
				}
					
				sparkLineChart.series[0].setData([]);
				sparkLineChart.series[1].setData([]);
						
			} else {
				// position not closed
			
				// Data update
				if (ResultsDisplayType == "Ticks") {
					tickerObj_MainWindow.find(".average").text(PointsToRound(trade.TheoreticalAverage, contract.TickSize));
					tickerObj_MainWindow.find(".delta").text((trade.TheoreticalAverageDelta > 0 ? "+" : "") + PointsToRound(trade.TheoreticalAverageDelta, contract.TickSize));
					tickerObj_MainWindow.find(".position").text(trade.Position);
					tickerObj_MainWindow.find(".MFE").text(PointsToTick(trade.MFE, contract.TickSize));
					tickerObj_MainWindow.find(".MAE").text(PointsToTick(trade.MAE, contract.TickSize));
					tickerObj_MainWindow.find(".BSO").text(PointsToTick(trade.BSO, contract.TickSize));
					tickerObj_MainWindow.find(".TIT").text(MillisecondsToTime(trade.TIT));
				} else if (ResultsDisplayType == "Dollars") {
					tickerObj_MainWindow.find(".average").text(PointsToRound(trade.TheoreticalAverage, contract.TickSize));
					tickerObj_MainWindow.find(".delta").text((trade.TheoreticalAverageDelta > 0 ? "+" : "") + PointsToRound(trade.TheoreticalAverageDelta, contract.TickSize));
					tickerObj_MainWindow.find(".position").text(trade.Position);
					tickerObj_MainWindow.find(".MFE").text(Round(trade.MFE * contract.ContractValue, 2));
					tickerObj_MainWindow.find(".MAE").text(Round(trade.MAE * contract.ContractValue, 2));
					tickerObj_MainWindow.find(".BSO").text(Round(trade.BSO, 2));
					tickerObj_MainWindow.find(".TIT").text(MillisecondsToTime(trade.TIT));
				} else if (ResultsDisplayType == "Points") {
					tickerObj_MainWindow.find(".average").text(PointsToRound(trade.TheoreticalAverage, contract.TickSize));
					tickerObj_MainWindow.find(".delta").text((trade.TheoreticalAverageDelta > 0 ? "+" : "") + PointsToRound(trade.TheoreticalAverageDelta, contract.TickSize));
					tickerObj_MainWindow.find(".position").text(trade.Position);
					tickerObj_MainWindow.find(".MFE").text(PointsToRound(trade.MFE, contract.TickSize));
					tickerObj_MainWindow.find(".MAE").text(PointsToRound(trade.MAE, contract.TickSize));
					tickerObj_MainWindow.find(".BSO").text(PointsToRound(trade.BSO, contract.TickSize));
					tickerObj_MainWindow.find(".TIT").text(MillisecondsToTime(trade.TIT));
				}

 				if (trade.Position > 0) {
					// Long
					tickerObj_MainWindow.find(".position-color").css("background-color", "#142f44");
					
					sparkLineChart.series[0].addPoint( PointsToRound(trade.MFE, contract.TickSize) , false, false , false );
					sparkLineChart.series[1].addPoint( -PointsToRound(trade.MAE, contract.TickSize) , false, false , false );

				} else {
					// Short
					tickerObj_MainWindow.find(".position-color").css("background-color", "#610e05");

					sparkLineChart.series[0].addPoint( PointsToRound(trade.MFE, contract.TickSize) , false, false , false );
					sparkLineChart.series[1].addPoint( -PointsToRound(trade.MAE, contract.TickSize) , false, false , false );

				}

			}

			if (name != "")	tickerObj_MainWindow.find(".contract-symbol").attr("title", name);
			
			sparkLineChart.reflow();
			sparkLineChart.redraw();
		
		} else {


			var barCreatorString =  "<table border='0' cellpadding='1' cellspacing='0' class='contract-summary-box' id='" + ID + "'>" + 
									"<tr height='25%' class='top-quarter'>" + 
										"<td rowspan='2' class='position-color contract-symbol' id='" + trade.TradeFillID + "'>" + ticker + "</td>" + 
										"<td class='display-border position-color'>POS</td>" + 
										"<td class='display-border'>Theo. Avg.</td>" + 
										"<td class='display-border'>MFE</td>" + 
										"<td class='display-border'>MAE</td>" + 
										"<td class='display-border'>BSO</td>" + 
										"<td>TiT</td>" + 
									"</tr>" + 
									"<tr height='25%'' class='middle-quarter'>" + 
										"<td class='display-border position position-color'></td>" + 
										"<td class='display-border'><span class='average'></span><span class='delta'></span></td>" + 
										"<td class='display-border MFE'></td>" + 
										"<td class='display-border MAE'></td>" + 
										"<td class='display-border BSO'></td>" + 
										"<td class='TIT'></td>" + 
									"</tr>" + 
									"<tr height='50%' class='bottom-half'>" + 
										"<td colspan='7'><span class='contract-summary-chart' id='" + ID + "'></span>" + 
										"</td>" + 
									"</tr>" + 
									"</table>";

			$(".split-pane-functional-window#summary-contracts-window .databar").append(barCreatorString);

			$(".contract-summary-chart#" + ID).highcharts(jQuery.extend(true, {}, sparkline_options));

			updateContractSummaryBar(contract);		
		}

	} catch (err) {
		console.log(err);
		//Raven.captureException(err);
	}



    // Update pop-out window if it exists
    try {
    	if (window.PopOutSummaryBar != null) {
        	$("#popup-contract-summary-container", window.PopOutSummaryBar.document).html($(".split-pane-functional-window#summary-contracts-window .databar").html());	
    	}
    } catch (err) {
    	console.log(err);
    }
    
}

/// This function plays sounds
function playSound( soundID ) {
	return;
	try {
		if (soundID != "") {
			if ($("#" + soundID).length > 0) {
				var audio = $("#" + soundID)[0];
				audio.play();
			}
		}
	}
	catch (err) {
		console.log("Your browser does not support sound play. Message: " + err);
	}
}

function newAccountSelected (selectedAccount) {
	if (selectedAccount == null || selectedAccount == "") {
		if ($("#account-selector").length > 0) selectedAccount = $("#account-selector").val().trim();
		else return false;
	}

	Trades = null;
	Trades = new TradeHandler();

	Trades.ReOrgContractsDropDownList();
	
	if (typeof ClearAllCharts == 'function') ClearAllCharts();
	$("#trades-window .databar").html("");
	$(".split-pane-functional-window#summary-contracts-window .databar").empty();

	$("#stats-current-pnl").text(0);
	$("#stats-num-of-trades").text(0);
	$("#stats-num-of-contracts-traded").text(0);
	$("#stats-current-expectancy").text(0);
	$("#stats-avg-win-duration").text("n.a.");
	$("#stats-avg-loss-duration").text("n.a.");

	// percent_win_dial = $('#percent-win-dial').highcharts();
	// percent_loss_dial = $('#percent-loss-dial').highcharts();
	// pnl_dial = $('#pnl-dial').highcharts();
	

	// percent_win_dial.series[0].points[0].update(0);
	// percent_loss_dial.series[0].points[0].update(0);
	// REZA
	//pnl_dial.yAxis[0].update({ min: -1, max: 1}, false);
	//pnl_dial.series[0].points[0].update(0);


	LookingAtAccount(selectedAccount);
}


function newContractSelected (ContractSelected) {
	if (typeof ClearAllCharts == "function") ClearAllCharts();
	Trades.DisplayTrades();	
}

function loadingWindow(isLoading) {
	if (isLoading) {
		// only show it if it's already hidden
		if ($("#dimmer").is(":hidden"))
			$("#dimmer").fadeIn('fast');
	} else {
		if (!$("#dimmer").is(":hidden"))
			$("#dimmer").fadeOut('fast');
	}
}

function LogUserOut_UUID_ERROR() {
	if (typeof popupNotification == "function") popupNotification ("You have lost your connection because you are logged in somewhere else.", "error", 0);
	$("#connection-status-dedicated").css("background-color", "darkred").attr("title", "Disconnected");
	$("#connection-status-shared").css("background-color", "darkred").attr("title", "Disconnected");
	window.pusher.disconnect();
}


function SplitPaneAdjusted(splitPane) {
	if (splitPane != null && typeof splitPane != "undefined") {
		//var id = $(splitPane).attr('id');
		if (typeof reflowChart === "function") reflowChart();
	}
}