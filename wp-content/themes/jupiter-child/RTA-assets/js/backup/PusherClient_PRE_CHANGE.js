/*
**  This is the RTA main PUSHER javascript file
*/

var Trades = new TradeHandler(null);

// Pusher Handler
$(document).ready(function(e) {
	
    console.log("Pusher JS loaded.");
	
	window.pusher = null;
	window.presenseChannel = null;
	window.dataChannel = null;
	
	
	/*
	Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };
	*/

	var isPusherSandbox = true; // (window.location.hostname == "localhost");
	var locationOfAuthenticator = (window.location.hostname == "localhost") ? "/stage5/" : "/";

	if (isPusherSandbox) {
		
		window.pusher = new Pusher("295bf81725f5f6a203da", {
			encrypted: true,
			authEndpoint: locationOfAuthenticator + 'pusher-test/pusher_auth.php',
			auth: {
			  params: {
				  'username' : username
			  }
			}		
		});

	} else {
		
		window.pusher = new Pusher(PUSHER_APP_KEY, {
			encrypted: true,
			authEndpoint: locationOfAuthenticator + 'pusher-auth/pusher_auth.php',
			auth: {
			  params: {
				  'username' : username
			  }
			}		
		});
	}

	window.pusher.connection.bind('connected', function () {
		$("#connection-status").text("Connected").css("background-color", "green");

		window.presenseChannel = pusher.subscribe('presence-users-channel');

		window.presenseChannel.bind('pusher:subscription_error', function(status) {
			if (typeof popupNotification == "function") popupNotification("Error connecting to presence channel.<br /><br />ERROR: " + status.toString(), "error", 30000);
			$("#connection-status").text("Disconnected").css("background-color", "red");
		});
		
		window.presenseChannel.bind('pusher:subscription_succeeded', function() {
			//if (typeof popupNotification == "function") popupNotification("Successfully connected to presence channel.", "success", 5000);
			$("#connection-status").text("Connected").css("background-color", "green");
		});


		var AllowAnyUUIDToConnect = false;

		// When succssfully connected to presence then connect to private data channel
		window.dataChannel = window.pusher.subscribe('private-data-pipe-' + username.toLowerCase());

		window.dataChannel.bind('pusher:subscription_succeeded', function() {
			var triggered = window.presenseChannel.trigger('client-NewSubscriber', { 'Username': username , 'UUID': UUID });
		});

		window.dataChannel.bind('pusher:subscription_error', function() {
			if (typeof popupNotification == "function") popupNotification("Disconnected from data channel.", "error", 0);
		});
			
		window.dataChannel.bind('client-HandShake', function(msg) {
			console.log("client-HandShake");
			console.log(msg);
			if (msg != null && msg.Username == username) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {
					LookingAtAccount(null);
				}
			}
		});
		
		window.dataChannel.bind('client-Shutdown', function(msg) {
			console.log("client-Shutdown");
			console.log(msg);
			if (typeof popupNotification == "function") popupNotification("Real-time data broker has shutdown. This could be for maintenance.<br />Please refresh your page in a few minutes.", "information", 0);
		});

		window.dataChannel.bind('client-AdminOperations', function(msg) {
			console.log("client-AdminOperations");
			console.log(msg);
			try {
				if (msg != null && msg.Data != null) {
					var jsonMsg = $.parseJSON(msg.Data);
					if (jsonMsg.Operation != null) {

						if (jsonMsg.Operation == "FillCheck") {
							if (Trades != null) Trades.CheckFillConsistency();

						} else if (jsonMsg.Operation  == "RefreshDashboard") {
							// Reload the page in a random time between zero to 15 seconds (15000 milliseconds)
							var refreshTimeInMilliseconds = Math.round(Math.random() * 15000) + 5000;
							if (typeof popupNotification == "function") popupNotification("Admin requested page refresh.  Your page will refresh in " + Math.round(refreshTimeInMilliseconds/1000) + " seconds...", "information", 0);
							setTimeout(function () {
								location.reload();
							}, refreshTimeInMilliseconds);

						}

					}
				}

			} catch (err) {
				console.error("There was an error in the admin function force.  Message: " + err);
			}
		});

		window.dataChannel.bind('client-PingByServer', function(msg) {
			console.log("PING");
			if (typeof popupNotification == "function") popupNotification("User pinged.", "alert", 2500);
			window.dataChannel.trigger('client-PongByUser', { 'Username': username });
		});

		window.dataChannel.bind('client-PongFromServer', function(msg) {
			console.log("Last server pong at " + new Date());
			if (typeof popupNotification == "function") popupNotification("Server returned pong.", "alert", 2500);
		});
		
		window.dataChannel.bind('client-NewTrade', function(msg) {
			console.log("client-NewTrade");
			//console.log(msg);
			if (msg.Username == username) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {
					Trades.InsertFill( $.parseJSON(msg.Data) , true );
				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}
		});
		
		window.dataChannel.bind('client-MarketData', function(msg) {
			if (msg.Username == username) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {
					if (Trades != null) Trades.PriceUpdate($.parseJSON(msg.Data));
				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}
		});
		
		window.dataChannel.bind('client-NewMessage', function(msg) {
			console.log("client-NewMessage");
			if (msg.Username == username) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {
					if (msg.Data != null) {
						var parsed = $.parseJSON(msg.Data);
						if (parsed.MessageType == "information") {
							if (typeof popupNotification == 'function') popupNotification (parsed.Message, parsed.MessageType, 0);
						}
						else {
							if (typeof popupNotification == 'function') popupNotification (parsed.Message, parsed.MessageType, 15000);
						}
					}

				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}
		});

		window.dataChannel.bind('client-BrokerDisconnected', function(msg) {
			if (msg.Username == username) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {
					if (typeof popupNotification == 'function') popupNotification ("Account (" + msg.Data + ") disconnected, connection with API lost.<br />This could be an internal or server error, please try another account or refresh in a few minutes." , "error", 30000);
				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}
		});

		window.dataChannel.bind('client-InitialTrades', function(msg) {
			console.log("client-InitialTrades");
			if (msg != null && msg.Username != null) {
				if (msg.UUID == UUID || msg.UUID == "---" || AllowAnyUUIDToConnect) {	
					if (msg.Data != null) {
						var parsedMsg = $.parseJSON(msg.Data);
						console.log(parsedMsg);
						if (parsedMsg.Page != null && parsedMsg.Total != null && parsedMsg.Data != null) {
							if (typeof loadingWindow == 'function') loadingWindow(true);
							if (Trades != null) Trades.InitFills(parsedMsg.Data, (parsedMsg.Page == parsedMsg.Total));
						}
					}
				} else {
					if (typeof LogUserOut_UUID_ERROR == "function") LogUserOut_UUID_ERROR();
				}
			}
		});



  
	});

		
});

var serverLastPinged = new Date();
var reponsePongByServerAt = new Date();
function PingServer () {
	if (new Date() - serverLastPinged > 30000) {
		window.dataChannel.trigger("client-PingFromClient", { 'Username': username });
		serverLastPinged = new Date();
		setTimeout( function () {
			// Check for pong response
			if (reponsePongByServerAt > serverLastPinged) {
				if (typeof popupNotification == 'function') popupNotification ("Pong received in " + (reponsePongByServerAt - serverLastPinged) + " milliseconds." , 'alert', 2500);
			} else {
				if (typeof popupNotification == 'function') popupNotification ("Pong not received." , 'warning', 2500);
			}
		}, 2500);
	}
}


function LookingAtAccount (account) {

	if (typeof account == 'undefined' || account == null || account == "") {
		if ($("#account-selector").length > 0) selectedAccount = $("#account-selector").val().trim();
		else return false;
	} else {
		selectedAccount = account;
	}

	console.log("Looking at account", selectedAccount);
	
	if (selectedAccount != "") {
		if (window.dataChannel != null)
			window.dataChannel.trigger('client-LookingAtAccount', { 'Username': username , 'UUID': UUID, 'Account': selectedAccount });
		else
			console.log("Data channel was null");
	} else {
		if (typeof popupNotification == 'function') popupNotification ("There was an error.  Account name is null or empty.  Please contact admin.", 'error', 5000);					
	}
}
	
