// RTA Trade Handler
window.FillsDisplayWindow = null;
window.PopOutSummaryBar = null;
window.ContractSummaryBarUpdateTimer = null;

window.ErrorHandler = function (err) {
	// reload after 5 seconds
	if (typeof popupNotification == "function") popupNotification ("An error occured, restarting your platform in 5 seconds.", "alert", 5000);

	var ErrorString = "Account: " + selectedAccount + " Contract: " + selectedContract + " Data: " + JSON.stringify(err);

	$.ajax({
		url: 'includes/Admin-data-dump.php',
		method: 'post',
		data: { Username: username, Data: ErrorString  },
		error: function (err) {},
		success: function (data) { 
			console.log("Error submitted.");
		}
	});

	setTimeout(function () {
		location.reload();	
	}, 5000);
}

var TradeHandler = function () {
	this.FillsBuffer = [];
	this.ContractsList = [];
	this.InitFillsIndexArray = [];

	try {
		if (window.ContractSummaryBarUpdateTimer == null) {
			window.ContractSummaryBarUpdateTimer = setInterval(function () {
				// start the timer that will call an update function to update the Contract Summary bar every second.
				if (Trades != null) Trades.UpdateSummaryBarDetails();
			}, 1000);
		}
	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.AdminDataDump = function () {
	// This will dump all inside the contractlist into a string and send it to server for storage
	$.ajax({
		url: 'includes/Admin-data-dump.php',
		method: 'post',
		data: { Username: username, Data: this.Account + "-" + JSON.stringify(this.ContractsList) },
		error: function (err) { 
			if (typeof popupNotification == "function") popupNotification (err, "error", 0);		
		},
		success: function (data) { 
			if (typeof popupNotification == "function") popupNotification (data, "alert", 5000);
		}
	});
};

TradeHandler.prototype.CheckForAllInitFills = function(total) {
	if (this.InitFillsIndexArray.length > 0)
		popupNotification("There was an error loading initial trades.  If reloading the page does not solve this, please contact admin.", "error", 5000);
};

TradeHandler.prototype.InitFills = function (parsedData) {
	console.log(parsedData);
	this.Account = selectedAccount;
	var tmpFillsArray = [];

	var page = parsedData.Page, 
		total = parsedData.Total,
		fills = parsedData.Data;

	if (typeof loadingWindow == 'function') loadingWindow(true);	

	var isFinalLoad = true;
	if (total > 1) {
		// If there is more than 1 page of data, then check order and completion
		var self = this;

		if ($.inArray(page, self.InitFillsIndexArray) < 0) {
			// If it's not in the array add it
			self.InitFillsIndexArray.push(page);
		}

		// Check if it's final load
		for (var i = 1; i <= total; i++) {
			if ($.inArray(i, self.InitFillsIndexArray) < 0) {
				isFinalLoad = false;

				// set timeout to check it in a few seconds
				setTimeout(function () {
					self.CheckForAllInitFills(total);
				}, 25000);

				break;
			}
		}
	}


	try {
		if (fills != null) {
			$.each(fills, function (i, fill) {
				/* FORMAT
		            obj["FillID"],
		            obj["OrderID"],
		            obj["Contract"],
		            obj["Side"],
		            obj["Quant"],
		            obj["Price"],
		            obj["TimestampUTC"],
		            obj["HighSince"],
		            obj["LowSince"],
		            obj["TypeOfTrade"],
		            obj["TickSize"],
		            obj["ContractSize"]
			    */

				var newFill = {
					FillID: fill[0],
					OrderID: fill[1],
					Contract: fill[2].replace(/\./g,'-'),
					Side: fill[3],
					Quant: fill[4],
					Price: fill[5],
					TimestampUTC: fill[6],
					HighSince: fill[7],
					LowSince: fill[8],
					TypeOfTrade: fill[9],
					TickSize: fill[10],
					ContractSize: fill[11]
				};
				tmpFillsArray.push(newFill);
			});
			
			this.FillsBuffer = $.merge(this.FillsBuffer, tmpFillsArray);
		}

		if (isFinalLoad) {
			this.Account = "";
			this.ContractsList = [];
			this.InitFillsIndexArray = [];

			if (typeof ClearAllCharts == 'function') ClearAllCharts();

			// sort fills buffer
			var tmpFillsArray = {};
			$.each(this.FillsBuffer, function (i, fill) {
				if (typeof (tmpFillsArray[fill.Contract]) == "undefined")
					tmpFillsArray[fill.Contract] = [fill];
				else
					tmpFillsArray[fill.Contract].push(fill);
			});
			this.FillsBuffer = [];


			$.each(tmpFillsArray, function (key, value) {
				var arrayOfFills = value;
				try {
					arrayOfFills.sort(function (a, b)
					{ 
						return (Number(a.FillID) - Number(b.FillID));
					});
				} catch (err) {
					console.log("Sorting array of fills had issues.");
				}

				$.each(arrayOfFills, function (i, fill) {
					Trades.InsertFill(fill, false);
				});
			});

			
			Trades.ReOrgContractsDropDownList();
			Trades.DisplayTrades();

			if (typeof loadingWindow == 'function') loadingWindow(false);
			
			// after 600 milliseconds call the load user metrics function
			setTimeout ( function () { Trades.GetUserTradeMetricsFromDB(); }, 600);
		}	

	} catch (err) {
		console.log(err);
		window.ErrorHandler(err);
		if (typeof loadingWindow == 'function') loadingWindow(false);
	}
};

TradeHandler.prototype.InsertFill = function (fill, isLive) {
	if (isLive == null) isLive = false;

	try {
		if ((isLive && fill.Account != null && fill.Account == selectedAccount) || (!isLive)) {
			
			// If this is a live fill, then show notification of the fill
			var popupString = "<b>" + fill.Contract + "</b> " + (fill.Side == "Buy" ? "Bought " : "Sold ") + fill.Quant + " " + " @ " + fill.Price + "<br /><span style='font-size: .8em'>" + moment.utc(fill.TimestampUTC).local().format('MMM D h:mma') + "</span>";
			if (isLive == true && typeof popupNotification == "function") popupNotification (popupString, "alert", 5000);
			
			// Search for the contract that this fill belongs to, call the contract's AddFill function
			var currentContract = null;
			$.each(this.ContractsList, function (i, contract) {
				// This ensures that all '.' is replaced with a '-' due to jQuery Errors
				fill.Contract = fill.Contract.replace(/\./g,'-');

				if (contract.Symbol == fill.Contract) {
					contract.AddFill(fill, isLive);
					currentContract = contract;
				}
			});
			
			
			// If no contracts exist, this is the initiating fill of a new contract
			// Create contract and add to list
			// Call the ReOrg function to add this to the Contract DropdownList

			if (currentContract == null) {
				if (selectedContract == null || selectedContract == "") selectedContract = fill.Contract;

				currentContract = new Contract(fill.Contract, fill.TickSize, fill.ContractSize, fill.StopTime);

				currentContract.AddFill(fill, isLive);
				this.ContractsList.push(currentContract);
				this.ReOrgContractsDropDownList();			
			}
			
			if (isLive) updateContractSummaryBar(currentContract);
		}


	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.PriceUpdate = function (priceObject) {
	// Update the contracts with prices

	try {
		$.each(this.ContractsList, function (i, contract) {
			if (priceObject.hasOwnProperty(contract.Symbol)) {
				contract.UpdatePrice(priceObject[contract.Symbol]);
			}
		});
	} catch (err) {
		window.ErrorHandler(err);
	}

};

TradeHandler.prototype.DisplayFillsForTrade = function (FillID, isSummaryToolBarClick) {
	console.log("DisplayFillsForTrade", FillID, isSummaryToolBarClick);
	try {

		$.each(this.ContractsList, function (i, contract) {
				if (contract.TradesArray != null && this.TradesArray.length > 0 && FillID != null) {
					$.each(this.TradesArray, function (i, trade) { 
						if (trade.TradeFillID == FillID) {
							if (trade.Fills.length > 0) {
								var tableHtml = "<table border='1' cellspacing=0 cellpadding=0 width='100%'><thead><th>FillID</th><th>OrderID</th><th>Side</th><th>Quant</th><th>Price</th><th>Timestamp (local time)</th><th>Type Of Fill</th></thead><tbody>";
								$.each(trade.Fills, function (key, fill) {
									tableHtml += "<tr><td>" + fill.FillID + "</td><td>" + fill.OrderID + "</td><td>" + fill.Side + "</td><td>" + fill.Quant + "</td><td>" + PointsToRound(fill.Price,  contract.TickSize) + "</td><td>" + moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().format("MMM DD - hh:mm:ss a") + "</td><td>" + fill.TypeOfTrade + "</td></tr>" ;
								});
								tableHtml += "</tbody></table>";
								
								var TradePnL = 0, TradePnLRaw = trade.PnLRealizedPts + trade.PnLUnrealizedPts;
								if (ResultsDisplayType == "Ticks") TradePnL = PointsToTick(TradePnLRaw, contract.TickSize);
								else if (ResultsDisplayType == "Dollars") TradePnL = Round(((TradePnLRaw * contract.ContractValue) - (trade.ContractsTraded * CommissionFee)), 2);
								else TradePnL = PointsToRound(TradePnLRaw, contract.TickSize);
								
								var params = "TickSize: " + contract.TickSize + "<br />Direction: " + (trade.isInitalLong ? "Long" : "Short");						
								var params2 = "Type: " + (TradePnL > 0 ? "Winner": ( TradePnL < 0 ? "Loser" : "Flat")) + "<br />P&L: " + (TradePnL) + (ResultsDisplayType != null ? ' (' + ResultsDisplayType + ')' : "");

								
								if (window.FillsDisplayWindow != null && window.FillsDisplayWindow.closed != null && window.FillsDisplayWindow.closed == false) {
									$("#display-header", window.FillsDisplayWindow.document).text(contract.Symbol); 
									$("#display-params", window.FillsDisplayWindow.document).html(params);
									$("#display-params2", window.FillsDisplayWindow.document).html(params2); 
									$("#display-fills", window.FillsDisplayWindow.document).html(tableHtml);
									
									window.FillsDisplayWindow.focus();
								} else {							
									window.FillsDisplayWindow = window.open(_BASE_URL + "/display-fills/", "Fills Window", "menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no,height=350,width=850", "NO");
									window.FillsDisplayWindow.onload = function () {
										$("#display-header", window.FillsDisplayWindow.document).text(contract.Symbol); 
										$("#display-params", window.FillsDisplayWindow.document).html(params);
										$("#display-params2", window.FillsDisplayWindow.document).html(params2); 
										$("#display-fills", window.FillsDisplayWindow.document).html(tableHtml);
										
										window.FillsDisplayWindow.focus();
									}
								}
							} else {
								if (typeof popupNotification == "function") popupNotification("An error occured.  This trade showed no fills.", "error", 30000);	
							}
						}
					});
				}
		});


	} catch (err) {
		window.ErrorHandler(err);
	}

};

TradeHandler.prototype.GetContract = function (Symbol) {
	try {
		var ReturnContract = null;
		$.each(this.ContractsList, function (i, contract) {
			if (contract.Symbol == Symbol) {
				ReturnContract = contract;
				return false;
			}
		});
		return ReturnContract;

	} catch (err) {
		window.ErrorHandler(err);
		return null;
	}
};

TradeHandler.prototype.UpdateSummaryBarDetails = function () {
	try {

		$.each(this.ContractsList, function (i, contract) {
			updateContractSummaryBar(contract);
		});

	} catch (err) {
		window.ErrorHandler(err);
	}
	
};

TradeHandler.prototype.GetUserTradeMetricsFromDB = function () {
	try {

		var FillIDArray = [];
		$.each(this.ContractsList, function (i, contract) {
			console.log("Getting trade metrics for " + contract.Symbol);
			if (contract.TradesArray != null && contract.TradesArray.length > 0) {
				$.each (contract.TradesArray, function (j, trade) {
					FillIDArray.push(trade.TradeFillID);
				});
			}
		});
		
		if (FillIDArray.length > 0) {
			// There were some inital Fill IDs to use for getting trade metrics	
			$.ajax({
				url: "includes/RTA-GetSetUserMetrics.php",
				method: "post",
				type: "post",
				data: { Operation: "GET", Username: username, FillIDArray: FillIDArray },
				error: function (err) { 
					console.log(err);
					if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
				},
				success: function (data) { 
					data = $.parseJSON(data);
					if (data != null && data.OutputType != null) {
						if (data.OutputType == "ERROR") {
							// There was an error that happened during the retrieval of data
							var errStr = data.Data != null ? data.Data : "Data field of error empty.";
							if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: " + errStr, "error", 15000);
						} else {
							// Successful
							if (data.Data != null) {
								var ResponseArray = data.Data;
								var CurrentContract = null;
								$.each (data.Data, function (i, fillData) {
									if (CurrentContract == null || CurrentContract.Symbol != fillData.Contract) CurrentContract = Trades.GetContract(fillData.Contract);
									if (CurrentContract != null) {
										$.each(CurrentContract.TradesArray, function (index, trade) {
											if (trade.TradeFillID == fillData.FillID) {
												if (fillData.TradeGrade != "") trade.TradeGrade = fillData.TradeGrade;
												if (fillData.TradeType != "") trade.TradeType = fillData.TradeType;
												if (fillData.TradeError != "") trade.TradeError = fillData.TradeError;
												
												// Traverse an already created and displayed table to change the User Trade Metrics
												if (selectedContract == fillData.Contract) {
													var TradeOBJ_onTable = $("#trades-table").find("tr#" + fillData.FillID);
													if (TradeOBJ_onTable.length > 0) {
														// if found
														var TradeGradeSelector = TradeOBJ_onTable.find(".trade-grade-selector"),
															TradeTypeSelector = TradeOBJ_onTable.find(".trade-type-selector"),
															TradeErrorSelector = TradeOBJ_onTable.find(".trade-error-selector");
															
															if (TradeGradeSelector.length > 0) TradeGradeSelector.val(fillData.TradeGrade);
															if (TradeTypeSelector.length > 0) TradeTypeSelector.val(fillData.TradeType);														
															if (TradeErrorSelector.length > 0) TradeErrorSelector.val(fillData.TradeError); 
													}
												}
												return false;	
											}
										});
									}
								});
								if (typeof popupNotification == "function") popupNotification ("Successfully loaded User Trade Metrics.", "success", 5000);
							}
						}
					} else {
						// no user trade metrics
						// if (typeof popupNotification == "function") popupNotification ("No User Trade Metrics were retrieved.", "warning", 5000);
					}
				}
			});
		}

	} catch (err) {
		window.ErrorHandler(err);
	}

};

TradeHandler.prototype.UpdateTradeType = function (tradeFillID, newType) {
	try {

		// Update in TradeType to DB
		$.ajax({
			url: 'includes/RTA-GetSetUserMetrics.php',
			type: 'post',
			method: 'post',
			data: { Operation: "SET", Username: username, Account: selectedAccount, Contract: selectedContract, FillID: tradeFillID, Metric: "TradeType", Value: newType },
			error: function (err) { 
				console.log(err);
				if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
			},
			success: function (data) { 
				console.log(data);
				parsedData = $.parseJSON(data);
				if (parsedData.OutputType == "ERROR") 
					if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
			}
		});

		$.each(this.ContractsList, function (i, contract) {
			if (selectedContract != null & contract.Symbol == selectedContract) {
				contract.UpdateTradeType(tradeFillID, newType);
			}
		});	


	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.UpdateTradeGrade = function (tradeFillID, newGrade) {
	try {

		$.ajax({
			url: 'includes/RTA-GetSetUserMetrics.php',
			type: 'post',
			method: 'post',
			data: { Operation: "SET", Username: username, Account: selectedAccount, Contract: selectedContract, FillID: tradeFillID, Metric: "TradeGrade", Value: newGrade },
			error: function (err) { 
				console.log(err);
				if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
			},
			success: function (data) { 
				console.log(data);
				parsedData = $.parseJSON(data);
				if (parsedData.OutputType == "ERROR") 
					if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
			}
		});

		$.each(this.ContractsList, function (i, contract) {
			if (selectedContract != null & contract.Symbol == selectedContract) {
				contract.UpdateTradeGrade(tradeFillID, newGrade);	
			}
		});	

	} catch (err) {
		window.ErrorHandler(err);
	}	
};

TradeHandler.prototype.UpdateTradeError = function (tradeFillID, newError) {
	try {

		$.ajax({
			url: 'includes/RTA-GetSetUserMetrics.php',
			type: 'post',
			method: 'post',
			data: { Operation: "SET", Username: username, Account: selectedAccount, Contract: selectedContract, FillID: tradeFillID, Metric: "TradeError", Value: newError },
			error: function (err) { 
				console.log(err);
				if (typeof popupNotification == "function") popupNotification ("There was an error loading User Trade Metrics.  Please let admin know.<br /><br />Message: ", "error", 15000);
			},
			success: function (data) { 
				parsedData = $.parseJSON(data);
				if (parsedData.OutputType == "ERROR") 
					if (typeof popupNotification == "function") popupNotification (parsedData.Data, "error", 15000);			
			}
		});

		$.each(this.ContractsList, function (i, contract) {
			if (selectedContract != null & contract.Symbol == selectedContract) {
				contract.UpdateTradeError(tradeFillID, newError);	
			}
		});	

	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.ReOrgContractsDropDownList = function () {
	try {

		if (this.ContractsList.length == 0) {
			$("#contract-selector").empty();
			$("<option />", { val: "--", text: "--" }).appendTo($("#contract-selector"));
		} else {
			var contracts = [];
			$.each(this.ContractsList, function (i, cont) { contracts.push( cont.Symbol ) });
			contracts.sort();
			$("#contract-selector").empty();
			$.each(contracts, function (i, cont) { $("<option />", { val: cont, text: cont }).appendTo( $("#contract-selector") ) });

			if (selectedContract == null || selectedContract == "") {
				selectedContract = contracts[0];
			}
			$("#contract-selector").val(selectedContract);
			var SelectorFirstOBJ = $("#contract-selector option:first-child");
			if (SelectorFirstOBJ.length > 0) {
				selectedContract = SelectorFirstOBJ.val();
				SelectorFirstOBJ.attr("selected", "selected");		// select first item		
			}
		}

	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.DisplayTrades = function () {
	// this function will display the trades' metrics to
	//		1) Stats toolbar,
	//		2) Charts
	//		3) Completed Trades table

	try {

		$.each(this.ContractsList, function(i, contract) {
			// This displays everything to screen according to which contract is the Selected Contract
			contract.DisplayToScreen();
			updateContractSummaryBar(contract);
		});

	} catch (err) {
		window.ErrorHandler(err);
	}
};

TradeHandler.prototype.CheckFillConsistency = function () {
	/*
		This function will check contracts/fills for consistency, such as
		1) Are fills in 'ascending' order?
		2) Do fills and position (ContractsHeld), and TypeOfTrade (O, S, C, -) match the position?
	*/
	console.log("Running Fill Consistency Check...");

	try {

		var errorStringTotal = "";
		$.each(this.ContractsList, function(i, contract) {
			// Run the CheckFillConsistency function for each contract
			errorStringTotal += contract.CheckFillConsistency();
		});

		if (errorStringTotal) {
			// There were errors
			
			/* Don't tell the user
			if (typeof popupNotification == "function")
					popupNotification ("<strong>Consistency Check Error:</strong><br />" + errorStringTotal, "warning", 0);

			*/

			$.ajax({
				url: 'includes/Admin-data-dump.php',
				method: 'post',
				data: { Username: username, Data: "Consistency Error (" + selectedAccount + ") - " +  errorStringTotal },
				error: function (err) { 
					if (typeof popupNotification == "function") popupNotification (err, "error", 0);		
				},
				success: function (data) { 
					//if (typeof popupNotification == "function") popupNotification (data, "alert", 5000);
				}	
			});

			// Also do a data dump
			try {
				this.AdminDataDump();	
			} catch (err) {
				console.error("RK - Data dump error in Consistency check", err);
			}
			

			// Something went wrong and there were errors, just ask the server to reset and give you accounts again
			setTimeout(function () {
				LookingAtAccount(selectedAccount);	
			}, 5 * 1000);

		} else {
			// no errors
			console.log("Fill consistency returned no errors.");
			//if (typeof popupNotification == "function") popupNotification ("Consistency check was successful.  No errors returned.", "success", 5000);
		}

	} catch (err) {
		window.ErrorHandler(err);
	}
	
};

var Trade = function() {
	this.TradeNumber = 0;
	this.TradeFillID = "";
	this.Fills = [];
	
	// Metrics
	this.EntryDateTime = 0;
	this.ExitDateTime = 0;
	this.EntryPrice = 0;
	this.ExitPrice = 0;
	this.isFlat = true
	this.isSplit = false;
	this.isInitalLong = null;
	
	this.Position = 0;
	this.AveragePrice = 0;
	this.TheoreticalAverage = 0;
	this.TheoreticalAverageDelta = 0;

	this.ContractsTraded = 0;
	this.ContractsLong = 0;
	this.ContractsShort = 0;
	
	this.TotalBuyContracts = 0;
	this.TotalSellContracts = 0;
	this.TotalBuyPoints = 0;
	this.TotalSellPoints = 0;
	this.MaxPosition = 0;

	this.Commissions = 0;
	this.PnLRealizedPts = 0;
	this.PnLUnrealizedPts = 0;
	this.PnLRealizedDollars = 0;
	this.PnLUnrealizedDollars = 0;

	this.BSO = 0;	
	this.IsBSOEligible = false;
	
	this.MAE = 0;
	this.MFE = 0;
	this.TIT = 0;
	
	// Database Stored individual Metrics
	this.TradeGrade = "";
	this.TradeError = "";
	this.TradeType = "";
};

Trade.prototype.UpdateTimeInTrade = function () {
	try {
		if (this.Position != 0) {
			var TiT = moment() - this.EntryDateTime
			this.TIT = (TiT < 0 ? 0 : TiT);
		}	
	} catch (err) {}
};

var Contract = function(Symbol, TickSize, ContractValue, ClosingTime) {
	// Contract Details
	this.Symbol = Symbol;
	this.TickSize = TickSize;
	this.ContractValue = ContractValue;
	this.ClosingTime = ClosingTime;

	this.FillsArray = [];
	this.TradesArray = [];
	
	// Total Trades Details
	this.NumberOfTrades = 0;
	this.NumberOfWins = 0;
	this.NumberOfLosses = 0;
	this.TotalWinsPts = 0;
	this.TotalWinsDollars = 0;
	this.TotalLossesPts = 0;
	this.TotalLossesDollars = 0;
	this.TotalWinDuration = 0;
	this.TotalLossDuration = 0;
	this.TotalBSOTrades = 0;
	this.TotalBSOEligibleTrades = 0;
	this.TotalFullStopTrades = 0;

	this.AverageWinDuration = 0;
	this.AverageLossDuration = 0;
	this.PercentWin = 0;
	this.PercentLoss = 0;
	
	this.MaxPosition = 0;
	this.ContractsTraded = 0;
	this.Expectancy = 0;
	this.PeakGain = 0;
	this.MaxDrawdown = 0;
	this.PercentFullStop = 0;
	this.PercentBSO = 0;
	
	this.isFlat = true;
	this.Position = 0;
	
	// References
	this.LastTrade = null;
	this.LastFill = null;
	
	// Metrics
	this.AveragePrice = 0;
	this.TheoreticalPrice = 0;
	this.TheoreticalPriceDelta = 0;
	this.MAE = 0;
	this.MFE = 0;
	this.BSO = 0;
	this.TIT = 0;
	this.TSB = 0;
	this.Commissions = 0;
	this.PnLRealizedPts = 0;
	this.PnLUnrealizedPts = 0;
	this.PnLRealizedDollars = 0;
	this.PnLUnrealizedDollars = 0;
};

Contract.prototype.UpdateTradeType = function (tradeFillID, newType) {
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeType = newType;
			return false;
		}
	});
};

Contract.prototype.UpdateTradeGrade = function (tradeFillID, newGrade) {
	console.log("UpdateTradeGrade ---> " + this.Symbol + " TradeID: " + tradeFillID + " newType: " + newGrade);
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeGrade = newGrade;
			return false;
		}
	});
};

Contract.prototype.UpdateTradeError = function (tradeFillID, newError) {
	console.log("UpdateTradeError ---> " + this.Symbol + " TradeID: " + tradeFillID + " newType: " + newError);
	$.each(this.TradesArray, function (i, trade) {
		if (trade.TradeFillID == tradeFillID) {
			trade.TradeError = newError;
			return false;
		}
	});	
};

Contract.prototype.SortFills = function () {
	this.FillsArray.sort(function (a, b) { return a.FillID - b.FillID; });
};

Contract.prototype.AddFill = function (fill, isLive) {
	isLive = (isLive != null && isLive == true) ? true : false;
	this.LastFill = fill;

	// Push the fill into the Contract's FillsArray
	this.FillsArray.push(fill);

	// Find out if this fill will fit into an already open trade or if it starts opening a new trade
	if (this.LastTrade == null) {
		// An opening for a new trade	
		
		// Set the initial trade metrics
		var trade = new Trade();
		trade.TradeNumber = this.TradesArray.length + 1;
		trade.TradeFillID = fill.FillID;
		trade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
		trade.EntryPrice = fill.Price;
		trade.isFlat = false;
		
		if (fill.Side == "Buy") {
			trade.isInitalLong = true;
			trade.Position = fill.Quant;
			trade.TotalBuyContracts = fill.Quant;
			trade.TotalBuyPoints = fill.Quant * fill.Price;
		} else {
			trade.isInitalLong = false;
			trade.Position = -fill.Quant;
			trade.TotalSellContracts = fill.Quant;
			trade.TotalSellPoints = fill.Quant * fill.Price;
		}

		trade.AveragePrice = fill.Price;
		trade.TheoreticalAverage = fill.Price;
		trade.TheoreticalAverageDelta = 0;
		trade.ContractsTraded = fill.Quant;
		trade.MaxPosition = fill.Quant;

		trade.Commissions += fill.Quant * CommissionFee;
		
		trade.Fills.push(fill);
		
		if (!isLive) {
			// Calculate MFE/MAE	
			if (trade.isInitalLong) {
				trade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
			} else {
				trade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
				trade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
			}
		}

		// Add trade to Contract's TradesArray
		this.TradesArray.push(trade);
		this.LastTrade = trade;

	} else {
		// This is part of an already open trade
		var trade = this.LastTrade;

		if (trade.isInitalLong) {
			// The trade is a long trade
			if (fill.Side == "Buy") {
				// Long & Buy -> Continuation Trade

				// Push fill into the Trade's Fills
				trade.Fills.push(fill);

				trade.ContractsTraded += fill.Quant;
				trade.TotalBuyContracts += fill.Quant;
				trade.TotalBuyPoints += fill.Quant * fill.Price;
				
				trade.Position += fill.Quant;
				trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
				trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);

				if (isLive) {
					var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
				} else {
					var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) - trade.PnLRealizedPts;					
				}
				

				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				
				//trade.BSO = ?;  NO BSO calculation here
				trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local() - trade.EntryDateTime;

				var newTheoreticalAverage = (trade.TotalBuyPoints - trade.TotalSellPoints) / trade.Position;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					
				
			} else {
				// Long & Sell -> Closing, Reduction, or Split Trade
				var newPosition = trade.Position - fill.Quant;

				if (newPosition == 0) {
					// Flatting fill
					// Push fill into the Trade's Fills
					trade.Fills.push(fill);

					trade.isFlat = true;
					
					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					trade.ExitPrice = fill.Price;
	
					trade.ContractsTraded += fill.Quant;
					trade.TotalSellContracts += fill.Quant;
					trade.TotalSellPoints += (fill.Quant * fill.Price);
					
					trade.Position = 0;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - trade.TotalBuyPoints);

					if (isLive) {
						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) - trade.PnLRealizedPts;					
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
		
					//trade.BSO = 0;
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
			
					this.LastTrade = null;
			
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);

				} else if (newPosition > 0) {
					// Reduction trade

					// Push fill into the Trade's Fills
					trade.Fills.push(fill);

					trade.ContractsTraded += fill.Quant;
					trade.TotalSellContracts += fill.Quant;
					trade.TotalSellPoints += (fill.Quant * fill.Price);
					
					trade.Position = newPosition;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - ((trade.TotalBuyPoints / trade.TotalBuyContracts) * trade.TotalSellContracts));
					
					if (isLive) {
						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) - trade.PnLRealizedPts;					
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);

					trade.BSO = Math.max( trade.BSO, (fill.Price - trade.AveragePrice));
					trade.IsBSOEligible = true;
					trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().diff(trade.EntryDateTime);

					// Theoretical Average Calcs
					var newTheoreticalAverage = (trade.TotalBuyPoints - trade.TotalSellPoints) / trade.Position;
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;					


										
				} else if (newPosition < 0) {
					// Split trade!

					// Push fill into the Trade's Fills
					// IMPORTANT - we have to account for the split
					var contractsToClosePosition = Math.abs(trade.Position);
					fill.Quant = contractsToClosePosition;
					trade.Fills.push(fill);


					// Check the trade's TypeOfTrade metric, it should be 'O'
					if (fill.TypeOfTrade != "S") {
						if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " fill was meant to be a split trade, but TypeOfTrade was " + fill.TypeOfTrade + "!<br /><br />" + FillToString(fill), "error", 30000);
					}
					
					trade.isFlat = true;
					trade.isSplit = true;

					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					trade.ExitPrice = fill.Price;
	
					trade.ContractsTraded += contractsToClosePosition;
					trade.TotalSellContracts += contractsToClosePosition;
					trade.TotalSellPoints += (contractsToClosePosition * fill.Price);

					trade.Position = 0;
					trade.AveragePrice = trade.TotalBuyPoints / trade.TotalBuyContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = trade.TotalSellPoints - trade.TotalBuyPoints;
					trade.PnLUnrealizedPts = 0;

					if (isLive) {
						var MFEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) - trade.PnLRealizedPts;					
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
					
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
				
					// SPLIT TRADE INIT					
					// var splitTrade = new Trade();
					// splitTrade.TradeNumber = this.TradesArray.length;
					// splitTrade.TradeFillID = fill.FillID;

					// Set the initial trade metrics
					var newFill = jQuery.extend(true, {}, fill);
					newFill.Quant = Math.abs(newPosition);

					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					splitTrade.isSplit = true;
					
					splitTrade.isInitalLong = false;
					splitTrade.Position = newPosition;
					//splitTrade.TotalSellContracts = fill.Quant;
					//splitTrade.TotalSellPoints = fill.Quant * fill.Price;
			
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;
		
					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					//splitTrade.Quant = Math.abs(newPosition);
					splitTrade.Fills.push(newFill);

					splitTrade.MaxPosition = newFill.Quant;
					splitTrade.ContractsTraded += newFill.Quant;
					splitTrade.TotalSellContracts += newFill.Quant;
					splitTrade.TotalSellPoints += newFill.Quant * fill.Price;
				
					if (!isLive) {
						// Calculate MFE/MAE	
						if (splitTrade.isInitalLong) {
							splitTrade.MFE = (fill.HighSince - fill.Price) * newFill.Quant;
							splitTrade.MAE = (fill.Price - fill.LowSince) * newFill.Quant;
						} else {
							splitTrade.MAE = (fill.HighSince - fill.Price) * newFill.Quant;
							splitTrade.MFE = (fill.Price - fill.LowSince) * newFill.Quant;
						}
					}
			
					// Add trade to Contract's TradesArray
					this.TradesArray.push(splitTrade);
					this.LastTrade = splitTrade;
					
					
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
			

				}				
			}
		} else {
			// this is a short trade
			if (fill.Side == "Buy") {
				// Short & Buy -> Closing, Reduction, or Split Trade
				var newPosition = trade.Position + fill.Quant;

				if (newPosition == 0) {
					// Flatting fill
					
					// Push fill into the Trade's Fills
					trade.Fills.push(fill);

					trade.isFlat = true;
					
					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					trade.ExitPrice = fill.Price;
	
					trade.ContractsTraded += fill.Quant;
					trade.TotalBuyContracts += fill.Quant;
					trade.TotalBuyPoints += (fill.Quant * fill.Price);
					
					trade.Position = 0;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - trade.TotalBuyPoints);
					trade.PnLUnrealized = 0;
						
					if (isLive) {
						var MFEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);

					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
					
					this.LastTrade = null;
					
					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
					
				} else if (newPosition < 0) {
					// Reduction trade

					// Push fill into the Trade's Fills
					trade.Fills.push(fill);

					trade.ContractsTraded += fill.Quant;
					trade.TotalBuyContracts += fill.Quant;
					trade.TotalBuyPoints += (fill.Quant * fill.Price);
					
					trade.Position = newPosition;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (((trade.TotalSellPoints / trade.TotalSellContracts) * trade.TotalBuyContracts) - trade.TotalBuyPoints);

					if (isLive) {
						var MFEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				
					trade.BSO = Math.max(trade.BSO, (trade.AveragePrice - fill.Price));
					trade.IsBSOEligible = true;
					trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local().diff(trade.EntryDateTime);

					// Theoretical Average Calcs
					var newTheoreticalAverage = (trade.TotalBuyPoints - trade.TotalSellPoints) / trade.Position;
					trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
					trade.TheoreticalAverage = newTheoreticalAverage;				
					
					
				} else if (newPosition > 0) {
					// Split trade! Short -> Long
					var contractsToClosePosition = Math.abs(trade.Position);

					// Push fill into the Trade's Fills
					fill.Quant = contractsToClosePosition;
					trade.Fills.push(fill);

					// Check the trade's TypeOfTrade metric, it should be 'O'
					if (fill.TypeOfTrade != "S") {
						if (typeof popupNotification == "function") popupNotification ("Contract " + this.Symbol + " fill was meant to be a split trade, but TypeOfTrade was " + fill.TypeOfTrade + "!<br /><br />" + FillToString(fill), "error", 30000);
					}
					
					
					trade.isFlat = true;
					trade.isSplit = true;

					trade.ExitDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					trade.ExitPrice = fill.Price;
						
					trade.ContractsTraded += contractsToClosePosition;
					trade.TotalBuyContracts += contractsToClosePosition;
					trade.TotalBuyPoints += (contractsToClosePosition * fill.Price);

					trade.Position = 0;
					trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
					trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);
					
					trade.PnLRealizedPts = (trade.TotalSellPoints - trade.TotalBuyPoints);
					trade.PnLUnrealizedPts = 0;

					if (isLive) {
						var MFEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					} else {
						var MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
							MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					}
					
					trade.MFE = Math.max(trade.MFE , MFEonThisFill);
					trade.MAE = Math.max(trade.MAE , MAEonThisFill);
				
					//trade.BSO = 0;
					trade.TIT = trade.ExitDateTime.diff(trade.EntryDateTime);
				
				
					// Set the initial trade metrics
					var newFill = jQuery.extend(true, {}, fill);
					newFill.Quant = Math.abs(newPosition);

					var splitTrade = new Trade();
					splitTrade.TradeNumber = this.TradesArray.length + 1;
					splitTrade.TradeFillID = fill.FillID;
					splitTrade.EntryDateTime = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local();
					splitTrade.EntryPrice = fill.Price;
					splitTrade.isFlat = false;
					splitTrade.isSplit = true;

					splitTrade.isInitalLong = true;
					splitTrade.Position = newPosition;
					//splitTrade.TotalSellContracts = fill.Quant;
					//splitTrade.TotalSellPoints = fill.Quant * fill.Price;
					
					splitTrade.AveragePrice = fill.Price;
					splitTrade.TheoreticalAverage = fill.Price;
					splitTrade.TheoreticalAverageDelta = 0;	
					splitTrade.Commissions += Math.abs(newPosition) * CommissionFee;

					splitTrade.Fills.push(newFill);

					splitTrade.MaxPosition = newFill.Quant;
					splitTrade.ContractsTraded = newFill.Quant;
					splitTrade.TotalBuyContracts += newFill.Quant;
					splitTrade.TotalBuyPoints += newFill.Quant * fill.Price;


					if (!isLive) {
						// Calculate MFE/MAE	
						if (splitTrade.isInitalLong) {
							splitTrade.MFE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MAE = (fill.Price - fill.LowSince) * fill.Quant;
						} else {
							splitTrade.MAE = (fill.HighSince - fill.Price) * fill.Quant;
							splitTrade.MFE = (fill.Price - fill.LowSince) * fill.Quant;
						}
					}
			
					// Add trade to Contract's TradesArray
					this.LastTrade = splitTrade;
					this.TradesArray.push(splitTrade);

					/* TRADE CLOSED -> This is for contract updating */
					if (isLive) this.UpdateMetricsWithTrade(trade);
							
				}
			} else {
				// Short & Sell -> This is a continuation trade
				// Push fill into the Trade's Fills
				trade.Fills.push(fill);

				trade.ContractsTraded += fill.Quant;
				trade.TotalSellContracts += fill.Quant;
				trade.TotalSellPoints += fill.Quant * fill.Price;
				
				trade.Position += -fill.Quant;
				trade.AveragePrice = trade.TotalSellPoints / trade.TotalSellContracts;
				trade.MaxPosition = Math.max(Math.abs(trade.Position), trade.MaxPosition);

				trade.Commissions += (fill.Quant * CommissionFee);

				if (isLive) {
					var MFEonThisFill = ((trade.AveragePrice - fill.Price) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((fill.Price - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
				} else {
					var MFEonThisFill = ((trade.AveragePrice - fill.LowSince) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
						MAEonThisFill = ((fill.HighSince - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts;
				}
				
				trade.MFE = Math.max(trade.MFE , MFEonThisFill);
				trade.MAE = Math.max(trade.MAE , MAEonThisFill);

				//trade.BSO = ?;  NO BSO calculation here
				trade.TIT = moment.utc(fill.TimestampUTC, "YYYY-MM-DD HH:mm:ss").local() - trade.EntryDateTime;

				var newTheoreticalAverage = (trade.TotalBuyPoints - trade.TotalSellPoints) / trade.Position;
				trade.TheoreticalAverageDelta = newTheoreticalAverage - trade.TheoreticalAverage;
				trade.TheoreticalAverage = newTheoreticalAverage;					
				
			}
			
		}
	}
};

Contract.prototype.UpdateMetricsWithTrade = function (trade) {
	// This msg now let's Display To Screen recalculate everything on the fly
	this.DisplayToScreen();
	return;
};

Contract.prototype.UpdatePrice = function (PriceObject) {
	try {
		var High = PriceObject["H"],
		    Low  = PriceObject["L"],
		    MarketPrice = High;

		//console.log("Market Price of " + this.Symbol + " Low: " + Low + " High: " + High + " MP: " + MarketPrice);

		var trade = this.LastTrade;
		if (trade != null) {
			var PnLUnrealized = trade.Position * (MarketPrice - trade.AveragePrice);	
			trade.PnLUnrealized = PnLUnrealized;
			this.PnLUnrealized = PnLUnrealized;
			

			if (trade.isInitalLong) {
				var MFEonPriceUpdate = ((High - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
					MAEonPriceUpdate = ((trade.AveragePrice - Low) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					
				trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
				trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);

			} else {
				var MFEonPriceUpdate = ((trade.AveragePrice - Low) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
					MAEonPriceUpdate = ((High - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					
				trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
				trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);
			}

			/* OLD
			if (trade.isInitalLong) {
				var MFEonPriceUpdate = ((MarketPrice - trade.AveragePrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
					MAEonPriceUpdate = ((trade.AveragePrice - MarketPrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					
				trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
				trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);

			} else {
				var MFEonPriceUpdate = ((trade.AveragePrice - MarketPrice) * Math.abs(trade.Position)) + trade.PnLRealizedPts,
					MAEonPriceUpdate = ((MarketPrice - trade.AveragePrice) * Math.abs(trade.Position)) - trade.PnLRealizedPts;
					
				trade.MFE = Math.max(trade.MFE , MFEonPriceUpdate);
				trade.MAE = Math.max(trade.MAE , MAEonPriceUpdate);
			}
			*/

			/*		
			var TiT = TimestampUTC - trade.EntryDateTime
			trade.TIT = (TiT < 0 ? 0 : TiT);
			*/

			// Update data bars		
			//updateContractSummaryBar (this);
		}
		
		if (this.LastFill != null) {
			if (MarketPrice > this.LastFill.HighSince) this.LastFill.HighSince = MarketPrice;
			if (MarketPrice < this.LastFill.LowSince) this.LastFill.LowSince = MarketPrice;	
		}

	} catch (err) {

		console.error("Contract " + this.Symbol + " market price error.", PriceObject);
	}
};

Contract.prototype.DisplayToScreen = function () {
	try {

		// The default display Type is Points
		if (ResultsDisplayType == null) ResultsDisplayType = "Ticks";

		if (selectedContract == null || selectedContract != this.Symbol) return;	// Not current Contract
		
		ClearAllCharts();
		
		win_loss_percentage_chart = $(".chart-container#win-loss-percentage-chart").highcharts();
		expectancy_chart = $(".chart-container#expectancy-chart").highcharts();
		equity_curve_chart = $(".chart-container#equity-curve-chart").highcharts();
		trade_distribution_chart = $(".chart-container#trade-distribution-chart").highcharts();

		var TickSize = this.TickSize,
			ContractValue = this.ContractValue,
			contract = this;
			
		var TradeTypeArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

		// Metrics that need to be calculated and displayed
		// Reset calculated metrics
		this.NumberOfWinningTrades = 0;
		this.NumberOfLosingTrades = 0;
		this.NumberOfTotalTrades = 0;
		this.NumberOfTotalContractsTraded = 0;
		this.PercentWinners = 0;
		this.PercentLosers = 0;
			
		this.Expectancy = 0;
		this.Efficiancy = 0;

		this.NetGain = 0;
		this.NetLoss = 0;
		this.GrossGain = 0;	
		this.GrossLoss = 0;
		this.TotalPnL = 0;

		this.AverageGain = 0;
		this.AverageDraw = 0;

		this.TotalTiTGain = 0;
		this.TotalTiTLoss = 0;
		this.TotalMFE = 0;
		this.TotalMAE = 0;

		this.TotalBSO = 0;
		this.PercentBSO = 0;
		this.NumberOfFullStops = 0;
		this.PercentFullStop = 0;
		this.TotalBSOTrades = 0;
		this.TotalBSOEligibleTrades = 0;
		
		
		// Cummnulative metrics (change per trades)
		var RunningPnL = 0,
			PeakGain = 0,
			MaxDraw = 0;
		
		var	TradeDistributionArray = [],
			TradeRunningPnLArray = [],
			TimeDateOfLastTrade = null;
		

		// Cumulative metrics
		// TO DO separate the metrics that need to be totaled and metrics that need to keep track of cummulative data (refer to notes)	
		
		var htmlBuilder = "<table id='trades-table' border=0 cellspacing=0 cellpadding=0><thead><th title='Trade Number'>#</th><th>L/S</th><th title='Max. Favorable Excursion'>MFE<br />(" + ResultsDisplayType + ")</th><th title='Max. Adverse Excursion'>MAE<br />(" + ResultsDisplayType + ")</th><th title='Best Scale Out'>BSO<br />(" + ResultsDisplayType + ")</th><th>Max Position Qty</th><th>Total Qty Traded</th><th>TiT</th><th>TSB</th><th>P&L<br />(" + ResultsDisplayType + ")</th><th>Running P&L<br />(" + ResultsDisplayType + ")</th><th>Entry Time</th><th>Exit Time</th><th>Entry Price</th><th>Exit Price</th><th>% Win</th><th>%Loss</th><th>Avg. Win</th><th>Avg. Loss</th><th>Expectancy</th><th>Peak Gain<br />(" + ResultsDisplayType + ")</th><th>Max Drawdown<br />(" + ResultsDisplayType + ")</th><th>Efficiency %</th><th>Avg. Duration in Win</th><th>Avg. Duration in Loss</th><th>% Full Stop</th><th>% BSO</th><th>TradeGrade (1-5)</th><th>Trade Error (1-10)</th><th>Trade Type</th></thead><tbody>";		

		if (this.TradesArray != null && this.TradesArray.length > 0) {
			var TradeCount = this.TradesArray.length;
			for (var i = 0; i < TradeCount; i++) {
				var trade = this.TradesArray[i];

				if (trade.isFlat) {

					// TSB
					var TSB = 0;				
					if (TimeDateOfLastTrade == null) {
						TimeDateOfLastTrade = trade.ExitDateTime;
						TSB = 0;
					} else {
						TSB = trade.EntryDateTime - TimeDateOfLastTrade;
						TimeDateOfLastTrade = trade.ExitDateTime;
					}
				
					// ITERATE TRADES
					var TotalCommissionsForTrade = trade.ContractsTraded * (CommissionFee != null ? CommissionFee : 0);
					var TradePnL = 0, TradePnL_Gross = 0, TradeMFE = 0, TradeMAE = 0, TradeBSO = 0;
					var bgColor = "";
					// Figure out how the PnL is defined, does commissions come into play?
					if (ResultsDisplayType == "Points") { 
						TradePnL = PointsToRound(trade.PnLRealizedPts, TickSize);
						TradePnL_Gross = TradePnL;
						TradeBSO = PointsToRound(trade.BSO, TickSize, true);
						TradeMFE = PointsToRound(trade.MFE, TickSize);
						TradeMAE = PointsToRound(trade.MAE, TickSize); }
					else if (ResultsDisplayType == "Ticks") {
						TradePnL = Round(trade.PnLRealizedPts / TickSize);
						TradePnL_Gross = TradePnL;
						TradeBSO = Math.floor(trade.BSO / TickSize);
						TradeMFE = Round(trade.MFE / TickSize);
						TradeMAE = Round(trade.MAE / TickSize); }
					else if (ResultsDisplayType == "Dollars") {
						TradePnL = Round(((trade.PnLRealizedPts * ContractValue) - TotalCommissionsForTrade), 2);
						TradePnL_Gross = TradePnL;
						TradeBSO = Math.floor((trade.BSO * ContractValue), 2);
						TradeMFE = Round((trade.MFE * ContractValue), 2);
						TradeMAE = Round((trade.MAE * ContractValue), 2); }
					
					//Calculate
					this.NumberOfTotalTrades++;
					this.NumberOfTotalContractsTraded += trade.ContractsTraded;
					RunningPnL += TradePnL;
					TradeDistributionArray.push(TradePnL);
					TradeRunningPnLArray.push(RunningPnL);
					
					// Is Winning OR Losing OR Flat?
					if (TradePnL > 0) {
						// Winning
						this.NumberOfWinningTrades++;
						this.NetGain += TradePnL;
						this.GrossGain += TradePnL_Gross;
						this.TotalTiTGain += trade.TIT;
						bgColor = " style='background-color: rgba(25,47,68,0.3);' ";
						
						PeakGain = Math.max(PeakGain, Math.abs(TradePnL));
						
					} else if (TradePnL < 0) {
						// Losing
						this.NumberOfLosingTrades++;
						this.NetLoss += TradePnL;
						this.GrossLoss += TradePnL_Gross;
						this.TotalTiTLoss += trade.TIT;	
						bgColor = " style='background-color: rgba(194,28,10,0.3);' ";	
						
						MaxDraw = Math.max(MaxDraw, Math.abs(TradePnL));
						
						// Fullstop is a loser trade that never had a BSO
						if (trade.BSO <= 0) {
							this.NumberOfFullStops++;	
						}
						
					} else {
						// Flat
						
					}

					// BSO
					if (trade.IsBSOEligible) {
						this.TotalBSOEligibleTrades++;
						if (trade.BSO > 0) 	this.TotalBSOTrades++;
					}

					// how is expectancy calculated depending on the "expected-value-per-contract" checkbox in settings
					if ( $("#expected-value-per-contract").length > 0 && !$("#expected-value-per-contract").is(":checked") ) {
						// Expected value is calculated as total (not per contract)
						this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / this.NumberOfTotalTrades;
					} else {
						// Expected value is calculated per contract
						this.Expectancy = (Math.abs(this.NetGain) - Math.abs(this.NetLoss)) / (this.NumberOfTotalContractsTraded / 2);
					}

					this.PercentWinners = ToPercent(this.NumberOfWinningTrades / this.NumberOfTotalTrades);
					this.PercentLosers = ToPercent(this.NumberOfLosingTrades / this.NumberOfTotalTrades);
					this.AverageGain = this.NumberOfWinningTrades == 0 ? 0 : this.NetGain / this.NumberOfWinningTrades;
					this.AverageDraw = this.NumberOfLosingTrades == 0 ? 0 : this.NetLoss / this.NumberOfLosingTrades;
					this.TotalMFE += TradeMFE;
					this.TotalMAE += TradeMAE;
					this.Efficiancy = this.TotalMFE == 0 ? 0 : this.NetGain / this.TotalMFE;

					this.PercentBSO = this.TotalBSOEligibleTrades > 0 ? (this.TotalBSOTrades / this.TotalBSOEligibleTrades) : 0;
					this.PercentFullStop = this.NumberOfTotalTrades > 0 ? (this.NumberOfFullStops / this.NumberOfTotalTrades) : 0;
					
					// Display the metrics, or add to table / graphs
					if (ResultsDisplayType == "Points") {
						// Display in points
		
						// Update Table
						htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
									+ "<td class='TradeFillID' id='" + trade.TradeFillID + "'>" + (i+1) + "</td>"
									+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
									+ "<td>" + PointsToRound(TradeMFE, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(TradeMAE, TickSize, true) + "</td>"
									+ "<td>" + Math.Floor(TradeBSO) + "</td>"
									+ "<td>" + trade.MaxPosition + "</td>"
									+ "<td>" + trade.ContractsTraded + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(trade.TIT) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(TSB) + "</td>"
									+ "<td>" + PointsToRound(TradePnL, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(RunningPnL, TickSize, true) + "</td>"
									+ "<td nowrap>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
									+ "<td nowrap>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
									+ "<td>" + PointsToRound(trade.EntryPrice, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(trade.ExitPrice, TickSize, true) + "</td>"
									+ "<td>" + this.PercentWinners + "</td>"
									+ "<td>" + this.PercentLosers + "</td>"
									+ "<td>" + PointsToRound(this.AverageGain, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(this.AverageDraw, TickSize, true)	+ "</td>"
									+ "<td>" + Round(this.Expectancy, 2) + "</td>"
									+ "<td>" + PointsToRound(PeakGain, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(MaxDraw, TickSize, true) + "</td>"
									+ "<td>" + ToPercent(this.Efficiancy) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
									+ "</td><td nowrap>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
									+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
									+ "<td>" + ToPercent(this.PercentBSO) + "</td>";
									
						// Update charts
						win_loss_percentage_chart.get("PercentWin").addPoint(this.PercentWinners, false, false, false);
						win_loss_percentage_chart.get("PercentLoss").addPoint(this.PercentLosers, false, false, false);
						expectancy_chart.get("Expectancy").addPoint(Round(this.Expectancy, 2), false, false, false);
						equity_curve_chart.get("MFE").addPoint(PointsToRound(TradeMFE, TickSize), false, false, false);
						equity_curve_chart.get("MAE").addPoint(-PointsToRound(TradeMAE, TickSize), false, false, false);
						equity_curve_chart.get("PnL").addPoint(PointsToRound(TradePnL, TickSize), false, false, false);
						equity_curve_chart.get("EquityCurve").addPoint(PointsToRound(RunningPnL,TickSize), false, false, false);			
			
									
					} else if (ResultsDisplayType == "Ticks") {
						// Display in Ticks

						// Update Table
						htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
									+ "<td class='TradeFillID' id='" + trade.TradeFillID + "'>" + (i+1) + "</td>"
									+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
									+ "<td>" + Round(TradeMFE) + "</td>"
									+ "<td>" + Round(TradeMAE) + "</td>"
									+ "<td>" + Math.Floor(TradeBSO) + "</td>"
									+ "<td>" + trade.MaxPosition + "</td>"
									+ "<td>" + trade.ContractsTraded + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(trade.TIT) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(TSB) + "</td>"
									+ "<td nowrap>" + Round(TradePnL) + "</td>"
									+ "<td nowrap>" + Round(RunningPnL) + "</td>"
									+ "<td>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
									+ "<td>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
									+ "<td>" + PointsToRound(trade.EntryPrice, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(trade.ExitPrice, TickSize, true) + "</td>"
									+ "<td>" + this.PercentWinners + "</td>"
									+ "<td>" + this.PercentLosers + "</td>"
									+ "<td>" + Round(this.AverageGain) + "</td>"
									+ "<td>" + Round(this.AverageDraw) + "</td>"
									+ "<td>" + Round(this.Expectancy, 2) + "</td>"
									+ "<td>" + Round(PeakGain) + "</td>"
									+ "<td>" + Round(MaxDraw) + "</td>"
									+ "<td>" + ToPercent(this.Efficiancy) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
									+ "</td><td nowrap>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
									+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
									+ "<td>" + ToPercent(this.PercentBSO) + "</td>";


						// Update charts
						win_loss_percentage_chart.get("PercentWin").addPoint(this.PercentWinners, false, false, false);
						win_loss_percentage_chart.get("PercentLoss").addPoint(this.PercentLosers, false, false, false);
						expectancy_chart.get("Expectancy").addPoint(Round(this.Expectancy, 2), false, false, false);
						equity_curve_chart.get("MFE").addPoint(Round(TradeMFE), false, false, false);
						equity_curve_chart.get("MAE").addPoint(-Round(TradeMAE), false, false, false);
						equity_curve_chart.get("PnL").addPoint(PointsToRound(TradePnL, TickSize), false, false, false);
						equity_curve_chart.get("EquityCurve").addPoint(PointsToRound(RunningPnL,TickSize), false, false, false);			
			

					} else if (ResultsDisplayType == "Dollars") {
						// Display in Dollars

						// Update Table
						htmlBuilder += "<tr " + bgColor + " id='" + trade.TradeFillID + "'>"
									+ "<td class='TradeFillID' id='" + trade.TradeFillID + "'>" + (i+1) + "</td>"
									+ "<td>" + (trade.isInitalLong ? "L" : "S") + "</td>"
									+ "<td>" + Round(TradeMFE, 2) + "</td>"
									+ "<td>" + Round(TradeMAE, 2) + "</td>"
									+ "<td>" + Math.Floor(TradeBSO) + "</td>"
									+ "<td>" + trade.MaxPosition + "</td>"
									+ "<td>" + trade.ContractsTraded + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(trade.TIT) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(TSB) + "</td>"
									+ "<td>" + Round(TradePnL, 2) + "</td>"
									+ "<td>" + Round(RunningPnL, 2) + "</td>"
									+ "<td nowrap>" + trade.EntryDateTime.format('MMM D h:mma') + "</td>"
									+ "<td nowrap>" + trade.ExitDateTime.format('MMM D h:mma') + "</td>"
									+ "<td>" + PointsToRound(trade.EntryPrice, TickSize, true) + "</td>"
									+ "<td>" + PointsToRound(trade.ExitPrice, TickSize, true) + "</td>"
									+ "<td>" + this.PercentWinners + "</td>"
									+ "<td>" + this.PercentLosers + "</td>"
									+ "<td>" + Round(this.AverageGain, 2) + "</td>"
									+ "<td>" + Round(this.AverageDraw, 2)	+ "</td>"
									+ "<td>" + Round(this.Expectancy, 2) + "</td>"
									+ "<td>" + Round(PeakGain, 2) + "</td>"
									+ "<td>" + Round(MaxDraw, 2) + "</td>"
									+ "<td>" + ToPercent(this.Efficiancy) + "</td>"
									+ "<td nowrap>" + MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades)
									+ "</td><td nowrap>" + MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades)
									+ "</td><td>" + ToPercent(this.PercentFullStop) + "</td>"
									+ "<td>" + ToPercent(this.PercentBSO) + "</td>";
						
						// Update charts
						win_loss_percentage_chart.get("PercentWin").addPoint(this.PercentWinners, false, false, false);
						win_loss_percentage_chart.get("PercentLoss").addPoint(this.PercentLosers, false, false, false);
						expectancy_chart.get("Expectancy").addPoint(Round(this.Expectancy, 2), false, false, false);
						equity_curve_chart.get("MFE").addPoint(Round(TradeMFE, 2), false, false, false);
						equity_curve_chart.get("MAE").addPoint(-Round(TradeMAE, 2), false, false, false);
						equity_curve_chart.get("PnL").addPoint(PointsToRound(TradePnL, TickSize), false, false, false);
						equity_curve_chart.get("EquityCurve").addPoint(PointsToRound(RunningPnL,TickSize), false, false, false);			
					}		

					

					// Handle Trade Grade
					var TradeGradeHtmlBuilder = "<td><select class='trade-grade-selector' id='" + trade.TradeFillID + "' data-contract='" + trade.Symbol + "' data-account='" + selectedAccount + "' data-tradeid='" + trade.TradeFillID + "'><option value='--'>--</option>";
					for (var jj = 5; jj > 0; jj--) {
						if (jj.toString() == trade.TradeGrade)
							TradeGradeHtmlBuilder += "<option value='" + jj + "' selected='selected'>" + jj + "</option>";
						else 
							TradeGradeHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";
					}
					TradeGradeHtmlBuilder += "</select></td>";					
					htmlBuilder += TradeGradeHtmlBuilder;


					// Handle Trade Error 
					var TradeErrorHtmlBuilder = "<td><select class='trade-error-selector' id='" + trade.TradeFillID + "' data-contract='" + trade.Symbol + "' data-account='" + selectedAccount + "' data-tradeid='" + trade.TradeFillID + "'><option value='--'>--</option>";
					for (var jj = 1; jj <= 10; jj++) {
						if (jj.toString() == trade.TradeError)
							TradeErrorHtmlBuilder += "<option value='" + jj + "' selected='selected'>" + jj + "</option>";
						else 
							TradeErrorHtmlBuilder += "<option value='" + jj + "'>" + jj + "</option>";
					}
					TradeErrorHtmlBuilder += "</select></td>";					
					htmlBuilder += TradeErrorHtmlBuilder;


					/*
					// Handle Trade Emoticon
					var TradeEmoticonBuilder = "<td nowrap>";
					$.each(tradeEmoticonArray, function (kk, emoticon) {
						if (emoticon == trade.TradeEmoticon)
							TradeEmoticonBuilder += "<span class='trade-error chosen' id='" + emoticon + "'></span>";
						else
							TradeEmoticonBuilder += "<span class='trade-error' id='" + emoticon + "'></span>";
					});
					TradeEmoticonBuilder += "</td>";
					htmlBuilder += TradeEmoticonBuilder;
					*/

					// Handle Trade Type
					var TradeTypeHtmlBuilder = "<td><select class='trade-type-selector' id='" + trade.TradeFillID + "' data-contract='" + trade.Symbol + "' data-account='" + selectedAccount + "' data-tradeid='" + trade.TradeFillID + "'><option value='--'>--</option>";
					$.each(TradeTypeArray, function (hh, Type) {
						if (trade.TradeType == Type)
							TradeTypeHtmlBuilder += "<option selected='selected' value='" + Type + "'>" + Type + "</option>";
						else 
							TradeTypeHtmlBuilder += "<option value='" + Type + "'>" + Type + "</option>";
					});
					TradeTypeHtmlBuilder += "</select></td>";
					htmlBuilder += TradeTypeHtmlBuilder;
					 
					htmlBuilder += "</tr>";
		
				}
			}
		}
		
		// Update outside of the iteration

		// Display Completed trades table
		htmlBuilder += "</tbody></table>";
		$("#trades-window .databar").html(htmlBuilder);

		// Initialize Dials
		// percent_win_dial = $('#percent-win-dial').highcharts();
		// percent_loss_dial = $('#percent-loss-dial').highcharts();
		// pnl_dial = $('#pnl-dial').highcharts();


		// Update Win/Loss Dials
		percent_win_dial.series[0].points[0].update(this.PercentWinners);
		percent_loss_dial.series[0].points[0].update(this.PercentLosers);
		this.TotalPnL = RunningPnL;
		
		// if ((TradeMFE == 0 && TradeMAE == 0) || !$.isNumeric(TradeMFE) || !$.isNumeric(TradeMAE)) {
		// 	// MFE / MAE not set
		// 	pnl_dial.yAxis[0].setExtremes(TradePnL-10, TradePnL+10);
		// } else {
		// 	pnl_dial.yAxis[0].setExtremes(-TradeMAE, TradeMFE);
		// }


		if (ResultsDisplayType == "Points") {
			// Display in points
			$("#stats-current-pnl").text(PointsToRound(RunningPnL, TickSize, true));
			$("#stats-current-expectancy").text(PointsToRound(this.Expectancy, TickSize, true));

			try {
				var PnLDialMin = Math.min.apply(null, TradeRunningPnLArray),
					PnLDialMax = Math.max.apply(null, TradeRunningPnLArray);

				if (TradeRunningPnLArray == null || TradeRunningPnLArray.length == 0 || (PnLDialMin == 0 && PnLDialMax == 0)) {
					pnl_dial.yAxis[0].setExtremes(0, 0);
					pnl_dial.series[0].points[0].update(0);
				} else {
					var PnLDialExtreme = Math.max( Math.abs(PnLDialMin) , Math.abs(PnLDialMax) );
					pnl_dial.yAxis[0].setExtremes(-PnLDialExtreme, PnLDialExtreme, false, false);
					pnl_dial.series[0].points[0].update(PointsToRound(RunningPnL, TickSize));			
				}
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in P&L gauge. Error: " + err.toString(), "warning", 5000);	
			}
			
		} else if (ResultsDisplayType == "Ticks") {
			// Display in Ticks
			$("#stats-current-pnl").text(Round(RunningPnL));
			$("#stats-current-expectancy").text(Round(this.Expectancy, 2));
			
			// Update 
			try {
				var PnLDialMin = Math.min.apply(null, TradeRunningPnLArray),
					PnLDialMax = Math.max.apply(null, TradeRunningPnLArray);

				if (TradeRunningPnLArray == null || TradeRunningPnLArray.length == 0 || (PnLDialMin == 0 && PnLDialMax == 0)) {
					pnl_dial.yAxis[0].setExtremes(0, 0);
					pnl_dial.series[0].points[0].update(0);
				} else {
					var PnLDialExtreme = Math.max( Math.abs(PnLDialMin), Math.abs(PnLDialMax) );
					pnl_dial.yAxis[0].setExtremes(-PnLDialExtreme, PnLDialExtreme, false, false);
					//pnl_dial.yAxis[0].update({ tickInterval: Math.ceil(PnLDialExtreme / 3) });
					pnl_dial.series[0].points[0].update(Round(RunningPnL));
				}
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in P&L gauge. Error: " + err.toString(), "warning", 5000);	
			}
			
		} else if (ResultsDisplayType == "Dollars") {
			// Display in Dollars
			$("#stats-current-pnl").text(Round(RunningPnL, 2));
			$("#stats-current-expectancy").text(Round(this.Expectancy, 2));

			// Update 
			try {
				var PnLDialMin = Math.min.apply(null, TradeRunningPnLArray),
					PnLDialMax = Math.max.apply(null, TradeRunningPnLArray);

				if (TradeRunningPnLArray == null || TradeRunningPnLArray.length == 0 || (PnLDialMin == 0 && PnLDialMax == 0)) {
					pnl_dial.yAxis[0].setExtremes(0, 0);
					pnl_dial.series[0].points[0].update(0);
				} else {
					var PnLDialExtreme = Math.max( Math.abs(PnLDialMin) , Math.abs(PnLDialMax) );
					pnl_dial.yAxis[0].setExtremes(-PnLDialExtreme, PnLDialExtreme, false, false);
					pnl_dial.series[0].points[0].update(Round(RunningPnL, 2));
				}
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in P&L gauge. Error: " + err.toString(), "warning", 5000);	
			}
		}		

		// Update Stats Toolbar
		$("#stats-num-of-trades").text(this.NumberOfTotalTrades);
		$("#stats-num-of-contracts-traded").text(this.NumberOfTotalContractsTraded);
		$("#stats-avg-win-duration").text(MillisecondsToTime(this.TotalTiTGain / this.NumberOfWinningTrades));
		$("#stats-avg-loss-duration").text(MillisecondsToTime(this.TotalTiTLoss / this.NumberOfLosingTrades));


		// Trade Distribution Chart calculations
		if (TradeDistributionArray.length == 0) {
			trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
			trade_distribution_chart.xAxis[0].addPlotLine({
				value: 0,
				color: 'red',
				width: 5,
				zIndex: 10,
				id: 'zeroLineValue'
			});

			//trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
			trade_distribution_chart.get("main").setData([[]]);

		} else if (TradeDistributionArray.length == 1) {
			// If list contains only one trade
			trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
			trade_distribution_chart.xAxis[0].addPlotLine({
				value: 0,
				color: 'red',
				width: 5,
				zIndex: 10,
				id: 'zeroLineValue'
			});

			//trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
			var PnL = TradeDistributionArray[0];
			trade_distribution_chart.get("main").setData([[Math.round(PnL), 1]]);
						
		} else if (TradeDistributionArray.length > 1) {
			TradeDistributionArray.sort();

			try {
				var TD_max = Math.max.apply(Math, TradeDistributionArray),
					TD_min = Math.min.apply(Math, TradeDistributionArray),
					TD_Final_Array = [],
					Processed_bucket_size;		
				
				// All the PnLs are not the same			
				var bucket_size_proposed = (TD_max - TD_min) / 31;		// this is in decimal format
				if (bucket_size_proposed > 100) 
					Processed_bucket_size = Math.floor(bucket_size_proposed / 100) * 100;			//try to make 15 buckets on each side of zero
				else if (bucket_size_proposed > 10)
					Processed_bucket_size = Math.floor(bucket_size_proposed / 10) * 10;				//try to make 15 buckets on each side of zero
				else if (bucket_size_proposed > 1)
					Processed_bucket_size = Math.floor(bucket_size_proposed);						//try to make 15 buckets on each side of zero
				else 
					Processed_bucket_size = 1;														//try to make 15 buckets on each side of zero

				var StartPoint,
					PointsBeforeZero = 0,
					PointsAfterZero = 0,
					PointsTotal = 0;

				if (TD_min == TD_max) {
					StartPoint = TD_min;
					PointsTotal = 1;

				} else if (TD_min < 0 && TD_max <= 0) {
					// no PnL on the positive side
					PointsBeforeZero = Math.ceil(Math.abs(TD_min) / Processed_bucket_size) + 1;
					PointsTotal = PointsBeforeZero;
					StartPoint = -1 * PointsBeforeZero * Processed_bucket_size;

				} else if (TD_min >= 0 && TD_max > 0) {
					// no PnL on the negative side
					PointsAfterZero = Math.ceil(Math.abs(TD_max) / Processed_bucket_size) + 1;
					PointsTotal = PointsAfterZero;
					StartPoint = 0;
				} else {
					// PnL spread on the negative and positive sides
					PointsBeforeZero = Math.ceil(Math.abs(TD_min) / Processed_bucket_size) + 1;
					PointsAfterZero = Math.ceil(Math.abs(TD_max) / Processed_bucket_size) + 1;
					PointsTotal = PointsBeforeZero + PointsAfterZero + 1;
					StartPoint = -1 * PointsBeforeZero * Processed_bucket_size;
				}

				for (var i = 0; i < PointsTotal; i++) {
					var point = StartPoint + (i * Processed_bucket_size);
					TD_Final_Array.push([point, 0]);
				}

				$.each(TradeDistributionArray, function (j, PnL) {
					var point = Math.floor((PnL - StartPoint)/Processed_bucket_size);

					// since the zero return has it's own data point where all the zero returns fall into, anything higher than zero will need one more notch
					//if (PointsBeforeZero > 0 && PointsAfterZero > 0 && PnL > 0) point++;
					
					TD_Final_Array[point][1]++;
				});
							
				trade_distribution_chart.xAxis[0].removePlotLine('zeroLineValue');
				trade_distribution_chart.xAxis[0].addPlotLine({
					value: 0,
					color: 'red',
					width: 5,
					zIndex: 10,
					id: 'zeroLineValue'
				});		
				trade_distribution_chart.xAxis[0].update({ tickInterval: Processed_bucket_size });
				trade_distribution_chart.setTitle({ text: "Trade Distribution (bucket size=" + Processed_bucket_size + ")"}, {}, false);
				trade_distribution_chart.get("main").setData(TD_Final_Array);
			} catch (err) {
				if (typeof popupNotification == "function") popupNotification("There was an error in 'Trade Distribution' chart calculations.<br /><br />Error: " + err.toString(), "warning", 5000);	
			}
		}

		// Redraw charts
		win_loss_percentage_chart.redraw();
		expectancy_chart.redraw();
		equity_curve_chart.redraw();

	} catch (err) {
		window.ErrorHandler(err);
	}
};

Contract.prototype.CheckFillConsistency = function () {

	try {

		var returnString = "";
		
		// Check Fills Order
		var FillIDArray = [];
		$.each(this.FillsArray, function (i, fill) {
			FillIDArray.push([fill.FillID]);
		});

		var FillIDArraySorted = FillIDArray.sort();
		if (FillIDArraySorted.toString() != FillIDArray.toString()) {
			// The fills are in the wrong order
			returnString += "Fill Order Error: there was a difference between fill order and sorted fill array. Sorted Array: " + FillIDArraySorted.toString() + ", actual array: " + FillIDArray.toString() + ". <br />"; 
		}

		// Check fills / position / PnL consistency
		$.each(this.TradesArray, function (i, trade) {
			var position = 0, PnLPoints = 0;
			var TotalBuyPoints = 0,	TotalSellPoints = 0;
			
			// Fills / position consistency
			$.each(trade.Fills, function (j, fill) {
				var directionalQuant = fill.Quant * (fill.Side == "Buy" ? 1 : -1);
				position += directionalQuant;

				if (trade.isFlat) {
					TotalBuyPoints += (fill.Side == "Buy" ? (fill.Quant * fill.Price) : 0);
					TotalSellPoints += (fill.Side == "Sell" ? (fill.Quant * fill.Price) : 0);				
				}

				if (j == 0) {
					if (fill.TypeOfTrade != "O" && fill.TypeOfTrade != "S") {
						returnString += "Trade (" + trade.TradeNumber + "|" + trade.TradeFillID + ") -- TypeOfTrade should be 'O' or 'S' but was '" + fill.TypeOfTrade + "'. <br />";
					}
					
				} else {
					// rest of the fills
					if (position == 0) {
						// is flat, or split
						if (fill.TypeOfTrade != "C" && fill.TypeOfTrade != "S") {
							returnString += "Trade (" + trade.TradeNumber + "|" + trade.TradeFillID + ") -- Fill (" + fill.FillID + ") TypeOfTrade is '" + fill.TypeOfTrade + "' but should be 'C' or 'S'. <br />";
						}
					} else {
						if (fill.TypeOfTrade != "-") {
							returnString += "Trade (" + trade.TradeNumber + "|" + trade.TradeFillID + ") -- Fill (" + fill.FillID + ") TypeOfTrade is '" + fill.TypeOfTrade + "' but should be '-'. <br />";
						}
					}
				}
			});

			// Check if PnL in points is within a certain percentage of the entry price
			// This is so that if there is a gross miscalculation that makes it a huge differece
			var AbsolutePnLVector = Math.abs(TotalSellPoints - TotalBuyPoints),
				PointsThreshold = 0.3 * trade.EntryPrice;

			if (AbsolutePnLVector > PointsThreshold) {
				// The absolute PnL was bigger than 10% of the entry price
				returnString += "Trade (" + trade.TradeNumber + "|" + trade.TradeFillID + ") -- PnL threshold 30% (" + PointsThreshold + ") breached, absolute PnL is: " + AbsolutePnLVector + ". <br />";
			}

		});

		return returnString;

	} catch (err) {

		window.ErrorHandler(err);
	}
};

function MillisecondsToTime (ms) {
	if (isNaN(ms)) {
		return "n.a.";
	} else {
		var dur = moment.duration(ms),
			days = Math.floor(dur.days()),
			hours = Math.floor(dur.hours()),
			minutes = Math.floor(dur.minutes()),
			seconds = Math.floor(dur.seconds());
		if (ms == 0 || ms < 0 || (days == 0 && hours == 0 && minutes == 0 && seconds == 0))
			return 0;
		else 
			return (days > 0 ? days + "d " : "") + (hours > 0 ? hours + "h " : "") + (minutes > 0 ? minutes + "m " : "") + (seconds > 0 ? seconds + "s " : "");
	}
}

function PointsToTick (points, tickSize) {
	if (tickSize > 0) {			
		return (Math.floor(points / tickSize));
	} else {
		return "Err";
	}
}

function PointsToRound (points, tickSize, outputToString) {
	outputToString = (outputToString == null ? false : true);
	if (tickSize > 0) {
		tickSize = tickSize.toString();
		var decPlace = (tickSize.split('.')[1] || []).length;
		return outputToString ? Number(points).toFixed(decPlace) : Number(Number(points).toFixed(decPlace));
	} else {
		return "Err";
	}
}

function Round (Input, DecimalPlaces) {
	if (DecimalPlaces == null) DecimalPlaces = 0;
	if (Input == "NaN") {
		return "NaN";
	} else if ($.isNumeric(Input) && $.isNumeric(DecimalPlaces)) {
		var m = Math.pow(10, Number(DecimalPlaces));
		return (Math.round(Number(Input) * m) / m);
	} else {
		return "ERR";
	}
}

function ToPercent(num) {
	return (num == 0 ? 0 :(Math.round(num * 10000)/100));
}

function FillToString(fill) {
	var str = fill.Account + " -> " + fill.Contract + "<br />" + fill.FillID + " " + fill.Side + " " + fill.Quant + " " + fill.Price + "<br />" + fill.TimestampUTC;
	return str;
}
