const path = require('path'),
    webpack = require('webpack'),
    StyleLintPlugin = require('stylelint-webpack-plugin'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    ImageminPlugin = require('imagemin-webpack-plugin').default;
const plugins = [
    new StyleLintPlugin({
        configFile: './.stylelintrc',
        allowEmptyInput: true,
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        },
    }),
    new webpack.LoaderOptionsPlugin({
        debug: process.env.NODE_ENV === 'development'
    }),
    new webpack.NamedModulesPlugin(),
    new CopyWebpackPlugin([{
        from: path.join(__dirname, 'assets/fonts/**/*'),
        to: path.join(__dirname, 'dist/fonts/[name].[ext]')
    }, {
        from: path.join(__dirname, 'assets/images/**/*'),
        to: path.join(__dirname, 'dist/images/[name].[ext]')
    }]),
    new CleanWebpackPlugin([
        path.join(__dirname, 'dist/')
    ]),
    new ImageminPlugin({test: /\.(jpe?g|png|gif|svg)$/i}),
];
const entry = [
    path.join(__dirname, 'assets/js/index.js'),
];
const rules = [
    {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'eslint-loader',
        include: [
            path.join(__dirname, 'assets'),
        ]
    },
    {
        loader: 'babel-loader',
        test: /\.js$/,
        include: [
            path.join(__dirname, 'assets'),
        ],
    },
    {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
            {
                loader: 'file-loader',
                options: {
                    hash: 'sha512',
                    digest: 'hex',
                    name: '[hash].[ext]'
                }
            },
            {
                loader: 'image-webpack-loader',
                query: {
                    mozjpeg: {
                        progressive: true,
                    },
                    gifsicle: {
                        interlaced: false,
                    },
                    optipng: {
                        optimizationLevel: 4,
                    },
                    pngquant: {
                        quality: '75-90',
                        speed: 3,
                    },
                }
            }
        ]
    },
    {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader?name=[name].[ext]'
    }
];

plugins.push(new ExtractTextPlugin({
    filename: '../dist/main.css'
}));
rules.push({
    test: /\.s?(c|a)ss$/,
    include: [
        path.join(__dirname, 'assets/scss'),
    ],
    use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
            {
                loader: 'css-loader',
                options: {
                    minimize: true,
                    sourceMap: true
                }
            },
            'sass-loader',
        ]
    })
});
plugins.push(new webpack.NoEmitOnErrorsPlugin());

module.exports = {
    context: __dirname,
    devtool: process.env.NODE_ENV === 'development' ?
        'inline-eval-cheap-source-map' : 'cheap-module-source-map',
    plugins,
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: 'main.js',
        publicPath: '/wp-content/themes/jupiter-child/dist/'
    },
    module: {
        rules
    },
    resolve: {
        modules: [
            path.join(__dirname, 'assets/js'),
            path.join(__dirname, 'node_modules')
        ],
        extensions: ['.js', '.jsx', '.scss', '.json', '.css'],
        alias: {
            styles: path.join(__dirname, 'assets/scss'),
            resources: path.join(__dirname, 'resources/js/main.js'),
        }
    },
    entry,
    externals: {
        jquery: 'jQuery',
        $: 'jQuery'
    }
};
