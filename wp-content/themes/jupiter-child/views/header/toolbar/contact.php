<?php global $mk_options ?>

<?php if (!empty($mk_options['header_toolbar_phone'])) { ?>

    <?php $phones = trim_explode($mk_options['header_toolbar_phone']) ?>
    <span class="header-toolbar-contact">
        <i class="mk-moon-phone-3"></i>
        <?php foreach ($phones as $i => $phone) { ?>
            <a href="tel:<?php echo str_replace(' ', '', str_replace('(0)', '', stripslashes($phone))); ?>"><?php echo stripslashes($phone); ?></a>
            <?php if (count($phones) - 1 != $i) { ?>
                <span class="sep">|</span>
            <?php } ?>
        <?php } ?>
    </span>

<?php } ?>

<?php if (!empty($mk_options['header_toolbar_email'])) { ?>

    <span class="header-toolbar-contact">
    	<i class="mk-moon-envelop"></i>
    	<a href="mailto:<?php echo antispambot($mk_options['header_toolbar_email']); ?>"><?php echo antispambot($mk_options['header_toolbar_email']); ?></a>
    </span>

<?php } ?>
