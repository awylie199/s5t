<?php

global $mk_options;
global $post;

mk_get_view('header', 'wp-toolbar', false);

$postName = $post->post_name;

/*$navItems = [
    'alternative-investments' => 'fourth-menu',
    'brokerage-solutions' => 'second-menu',
    'platform-solutions' => 'fifth-menu',
];*/

/*if (!in_array($postName, array_keys($navItems))) {
    $postName = get_post($post->post_parent)->post_name;
}*/

/*$navMenu = isset($navItems[$postName]) ? $navItems[$postName] : 'second-menu';

wp_nav_menu(['theme_location' => $navMenu, 'items_wrap' => '<div class="secondary-menu-toggle">' . $post->post_title . '</div><ul class="%2$s">%3$s</ul>']);*/

if (have_posts()) {
    while (have_posts()) {
        the_post();

        do_action('mk_page_before_content');

        the_content();

        do_action('mk_page_after_content');
        ?>
        <div class="clearboth"></div>
        <?php
        wp_link_pages('before=<div id="mk-page-links">' . __('Pages:', 'mk_framework') . '&after=</div>');
    }
}
