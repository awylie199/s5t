
<?php if (isset($view_params['testimonials']) && $view_params['testimonials']) { ?>
<?php $testimonials = new WP_Query(['post_type' => 'testimonial', 'testimonial_category' => 'footer-testimonial', 'orderby' => 'rand']) ?>

<section class="testimonial-slider footer-testimonials<?= isset($view_params['full_width']) && $view_params['full_width'] ? ' full-width' : '' ?>">
    <ul>
        <?php foreach ($testimonials->get_posts() as $testimonial) { ?>
            <?php $fields = get_post_meta($testimonial->ID) ?>
            <li class="slide-testimonial">
                <p class="testimonial-content"><?= $fields['_desc'][0] ?></p>
                <p class="testimonial-author"><?= $fields['_author'][0] ?><?= isset($fields['_company']) ? ', ' . $fields['_company'][0] : '' ?></p>
                <p class="testimonial-disclaimer">Testimonials are not necessarily indicative of future performance or success and may not represent the opinion of all participants. There was no compensation to the trader for this opinion.</p>
            </li>
        <?php } ?>
    </ul>
</section>
<?php } ?>
