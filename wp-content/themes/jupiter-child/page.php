<?php

get_header();

mk_get_view('header', 'wp-toolbar', false);

mk_build_main_wrapper(mk_get_view('singular', 'wp-page', true));

mk_get_view('singular', 'wp-page-footer', false, ['full_width' => true]);

get_footer();
