<?php 
/*
Template Name: Display Fills Trade Analyzer
*
*	This website created by Reza Khanahmadi
*	Trading Algo X
*	Created on May 1, 2017
*/ 
$BaseLocation = get_stylesheet_directory_uri();// . "/RTA-assets/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo "$BaseLocation/RTA-assets/"; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contract Summary</title>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
<link rel="icon" href="images/S5TA.favicon.ico" />

<link rel="stylesheet" href="css/UniversalElementStyles.css" />

<style type="text/css">
	html, body {
		height: 100%;
		min-height: 100%;
		margin: 0;
		padding: 3px 5px;
		background-color: #32363b;
		color: #f2f2f2;
		font-family: 'Ubuntu', sans-serif;
		box-sizing: border-box;
	}
	
	#container {
		display: block;
		width: 100%;
		height: 100%;
	}

	#display-fills {
		display: block;
		width: 100%;
		margin-top: 7px;
	}
	
	#display-fills td {
		text-align: center;
	}
	
	#display-params, #display-params2 { font-size: 0.8em; }
</style>

</head>

<body>
<div id="container">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
    	<td id="display-header" style="font-weight: bold; font-size: 1.5em">TICKER</td>
        <td id="display-params"></td>
        <td id="display-params2"></td>
        <td style="text-align: right;"><img src="images/top-logo.png" /></td>
        </tr>
    </table>
    <div id="display-fills">
	<br />
    <strong>Note:</strong><br /><br />This table cannot be refreshed.  It must be loaded by clicking a ticker from the 'Completed Trades' table in the Historical Trade Analyzer.
    </div>
</div>



</body>
</html>
