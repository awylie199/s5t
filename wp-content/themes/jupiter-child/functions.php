<?php

require_once get_stylesheet_directory() . DIRECTORY_SEPARATOR . '/bootstrap.php';

// Change default WordPress email address
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from($old) {
return 'webadmin@s5trading.com.com';
}
function new_mail_from_name($old) {
return 'Stage 5 Trading Server';
}
/**
 * Cascade Includes
 *
 * The $cascade_includes array provides access to all internal functions for
 * the Cascade theme library. Missing files will produce a fatal error.
 */
$cascade_includes = array(
    'libraries/setup.php',              // Initial Theme Setup
    'libraries/custom-post-types.php',  // Custom Post Types and Fields
    'libraries/helpers.php',            // Custom Helper Functions
/*    'libraries/ninja-forms.php',        // Custom Ninja Form logic */
    'libraries/gravity-forms.php',      // Custom Gravity logic
    'libraries/GWEmailDomainControl.php', // E-mail ban plug-in for gravity-forms
    'libraries/CQG_demoScript.php', // Custom CQG logic for internal and external forms
);

foreach ($cascade_includes as $file) {
    $path = locate_template($file);

    if (!$path) {
        trigger_error(sprintf('Error locating "%s" in functions.php', $file));
    }

    require_once $path;
}
unset($file, $path);

// Where to send the error messages
$errorMsgAddrs = array('tech@s5trading.com', 'demo@s5trading.com');
//$errorMsgAddrs = array('kbc@arquusia.dk');

function sendErrorMail($errorType, $errorMessage, $fields) {
  global $errorMsgAddrs;
  $headers  = "From: Stage 5 Trading Corp. <demo@s5trading.com>\r\n";
  $headers .= "Reply-To: S5 Demo <demo@s5trading.com>\r\n";
  $headers .= "Return-Path: S5 Demo <demo@s5trading.com>\r\n";
  $headers .= "Organization: Stage 5 Trading Corp\r\n";
  $headers .= "Content-Type: text/plain; charset=iso-8859-1\r\n";
  $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

  $subject = "Demo subscription error";

  $message  = "Hello,\r\n\r\n";
  $message .= "A visitor has tried to subscribe for a demo, but an error has occured.\r\n";
  $message .= "Error Type: " . $errorType . "\r\n";
  $message .= "Gain Error Message: " . $errorMessage . "\r\n\r\n";
  foreach($fields as $fieldName => $fieldValue)
  {
    $message .= $fieldName . ": " . $fieldValue . "\r\n";
  }

  foreach($errorMsgAddrs as $emailAddr)
  {

  	if(!mail($emailAddr, $subject, $message, $headers))
  	{
  	  error_log("Demo script warning: Could not send error notice to " . $emailAddr);
  	}
  }
}


// See https://gravitywiz.com/banlimit-email-domains-for-gravity-form-email-fields/ to find out how it works
// The example below for the test form created in the WP-Admin
// WARNING !!!!!! The form has to be activated
new GWEmailDomainControl(array(
    'domains' => array('sharklasers.com', 'guerillamail.info', 'grr.la', 'guerillamail.biz', 'guerillamail.com', 'guerillamail.de', 'guerillamail.net', 'guerillamail.org', 'guerillamailblock.com', 'pokemail.net', 'spam4.me', 'contbay.com', 'damnthespam.com', 'trashmail.com', 'trash-mail.com', 're-gister.com', 'trash-me.com', 'you-spam.com', 'fake-box.com', 'opentrash.com')
    ));




/**
 *    Mimics XDebug's var_dump() altered functionality to better display data.
 *    @param   mixed  $data  data to display
 *    @return  none          displays results
 */
function dump($data) {
    // display headers for each data set
    if (is_object($data) || is_array($data)) {
        $arr_obj = new ArrayObject($data);
        echo '<pre style="display: inline; margin: 0;"><strong>' . gettype($data) . '</strong>';
        echo '(<em>' . (is_object($data) ? get_class($data) : '') . '</em>)';
        echo ' (' . $arr_obj->count() . ')</pre>';
        // format object properties
        if (is_object($data)) {
            $object_data = [];
            $reflection = new ReflectionClass($data);
            $properties = $reflection->getProperties();
            foreach ($properties as $property) {
                $property->setAccessible(true);
                $key  = '"' . $property->getName() . '"';
                $key .= $property->isProtected() ? ':protected' : '';
                $key .= $property->isPrivate() ? ':private' : '';
                $object_data[$key] = $property->getValue($data);
            }
            foreach (get_object_vars($data) as $key => $value) {
                $object_data['"' . $key . '"'] = $value;
            }
            $data = $object_data;
        } else {
            $array_data = [];
            // format array names
            foreach ($data as $key => $value) {
                $array_data['"' . $key . '"'] = $value;
            }
            $data = $array_data;
        }
        // normalize spacing
        $pad = ['key' => 0, 'type' => 0];
        foreach ($data as $key => $value) {
            $pad['key'] = (strlen($key) + 4 > $pad['key']) ? (strlen($key) + 4) : $pad['key'];
            $pad['type'] = (strlen(gettype($value)) + 6 > $pad['type']) ? (strlen(gettype($value)) + 6) : $pad['type'];
        }
        echo '<ul style="list-style: none; margin: 0 0 0 20px; padding: 0;">';
        foreach ($data as $key => $value) {
            if (is_object($value) || is_array($value)) {
                echo '<li style="margin: 4px 0;"><pre style="display: inline; margin: 0;">' . $key . ' </pre>';
                dump($value);
                echo '</li>';
                continue;
            }
            $color = !is_string($value) ? (is_float($value) ? 'f57900' : '4e9a06') : 'c00';
            echo '<li><pre style="margin: 0 0 0 0">' . str_pad($key, $pad['key']);
            echo '<span style="color: #000; font-size: 0.8em">' . str_pad(gettype($value) . ' ' . strlen($value), $pad['type']) . '</span>';
            echo '<span style="color: #' . $color . '"> ' . htmlspecialchars($value) . '</span></pre></li>';
        }
        echo '</ul>';
    } else {
        $color = !is_string($data) ? (is_float($data) ? 'f57900' : '4e9a06') : 'c00';
        echo '<pre>';
        echo '<span style="font-size: 0.8em">' . gettype($data) . '</span>';
        echo '<span style="white-space: pre-wrap; color: #' . $color . '"> ' . htmlspecialchars($data) . '</span></pre>';
    }
}

//Show field label visibility option in Gravity
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
