<?php
/**
 * Template Name: Infographic2
 */

global $post;

get_header();

mk_get_view('header', 'wp-toolbar', false);

mk_build_main_wrapper(mk_get_view('singular', 'wp-page', true));

?>

<div style="height: 430px;" class="landing-infographic">
    <ul class="progress-bars horizontal">
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">98.2<sup>%</sup></span>
                <span class="data-description">Recommended by existing clients <sup>*</sup></span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">200+</span>
                <span class="data-description">Live Training Sessions Completed</span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">30+</span>
                <span class="data-description">Platforms Supported</span>
            </div>
        </li>
    </ul>
</div>

<div style="height: 430px;" class="landing-infographic">
    <ul class="progress-bars horizontal">
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">98.2<sup>%</sup></span>
                <span class="data-description">Recommended by<br>existing clients <sup>*</sup></span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">200+</span>
                <span class="data-description">Live Training<br>Sessions Completed</span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">30+</span>
                <span class="data-description">Platforms<br>Supported</span>
            </div>
        </li>
    </ul>
</div>

<div style="height: 430px;" class="landing-infographic">
    <ul class="columns">
        <li class="data-item">
            <div class="content-wrap">
                <figure class="data-image">
                    <img src="/wp-content/themes/jupiter-child/resources/images/numbers.gif">
                </figure>
                <div class="data-content">
                    <span class="data-highlight">Real Time</span>
                    <span class="data-description">Theoretical Average<br>to Manage Risk</span>
                </div>
            </div>
        </li>
        <li class="data-item">
            <div class="content-wrap">
                <figure class="data-image">
                    <img src="/wp-content/themes/jupiter-child/resources/images/server.gif">
                </figure>
                <div class="data-content">
                    <span class="data-highlight">Server Side</span>
                    <span class="data-description">OCO Order<br>Management</span>
                </div>
            </div>
        </li>
        <li class="data-item">
            <div class="content-wrap">
                <figure class="data-image">
                    <img src="/wp-content/themes/jupiter-child/resources/images/graph.gif">
                </figure>
                <div class="data-content">
                    <span class="data-highlight">Detailed Charts</span>
                    <span class="data-description">on Trade<br>Performance</span>
                </div>
            </div>
        </li>
    </ul>
</div>

<div style="height: 430px;" class="landing-infographic">
    <ul class="progress-bars vertical">
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">$15,000</span>
                <span class="data-description">Professionally-Developed<br>Trading Systems<br><span class="type-secondary">for as little as</span></span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">$25,000</span>
                <span class="data-description">Managed Futures<br>Solutions<span class="type-secondary">starting from</span></span>
            </div>
        </li>
        <li class="data-item">
            <div class="data-content">
                <span class="data-highlight">Futures</span>
                <span class="data-description">Futures investing<br>on your IRA or<br><span class="type-secondary">Retirement Account</span></span>
            </div>
        </li>
    </ul>
</div>


<?php

mk_get_view('singular', 'wp-page-footer', false, ['full_width' => true]);

get_footer();
