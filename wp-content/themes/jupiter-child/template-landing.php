<?php
/**
 * Template Name: Landing Page
 */

get_header();

mk_get_view('header', 'wp-toolbar', false);

// wp_nav_menu(['theme_location' => 'third-menu']);

?>

<?php

mk_build_main_wrapper(mk_get_view('singular', 'wp-page-internal-p8', true));

mk_get_view('singular', 'wp-page-footer', false, ['full_width' => true, 'testimonials' => true]);

get_footer();
