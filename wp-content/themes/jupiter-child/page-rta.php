<?php 
/*
Template Name: Real-time Trade Analyzer
*
*	This website created by Reza Khanahmadi
*	Trading Algo X
*	Created on June 1, 2015
*	Updated on: September 30, 2015
*/ 

function PHPErrorHandler ($errno, $errmsg, $errfile, $errline)
{
	echo "<error>ERROR OCCURED<br /><br />Error Number: $errno<br />Line: $errline<br />Message: $errmsg</error>";
	echo "<style type='text/css'>";
	echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
	echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; padding: 10px 0px; border: 2px solid red; background-color: red; }";
	echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
	echo "</style>";
	echo "<a href='" . home_url() . "'>Redirect to Home</a>";
	exit();
}

function PHPShutdownFunction () {
	$error = error_get_last();
	if ($error != null) {
		echo "<error>A FATAL PHP ERROR OCCURED<br /><br />" . $error['message'] . "</error>";
		echo "<style type='text/css'>";
		echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
		echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; border: 2px solid red; background-color: red; }";
		echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
		echo "</style>";
		echo "<a href='" . home_url() . "'>Redirect to Home</a>";
		exit();
	}
}


function ShowNiceError($errmsg) {
    echo "<error>$errmsg</error>";
    echo "<style type='text/css'>";
    echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
    echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; padding: 10px 0px; border: 2px solid red; background-color: red; }";
    echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
    echo "</style>";
    echo "<a href='" . (home_url() . "/contact/") . "'>Redirect to Contact Page</a><br />";
    echo "<a href='" . home_url() . "'>Redirect to Home</a>";
    exit();
}

// Shut off Error reporting and handle errors and Fatal errors internally via a message to the screen
error_reporting(0);
set_error_handler("PHPErrorHandler");
//register_shutdown_function("PHPShutdownFunction");


/* aMember login authenticator */
if (strpos($_SERVER['SERVER_NAME'], "localhost") === false) {
        // production
    require_once '/home/s5trading/public_html/s5-members/library/Am/Lite.php';

} else {
    // localhost
    require_once  $_SERVER['DOCUMENT_ROOT'] . '/stage5/s5-members/library/Am/Lite.php';
}

if (! Am_Lite::getInstance()->isLoggedIn() ) {
    // If user is not logged in redirect to login page
    header("Location: " . Am_Lite::getInstance()->getLoginURL() ); // . "?amember_redirect_url=/rta/" );
    exit();
}

$username = Am_Lite::getInstance()->getUsername();
$name = Am_Lite::getInstance()->getName();

//echo "Username: " . $username . "<br />";
//echo "We are perfoming critial maintenance on the site.  Please check back in a few hours.";
//exit();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stage 5 Trading - Real-Time Analyzer</title>

<?php
$debug = true;

$LOGIN_GRANTED = false;
$LinkedAccounts_GAIN_LIVE = array();
$LinkedAccounts_GAIN_DEMO = array();
$LinkedAccounts_CQG_LIVE = array();
$CommissionsEnabled = false;
$Commissions = 0;

$isAdmin = (trim($username) == "ta-admin");

$jsCachingAppend = "?" . round(microtime(true));


// Check to see if the user has access to RTA and which accounts
require (get_stylesheet_directory() . "/RTA-assets/includes/Constants.php");

$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);

if ($mysqliConn->connect_errno) {
	trigger_error("Failed to connect to MYSQL DB (Error #" . $mysqliConn->connect_errno . ") " . $mysqliConn->connect_error , E_USER_WARNING);
} else {
	
	$queryUsername = $mysqliConn->real_escape_string($username);


	if ($isAdmin) {
		$query = "SELECT Broker, Account FROM admin_accounts ORDER BY Broker, Account ASC LIMIT 100000";
	} else {
		$query = "SELECT * FROM user_access_control WHERE LOWER(Username) = LOWER('$queryUsername') LIMIT 1";
	}

	$resultObject = $mysqliConn->query($query, MYSQLI_STORE_RESULT);

	if ($resultObject->num_rows > 0) {
		while (	$result = $resultObject->fetch_assoc() ) {

			if ($result != NULL && $result) {
				if ($isAdmin) {
					// ADMIN rights
					$Broker = trim($result['Broker']);
					$Account = trim($result['Account']);

					if ($Broker == "GAIN_LIVE")
						array_push($LinkedAccounts_GAIN_LIVE, $Account);
					else if ($Broker == "GAIN_DEMO")
						array_push($LinkedAccounts_GAIN_DEMO, $Account);
					else if ($Broker == "CQG_LIVE") 
						array_push($LinkedAccounts_CQG_LIVE, $Account);

				} else {

					// User rights
					if ($result['CanAccessRTA'] == "1") {

						// WORK
						$LinkedAccounts_GAIN_LIVE = trim($result['GAIN_LIVE']) != "" ? explode(",", str_replace(" ", "", $result['GAIN_LIVE'])) : array();
						$LinkedAccounts_GAIN_DEMO = trim($result['GAIN_DEMO']) != "" ? explode(",", str_replace(" ", "", $result['GAIN_DEMO'])) : array();
						$LinkedAccounts_CQG_LIVE = trim($result['CQG_LIVE']) != "" ? explode(",", str_replace(" ", "", $result['CQG_LIVE'])) : array();

						$CommissionsEnabled = $result['CommissionsEnabled'] == '1' ? true : false;

                        if (count($LinkedAccounts_GAIN_LIVE) > 0 || count($LinkedAccounts_GAIN_DEMO) > 0 || count($LinkedAccounts_CQG_LIVE) > 0) {
                            // there are one or more assigned accounts
                            $LOGIN_GRANTED = true;
                        } else {
                            // login is correct, and user has access to RTA, but no accounts are assigned to user
                            ShowNiceError("Please note that no accounts have been assigned to this username.  Please contact the Stage 5 Admin.");
                        }

					} else {
						// Access not allowed by admin	
						ShowNiceError("Access to the Real-Time Trade Analyzer has not been granted by Admin.  Please contact the Stage 5 Admin.");
					}
				}
			} else {
				// Username not in database
				ShowNiceError("We are sorry. Your account has not been assigned to your login profile yet. Please contact the Stage 5 Admin.");
			}

		}
	} else {
		ShowNiceError("We are sorry. Your account has not been assigned to your login profile yet. Please contact the Stage 5 Admin.");
	}


	$DisplayMetric = "Ticks";
	$Commissions = 0;
	$CalcMethod = "PerContract";
	$FontSize = "normal";
	$Sound = 1;

	$query_for_settings = "SELECT * FROM user_settings where LOWER(Username)=LOWER('$queryUsername') LIMIT 1";
	$settings_results = $mysqliConn->query($query_for_settings);
	if ($settings_results && $settings_results->num_rows > 0) {
		try {
			$row = $settings_results->fetch_assoc();
			$DisplayMetric = $row["RTA_DisplayMetric"];
			$Commissions = ($DisplayMetric == "Dollars") ? $row["RTA_Commissions"] : 0;
			$CalcMethod = $row["RTA_CalcMethod"];
			$FontSize = $row["RTA_FontSize"];
			$Sound = $row["RTA_Sound"];
		}
		catch (Exception $ex) {}
	}

	$resultObject->free();
	$mysqliConn->close();
}


$BaseLocation = get_stylesheet_directory_uri();
$BaseURL = get_site_url();

?>

<base href="<?php echo "$BaseLocation/RTA-assets/"; ?>" />
<link rel="icon" href="images/S5TA.favicon.ico" />

<!-- Local CSS load -->
<link rel="stylesheet" href="libraries/split-pane-management/split-pane.css" />
<link rel="stylesheet" href="css/UniversalElementStyles.css<?php echo $jsCachingAppend; ?>" />
<link rel="stylesheet" href="css/ModuleStyles.css<?php echo $jsCachingAppend; ?>" />
<link rel="stylesheet" href="css/RTAMainStyles.css<?php echo $jsCachingAppend; ?>" />

<!-- RAVEN for Sentry, bug and error tracker -->
<script src="https://cdn.ravenjs.com/3.8.0/raven.min.js"></script>

<!-- WP identification & constant variables-->
<script type="text/javascript">
	const username = "<?php echo $username; ?>";
	const UUID = "<?php echo uniqid(); ?>";

	const CommissionsEnabled = <?php echo ($CommissionsEnabled ? "true":"false"); ?>;
    const _PUSHER_SANDBOX = <?php echo ((isset($_GET['SANDBOX']) && $_GET['SANDBOX'] == 'true') ? "true":"false"); ?>;
	const isAdmin = "<?php echo $isAdmin ? 'true': 'false'; ?>" == 'true';
    const _BASE_URL = "<?php echo $BaseURL; ?>";
	var CommissionFee = Number("<?php echo $Commissions; ?>");
	var	ResultsDisplayType = "<?php echo $DisplayMetric; ?>";
	// var Sound = "<?php echo $Sound; ?>" == "1";

	var popoutWindow = null;
	var selectedAccount = null, selectedContract = "";
	
	const PUSHER_APP_KEY = "<?php echo PUSHER_APP_KEY; ?>";


    // bug && error tracker
    
    Raven.config('https://0c8b4c774d33469883606ee2671705ef@sentry.io/112704').install();
    Raven.setUserContext({
        username            : username,
        ResultsDisplayType  : ResultsDisplayType,
        selectedAccount     : selectedAccount,
        selectedContract    : selectedContract
    });
    
</script>


<!-- CDNs -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script type="text/javascript" src="//code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" src="//code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="//code.highcharts.com/highcharts-more.js"></script>
<script type="text/javascript" src="//code.highcharts.com/modules/solid-gauge.js"></script>
<script type="text/javascript" src="//js.pusher.com/3.0/pusher.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<link href='//fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>

<!-- Local JS -->
<script type="text/javascript" src="libraries/split-pane-management/split-pane.js"></script>

<!--<script type="text/javascript" src="libraries/knob-master/js/jquery.knob.js"></script>	-->
<!-- <script type="text/javascript" src="libraries/gauge/jquery.gauge.min.js"></script> -->
<script type="text/javascript" src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="libraries/chartjs/themes/dark-unica.js"></script>	

<script type="text/javascript" src="js/Charts.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/TradesHandler-RTA.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/NotificationClient.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/PusherClient.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/Functions.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/WindowFunctions.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/DebugFunctions.js<?php echo $jsCachingAppend; ?>"></script>

<!-- load sounds -->
<!-- <audio id="alert" class="audio" preload="auto"><source src="sounds/alert.mp3" type="audio/mpeg"><source src="sounds/alert.ogg" type="audio/ogg"></audio> -->


<script type="text/javascript">
// this function initiates the split panes
    $(function() { 
    	$('div.split-pane').splitPane();

    	var FontSize = "<?php echo $FontSize; ?>";
		if (FontSize != "medium") setFontSize(FontSize);
    });

    // Assign accounts
    const LinkedAccounts_GAIN_LIVE = <?php echo json_encode($LinkedAccounts_GAIN_LIVE); ?>,
          LinkedAccounts_GAIN_DEMO = <?php echo json_encode($LinkedAccounts_GAIN_DEMO); ?>,
          LinkedAccounts_CQG_LIVE = <?php echo json_encode($LinkedAccounts_CQG_LIVE); ?>;
</script>


</head>

<div id="dimmer" style="display:block; display: none; height: 100%; width: 100%; background-color: #666; position: absolute; top: 0; left: 0; z-index:99; opacity: 0.9;">
<div style="display: block; text-align: center; margin: 10% auto; color: #fff; font-size: 2em; font-weight: bold; z-index: 100;">
	Loading trading metrics...<br  />
    <br /><br />
    <span style="font-size: .8em; font-weight: normal;">This may take up to 30 seconds, depending on the number of transactions.</span>
</div>
</div>

<body style="position: relative; z-index: 1; top: 0; left: 0;">
<div class="toolbar-container">
	<div class="top-top" style="background-color: #32363b; padding: 7px 10px; height: 40px; line-height: 40px;">
    	<div class="toolbar-left-section">
			<img src="images/top-logo.png">
        </div>
        <div class="toolbar-right-section">
			<a class="switch-platform-icon" href="/hta/" title="Switch to the Historical Trade Analyzer Platform">Switch to HTA</a> 
           	<a class="icon" id="bug-report" onclick="javascript:void(0)" title="Bug Report"></a>
            <a class="icon" id="settings" onclick="javascript:void(0)" title="Settings"></a>
            <a class="icon" id="help" href="https://stage5trading.com/s5-trade-analyzer-instructions/" target="_blank" title="Help"></a>
        	<a class="icon" id="logout" href="<?php echo Am_Lite::getInstance()->getLogoutURL(); ?>" title="Logout"></a>
        </div>
    </div>
    <div id="selector-toolbar" style="font-size: .9em; overflow: hidden;">
    	<div class="toolbar-left-section">
            Welcome <b title="<?php echo $username; ?>"><?php echo $name; ?></b>! Account:
            <?php
            	if ($isAdmin) {
            		echo "<input id='admin-account-selector' />";
            	} else {
            		echo "<select id='account-selector' style='margin-left: 5px;'>";

                    // GAIN_LIVE
                    if (count($LinkedAccounts_GAIN_LIVE) > 0) {
                        foreach ($LinkedAccounts_GAIN_LIVE as $Account) {
                        	$MaskedAccount = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
                            if (trim($Account) != "") echo "<option value='GAIN_LIVE|" . $Account . "'>GAIN-LIVE - " . $MaskedAccount . "</option>";
                        }
                    }
                        
                    // GAIN_DEMO
                    if (count($LinkedAccounts_GAIN_DEMO) > 0) {
                        foreach ($LinkedAccounts_GAIN_DEMO as $Account) {
                        	$MaskedAccount1 = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
                            if (trim($Account) != "") echo "<option value='GAIN_DEMO|" . $Account . "'>GAIN-DEMO - " . $MaskedAccount1 . "</option>";
                        }
					}

                    // CQG_LIVE
                    if (count($LinkedAccounts_CQG_LIVE) > 0) {
                        foreach ($LinkedAccounts_CQG_LIVE as $Account) {
                        	$MaskedAccount = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
                            if (trim($Account) != "") echo "<option value='CQG_LIVE|" . $Account . "'>CQG-LIVE - " . $MaskedAccount . "</option>";                           		
                        }
            		}

                    echo "</select>";
            	}
            ?>

            <span id="selected-contract-display">Contract:
            	<select id="contract-selector" style="margin-left: 5px;"><option>--</option></select>
            </span>
            <span id="results-type-display">Display: <b>Points</b></span>
            <?php if ($CommissionsEnabled) echo "<span id='commissions-display'>Commissions: $<b>$Commissions</b>/contract</span>"; ?>
		</div>
       	<div class="toolbar-right-section">
            Connections:
            <span class="connection-status" id="connection-status-pusher" title="Disconnected">Cloud Uplink</span>
            <span class="connection-status" id="connection-status-worker" title="Disconnected">Stage 5 Data</span>
        </div>
    </div>
    <div class="hidden-toolbar" id="settings-toolbar">
    	<div class="settings-toolbar-top">
			Settings
        </div>
        <div class="settings-toolbar-bottom">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
            	<td>Display Results in
            		<select id="settings-display-results">
            		<?php 
            			$ResultsTypeArray = array("Ticks", "Points", "Dollars");
            			foreach($ResultsTypeArray as $val) {
            				echo "<option " . ($val == $DisplayMetric ? "selected='selected'" : "") . " value='$val'>$val</option>";
            			}
            		?>
            		</select>
            	</td>
            	<td>Commissions:
					<select id="settings-commission-fee">
						<?php 
							$commissionArray = array("0.00", "1.00", "1.50", "2.00", "2.50", "3.00", "3.50", "4.00");
							foreach ($commissionArray as $val) {
								if ($DisplayMetric == "Dollars")
									echo "<option " . (floatval($val) == floatval($Commissions) ? "selected='selected'" : "") . " value='" . floatval($val) . "'>$ $val</option>";
								else
									echo "<option " . (floatval($val) == floatval(0.00)		    ? "selected='selected'" : "") . " value='" . floatval($val) . "'>$ $val</option>";
							}
						?>
					</select>
            	 </td>
            <td><input type="button" id="settings-save" value="Calculate" /><input id="settings-cancel" type="button" value="Cancel" /></td>
            </tr>
            <tr><td colspan="3" id="settings-warning" style="padding: 20px 0 5px 0; text-align: center;"><small>NOTE: When results are displayed in Ticks or Points Commissions are set to zero.</small></td></tr>
            </table>
            <br />
            <table border="1" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                	<td height="30">Font Size</td><td></td><td>
                		<a class='font-control' id='large'>Large</a><a class='font-control' id='medium'>Medium</a><a class='font-control' id='small'>Small</a>
                	</td>
                </tr>
                <tr>
                	<td height="30"><label>
                		<?php echo "<input type='checkbox' id='expected-value-per-contract' " . ($CalcMethod == "PerContract" ? "checked='checked'" : "") . " />"; ?>Expected Value is calculated per contract</label>
                	</td><td></td><td></td>
                </tr>
            </table>
            <br />

        </div> 
	</div>  
 
    <div class="hidden-toolbar" id="bug-report-toolbar">
    	<div class="settings-toolbar-top">
			Bug Report
        </div>
        <div class="settings-toolbar-bottom">
	        <div style="height: 100%; margin: 0.5em auto;">
            	<textarea id="bug-report-text" style="display: block; width: 90%; height: 100%; min-height: 3em; margin: 0 auto; resize: vertical; font-size: 1em; line-height: 1.2em;" placeholder="Please type in your bug report.  Do not use any html characters; only plain text."></textarea><br />
                <input type="button" id="bug-report-submit" value="Submit" /><input type="button" id="bug-report-cancel" value="Cancel" />
            </div>
        </div> 
	</div>  
</div>
<div class="content">
    <div class="panel-container">
        <div class="split-pane fixed-top">
            <div class="split-pane-component" id="top-component">
                <div class="split-pane-inner">
                    <div class="split-pane-functional-window" id="summary-contracts-window">
                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Contract Summary Toolbar<a id="popout" onclick="javascript:void(0)" title="Pop out">popout</a><label><input type="checkbox" id="closed-positions-display" checked="checked" style="vertical-align: middle; margin-right: 3px !important;" />Display Closed Positions</label></div>
                        <div class="databar">
						<!-- content here -->

                        </div>
                    </div>
                </div>
            </div>
            <div class="split-pane-divider" id="horizontal-divider"></div>
            <div class="split-pane-component" id="bottom-component">
                <!-- bottom part -->
                <div class="split-pane fixed-top">
                    <div class="split-pane-component" id="top-component2">
                        <div id="split-pane-1" class="split-pane fixed-left">
                            <div class="split-pane-component" id="left-component">
                                <div class="split-pane-inner">
                                    <div class="split-pane-functional-window" id="stats-window">
                                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Stats Toolbar</div>
                                        <div class="databar">
                                        <!-- content here -->
                                        <div style="width: 400px; height: 120px; margin: 0 auto;">
                                            <div id="percent-win-dial" title="Winning Trades (does not include flat trades)" style="display: inline-block; width: 130px; height: 120px; margin: 0;"></div>
                                            <div id="percent-loss-dial" title="Losing Trades (does not include flat trades)" style="display: inline-block; width: 130px; height: 120px; margin: 0;"></div>
                                            <div id="pnl-dial" title="Current Running P&L for Contract" style="display: inline-block; width: 130px; height: 120px; margin: 0;"></div>
                                        </div>
                                        
                                        <table border="1" cellpadding="4" cellspacing="0" width="85%" style="margin: 0px auto; font-size: 0.80em;">
                                            <tr><td>Current P&L </td><td id="stats-current-pnl">n.a.</td></tr>
                                            <tr><td># of Trades</td><td id="stats-num-of-trades">n.a.</td></tr>
                                            <tr><td># of Contracts Traded</td><td id="stats-num-of-contracts-traded">n.a.</td></tr>
                                            <tr><td>Current Expectancy</td><td id="stats-current-expectancy">n.a.</td></tr>
                                            <tr><td>Avg. Win Duration</td><td id="stats-avg-win-duration">n.a.</td></tr>
                                            <tr><td>Avg. Loss Duration</td><td id="stats-avg-loss-duration">n.a.</td></tr>
                                        </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="split-pane-divider" id="vertical-divider"></div>
								<div class="split-pane-component" id="right-component">
                                <div class="split-pane-inner">
                                    <div class="split-pane-functional-window" id="charts-window">
                                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Charts Toolbar<a class="chart-change" id="win-loss-percentage-chart">Win/Loss %</a><a class="chart-change" id="equity-curve-chart">Equity Curve</a><a class="chart-change" id="expectancy-chart">Expectancy</a><a class="chart-change" id="trade-distribution-chart">Trade Distribution</a></div>
                                        <div class="databar" id="mychart">
                                        <!-- content here -->
                                            <div class="chart-container" id="win-loss-percentage-chart"></div>
                                            <div class="chart-container" id="equity-curve-chart"></div>
                                            <div class="chart-container" id="expectancy-chart"></div>
                                            <div class="chart-container" id="trade-distribution-chart"></div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="split-pane-divider" id="horizontal-divider2"></div>
                    <div class="split-pane-component" id="bottom-component2">
             	       <div class="split-pane-inner" id="completed-trades-pane">
                            <div class="split-pane-functional-window" id="trades-window">
                                <div class="titlebar"><span><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Completed Trades</span>
                                <!-- TODO RK - make view all button work and appear -->
                                <!-- <a id="time-period-change">View All Trades</a> --> 
                                <a id="export-trades-to-csv">Export Trades to CSV</a>
                                <!-- <a id="load-user-trade-metrics">Load User Metrics</a><a id="save-user-trade-metrics">Save User Metrics</a> -->
                                </div>
                                <div class="databar">
                                <!-- content here -->
                                </div>
                            </div>
                            <div style="position: absolute; left: 0; bottom: 0; display: block; width: 100%;">RISK NOTICE: Derivatives trading is not suitable for all investors. Past performance is not indicative of future results.</div>
                        </div>
                    </div>
				</div>       
            </div>
        </div>    
    </div>
</div>

<script type="text/javascript">

</script>

</body>
</html>
