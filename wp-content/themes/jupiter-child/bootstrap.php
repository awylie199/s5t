<?php
if (! defined('ABSPATH')) {
    exit;
}

use s5t\Setup;

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

Setup::instance();
