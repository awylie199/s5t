<?php 
/*
Template Name: Historical Trade Analyzer
*
*	This website created by Reza Khanahmadi
*	Trading Algo X
*	Created on March 1, 2016
*	Updated on: August 10, 2016
*/ 

session_start();


function PHPErrorHandler ($errno, $errmsg, $errfile, $errline)
{
	echo "<error>ERROR OCCURED<br /><br />Error Number: $errno<br />Line: $errline<br />Message: $errmsg</error>";
	echo "<style type='text/css'>";
	echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
	echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; padding: 10px 0px; border: 2px solid red; background-color: red; }";
	echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
	echo "</style>";
	echo "<a href='" . home_url() . "'>Redirect to Home</a>";
	exit();
}

function PHPShutdownFunction () {
	$error = error_get_last();
	if ($error != null) {
		echo "<error>A FATAL PHP ERROR OCCURED<br /><br />" . $error['message'] . "</error>";
		echo "<style type='text/css'>";
		echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
		echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; border: 2px solid red; background-color: red; }";
		echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
		echo "</style>";
		echo "<a href='" . home_url() . "'>Redirect to Home</a>";
		exit();
	}
}

function ShowNiceError($errmsg) {
    echo "<error>$errmsg</error>";
    echo "<style type='text/css'>";
    echo "html, body { background-color: #32363b; color: #f2f2f2; text-align: center; }";
    echo "error { font-size: 25px; line-height: 40px; display: block; width: 100%; margin: 25px auto; padding: 10px 0px; border: 2px solid red; background-color: red; }";
    echo "a, a:hover, a:link, a:active { display: block; width: 100%; color: #f2f2f2; }";
    echo "</style>";
    echo "<a href='" . (home_url() . "/contact/") . "'>Redirect to Contact Page</a><br />";
    echo "<a href='" . home_url() . "'>Redirect to Home</a>";
    exit();
}

// Shut off Error reporting and handle errors and Fatal errors internally via a message to the screen
error_reporting(0);
set_error_handler("PHPErrorHandler");
//register_shutdown_function("PHPShutdownFunction");

/* aMember login authenticator */
if (strpos($_SERVER['SERVER_NAME'], "localhost") === false) {
	    // production
    require_once '/home/s5trading/public_html/s5-members/library/Am/Lite.php';

} else {
    // localhost
    require_once  $_SERVER['DOCUMENT_ROOT'] . '/stage5/s5-members/library/Am/Lite.php';
}

if (! Am_Lite::getInstance()->isLoggedIn() ) {
    // If user is not logged in redirect to login page
    header("Location: " . Am_Lite::getInstance()->getLoginURL() ); // . "?amember_redirect_url=/hta/" );
    exit();
}

$username = Am_Lite::getInstance()->getUsername();
$name = Am_Lite::getInstance()->getName();

if (!$username) {
	echo "User error";
	exit();
}

$_SESSION["S5-USERNAME"] = $username;

//echo "Username: " . $username . "<br />";
//echo "We are perfoming critial maintenance on the site.  Please check back in a few hours.";
//exit();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stage 5 Trading - Historical Trade Analyzer</title>

<?php

$isAdmin = (trim($username) == "ta-admin");

$LOGIN_GRANTED = false;
$LinkedAccounts_GAIN_LIVE = array();
$LinkedAccounts_GAIN_DEMO = array();
$LinkedAccounts_CQG_LIVE = array();
$CommissionsEnabled = false;
$Commissions = 0;

$jsCachingAppend = "?" . round(microtime(true));

// Check to see if the user has access to RTA and which accounts

//echo (get_stylesheet_directory() . "/HTA-assets/includes/Constants.php");
require (get_stylesheet_directory() . "/HTA-assets/includes/Constants.php");
//require (get_template_directory_uri() . "/HTA-assets/includes/Constants.php");

$mysqliConn = @new MySQLi(PROD_DB_HOST, PROD_DB_USERNAME_USER, PROD_DB_PASSWORD_USER, PROD_DB_NAME);

if ($mysqliConn->connect_errno) {

	trigger_error("Failed to connect to MYSQL DB (Error #" . $mysqliConn->connect_errno . ") " . $mysqliConn->connect_error , E_USER_WARNING);
} else {
	
	$queryUsername = $mysqliConn->real_escape_string($username);

	if ($isAdmin) {
		$query = "SELECT Broker, Account FROM admin_accounts ORDER BY Broker, Account ASC LIMIT 100000";
	} else {
		$query = "SELECT * FROM user_access_control WHERE LOWER(Username) = LOWER('$queryUsername') LIMIT 1";	
	}
	
	$resultObject = $mysqliConn->query($query, MYSQLI_STORE_RESULT);

	if ($resultObject->num_rows > 0) {
		while (	$result = $resultObject->fetch_assoc() ) {

			if ($result != NULL && $result) {
				if ($isAdmin) {
					// ADMIN rights
					$Broker = trim($result['Broker']);
					$Account = trim($result['Account']);

					if ($Broker == "GAIN_LIVE")
						array_push($LinkedAccounts_GAIN_LIVE, $Account);
					else if ($Broker == "GAIN_DEMO")
						array_push($LinkedAccounts_GAIN_DEMO, $Account);
					else if ($Broker == "CQG_LIVE") 
						array_push($LinkedAccounts_CQG_LIVE, $Account);

				} else {

					// User rights
					if ($result['CanAccessHTA'] == "1") {
						// WORK
						$LinkedAccounts_GAIN_LIVE = trim($result['GAIN_LIVE']) != "" ? explode(",", str_replace(" ", "", $result['GAIN_LIVE'])) : array();
						$LinkedAccounts_GAIN_DEMO = trim($result['GAIN_DEMO']) != "" ? explode(",", str_replace(" ", "", $result['GAIN_DEMO'])) : array();
						$LinkedAccounts_CQG_LIVE = trim($result['CQG_LIVE']) != "" ? explode(",", str_replace(" ", "", $result['CQG_LIVE'])) : array();

						$CommissionsEnabled = $result['CommissionsEnabled'] == '1' ? true : false;
						$LOGIN_GRANTED = true;

						if (count($LinkedAccounts_GAIN_LIVE) > 0 || count($LinkedAccounts_GAIN_DEMO) > 0 || count($LinkedAccounts_CQG_LIVE) > 0) {
                            // there are one or more assigned accounts
                            $LOGIN_GRANTED = true;
                        } else {
                            // login is correct, and user has access to RTA, but no accounts are assigned to user
                            ShowNiceError("Please note that no accounts have been assigned to this username.  Please contact the Stage 5 Admin.");
                        }

					} else {
						// Access not allowed by admin	
						ShowNiceError("Access to the Historical Trade Analyzer has not been granted by Admin.  Please contact the Stage 5 Admin.");
					}
				}
			} else {
				// Username not in database
				ShowNiceError("We are sorry. Your account has not been assigned to your login profile yet. Please contact the Stage 5 Admin.");
			}

		}
	} else {
		ShowNiceError("We are sorry. Your account has not been assigned to your login profile yet. Please contact the Stage 5 Admin.");
	}


	$DisplayMetric = "Ticks";
	$Commissions = 0;
	$CalcMethod = "PerContract";
	$FontSize = "normal";
	$PrintoutIDs = "";

	$query_for_settings = "SELECT * FROM user_settings where LOWER(Username)=LOWER('$queryUsername') LIMIT 1";
	$settings_results = $mysqliConn->query($query_for_settings);

	if ($settings_results && $settings_results->num_rows > 0) {
		try {
			$row = $settings_results->fetch_assoc();
			$DisplayMetric = $row["HTA_DisplayMetric"];
			$Commissions = ($DisplayMetric == "Dollars") ? $row["HTA_Commissions"] : 0;
			$CalcMethod = $row["HTA_CalcMethod"];
			$FontSize = $row["HTA_FontSize"];
			$PrintoutIDs = $row["HTA_PrintOutColumns"];
			
		}
		catch (Exception $ex) {}
	}

	$resultObject->free();
	$mysqliConn->close();

}

$BaseLocation = get_stylesheet_directory_uri();
$BaseURL = get_site_url();
?>

<base href="<?php echo "$BaseLocation/HTA-assets/"; ?>" />
<link rel="icon" href="images/S5TA.favicon.ico" />

<!-- WP identification -->
<script type="text/javascript">
	const username = "<?php echo $username; ?>";
	const UUID = "<?php echo uniqid(); ?>";
	const CommissionsEnabled = <?php echo ($CommissionsEnabled ? "true":"false"); ?>;
	const isAdmin = "<?php echo $isAdmin ? 'true' : 'false'; ?>" == 'true';
	const _BASE_URL = "<?php echo $BaseURL; ?>";

	var CommissionFee = Number("<?php echo $Commissions; ?>"),
		ResultsDisplayType = "<?php echo $DisplayMetric; ?>";

</script>


<!-- CDNs -->
<!-- <script src="https://code.jquery.com/jquery-1.12.2.min.js" integrity="sha256-lZFHibXzMHo3GGeehn1hudTAP3Sc0uKXBXAzHX1sjtk=" crossorigin="anonymous"></script> -->
<script  src="//code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<!-- data table -->
<!-- 
<script type="text/javascript" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
 -->

<!-- jquery multi select -->
<link rel="stylesheet" type="text/css" href="libraries/multiselect/jquery.multiselect.css" />


<script type="text/javascript" src="libraries/multiselect/jquery.multiselect.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" src="//code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="//code.highcharts.com/highcharts-more.js"></script>
<script type="text/javascript" src="//code.highcharts.com/modules/solid-gauge.js"></script>

<link href='//fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>

<!-- Local JS & CSS -->
<script type="text/javascript" src="libraries/split-pane-management/split-pane.js"></script>
<link rel="stylesheet" href="libraries/split-pane-management/split-pane.css" />

<script type="text/javascript" src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="libraries/chartjs/themes/dark-unica.js"></script>	

<script type="text/javascript" src="libraries/fixed-header/jquery.fixedheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="libraries/fixed-header/mainTheme.css" />

<script type="text/javascript" src="js/NotificationClient.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/TradeHandler-HTA.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/Charts.js<?php echo $jsCachingAppend; ?>"></script>

<!-- For printing -->
<script type="text/javascript" src="libraries/table-print/libs/jspdf.min.js"></script>
<script type="text/javascript" src="libraries/table-print/libs/jspdf.plugin.autotable.src.js"></script>
<script type="text/javascript" src="js/Print.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/Functions.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/WindowFunctions.js<?php echo $jsCachingAppend; ?>"></script>
<script type="text/javascript" src="js/DebugFunctions.js<?php echo $jsCachingAppend; ?>"></script>


<link rel="stylesheet" href="css/UniversalElementStyles.css" />
<link rel="stylesheet" href="css/ModuleStyles.css" />
<link rel="stylesheet" type="text/css" href="css/HTAMainStyles.css" />

<script>
	var TextOfPrintoutIDs = "<?php echo $PrintoutIDs; ?>",
		ArrayOfPrintOutIDs = $.merge([], TextOfPrintoutIDs.split(",")),
		FontSize = "<?php echo $FontSize; ?>";

	
	// this function initiates the split panes
	$(function() {
		$('div.split-pane').splitPane();

		if (FontSize != "medium") setFontSize(FontSize);

		// Assign the printout IDs to make sure the right checkboxes are checked 
		setTimeout(function () {
			$.each(ArrayOfPrintOutIDs, function (i, val) {
				if (val != "") $(".table-print-pref input#" + val).attr("checked", "checked");
			});
		}, 2500);
	});

	// Assign accounts
	const LinkedAccounts_GAIN_LIVE = <?php echo json_encode($LinkedAccounts_GAIN_LIVE); ?>,
		  LinkedAccounts_GAIN_DEMO = <?php echo json_encode($LinkedAccounts_GAIN_DEMO); ?>,
		  LinkedAccounts_CQG_LIVE = <?php echo json_encode($LinkedAccounts_CQG_LIVE); ?>;

</script>

</head>

<body style="position: relative; z-index: 1; top: 0; left: 0;">
<div class="toolbar-container">
	<div class="top-top" style="background-color: #32363b; padding: 7px 10px; height: 40px; line-height: 40px;">
    	<div class="toolbar-left-section">
			<img src="images/top-logo.png">
        </div>
        <div class="toolbar-right-section">
           	<a class="switch-platform-icon" href="/rta/" title="Switch to the Real-time Trade Analyzer Platform">Switch to RTA</a>        
           	<a class="icon" id="bug-report" onclick="javascript:void(0)" title="Bug Report"></a>
            <a class="icon" id="settings" onclick="javascript:void(0)" title="Settings"></a>
            <a class="icon" id="print" onclick="javascript:void(0)" title="Print All Metrics"></a>
            <a class="icon" id="help" href="https://stage5trading.com/s5-trade-analyzer-instructions/" target="_blank" title="Help"></a>
        	<a class="icon" id="logout" href="<?php echo Am_Lite::getInstance()->getLogoutURL(); ?>" title="Logout"></a>
        </div>
    </div>
    <div id="selector-toolbar" style="font-size: .9em; overflow: hidden;">
    	<div class="toolbar-left-section">
        	<table>
            	<tr>
                	<td>Start Date</td>
                    <td>End Date</td>
                    <td>Account</td>
                    <td>Contract</td>
                    <td></td>
                    <td><td>
                </tr>
                <tr>
                	<td><input class="datepicker" id="from-datepicker" type="text" /></td>
                    <td><input class="datepicker" id="to-datepicker" type="text" /></td>
                    <td>
                        <?php
                        	if ($isAdmin) {
                        		echo "<input id='admin-account-selector' />";                                    
                        	} else {
                        		echo "<select id='account-selector' style='margin-left: 5px;'>";

                                // GAIN_LIVE
                                if (count($LinkedAccounts_GAIN_LIVE) > 0) {
	                                foreach ($LinkedAccounts_GAIN_LIVE as $Account) {
	                                	$MaskedAccount = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
	                                    if (trim($Account) != "") echo "<option value='GAIN_LIVE|" . $Account . "'>GAIN-LIVE - " . $MaskedAccount . "</option>";
	                                }
	                            }
                                    
                                // GAIN_DEMO
                                if (count($LinkedAccounts_GAIN_DEMO) > 0) {
	                                foreach ($LinkedAccounts_GAIN_DEMO as $Account) {
	                                	$MaskedAccount1 = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
	                                    if (trim($Account) != "") echo "<option value='GAIN_DEMO|" . $Account . "'>GAIN-DEMO - " . $MaskedAccount1 . "</option>";
	                                }
								}

                                // CQG_LIVE
                                if (count($LinkedAccounts_CQG_LIVE) > 0) {
	                                foreach ($LinkedAccounts_CQG_LIVE as $Account) {
	                                	$MaskedAccount = str_repeat('*', strlen($Account)-3) . substr($Account, strlen($Account)-3, 3);
	                                    if (trim($Account) != "") echo "<option value='CQG_LIVE|" . $Account . "'>CQG-LIVE - " . $MaskedAccount . "</option>";                           		
	                                }
                        		}

                                echo "</select>";
                        	}
                        ?>
                    </td>
                    <td><select id="contract-selector"></select></td>
                    <td><img src="images/loading-static-icon.png" id="contract-loading-icon" class="spin" /></td>
                    <td><input type="button" id="analyze" value="Analyze" /></td>
                </tr>
        	</table>
		</div>
       	<div class="toolbar-right-section">
        </div>
    </div>
    <div class="hidden-toolbar" id="settings-toolbar">
    	<div class="settings-toolbar-top">
			Settings
        </div>
        <div class="settings-toolbar-bottom">

           <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
            	<td>Display Results in
            		<select id="settings-display-results">
            		<?php 
            			// $ResultsTypeArray = array("Ticks", "Points", "Dollars");
            			$ResultsTypeArray = array("Ticks", "Dollars");
            			foreach($ResultsTypeArray as $val) {
            				echo "<option " . ($val == $DisplayMetric ? "selected='selected'" : "") . " value='$val'>$val</option>";
            			}
            		?>
            		</select>
            	</td>
            	<td>Commissions:
					<select id="settings-commission-fee">
						<?php 
							$commissionArray = array("0.00", "1.00", "1.50", "2.00", "2.50", "3.00", "3.50", "4.00");
							foreach ($commissionArray as $val) {
								if ($DisplayMetric == "Dollars")
									echo "<option " . (floatval($val) == floatval($Commissions) ? "selected='selected'" : "") . " value='" . floatval($val) . "'>$ $val</option>";
								else
									echo "<option " . (floatval($val) == floatval(0.00)		    ? "selected='selected'" : "") . " value='" . floatval($val) . "'>$ $val</option>";
							}
						?>
					</select>
            	 </td>
            <td><input type="button" id="settings-save" value="Calculate" /><input id="settings-cancel" type="button" value="Cancel" /></td>
            </tr>
            <tr><td colspan="3" id="settings-warning" style="padding: 20px 0 5px 0; text-align: center;"><small>NOTE: When results are displayed in Ticks, commissions are set to zero.</small></td></tr>
            </table>
            <br />
            <table border="1" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                	<td height="30">Font Size</td><td></td><td>
                		<a class='font-control' id='large'>Large</a><a class='font-control' id='medium'>Medium</a><a class='font-control' id='small'>Small</a>
                	</td>
                </tr>
                <tr>
                	<td height="30"><label>
                		<?php echo "<input type='checkbox' id='expected-value-per-contract' " . ($CalcMethod == "PerContract" ? "checked='checked'" : "") . " />"; ?>Expected Value is calculated per contract</label>
                	</td><td></td><td></td>
                </tr>
            </table>
            <br />

<!--             <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr><td>Display Results in <select id="settings-display-results"><option selected="selected" value="Ticks">Ticks</option><option value="Dollars">Dollars</option></select></td>
            <td>Commissions: <select id="settings-commission-fee"><option selected="selected" value="0">$ 0.00</option><option value="1">$ 1.00</option><option value="1.5">$ 1.50</option><option value="2">$ 2.00</option><option value="2">$ 2.50</option><option value="2">$ 3.00</option><option value="2">$ 3.50</option><option value="2">$ 4.00</option></select></td>
            <td><input type="button" id="settings-save" value="Calculate" /><input id="settings-cancel" type="button" value="Cancel" /></td>
            </tr>
            <tr><td colspan="3" id="settings-warning" style="padding: 20px 0 5px 0; text-align: center;"><small>NOTE: When results are displayed in Ticks or Points Commissions are set to zero.</small></td></tr>
            </table>
            <br />

            <table border="1" cellpadding="0" cellspacing="0" style="width: 100%;">
            	<tr><td height="30">Font Size</td><td></td><td><a class='font-control' id='large'>Large</a><a class='font-control' id='medium'>Medium</a><a class='font-control' id='small'>Small</a></td></tr>
            	<tr><td height="30"><label><input type="checkbox" id="expected-value-per-contract" checked="checked" />Expected Value is calculated per contract</label></td><td></td><td></td></tr>
            </table>
            <br />
 -->

            <table border="0" cellpadding="2" cellspacing="0" style="width: 100%;">
            	<tr>
	            	<td colspan="6" style="text-align: left;">Trade metrics to inlcude in print out:</td>
            	</tr>
            	<tr class="table-print-pref">
            		<style type="text/css">
            			.table-print-pref td { text-align: left; font-size: 0.8em; }
            			.table-print-pref input { margin-left: 10px; margin-right: 2px; vertical-align: middle; }
            			.table-print-pref label { display:  block; }

            		</style>
            		<input type="checkbox" checked="checked" id='id' hidden="hidden" />
            		<input type="checkbox" checked="checked" id='contract' hidden="hidden" />
	            	<td>
	            		<label><input type="checkbox" id='longShort' />L/S</label>
	            		<label><input type="checkbox" id='mfe' />MFE</label>
	            		<label><input type="checkbox" id='mae' />MAE</label>
	            		<label><input type="checkbox" id='bso' />BSO</label>
	            		<label><input type="checkbox" id='maxPosition' />Max Position Qty</label>
	            	</td>
	            	<td>
	            		<label><input type="checkbox" id='TotalQtyTraded' />Total Quantity Traded</label>
	            		<label><input type="checkbox" id='tit' />TiT</label>
	            		<label><input type="checkbox" id='tsb' />TSB</label>
	            		<label><input type="checkbox" id='pnl' />P&L</label>
	            		<label><input type="checkbox" id='pnlRunning' />Running P&L</label>
	            	</td>
	            	<td>
	            		<label><input type="checkbox" id='entryTime' />Entry Time</label>
	            		<label><input type="checkbox" id='exitTime' />Exit Time</label>
	            		<label><input type="checkbox" id='entryPrice' />Entry Price</label>
	            		<label><input type="checkbox" id='exitPrice' />Exit Price</label>
	            	</td>
	            	<td>
	            		<label><input type="checkbox" id='winPercentage' />% Win</label>
	            		<label><input type="checkbox" id='lossPercentage' />% Loss</label>
	            		<label><input type="checkbox" id='avgWin' />Avg. Win</label>
	            		<label><input type="checkbox" id='avgLoss' />Avg. Loss</label>
	            		<label><input type="checkbox" id='expectancy' />Expectancy</label>
	            	</td>
	            	<td>
	            		<label><input type="checkbox" id='peakGain' />Peak Gain</label> 	            		
	            		<label><input type="checkbox" id='maxDraw' />Max Drawdown</label>
	            		<label><input type="checkbox" id='efficiency' />Efficiency %</label>
	            		<label><input type="checkbox" id='avgDurWin' />Avg. Duration in Win</label>
	            		<label><input type="checkbox" id='avgDurLoss' />Avg. Duration in Loss</label>
	            	</td>
	            	<td>
	            		<label><input type="checkbox" id='fullstopPercentage' />% Full Stop</label>
	            		<label><input type="checkbox" id='bsoPercentage' />% BSO</label>
	            		<label><input type="checkbox" id='tradeGrade' />Trade Grade</label>
	            		<label><input type="checkbox" id='tradeError' />Trade Error</label>
	            		<label><input type="checkbox" id='tradeType' />Trade Type</label>
	            	</td>
            	</tr>
            </table>

        </div> 
	</div>  
 
    <div class="hidden-toolbar" id="bug-report-toolbar">
    	<div class="settings-toolbar-top">
			Bug Report
        </div>
        <div class="settings-toolbar-bottom">
	        <div style="height: 100%; margin: 0.5em auto;">
            	<textarea id="bug-report-text" style="display: block; width: 90%; height: 100%; min-height: 3em; margin: 0 auto; resize: vertical; font-size: 1em; line-height: 1.2em;" placeholder="Please type in your bug report.  Do not use any html characters; only plain text."></textarea><br />
                <input type="button" id="bug-report-submit" value="Submit" /><input type="button" id="bug-report-cancel" value="Cancel" />
            </div>
        </div> 
	</div>  
</div>	
<div class="content">
    <div class="panel-container">
        <div class="split-pane fixed-top">
            <div class="split-pane-component" id="top-component">
                <div class="split-pane-inner">
                    <div class="split-pane-functional-window" id="summary-contracts-window">
                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Contract Summary Toolbar</div>
                        <div class="databar">
						<!-- content here -->

                        	<table border="0" width="100%" style="overflow: hidden;">
                            	<tr>
                                	<td style="vertical-align: top;">
                                    	<!-- Summary stats -->
                                        <table border="0" width="100%" class="display-table">
                                        	<thead>
                                            	<th colspan="12">Summary Status</th>
                                            </thead>
                                            <tbody>
                                        	<tr>
                                            	<td># of Days Traded</td>
                                                <td id="days-traded">n.a.</td>
                                                <td>Trades Completed</td>
                                                <td id="completed-trades">n.a.</td>
                                                <td>Avg. Trades / Day</td>
                                                <td id="avg-trades-per-day">n.a.</td>
                                                <td>Contract Count</td>
                                                <td id="contract-count">n.a.</td>
                                                <td>Profit Factor</td>
                                                <td id="profit-factor">n.a.</td>
                                                <td>Total PnL</td>
                                                <td id="total-pnl">n.a.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%">
                                        	<tbody>
                                        	<tr>
                                            	<td><div id="percent-win-dial" style="display: inline-block; width: 160px; height: 100px; margin: 0;"></div></td>
                                                <td><div id="percent-loss-dial" style="display: inline-block; width: 160px; height: 100px; margin: 0;"></div></td>
                                                <td><div id="percent-bso-dial" style="display: inline-block; width: 160px; height: 100px; margin: 0;"></div></td>
                                                <td><div id="expected-value-dial" style="display: inline-block; width: 160px; height: 100px; margin: 0;"></div></td>
                                                <td><div id="netto-number-dial" style="display: inline-block; width: 160px; height: 100px; margin: 0;"></div></td>                                                                                                
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="vertical-align: top;">
                                    	<!-- Risk Units -->
                                    	<table border="0" width="100%" class="display-table risk-table" style="margin-bottom: 4px;">
                                        	<thead>
                                            	<th colspan="2">Unit of Risk Input</th>
                                            </thead>
                                            <tbody>
                                            	<tr>
                                                	<td id="daily-loss-limit">Risk Budget for Period:</td>
                                                    <td id="daily-loss-limit" style="text-align: right;">
                                                        <form id="daily-loss-limit-form" action="#" method="#">
                                                            <label id="daily-loss-limit">$ </label><input id="daily-loss-limit" value="2000" style="width: 65px; text-align: right; height: 15px;" />
                                                        </form>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    	<table border="0" width="100%" class="display-table risk-table">
                                        	<thead>
                                            	<th colspan="2">Captured Data for Selected Parameters</th>
                                            </thead>
                                            <tbody>
                                            	<tr>
                                                	<td>Date Range</td>
                                                    <td style="text-align: right;" id="date-range">n.a.</td>
                                                </tr>
                                                <tr>
                                                    <td>Products</td>
                                                    <td style="text-align: right;" id="products-traded">n.a.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="split-pane-divider" id="horizontal-divider"></div>
            <div class="split-pane-component" id="bottom-component">
                <!-- bottom part -->
                <div class="split-pane fixed-top">
                    <div class="split-pane-component" id="top-component2">
                        <div id="split-pane-1" class="split-pane fixed-left">
                            <div class="split-pane-component" id="left-component">
                                <div class="split-pane-inner">
                                    <div class="split-pane-functional-window" id="stats-window">
                                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Stats Summary for Period</div>
                                        <div class="databar">
                                        <!-- content here -->
                                        	<table border="0" cellpadding="0" cellspacing="" width="100%">
                                            	<tr>
                                                	<td width="50%">
                                                    	<!-- General Stats for Long Positions -->
                                                         <table border="0" cellpadding="0" cellspacing="0" width="99%" class="display-table stats-table">
                                                         	<thead>
                                                            	<th colspan="2">Stats for Longs</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Percent Long</td>
                                                                    <td class="stat" id="long-percent">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average Long Position</td>
                                                                    <td class="stat" id="avg-long-position">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Long Position</td>
                                                                    <td class="stat" id="max-long-position">n.a.</td>
                                                                </tr>
															</tbody>                                                            
                                                         </table>
                                                    </td>
                                                    <td width="50%">
	                                                    <!-- General Stats for Short Positions -->
                                                         <table border="0" cellpadding="0" cellspacing="0" width="99%" class="display-table stats-table">
                                                         	<thead>
                                                            	<th colspan="2">Stats for Shorts</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Percent Short</td>
                                                                    <td class="stat" id="short-percent">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average Short Position</td>
                                                                    <td class="stat" id="avg-short-position">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Short Position</td>
                                                                    <td class="stat" id="max-short-position">n.a.</td>
                                                                </tr>
															</tbody>                                                            
                                                         </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td width="50%">
	                                                    <!-- Favorable Stats -->
                                                         <table border="0" cellpadding="0" cellspacing="0" width="99%" class="display-table stats-table">
                                                         	<thead>
                                                            	<th colspan="2">Favorable Stats</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Average Gain</td>
                                                                    <td class="stat" id="average-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Gain</td>
                                                                    <td class="stat" id="max-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average TiT Gain</td>
                                                                    <td class="stat" id="avg-tit-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max TiT Gain</td>
                                                                    <td class="stat" id="max-tit-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max MFE</td>
                                                                    <td class="stat" id="max-mfe">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average MFE</td>
                                                                    <td class="stat" id="avg-mfe">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Peak Running Gain</td>
                                                                    <td class="stat" id="peak-running-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gross Gain</td>
                                                                    <td class="stat" id="gross-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gain Available</small></td>
                                                                    <td class="stat" id="gain-available">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Trader Gain Efficiency</td>
                                                                    <td class="stat" id="gain-efficiancy">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Consecutive Gains</td>
                                                                    <td class="stat" id="max-consecutive-gain">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max BSO</td>
                                                                    <td class="stat" id="max-bso">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average BSO</td>
                                                                    <td class="stat" id="avg-bso">n.a.</td>
                                                                </tr>
															</tbody>                                                            
                                                         </table>
                                                    
                                                    </td>
                                                    <td width="50%">
	                                                    <!-- Favorable Stats -->
                                                         <table border="0" cellpadding="0" cellspacing="0" width="99%" class="display-table stats-table">
                                                         	<thead>
                                                            	<th colspan="2">Adverse Stats</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Average Loss</td>
                                                                    <td class="stat" id="average-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Loss</td>
                                                                    <td class="stat" id="max-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average TiT Loss</td>
                                                                    <td class="stat" id="avg-tit-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max TiT Loss</td>
                                                                    <td class="stat" id="max-tit-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max MAE</td>
                                                                    <td class="stat" id="max-mae">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Average MAE</td>
                                                                    <td class="stat" id="avg-mae">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Running Drawdown</td>
                                                                    <td class="stat" id="max-running-drawdown">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gross Losses</td>
                                                                    <td class="stat" id="gross-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Loss Available</td>
                                                                    <td class="stat" id="draw-available">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Trader Loss Drag</td>
                                                                    <td class="stat" id="draw-drag">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Consecutive Losses</td>
                                                                    <td class="stat" id="max-consecutive-draw">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max Products Traded Per Day</td>
                                                                    <td class="stat" id="max-products-traded">n.a.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Most Traded Products</td>
                                                                    <td class="stat" id="most-traded-products">n.a.</td>
                                                                </tr>
															</tbody>                                                            
                                                         </table>
                                                    
                                                    </td>
                                                </tr>
                                             </table>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="split-pane-divider" id="vertical-divider"></div>
								<div class="split-pane-component" id="right-component">
                                <div class="split-pane-inner">
                                    <div class="split-pane-functional-window" id="charts-window">
                                        <div class="titlebar"><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Charts Toolbar</div>
                                        <div class="databar">
                                        <!-- content here -->
                                            <div class="chart" id="expectancy"></div>
                                            <div class="small-chart" id="trade-count"></div><div class="small-chart" id="hourly-performance"></div><div class="small-chart" id="dayofweek-performance"></div>
                                            <div class="chart" id="trade-distribution"></div>
                                            <div class="chart" id="daily-pnl"></div>
                                            <div class="chart" id="equity-curve"></div>
											<div class="chart" id="win-loss-percentage"></div>
											<div class="chart" id="bso-attained"></div>
											<div class="chart" id="mfe-mae"></div>                                                                                                                                  
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="split-pane-divider" id="horizontal-divider2"></div>
                    <div class="split-pane-component" id="bottom-component2">
             	       <div class="split-pane-inner">
                            <div class="split-pane-functional-window" id="trades-window" style="overflow-y: scroll;">
                                <div class="titlebar"><span><img class='s5-watermark' src='./images/S5TA_Small_Logo20px.png' />Completed Trades</span>
                                 <small> (Shown in Device Time)</small></span><a id="export-trades-to-csv">Export Trades to CSV</a><a id="load-user-trade-metrics">Reload User Metrics</a></div>
                                <div class="databar" style="overflow: hidden;">
                                <!-- content here -->
							    	<div class="container_12" style="display: block; width: 100%;">
							    		<div class="grid_12" id="completed-trades-table" style="height: 260px;">
							    		</div>
							    	</div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>       
            </div>
        </div>    
    </div>
</div>
</body>
</html>
