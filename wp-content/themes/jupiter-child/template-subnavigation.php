<?php
/**
 * Template Name: Internal Page with Menu
 */

get_header();

mk_get_view('header', 'wp-toolbar', false);

wp_nav_menu(['theme_location' => 'third-menu']);
?>

<div class="internal-page-wrapper<?= is_home() ? ' home' : '' ?>">
    <?php 

    mk_build_main_wrapper(mk_get_view('singular', 'wp-page-internal', true));

    mk_get_view('singular', 'wp-page-footer');

    ?>
</div>

<?php

get_footer();
