<?php if ( ! defined( 'ABSPATH' ) || ! class_exists( 'NF_Abstracts_Action' )) exit;

/**
 * Class NF_MailChimp_Actions_MailChimp
 */
final class NF_MailChimp_Actions_MailChimp extends NF_Abstracts_ActionNewsletter
{
    /**
     * @var string
     */
    protected $_name  = 'mailchimp';

    /**
     * @var array
     */
    protected $_tags = array();

    /**
     * @var string
     */
    protected $_timing = 'normal';

    /**
     * @var int
     */
    protected $_priority = '10';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_nicename = __( 'Mail Chimp', 'ninja-forms' );

        $settings = NF_MailChimp()->config( 'ActionMailChimpSettings' );
        $this->_settings =  array_merge( $this->_settings, $settings );
    }

    /*
    * PUBLIC METHODS
    */

    public function save( $action_settings )
    {

    }

    public function process( $action_settings, $form_id, $data )
    {
        if( ! $this->is_opt_in( $data ) ) return $data;

        $list = $action_settings[ 'newsletter_list' ];
        $double_opt_in = $action_settings[ 'double_opt_in' ];

        $merge_vars = $this->get_merge_vars( $action_settings, $list );

        $data[ 'actions' ][ 'mailchimp' ][ 'list' ] = $list;
        $data[ 'actions' ][ 'mailchimp' ][ 'settings' ] = $action_settings;
        $data[ 'actions' ][ 'mailchimp' ][ 'merge_vars' ] = $merge_vars;

        $response = NF_MailChimp()->subscribe( $list, $merge_vars, $double_opt_in );

        if( $response && ! isset( $response[ 'error' ] ) ){
            $data[ 'extra' ][ 'mailchimp_list' ] = $list;
            $data[ 'extra' ][ 'mailchimp_euid' ]  = $response[ 'euid' ];
            $data[ 'extra' ][ 'mailchimp_leid' ]  = $response[ 'leid' ];
            $data[ 'extra' ][ 'mailchimp_email' ] = $response[ 'email' ];
            $data[ 'extra' ][ 'mailchimp_merge_vars' ] = $merge_vars;
        }

        $data[ 'actions' ][ 'mailchimp' ][ 'subscribe' ] = $response;

        return $data;
    }

    protected function is_opt_in( $data )
    {
        $opt_in = TRUE;
        foreach( $data[ 'fields' ]as $field ){

            if( 'mailchimp-optin' != $field[ 'type' ] ) continue;

            if( ! $field[ 'value' ] ) $opt_in = FALSE;
        }
        return $opt_in;
    }

    protected function get_merge_vars( $action_settings, $list_id )
    {
        $merge_vars = array();
        foreach( $action_settings as $key => $value ){

            if( FALSE === strpos( $key, $list_id ) ) continue;

            $field = str_replace( $list_id . '_', '', $key );

            if( FALSE !== strpos( $key, 'group_' ) ){

                $key = str_replace( $list_id . '_group_', '', $key );

                if( $value ) {
                    $group = explode('_', $key);
                    $merge_vars[ 'groupings' ][ $group[ 0 ] ][ 'id' ] = $group[ 0 ];
                    $merge_vars[ 'groupings' ][ $group[ 0 ] ][ 'groups' ][] = $group[ 1 ];
                }
            } else {
                $merge_vars[ $field ] = $value;
            }
        }
        return $merge_vars;
    }

    protected function get_lists()
    {
        return NF_MailChimp()->get_lists();
    }
}
