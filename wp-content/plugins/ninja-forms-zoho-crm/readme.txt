=== Ninja Forms - Zoho CRM ===


== Changelog ==

= 1.7.1= 
2015.09.16
Change field map value for Contact "Mailing" and "Other" Zip Code


= 1.7 =
2015.04.22
Add custom field mapping for Contact Module

= 1.6.1 =
2014.11.20
Add mapping capability to more NF fields
Change custom field preg_replace to esc_html
 - allows for special characters in custom fields

= 1.5.1 =
Escape attributes for form submission data; prevents error when certain characters are included

= 1.5 =
2014.08.06
New Features:
Add modules for Accounts, Contacts, Potentials, Tasks, and Notes
Add parameters for Approvals and Workflow Triggers
Tweaks:
Convert to JSON for response handling and OOP for communication

= 1.4.1 =
2014.06.24 Escape attributes for pick list options to prevent comm failure when selections include special characters

= 1.4 =
2014.06.03 Add raw communication data to settings page to enable faster customer support

= 1.3.4 =
2014.05.19 Modify zoho crm scope to remove key from value

= 1.3.3 =

Update tags

= 1.3.2 =
Custom mapped field to allow dash 

= 1.3.1 =
Use User Analytics' field grabbing function created by Patrick Rauland instead of custom function originally used


= 1.3 = 
Add Custom Field Mapping

--data-processing.php
Add Custom Field Mapping option

Modify Annual Revenue validation to remove all non-numeric and conver to integer

Remove function that turns on error log writing

--field-registration.php
modify field mapping function to make it extendable by other plugins with custom fields
add custom mapping field



= 1.2 =
Initial communication with User Analytics

Add User Analytics fields 'country' 'region' 'postal_code' 'city' as
mappable fields

In field-registration.php, change mapped field values for dividers from "none" to "divider" and
add code to reset to "none" before processing. The resetting prevents
unwanted fields from being mapped.

In data-processing.php, add secondary check during mapping to ensure divider
fields don't get mapped


= 1.1 =
Modify log data status and debug information based on recent learning from Elicere support ticket

Add preg_replace code for Annual Revenue field to prevent comm failure - Zoho requires integer for Annual Revenue

Format documentation with html headers
Modify instructions for end user documentation


= 1.0 =
Changed the name of the plugin to fit in with the Ninja Forms standard: Ninja Forms - Zoho CRM.
Fixed the version number constant.
Changed the licensing option call to the correct format.
Add text-domain for all translations (ninja-forms-zoho-crm)
Add gettext function around 'Map this field to your Zoho CRM' - missing it prior
Remove .nff file from plugin - to be uploaded to NF site after approval
Remove authtoken test value from remarks
Add default .po and .mo to initiate translations