<?php
session_start();
require_once("db_connect.php");
require_once("PDFCreator.php");
header("Content-Type: application/pdf;");
if(isset($_SESSION['rooms']))
{
  foreach($_SESSION['rooms'] as $room)
  {
    $roomId = $mysql->real_escape_string($_GET['chatId']);
    $typeId = $mysql->real_escape_string($_GET['typeId']);
    if($room[0] == $roomId && (($query = $mysql->query("SELECT r.room,t.fileName,t.fromNow FROM reportusers r JOIN reportupdates t WHERE r.id=".$roomId." AND t.id=".$typeId)) && ($raw = $query->fetch_array())))
    {
      $room = $raw['room'];
      $roomName = sanitize($room, false);
      $fileName = $raw['fileName'];
      $fromNow = $raw['fromNow'];
      $esc_room = $mysql->real_escape_string($room);
      if(isset($_GET['search']))
      {
        $filePath = "updater_chatlogs/chatReports/" . $roomName . "/" . $roomName . "_custom.pdf";
        header("Content-disposition: ".(isset($_GET['dl']) ? 'attachment' : 'inline')."; filename=\"" . substr($filePath, strrpos($filePath, "/")+1) . "\"");

        $cond = composeCondition($_GET['search']);
        $lower_headline = "Filter applied: " . $_GET['search'];
        //echo $lower_headerline . "<br>";
        //echo $cond . "<br><br>";
        $zone = date_default_timezone_get();
        date_default_timezone_set('America/Chicago');
        $issued = "Issued " . date("Y-m-d \\a\\t H:i") . " (Central Time)";
        $earliestMoment = date("Y-m-d H:i:s", time() - $fromNow);
        date_default_timezone_set($zone);
        $messages = array();
        //echo $issued . "<br>";
        //echo $earliestMoment . "<br>";
        if($query = $mysql->query("SELECT * FROM messages WHERE `datetime`>'".$earliestMoment."' && room='".$esc_room."' && (" . $cond . ") ORDER BY `datetime`"))
        {
          while($raw = $query->fetch_array())
          {
            $messages[] = array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']);
            //print_r(array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']));
            //echo "<br><br>";
          }
        }
        //echo $mysql->error;
        //print_r($messages);

        $headline = $room . " - Custom logs for the past " . (($fromNow > 86400) ? (round($fromNow/60/60/24) . " days") : (round($fromNow/60/60) . " hours"));
        $pdf = createPDF($roomName . "_custom.pdf", $issued, $room, $headline, $messages, $lower_headline);
        $pdf->Output("php://output", "F");
      }
      else
      {
        //header("Content-Type: application/octet-stream");
        //header("Content-Transfer-Encoding: Binary");
        $filePath = "updater_chatlogs/chatReports/" . $roomName . "/" . $roomName . "_" . $fileName . ".pdf";
        header("Content-disposition: ".(isset($_GET['dl']) ? 'attachment' : 'inline')."; filename=\"" . substr($filePath, strrpos($filePath, "/")+1) . "\"");

        readfile($filePath);
        break;
      }
    }
  }
}
?>
