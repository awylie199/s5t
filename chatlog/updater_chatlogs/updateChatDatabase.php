<?php

error_reporting( E_ALL );
ini_set( 'display_errors', 'On' );
//require_once("../db_connect.php");
/*function blazerTime()
{
	echo date("Y-m-d H:i:s") . "<br>";
	$zone = date_default_timezone_get();
	date_default_timezone_set('America/New_York');
	echo date("Y-m-d H:i:s") . "<br>";
	$goodTime = time();
	date_default_timezone_set($zone);
	return $goodTime;
}*/

if(file_exists("lastUpdate.kmod") && !isset($_GET['forceUpdate']))
{
	$fileHandler = fopen("lastUpdate.kmod", "r");
	$lastUpdate = fread($fileHandler, filesize("lastUpdate.kmod"));
	fclose($fileHandler);
}
else
{
	//$lastUpdate = time()-2592000;
	$lastUpdate = time()-1036800; //last 12 days
	//$lastUpdate = time()-172800;
}


$now = time();
$CSV_str = "";
$zone = date_default_timezone_get();
date_default_timezone_set('America/New_York');
$params = array(
	"start_date" => date("Y-m-d H:i:s", $lastUpdate-300),
	"end_date" => date("Y-m-d H:i:s", $now+300)
);
date_default_timezone_set($zone);
$error = false;

$ch = curl_init("https://host5.chatblazer.com/sitelogCBS546.php");
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_POST, $params);

$fields_str = "";
foreach($params as $index=>$value)
{
	$fields_str .= $index.'='.$value.'&';
}
rtrim($fields_str, '&');
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_str);


$CSV_str = curl_exec($ch);
echo $CSV_str;
//echo $fields_str . "<br>";
//echo $CSV_str;
//echo $CSV_str;
if($errno = curl_errno($ch)) {
$error_message = curl_strerror($errno);
echo "cURL error ({$errno}):\n {$error_message}";
}
if(strcasecmp($CSV_str, "Missing parameters") == 0 || strcasecmp($CSV_str, "Not allowed") == 0)
{
	echo "error";
	$error = true;
}

if(!$error)
{
	$fileHandler = fopen("output.kmod", "w");
	fwrite($fileHandler, "");
	fclose($fileHandler);
	chmod("output.kmod", 0700);
	$fileHandler = fopen("output.kmod", "w");
	$lastUpdate = fwrite($fileHandler, $CSV_str);
	fclose($fileHandler);

	$messageIds = array();
	if($query = $mysql->query("SELECT messageId FROM messages"))
	{
		while($message = $query->fetch_array())
		{
			$messageIds[] = $message['messageId'];
		}
	}
	else
	{
		$error = true;
	}
	//$error = true;
	if(!$error)
	{
		$addedMessages = 0;

		$fileHandler = fopen("output.kmod", "r");
		$first = true;
		while($row = fgetcsv($fileHandler))
		{
			if($first)
			{
				$first = false;
			}
			else
			{
				$messageId = $mysql->real_escape_string($row[0]);
				$messageType = $mysql->real_escape_string($row[1]);


				$msgDatetime = $mysql->real_escape_string($row[2]);
				$msgTime = strtotime($msgDatetime)-3600;
				$chicargoMsgDatetime = date("Y-m-d H:i:s", $msgTime);


				$room = $mysql->real_escape_string($row[3]);
				$sender = $mysql->real_escape_string($row[4]);
				$alias = $mysql->real_escape_string($row[5]);
				$recipient = $mysql->real_escape_string($row[6]);
				$msgidx = $mysql->real_escape_string($row[7]);
				$message = stripChat($mysql->real_escape_string($row[8]));
				if(!in_array($messageId, $messageIds) && strlen($room) != 0  && stripos($room, "test") !== 0 && stripos($room, "pc") !== 0)
				{
					$addedMessages++;
					if(!$mysql->query("INSERT INTO messages (messageId, messageType, datetime, room, sender, alias, recipient, msgidx, message) VALUES
						('".$messageId."', '".$messageType."', '".$chicargoMsgDatetime."', '".$room."', '".$sender."', '".$alias."', '".$recipient."', '".$msgidx."', '".$message."')"))
					{
						echo "error</br>";
					}
				}
			}
		}
		fclose($fileHandler);
		unlink("output.kmod");
		$fileHandler = fopen("lastUpdate.kmod", "w");
		$lastUpdate = fwrite($fileHandler, $now);
		fclose($fileHandler);
	}
}

function stripChat($str)
{
	if(strpos($str, "[") !== FALSE && strpos($str, "]") !== FALSE && strpos($str, "[/") !== FALSE)
	{
		return $str = preg_replace("/(\[)[^]]+\]/", "", $str);
	}
	return $str;
}
?>
