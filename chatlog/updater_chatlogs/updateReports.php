<?php
ini_set('memory_limit','312M');
require_once("../fpdf/fpdf.php");
require_once("../db_connect.php");
error_reporting(E_ALL);
ini_set("display_errors", 1);
set_time_limit(480);
$days = array();
$query = $mysql->query("SELECT * FROM reportupdates");

$forceUpdate = false;
if(isset($_GET['forceUpdate']) && $_GET['forceUpdate'] == "yes")
{
	echo "Forcing update<br>";
	$forceUpdate = true;
}
/*if(!isset($_GET['test']))
{
	exit();
}*/

while($day = $query->fetch_array())
{
	$lastReportUpdate = 0;
	$fileName = "lastUpdate_" . $day['fileName'] . ".kmod";
	if(file_exists($fileName) && !$forceUpdate)
	{
		$fileHandler = fopen($fileName, "r");
		$lastReportUpdate = fread($fileHandler, filesize($fileName));
		fclose($fileHandler);
	}
	else
	{
		$lastReportUpdate = 0;
	}
	//echo $day['updateInterval'] . " : " . date("r", $lastReportUpdate) . "<br>";
	if((time()-$lastReportUpdate) > $day['updateInterval'])
	{
		$days[] = array("fileName"=>$day['fileName'], "fromNow"=>$day['fromNow'], "updateInterval"=>$day['updateInterval']);
	}
}
$rooms = array();
if(count($days) > 0)
{
	include "updateChatDatabase.php";
	$query = $mysql->query("SELECT DISTINCT room FROM messages");
	while($room = $query->fetch_array())
	{
		$rooms[] = $room['room'];
	}
}
$reportUsers = array();
if($query = $mysql->query("SELECT paths, room FROM reportusers"))
{
	while($reportUser = $query->fetch_array())
	{
		foreach(unserialize($reportUser['paths']) as $path)
		{
			$reportUsers[$reportUser['room']][] = $path;
		}
	}
}

function chicargoTime()
{
	$zone = date_default_timezone_get();
	date_default_timezone_set('America/Chicago');
	$goodTime = time();
	date_default_timezone_set($zone);
	return $goodTime;
}
//print_r($rooms);
foreach($rooms as $room)
{
	$messages = array();
	if($query = $mysql->query("SELECT * FROM messages WHERE room='".$room."' AND messageType != 'WC' AND datetime>'".date("Y-m-d H:i:s", (time()-2592000))."' AND messageType != 'PC' AND included = 1 GROUP BY messageId ORDER BY datetime"))
	{

		while($message = $query->fetch_array())
		{
			$messages[] = $message;
		}
		$query->close();
		unset($message);
		unset($query);
	}
	else
	{
		echo $mysql->error;
	}
	//echo $room . "<br>";
	foreach($days as $day)
	{
		$dayTime = time()-$day['fromNow'];
		$fromDate = date("Y-m-d H:i:s", $dayTime);
		$zone = date_default_timezone_get();
		$newRoom = sanitize($room, false);
		date_default_timezone_set('America/Chicago');
		$dayTimeStr = date("Y-m-d H:i:s", $dayTime);
		$issued = "Issued " . date("Y-m-d \\a\\t H:i") . " (Central Time)";
		date_default_timezone_set($zone);
		$fontName = "Arial";
		$pdf = new FPDF();
		$pdf->SetTitle($newRoom . "_" . $day['fileName']);
		$pdf->SetAuthor("Stage5Trading");
		$pdf->SetCreator("Stage5Trading");
		$pdf->SetAutoPageBreak(true, 0.2);
		$pdf->AddPage("P", "Letter");

		// Placing watermark
		$pdf->SetTextColor(228, 228, 228);
		$pdf->setY(120);
		$pdf->SetFont($fontName,'B',40);
		/*$pdf->Rotate(45, 100, 120);
		$pdf->Cell(0,20,"Stage5 Member Use Only", 0, 2, 'C');
		$pdf->Rotate(0);*/
		$pdf->Image("watermark.png", 20, 70, 180);
		$pdf->setY(0);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetFont($fontName,'B',8);
		$pdf->Cell(0,4,"Stage5 Chat Log", 0, 2, 'C');
		$pdf->setY(0);
		$pdf->SetFont($fontName,'i',8);
		$pdf->Cell(0,4,$issued, 0, 2, 'R');
		$pdf->Image("logo.png", 75, 5, 65);
		$pdf->Cell(0,20,"", 0, 2, 'C');
		$pdf->SetFont($fontName,'B',16);
		$str = $room . " - Logs for the past " . (($day['fromNow'] > 86400) ? (round($day['fromNow']/60/60/24) . " days") : (round($day['fromNow']/60/60) . " hours"));
		//$pdf->SetTextColor(0, 51, 132);
		$pdf->Cell(0, 8, $str, 0, 1, 'C');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 2, "", 0, 1, '');
		$pdf->SetFont($fontName,'',10);
		$note = "Note: Trading Futures and other instruments involves substantial risk of loss and is not suitable for all investors.
			You may lose all or more of your initial investment. Information shared here is for educational purposes only.";
		$pdf->MultiCell(0,4, $note, 0, 1, '');
		$pdf->Cell(0, 2, "", 0, 1, '');
		$pdf->SetFont($fontName,'',10);
		$pdf->SetFillColor(192, 192, 192);
		$pdf->Cell(25,5,"Date", 1, 0, 'C', true);
		$pdf->Cell(22,5,"Time(CT)", 1, 0, 'C', true);
		$pdf->Cell(40,5,"From", 1, 0, 'C', true);
		$pdf->Cell(110,5,"Message", 1, 1, 'C', true);
		//$pdf->Cell(0,8,"From " . $fromDate . " to " . date("Y-m-d H:i:s"), 0, 2, 'C');
		$pdf->SetFont($fontName,'',10);
		$pdf->SetTextColor(110, 110, 110);

		$messageCount = 0;
		$hasMessage = false;
		foreach($messages as $message)
		{
			if(strtotime($message['datetime']) > strtotime($dayTimeStr))
			{
				$hasMessage = true;
				$pdf->SetTextColor(110, 110, 110);
				if($pdf->getY() > 240)
				{
					$pdf->SetY(268);
					$pdf->SetFont($fontName,'I',8);
					$pdf->Cell(0,10,'Page '.$pdf->PageNo(),0,1,'C');
					$pdf->AddPage("P", "Letter");

					// Placing watermark
					$pdf->SetTextColor(228, 228, 228);
					$pdf->setY(120);
					$pdf->SetFont($fontName,'B',40);
					/*$pdf->Rotate(45, 100, 120);
					$pdf->Cell(0,20,"Stage5 Member Use Only", 0, 2, 'C');
					$pdf->Rotate(0);*/
					$pdf->Image("watermark.png", 20, 70, 180);
					$pdf->setY(10);
					$pdf->SetTextColor(0, 0, 0);
				}
				$pdf->SetTextColor(110, 110, 110);
				$pdf->SetFont($fontName,'',10);
				if(strpos($message['sender'], "S5_") !== FALSE)
				{
					$pdf->SetFont($fontName,'B',11);
					$pdf->SetTextColor(0, 0, 0);
				}
				$theDateTime = explode(" ", $message['datetime']);
				$pdf->Cell(25,5, $theDateTime[0], 1, 0, '');
				$pdf->Cell(22,5, $theDateTime[1], 1, 0, '');
				$pdf->Cell(40,5,$message['sender'], 1, 0, '');
				$msgTemp = $message['message'];
				/*if(FALSE && strpos($msgTemp, "data:image/") !== FALSE)
				{
					$imgStr = substr($msgTemp, strpos($msgTemp, "data:image/"));
					if(strpos($imgStr, " ") !== FALSE)
					{
						$imgStr = substr($imgStr, 0, strpos($imgStr, " "));
					}
					$imgExt = substr($imgStr, strpos($imgStr, "/")+1);
					$imgExt = substr($imgExt, 0, strpos($imgExt, ";"));
					//echo $imgStr;
					$imgPath = 'tmp/' . md5($imgStr) . '.' . $imgExt;
					$stream = @fopen( $imgPath, 'w' );
					fwrite($stream, base64_decode($imgStr));
					fclose($stream);

					$pdf->Image($imgPath, 0, 0, 110);
					//$msgTemp = substr($msgTemp, 0, strpos($msgTemp, "data:image/"));
				}
				else {*/
					$pdf->MultiCell(110,5,$message['message'], 1, 1, '');
				//}




				/*data:image/jpeg;base64,
				$pdf->Cell(200,5, "", 0, 1, false);
				$pdf->SetTextColor(204, 0, 102);
				$pdf->MultiCell(200,5, $message['sender'] . " [" . $message['datetime'] . "]:", 0, 1, false);
				$pdf->SetTextColor(100, 100, 100);
				$pdf->MultiCell(200,5, $message['message'], 0, 1, false);*/
				$messageCount++;
			}
		}

		if(!file_exists("chatReports"))
		{
			mkdir("chatReports");
		}
		$folderName = "chatReports/" . $newRoom;
		if(!file_exists($folderName))
		{
			mkdir($folderName);
		}
		$fileName = $folderName . "/" . $newRoom . "_" . $day['fileName'] . ".pdf";
		if(!file_exists($fileName))
		{
			$fileHandler = fopen($fileName, "w");
			fclose($fileHandler);
		}
		chmod ($fileName , 0700);
		if(strlen($room) == 0)
		{
			continue;
		}
		if(!isset($reportUsers[$room]))
		{
			$username = $newRoom;
			$accessKey = sha1($room . $day['fileName'] . time());
			$paths = array($fileName);
			$mysql->query("INSERT INTO reportusers (username, accessKey, room, paths) VALUES ('".$username."', '".$accessKey."', '".$room."', '".serialize($paths)."')");
			$reportUsers[$room] = $paths;
		}
		else
		{
			$query = $mysql->query("SELECT paths FROM reportusers WHERE room='".$room."'");
			$paths = unserialize($query->fetch_array()['paths']);
			if(!in_array($fileName, $paths))
			{
				$paths[] = $fileName;
				$mysql->query("UPDATE reportusers SET paths='".serialize($paths)."' WHERE room='".$room."'");
			}
		}

		$pdf->Output($fileName, "F");
		if($hasMessage)
		{
			//$pdf->Output($fileName, "F");
			//echo "updating: " . $fileName . "<br>";
		}
		else if(file_exists($fileName))
		{
			//echo "deleting: " . $fileName . " - " . filesize($fileName) . "<br>";
			//unlink($fileName);
		}
		else {
			//echo "Do nothing: " . $fileName . "<br>";
		}
		//$pdf->Output();
		//break;
	}
}

foreach($days as $day)
{
	$fileName = "lastUpdate_" . $day['fileName'] . ".kmod";
	$fileHandler = fopen($fileName, "w");
	fwrite($fileHandler, time());
	fclose($fileHandler);
}

if($forceUpdate)
{
	echo "<center><h4>Update completed</h4></center>";
}
?>
