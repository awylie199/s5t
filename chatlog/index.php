<?php
session_start();
require_once("db_connect.php");
$helpDiv = "
  <b>Available search operators: </b><br>
  OR, AND, NOT, -, (), “”, user:, msg:<br><br>
  <b>Example 1:</b>
  <i>volume point of control</i><br>
  This will return messages where all of the words are present in the username or message body in any order.<br><br>
  <b>Example 2:</b>
  <i>volume OR point OR control</i><br>
  This will return messages where any of the words are present in the username or message body in any order.<br><br>
  <b>Example 3:</b>
  <i>S5_Morad AND “opening swing”</i><br>
  This will return any message with “S5_Morad” in the username or message body and the phrase “opening swing”.<br><br>
  <b>Example 4:</b>
  <i>user:Yoda AND msg:profile</i><br>
  This will return messages by any user with “Yoda” in their username, with the word “profile” in the message body.";

$helpText_logRange = "
  Shortly after you've subscribed to a new log, you will receive the last complete log.<br>
  You'll then be emailed the next log on the day following the last completed period subscribed to, at 3am GMT. E.g. Sunday 3am GMT for a Weekly log.";

?>

<style>
input[type=checkbox].w3-check:checked+.w3-validate,
input[type=radio].w3-radio:checked+.w3-validate {
    color: #89B25B !important;
}

.filterHelpContainer {
  position: relative;
  width: 100%;
}

.filterHelpDiv {
  position:absolute;

  transform: translate(0px,-110%);;
  -webkit-transform: translate(0px,-110%); /** Chrome & Safari **/
  -o-transform: translate(0px,-110%);; /** Opera **/
  -moz-transform: translate(0px,-110%);; /** Firefox **/

  padding: 10px;
  width: 100%;
  min-height: 100px;
  background: #385577;
  display: none;
  -webkit-box-shadow: 0px 6px 16px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 6px 16px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 6px 16px 0px rgba(0,0,0,0.75);
  color: white;
}

.filterHelpIcon {
  font-size: 28px !important;
  margin-top: -40px;
  float: right;
}

#tooltipDiv_logRange {
  transform: translate(0px,-132%);;
  -webkit-transform: translate(0px,-132%); /** Chrome & Safari **/
  -o-transform: translate(0px,-132%);; /** Opera **/
  -moz-transform: translate(0px,-132%);; /** Firefox **/
}

#tooltip_logRange:hover ~ #tooltipDiv_logRange {
  display: initial;
}

#filterHelpIconOne:hover ~ #filterHelpDivOne {
  display: initial;
}

#filterHelpIconTwo:hover ~ #filterHelpDivTwo {
  display: initial;
}

#filterHelpIconThree:hover ~ #filterHelpDivThree {
  display: initial;
}

</style>
<!DOCTYPE html>
<html>
  <head>
    <title>Stage5 Chat Logger</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="W3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  </head>
  <body style="background: url('images/background.png') no-repeat center center fixed; -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">

    <div id="test" class="w3-card-4 w3-white" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:50%;max-width:450px; min-width:300px">
      <div class="w3-center w3-padding-8">
        <img src="images/s5logo.png" alt="stage5" style="width:80%;">
      </div>

      <hr>

      <div class="w3-center">
        <img src="images/chat.svg" alt="stage5" style="width:25%;">
      </div>

      <iframe src="dummy.html" name="dummy" style="display: none"></iframe>
      <form class="w3-container" onsubmit="getReports(event)" action="" target="dummy">
        <br>
        <div id="notice" class="w3-panel w3-pale-green w3-leftbar w3-border-green">
          <p>
            Please input an access key to download the logs
          </p>
        </div>
        <input id="accessKey" class="w3-input w3-border w3-animate-input" type="password" style="width:50%; min-width: 160px;" placeholder="Type your access key..."><br>
        <input type="submit" class="w3-btn-block w3-blue w3-large" style="margin: 0px -16px -16px -16px; width: calc(100% + 32px);" value="Login">
      </form>

    </div>

    <div id="id01" class="w3-modal">
      <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:500px">

        <div class="w3-center w3-padding-8"><br>
          <span onclick="logout()" class="w3-closebtn w3-hover-red w3-container w3-padding-8 w3-display-topright" title="Logout"><i class="fa fa-close w3-xxlarge"></i></span>
          <img src="images/chatlog_logo_dialog.jpg" style="width:80%" class="w3-margin-top">
        </div>

        <form class="w3-container" onsubmit="openReport(event)" autocomplete="off">
          <div class="w3-section">
            <h4>Report type:</h4>
            <input id="actionType1" class="w3-radio" type="radio" onchange="changeView(event)" name="actionType" value="actionTypeDiv1" checked>
            <label class="w3-validate" for="actionType1" >View</label>&nbsp;&nbsp;
            <input id="actionType2" class="w3-radio" type="radio" onchange="changeView(event)" name="actionType" value="actionTypeDiv2">
            <label class="w3-validate" for="actionType2" >Download</label>&nbsp;&nbsp;
            <input id="actionType3" class="w3-radio" type="radio" onchange="changeView(event)" name="actionType" value="actionTypeDiv3">
            <label class="w3-validate" for="actionType3" >Subscribe</label>&nbsp;&nbsp;
            <h4>Log range:</h4>
            <div id="tooltop_logRangeContainer" class="filterHelpContainer" style="display: none;">
              <i id="tooltip_logRange" class="fa fa-question-circle filterHelpIcon" ></i>
              <div id="tooltipDiv_logRange" class="filterHelpDiv"><?php echo $helpText_logRange; ?></div>
            </div>
            <select id="typeSelect" class="w3-select w3-border" name="option" required>
              <?php
              if($query = $mysql->query("SELECT id,name FROM reportupdates"))
              {
                while($raw = $query->fetch_array())
                {
                  echo "<option value='".$raw['id']."'>".$raw['name']."</option>";
                }
              }
              ?>
            </select>
            <div id="actionTypeDiv1">
              <h4>View log for:</h4>
              <select id="chatSelect" class="w3-select w3-border"></select>
              <br>
              <h4>Filter by keyword or username:</h4>
              <div class="filterHelpContainer">
                <i id="filterHelpIconOne" class="fa fa-question-circle filterHelpIcon" ></i>
                <div id="filterHelpDivOne" class="filterHelpDiv"><?php echo $helpDiv; ?></div>
              </div>
              <input id="search" type="text" class="w3-input"  placeholder="Keyword or Username">
              <button class="w3-btn-block w3-section w3-padding" style="background-color: #89B25B">View log</button>
            </div>
            <div id="actionTypeDiv2" style="display: none;">
              <h4>Download log(s) for:</h4>
              <div id="chatDiv"></div>
              <h4>Filter by keyword or username:</h4>
              <div class="filterHelpContainer">
                <i id="filterHelpIconTwo" class="fa fa-question-circle filterHelpIcon" ></i>
                <div id="filterHelpDivTwo" class="filterHelpDiv"><?php echo $helpDiv; ?></div>
              </div>
              <input id="searchTwo" type="text" class="w3-input"  placeholder="Keyword or Username">
              <br>
              <button class="w3-btn-block w3-section w3-padding" style="background-color: #89B25B" >Download log(s)</button>
            </div>
            <div id="actionTypeDiv3" style="display: none;">
              <h4>Subscribe to log(s) for:</h4>
              <div id="chatDivTwo"></div>
              <br>
              <h4>Filter by keyword or username:</h4>
              <div class="filterHelpContainer">
                <i id="filterHelpIconThree" class="fa fa-question-circle filterHelpIcon" ></i>
                <div id="filterHelpDivThree" class="filterHelpDiv"><?php echo $helpDiv; ?></div>
              </div>
              <input id="searchThree" type="text" class="w3-input"  placeholder="Keyword or Username">
              <input id="email" type="email" class="w3-input" placeholder="E-mail address" style="margin-top: 10px;">
              <button class="w3-btn-block w3-section w3-padding" style="background-color: #89B25B">Subscribe now</button>
              <div id="msg" style="margin: -10px 0px; text-align: center;"></div>
            </div>
          </div>
        </form>

        <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
          <button onclick="logout()" type="button" class="w3-btn w3-red">Logout</button>
          <span class="w3-right w3-padding w3-hide-small"><a target="_blank" href="http://www.stage5trading.com">www.stage5trading.com</a></span>
        </div>

      </div>
    </div>

  </body>
</html>

<script>

function logout(e)
{
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'logout.php');
  xhr.send();
  document.getElementById('id01').style.display='none';
}

function changeView(e)
{
  var email = document.getElementById("email");
  var tooltop_logRangeContainer = document.getElementById("tooltop_logRangeContainer");

  document.getElementById("actionTypeDiv1").style.display = "none";
  document.getElementById("actionTypeDiv2").style.display = "none";
  document.getElementById("actionTypeDiv3").style.display = "none";
  tooltop_logRangeContainer.style.display = "none";
  email.removeAttribute("required");

  document.getElementById(e.target.value).style.display = "";
  if("actionTypeDiv3" == e.target.value)
  {
    tooltop_logRangeContainer.style.display = "";
    email.setAttribute("required", "");
  }
}

function getReports(e)
{
  //e.preventDefault();
  var accessKey = document.getElementById("accessKey").value;
  console.log(accessKey);
  verifyLogin(accessKey);
  return true;
}

function verifyLogin(accessKey)
{
  var chatSelect = document.getElementById("chatSelect");
  var chatDiv = document.getElementById("chatDiv");
  var chatDivTwo = document.getElementById("chatDivTwo");
  var notice = document.getElementById("notice");

  var formData = new FormData();
    formData.append('action', "getReports");
    if(typeof accessKey != "undefined")
    {
      formData.append('accessKey', accessKey);
    }
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'getInfo.php');
  xhr.onload = function () {
    if (xhr.status === 200)
    {
      console.log(xhr.responseText);
      var data = JSON.parse(xhr.responseText);
      if(data[0] == "rooms")
      {
        while(chatSelect.firstElementChild != null)
        {
          chatSelect.removeChild(chatSelect.firstElementChild);
        }
        while(chatDiv.firstChild != null)
        {
          chatDiv.removeChild(chatDiv.firstChild);
        }
        while(chatDivTwo.firstChild != null)
        {
          chatDivTwo.removeChild(chatDivTwo.firstChild);
        }
        var rooms = data[1];
        for(var i=0; i<rooms.length; i++)
        {
          var option = document.createElement("option");
            option.value = rooms[i][0];
            option.innerHTML = rooms[i][1];
          chatSelect.appendChild(option);

          var checkbox = document.createElement("input");
          checkbox.id = "chatId_" + rooms[i][0];
          checkbox.type = "checkbox";
          checkbox.name = "chatIds[]";
          checkbox.className = "w3-check";
          checkbox.value = rooms[i][0];
          chatDiv.appendChild(checkbox);
          chatDiv.innerHTML += "&nbsp;";
          var label = document.createElement("label");
          label.className = "w3-validate";
          label.setAttribute("for", "chatId_" + rooms[i][0]);
          label.innerHTML = rooms[i][1];
          chatDiv.appendChild(label);
          chatDiv.innerHTML+="<br>";

          var checkbox = document.createElement("input");
          checkbox.id = "chatId_sub_" + rooms[i][0];
          checkbox.type = "checkbox";
          checkbox.name = "chatIds[]";
          checkbox.className = "w3-check";
          checkbox.value = rooms[i][0];
          chatDivTwo.appendChild(checkbox);
          chatDivTwo.innerHTML += "&nbsp;";
          var label = document.createElement("label");
          label.className = "w3-validate";
          label.setAttribute("for", "chatId_sub_" + rooms[i][0]);
          label.innerHTML = rooms[i][1];
          chatDivTwo.appendChild(label);
          chatDivTwo.innerHTML+="<br>";
        }

        document.getElementById('id01').style.display='block';

        notice.className = notice.className.replace(/red/g, "green");
        notice.innerHTML = "<p>Please input an access key to download the logs</p>"
      }
      else if(data[0] == "error" && typeof accessKey != "undefined")
      {
        notice.className = notice.className.replace(/green/g, "red");
        document.getElementById("accessKey").value = "";
        notice.innerHTML = "<p>The access key is invalid</p>";
      }
    }
  };
  xhr.send(formData);
}

function openReport(e)
{
  var actionTypeDiv1 = document.getElementById("actionTypeDiv1");
  var actionTypeDiv2 = document.getElementById("actionTypeDiv2");
  var actionTypeDiv3 = document.getElementById("actionTypeDiv3");
  var search = document.getElementById("search");
  var searchTwo = document.getElementById("searchTwo");
  var searchThree = document.getElementById("searchThree");
  var chatSelect = document.getElementById("chatSelect");
  var typeSelect = document.getElementById("typeSelect");
  var chatDiv = document.getElementById("chatDiv");
  var chatDivTwo = document.getElementById("chatDivTwo");
  var msg = document.getElementById("msg");
  var email = document.getElementById("email");



  e.preventDefault();
  if(actionTypeDiv1.style.display == "")
  {
    var searchStr = "";
    if(search.value.length > 0)
    {
      searchStr = "&search="+search.value;
    }
    window.open("getReport.php?chatId="+chatSelect.value+"&typeId="+typeSelect.value+searchStr,'_blank');
  }
  else if(actionTypeDiv2.style.display != "none")
  {
    var searchStr = "";
    if(searchTwo.value.length > 0)
    {
      searchStr = "&search="+searchTwo.value;
    }
    var chatIds = chatDiv.getElementsByClassName("w3-check");
    for(var i=0; i<chatIds.length; i++)
    {
      if(chatIds[i].type == "checkbox" && chatIds[i].checked)
      {
        window.open("getReport.php?dl=true&chatId="+chatIds[i].value+"&typeId="+typeSelect.value+searchStr, '_blank');
      }
    }
  }
  else if(actionTypeDiv3.style.display != "none")
  {
    if(email.value == ""
      || email.value.indexOf("@") == -1
      || email.value.indexOf(".") == -1
      || (email.value.indexOf(".")+1) == email.value.length)
    {
      msg.style.color = "red";
      msg.innerHTML = "Please fill out the e-mail field";
      return;
    }
    var chatIdsStr = "";
    if(searchThree.value.length > 0)
    {
      chatIdsStr = "&search=" + searchThree.value;
    }
    var chatIds = chatDivTwo.getElementsByClassName("w3-check");
    var hasId = false;
    for(var i=0; i<chatIds.length; i++)
    {
      if(chatIds[i].type == "checkbox" && chatIds[i].checked)
      {
        chatIdsStr+= "&";
        chatIdsStr+= "roomIds[]="+chatIds[i].value;
        hasId = true;
      }
    }
    if(!hasId)
    {
      msg.style.color = "red";
      msg.innerHTML = "Please choose at least one room";
      return;
    }
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "subscriber.php?&typeId="+typeSelect.value+"&mail="+email.value+chatIdsStr);
    xhr.onload = function () {
      console.log(this.responseText);
      var response = JSON.parse(this.responseText);
      msg.style.color = (response[0] == 0 ? "red" : "green");
      msg.innerHTML = response[1];
      setTimeout(function() {msg.innerHTML="";}, 4000);
      if(response[0] == 1)
      {
        email.value = "";
        searchThree.value = "";
      }
    };
    xhr.send();
  }
}

</script>

<?php
if(isset($_SESSION['accessKey']))
{
  ?>
  <script>
    //verifyLogin();
  </script>
  <?php
}
?>
