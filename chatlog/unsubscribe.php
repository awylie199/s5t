<?php
require_once("db_connect.php");

$exists = false;
$unbsubbed = false;
$unsubKey = $mysql->real_escape_string($_GET['unsubKey']);
$subs = array();
if(($query = $mysql->query("SELECT s.id,email,r.name,room,roomId,filter,sar.id AS underSubId FROM subscriptions s
  JOIN subscribtion_ass_reportusers sar ON sar.subId=s.id && unsubTime='0000-00-00 00:00:00'
  JOIN reportupdates r ON s.typeId=r.id
  JOIN reportusers u ON sar.roomId=u.id
  WHERE unsubKey='".$unsubKey."'")))
{
  while($raw = $query->fetch_array())
  {
    $subs[] = array(
      "roomId" => $raw['roomId'],
      "roomName" => $raw['room'],
      "filter" => $raw['filter'],
      "underSubId" => $raw['underSubId'],
    );
    $exists = true;

    $id = $raw['id'];
    $email = $raw['email'];
    $typeName = $raw['name'];
  }
}

if(isset($_POST['unsubscribe']) && $exists && isset($_POST['underSubIds']))
{
  foreach($_POST['underSubIds'] as $underSubId)
  {
    $underSubId = $mysql->real_escape_string($underSubId);
    if($mysql->query("UPDATE subscribtion_ass_reportusers SET unsubTime=CURRENT_TIMESTAMP WHERE id=".$underSubId))
    {
      $i = array_search($underSubId, array_column($subs, 'underSubId'));
      unset($subs[$i]);
      $unsubbed = true;
    }
  }
}
?>
<style>
input[type=checkbox].w3-check:checked+.w3-validate,
input[type=radio].w3-radio:checked+.w3-validate {
    color: #89B25B !important;
}
</style>
<html>
  <head>
    <title>Stage5 Chat Logger</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="W3.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  </head>
  <body style="background: url('images/background.png') no-repeat center center fixed; -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">

    <div class="w3-card-4 w3-white" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:50%;max-width:450px; min-width:300px">
      <div class="w3-center w3-padding-8">
        <img src="images/s5logo.png" alt="stage5" style="width:80%;">
      </div>

      <br>

        <?php
        if($exists)
        {
        ?>
          <form action="" method="POST">
            <div class="w3-container">
              <?php
              if($unsubbed)
              {
                echo "<p style='color: green;''>The chosen log(s) are now unsubcribed.</p>";
              }
              ?>
              <p>
                Please choose which log(s) you would like to unsubscribe the <b><?php echo $typeName; ?></b> Chat Log(s) from:<br>
              </p>
              <?php
              if(count($subs) > 0)
              {
                foreach($subs as $sub)
                {
                  echo "<input id='underSubId_".$sub['underSubId']."' name='underSubIds[]' value='".$sub['underSubId']."' type='checkbox' class='w3-check'>&nbsp;&nbsp;";
                  echo "<label class='w3-validate' for='underSubId_".$sub['underSubId']."'>" . $sub['roomName'] . (strlen($sub['filter']) > 0 ? "<br><i>Filter applied: ".$sub['filter'] . "</i>" : "")."</label><br>";
                }
              }
              else {
                echo "<i>You have unsubscribed all rooms</i>";
              }
              ?>
            </div>
            <br>
            <input name="unsubscribe" class="w3-btn" value="Unsubscribe" type="submit" style="float: right; width: 100%; background-color: #89B25B;">
          </form>
        <?php
        }
        else
        {
        ?>
          <div class="w3-container">
            <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
              <p>
                We did not find a subscription using this key
              </p>
            </div>
          </div>
        <?php
        }
        ?>

    </div>
  </body>
</html>
