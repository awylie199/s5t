<?php
session_start(['cookie_lifetime' => 10]);
require_once("db_connect.php");
header('Content-Type: application/json');
$response = "";
if(isset($_POST['action']))
{
  if($_POST['action'] == "getReports")
  {
    if(isset($_POST['accessKey']))
    {
      //echo $_POST['accessKey'];
      $accessKey = $mysql->real_escape_string($_POST['accessKey']);
    }
    else {
      $accessKey = "notGood";//$_SESSION['accessKey'];
    }
    if($query = $mysql->query("SELECT roomsAllowed FROM groups WHERE accessKey='".$accessKey."'"))
    {
      if($raw = $query->fetch_array())
      {
        $_SESSION['accessKey'] = $accessKey;
        $roomsAllowed = json_decode($raw['roomsAllowed']);
        if($query = $mysql->query("SELECT id,room FROM reportusers WHERE id IN (".implode(",", $roomsAllowed).")"))
        {
          $rooms = array();
          $roomIds = array();
          while($raw = $query->fetch_array())
          {
            $rooms[] = array($raw['id'], $raw['room']);
            $roomIds[] = $raw['id'];
          }
          $_SESSION['rooms'] = $rooms;
          $_SESSION['roomIds'] = $roomIds;
          $response = array("rooms", $rooms);
        }
      }
      else
      {
        $response = array("error", "Wrong key");
      }
    }
    else
    {
      $response = array("error", "something went wrong");
    }
  }



  echo json_encode($response);
}
?>
