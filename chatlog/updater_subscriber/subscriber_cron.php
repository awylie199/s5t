<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("../db_connect.php");
//require_once("../PDFCreator.php");
//require_once("../PHPMailer/PHPMailerAutoload.php");

$mailsToSend = array();
if($query = $mysql->query("SELECT s.id,email,roomId,typeId,fromNow,fileName,t.name,room,nextMail,cond,filter,t.mailingTime FROM subscriptions s
  JOIN subscribtion_ass_reportusers sar ON sar.subId=s.id && sar.unsubTime='0000-00-00 00:00:00'
  JOIN reportupdates t ON s.typeId=t.id
  JOIN reportusers u ON sar.roomId=u.id
  WHERE unsubTime='0000-00-00 00:00:00' && nextMail<CURRENT_TIMESTAMP"))
{
  while($raw = $query->fetch_array())
  {

    if(!isset($mailsToSend[$raw['id']]))
    {
      $mailsToSend[$raw['id']] = array();
    }
    $mailsToSend[$raw['id']][] = array(
      "email" => $raw['email'],
      "subId" => $raw['id'],
      "roomId" => $raw['roomId'],
      "typeId" => $raw['typeId'],
      "fromNow" => $raw['fromNow'],
      "typeName" => $raw['name'],
      "fileName" => $raw['fileName'],
      "room" => $raw['room'],
      "nextMail" => $raw['nextMail'],
      "cond"=>$raw['cond'],
      "filter"=>$raw['filter'],
      "mailingTime"=>$raw['mailingTime']
    );
  }
}
sendReports($mailsToSend);
  //print_r($mailsToSend);


?>
