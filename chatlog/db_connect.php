<?php
/*	define("BASE_PATH", "/home/s5tradin/production/public_html/web/chatlog/");
	define("BASE_URL", "http://stage5trading.com/chatlog/");
*/
	define("BASE_PATH", "/home/s5trading/public_html/chatlog/");
	define("BASE_URL", "https://stage5trading.com/chatlog/");

    date_default_timezone_set('UTC');

	$mysql = new mysqli("localhost", "s5tradin_blazer", 'chat$user#2017', "s5tradin_chatBlazerReports");
	if ($mysql->connect_errno)
	{
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	function sanitize($string, $force_lowercase = true, $anal = false) {
	    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
	                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
	                   "—", "–", ",", "<", ".", ">", "/", "?");
	    $clean = trim(str_replace($strip, "", strip_tags($string)));
	    $clean = preg_replace('/\s+/', "-", $clean);
	    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	    return ($force_lowercase) ?
	        (function_exists('mb_strtolower')) ?
	            mb_strtolower($clean, 'UTF-8') :
	            strtolower($clean) :
	        $clean;
	}


	function get_timezone_offset($remote_tz, $origin_tz = null) {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

function sendReports($mailsToSend, $firstSubscription=FALSE)
{
	require_once(BASE_PATH . "PDFCreator.php");
	require_once(BASE_PATH . "PHPMailer/PHPMailerAutoload.php");
  global $mysql;
  $subscriber_settings = array();
  if($query = $mysql->query("SELECT * FROM subscriber_settings"))
  {
    while($raw = $query->fetch_array())
    {
      $subscriber_settings[$raw['optName']] = $raw['optValue'];
    }
  }
  $reportsNotEmpty = array();
	$roomsString = "";
  foreach($mailsToSend as $mail=>$reports)
  {
    $firstData = $reports[0];
    $mail = $firstData['email'];
    $fromNow = $firstData['fromNow'];

    $sendTime = $subscriber_settings['sendHour'] . ":" . $subscriber_settings['sendMinute'];
    /*$zone = date_default_timezone_get();
    date_default_timezone_set('America/Chicago');*/
    //$nextMail = date("Y-m-d  ".$sendTime.":00");

    $fillWhen = "this";
    if($firstData['nextMail'] != '0000-00-00 00:00:00')
    {
      //$nextMail = date("Y-m-d ".$sendTime.":00", strtotime($nextMail . " +".round($firstData['fromNow']/24/60/60)." day"));
      $fillWhen = "next";
    }

    //$nextMail1 = $nextMail;

    $nextMail = new DateTime();
    switch($firstData['mailingTime'])
    {
      case 0:
        $nextMail->modify("+" . $firstData['fromNow'] . "seconds");
        break;
      case 1:
        $nextMail->modify("Tomorrow " . $sendTime);
        break;
      case 2:
        $nextMail->modify("Sunday " . $fillWhen . " week " . $sendTime);
        break;
      case 3:
        $nextMail->modify("first day of next month " . $sendTime);
        break;
      case 4:
        $nextMail->modify("first day of next year " . $sendTime);
        break;
    }

    /*echo "<br>mail: " . $firstData['email'];
    echo "<br>Report type: " . round($firstData['fromNow']/24/60/60) . "days";
    echo "<br>Mailing time: " . $firstData['mailingTime'];
    echo "<br>nextMail: " . $nextMail->format("Y-m-d H:i:s");*/
    $nextMail = $nextMail->format("Y-m-d H:i:s");


    //$nextMail = $nextMail1;

    //date_default_timezone_set($zone);
    $unsubKey = $firstData['subId'] . "_" . md5("random string" . time());
    //echo "UPDATE subscriptions SET nextMail='".$nextMail."', unsubKey='".$unsubKey."' WHERE id=".$firstData['subId'];
    if($mysql->query("UPDATE subscriptions SET nextMail='".$nextMail."', unsubKey='".$unsubKey."' WHERE id=".$firstData['subId']))
    {
      $email = new PHPMailer;
      $email->Debugoutput = 'html';
      $email->setFrom($subscriber_settings['fromMail'], $subscriber_settings['fromName']);
      $email->addAddress($mail);

      $subject = str_replace("[reportType]", $firstData['typeName'], $subscriber_settings['subject']);
      $subject = str_replace("[unsubKey]", $unsubKey, $subject);

      $email->Subject = $subject;

      $content = $subscriber_settings['content'];
      $content = str_replace("\n", "<br>", $content);
      $content = str_replace("[reportType]", $firstData['typeName'], $content);
      $content = str_replace("[unsubKey]", $unsubKey, $content);
      $hasAttachment = false;
      foreach($reports as $report)
      {
        $roomName = sanitize($report['room'], false);
				$roomsString.= "<li>" . $roomName . "</li>";
        $filter_sanitized = sanitize($report['filter'], false);
        $zone = date_default_timezone_get();
        date_default_timezone_set('America/Chicago');
        $timeZoneOffset = get_timezone_offset($zone, date_default_timezone_get());
        $issued = "Issued " . date("Y-m-d \\a\\t H:i") . " (Central Time)";
        $earliestMoment = date("Y-m-d H:i:s", time() - $fromNow);

        if($firstData['nextMail'] == '0000-00-00 00:00:00')
        {
          $hour = $subscriber_settings['sendHour'] + ($timeZoneOffset/60/60);
          if($hour < 0 || $hour > 23)
          {
            $hour = ($hour<0 ? $hour+=24 : $hour-=24);
          }

          switch($firstData['mailingTime'])
          {
            case 0:
              $moment = new DateTime();
              $latestMoment = $moment->format("Y-m-d H:i:s");
              $moment->modify("-" . $firstData['fromNow'] . "seconds");
              $latestMoment = $moment->format("Y-m-d H:i:s");
              break;
            case 1:
              $timeStr = "yesterday";
              break;
            case 2:
              $timeStr = "Sunday last week";
              break;
            case 3:
              $timeStr = "first day of last month";
              break;
            case 4:
              $timeStr = "first day of last year";
              break;
          }
          if($firstData['mailingTime'] != 0)
          {
            $moment = new DateTime();
            $moment->modify($timeStr);
            $moment->setTime($hour, $subscriber_settings['sendMinute']);
            $latestMoment = $moment->format("Y-m-d H:i:s");
            $moment->modify($timeStr);
            $moment->setTime($hour, $subscriber_settings['sendMinute']);
            $earliestMoment = $moment->format("Y-m-d H:i:s");
          }
        }
        //echo "<br>Log from: " . $earliestMoment . " to " . $latestMoment;
        date_default_timezone_set($zone);
        if(strlen($report['cond']) > 0 || $firstData['nextMail'] == '0000-00-00 00:00:00')
        {
          $pdfName = $roomName . "_" . $report['typeName'] . (strlen($report['cond']) > 0 ? "_" . $filter_sanitized : "") . ".pdf";
          $filePath = BASE_PATH . "updater_subscriber/tmp/" . $pdfName;

          $lower_headline = (strlen($report['cond']) > 0 ? "Filter applied: " . $report['filter'] : "");
          $messages = array();
          $esc_room = $mysql->real_escape_string($report['room']);
          //echo "SELECT * FROM messages WHERE `datetime`>'".$earliestMoment."' && room='".$esc_room."' && (" . $report['cond'] . ") ORDER BY `datetime`";

          $queryStr = "SELECT * FROM messages WHERE `datetime`>'".$earliestMoment."' " .
            ($firstData['nextMail'] == '0000-00-00 00:00:00' ? "&& `datetime`<'".$latestMoment."'" : "") . "&& room='".$esc_room."'" .
            (strlen($report['cond']) > 0 ? "&& (" . $report['cond'] . ")" : "") . " ORDER BY `datetime`";

          if($query = $mysql->query($queryStr))
          {
            while($raw = $query->fetch_array())
            {
              $messages[] = array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']);
            }
          }
          //echo $mysql->error;
          //print_r($messages);
          if(count($messages) > 0)
          {
            $headline = $roomName . " - " . (strlen($report['cond']) > 0 ? "Custom" : "Old") . " logs";
            if($firstData['nextMail'] != '0000-00-00 00:00:00')
            {
              $headline.= " for the past " . (($fromNow > 86400) ? (round($fromNow/60/60/24) . " days") : (round($fromNow/60/60) . " hours"));
            }
            else {
              $headline.= " from " . date("Y-m-d(H:i)", strtotime($earliestMoment)) . " to " . date("Y-m-d(H:i)", strtotime($latestMoment));
            }


            //echo "<b>" . $mail . "</b> : " . $headline . "<br>";
            $pdf = createPDF($pdfName, $issued, $report['room'], $headline, $messages, $lower_headline);
            $pdf->Output($filePath, "F");
            $email->AddAttachment($filePath);
            //echo $filePath . "<br>";
            $hasAttachment = true;
          }
          else {
            //echo "Not found: " . $filePath . "<br>";
          }
        }
        else
        {
          $esc_room = $mysql->real_escape_string($report['room']);
          $empty = true;
          $filePath = BASE_PATH . "updater_chatlogs/chatReports/" . $roomName . "/" . $roomName . "_" . $report['fileName'] . ".pdf";
          if(in_array($filePath, $reportsNotEmpty))
          {
            $empty = false;
          }
          else if($query = $mysql->query("SELECT null FROM messages WHERE `datetime`>'".$earliestMoment."' && room='".$esc_room."' LIMIT 10"))
          {
            if($query->num_rows > 0)
            {
              //echo $filePath . "<br>";
              $reportsNotEmpty[] = $filePath;
              $empty = false;
            }
          }
          if(!$empty)
          {
            $email->AddAttachment($filePath);
            $hasAttachment = true;
          }
        }
      }
      //echo "<br>";
      if($hasAttachment || $firstSubscription)
      {
				if(!$firstSubscription)
				{
					$email->msgHTML($content);
				}
				else {
					$email->msgHTML("Hi,<br><br>

						You have now been subscribed to the following " . $firstData['typeName'] . " logs:<br><ul>" . $roomsString .
					"</ul>The logs not empty has been attached to this mail from the last time period.");
				}
        //echo "sending mail";
        $email->send();
      }
    }
    echo $mysql->error;
  }
}
?>
