<?php
session_start();
header('Content-Type: application/json');
require_once("db_connect.php");
require_once("PDFCreator.php");
$response = "";
if(isset($_SESSION['rooms']) && isset($_GET['roomIds']) && isset($_GET['typeId']))
{
  $mailsToSend = array();
  $added = false;
  $alreadyAdded = false;
  $firstOcc = true;
  $filter = $_GET['search'];
  if(isset($filter) && strlen($filter) > 0)
  {
    $cond = composeCondition($filter);
  }
  else
  {
      $cond = "";
  }
  $filter = $mysql->real_escape_string($filter);
  $cond = $mysql->real_escape_string($cond);
  foreach($_GET['roomIds'] as $roomId)
  {
    if(!in_array($roomId, $_SESSION['roomIds'])){continue;}
    $weAreReady = false;
    $typeId = $mysql->real_escape_string($_GET['typeId']);
    $mail = $mysql->real_escape_string(strtolower($_GET['mail']));
    if($firstOcc)
    {
      if(($query = $mysql->query("SELECT id FROM subscriptions WHERE email='".$mail."' && typeId=".$typeId)) && $query->num_rows == 0)
      {
        $mysql->query("INSERT INTO subscriptions (email, typeId) VALUES ('".$mail."', ".$typeId.")");
        $subId = $mysql->insert_id;
      }
      else if(($raw = $query->fetch_array()) && $raw['unsubTime'] != '0000-00-00 00:00:00')
      {
        $subId = $raw['id'];
        //$mysql->query("UPDATE subscriptions SET nextMail='0000-00-00 00:00:00' WHERE id=".$subId);
      }
      else
      {
        continue;
      }
      $firstOcc = false;
    }

    if(($query = $mysql->query("SELECT unsubTime FROM subscribtion_ass_reportusers WHERE subId=".$subId." && roomId=".$roomId." && filter='".$filter."'")))
     {
       if(($raw = $query->fetch_array()) && $raw['unsubTime'] == '0000-00-00 00:00:00')
       {
         $alreadyAdded = true;
       }
       else if($raw && $mysql->query("UPDATE subscribtion_ass_reportusers SET unsubTime='0000-00-00 00:00:00' WHERE subId=".$subId." && roomId=".$roomId." && filter='".$filter."'"))
       {
         //$mysql->query("UPDATE subscriptions SET nextMail='0000-00-00 00:00:00' WHERE id=".$subId);
         $response = array(1, "Subscription added");
         $added = true;
         $weAreReady = true;
       }
       else if($mysql->query("INSERT INTO subscribtion_ass_reportusers (roomId, subId, filter, cond) VALUES ('".$roomId."', '".$subId."', '".$filter."', '".$cond."')"))
       {
         $response = array(1, "Subscription added");
         $added = true;
         $weAreReady = true;
       }
       echo $mysql->error;
     }

     if($weAreReady
      && ($query = $mysql->query("SELECT fromNow,name,fileName,mailingTime FROM reportupdates WHERE id=".$typeId)) && ($reportType = $query->fetch_array())
      && ($query = $mysql->query("SELECT room FROM reportusers WHERE id=".$roomId)) && ($roomInfo = $query->fetch_array()))
     {
       if(!isset($mailsToSend[$subId]))
       {
         $mailsToSend[$subId] = array();
       }
       $mailsToSend[$subId][] = array(
         "email" => $mail,
         "subId" => $subId,
         "roomId" => $roomId,
         "typeId" => $typeId,
         "fromNow" => $reportType['fromNow'],
         "typeName" => $reportType['name'],
         "fileName" => $reportType['fileName'],
         "room" => $roomInfo['room'],
         "nextMail" => '0000-00-00 00:00:00',
         "cond"=>$cond,
         "filter"=>$filter,
         "mailingTime"=>$reportType['mailingTime']
       );
     }

  }
  if(!$added)
  {
    if($alreadyAdded)
    {
      $response = array(0, "Subscription already done");
    }
    else
    {
      $response = array(0, "Subscription could not be completed");
    }
  }
  else {
    sendReports($mailsToSend,true);
  }
}

echo json_encode($response);

?>
