<?php

require_once("fpdf/fpdf.php");

function createPDF($title, $issued, $room, $headline, $messages, $lower_headline="")
{
  $fontName = "Arial";
  $pdf = new FPDF();
  $pdf->SetTitle($title);
  $pdf->SetAuthor("Stage5Trading");
  $pdf->SetCreator("Stage5Trading");
  $pdf->SetAutoPageBreak(true, 0.2);
  $pdf->AddPage("P", "Letter");

  // Placing watermark
  $pdf->SetTextColor(228, 228, 228);
  $pdf->setY(120);
  $pdf->SetFont($fontName,'B',40);
  /*$pdf->Rotate(45, 100, 120);
  $pdf->Cell(0,20,"Stage5 Member Use Only", 0, 2, 'C');
  $pdf->Rotate(0);*/
  $pdf->Image(BASE_PATH . "images/watermark.png", 20, 70, 180);
  $pdf->setY(0);
  $pdf->SetTextColor(0, 0, 0);

  $pdf->SetFont($fontName,'B',8);
  $pdf->Cell(0,4,"Stage5 Chat Log", 0, 2, 'C');
  $pdf->setY(0);
  $pdf->SetFont($fontName,'i',8);
  $pdf->Cell(0,4,$issued, 0, 2, 'R');
  $pdf->Image(BASE_PATH . "images/logo.png", 75, 5, 65);
  $pdf->Cell(0,20,"", 0, 2, 'C');
  $pdf->SetFont($fontName,'B',16);
  //$pdf->SetTextColor(0, 51, 132);
  $pdf->Cell(0, 8, $headline, 0, 1, 'C');
  if(strlen($lower_headline) > 0)
  {
    $pdf->SetFont($fontName,'I',10);
    $pdf->Cell(0, 6, $lower_headline, 0, 1, 'C');
  }
  $pdf->SetTextColor(0, 0, 0);
  $pdf->Cell(0, 2, "", 0, 1, '');
  $pdf->SetFont($fontName,'',10);
  $note = "Note: Trading Futures and other instruments involves substantial risk of loss and is not suitable for all investors.
    You may lose all or more of your initial investment. Information shared here is for educational purposes only.";
  $pdf->MultiCell(0,4, $note, 0, 1, '');
  $pdf->Cell(0, 2, "", 0, 1, '');
  $pdf->SetFont($fontName,'',10);
  $pdf->SetFillColor(192, 192, 192);
  $pdf->Cell(25,5,"Date", 1, 0, 'C', true);
  $pdf->Cell(22,5,"Time(CT)", 1, 0, 'C', true);
  $pdf->Cell(40,5,"From", 1, 0, 'C', true);
  $pdf->Cell(110,5,"Message", 1, 1, 'C', true);
  //$pdf->Cell(0,8,"From " . $fromDate . " to " . date("Y-m-d H:i:s"), 0, 2, 'C');
  $pdf->SetFont($fontName,'',10);
  $pdf->SetTextColor(110, 110, 110);

  foreach($messages as $message)
  {
    $pdf->SetTextColor(110, 110, 110);
    if($pdf->getY() > 240)
    {
      $pdf->SetY(268);
      $pdf->SetFont($fontName,'I',8);
      $pdf->Cell(0,10,'Page '.$pdf->PageNo(),0,1,'C');
      $pdf->AddPage("P", "Letter");

      // Placing watermark
      $pdf->SetTextColor(228, 228, 228);
      $pdf->setY(120);
      $pdf->SetFont($fontName,'B',40);
      /*$pdf->Rotate(45, 100, 120);
      $pdf->Cell(0,20,"Stage5 Member Use Only", 0, 2, 'C');
      $pdf->Rotate(0);*/
      $pdf->Image(BASE_PATH . "images/watermark.png", 20, 70, 180);
      $pdf->setY(10);
      $pdf->SetTextColor(0, 0, 0);
    }
    $pdf->SetTextColor(110, 110, 110);
    $pdf->SetFont($fontName,'',10);
    if(strpos($message['sender'], "S5_") !== FALSE)
    {
      $pdf->SetFont($fontName,'B',11);
      $pdf->SetTextColor(0, 0, 0);
    }
    $theDateTime = explode(" ", $message['datetime']);
    $pdf->Cell(25,5, $theDateTime[0], 1, 0, '');
    $pdf->Cell(22,5, $theDateTime[1], 1, 0, '');
    $pdf->Cell(40,5,$message['sender'], 1, 0, '');
    $pdf->MultiCell(110,5,$message['message'], 1, 1, '');
  }
  return $pdf;
}

function composeCondition($searchStr)
{
  global $mysql;
  $searchStr = preg_replace("/user:\s*/", "user: ", $searchStr);
  $searchStr = preg_replace("/msg:\s*/", "msg: ", $searchStr);
  $searchStr = preg_replace("/s*\NOT\s*/", " NOT ", $searchStr);
  $searchStr = preg_replace("/s*\-/", " - ", $searchStr);
  $searchStr = preg_replace("/s*\(\s*/", " ( ", $searchStr);
  $searchStr = preg_replace("/s*\)\s*/", " ) ", $searchStr);
  $searchStr = preg_replace("/s*\"\s*/", " \" ", $searchStr);
  $searchStr = trim($searchStr);
  //echo $searchStr . "<br><br>";
  $cond = "";
  $inQuotes = FALSE;
  $quoteStr = "";
  $needModif = FALSE;
  $searches = explode(" ", $searchStr);
  $userOnly = FALSE;
  $msgOnly = FALSE;
  foreach($searches as $search)
  {
    if($search == "\"" && !$inQuotes)
    {
      $inQuotes = TRUE;
      $quoteStr = "";
      continue;
    }
    else if($search == "\"" && $inQuotes)
    {
      $inQuotes = FALSE;
      $quoteStr = substr($quoteStr, 1);
      $search = $quoteStr;
      $quoteStr = "";
    }
    else if($inQuotes)
    {
      $quoteStr.= " " . $search;
      continue;
    }

    if($search == "(" || $search == ")" || $search == "NOT")
    {
      $cond.= $search;
      continue;
    }
    if($search == "-")
    {
      $cond.= "NOT";
      continue;
    }
    else if($search == "AND")
    {
      $needModif = FALSE;
      $cond.= " && ";
      continue;
    }
    else if($search == "OR")
    {
      $needModif = FALSE;
      $cond.= " || ";
      continue;
    }
    else if($needModif)
    {
      $needModif = FALSE;
      $cond.= " && ";
    }

    if(($pos = strpos($search, "user:")) !== FALSE)
    {
      $userOnly = TRUE;
      $msgOnly = FALSE;
    }
    else if(($pos = strpos($search, "msg:")) !== FALSE)
    {
      $userOnly = FALSE;
      $msgOnly = TRUE;
    }
    else if(strlen($search) > 0 && $search != " ")
    {
      $needModif = TRUE;
      $search = $mysql->real_escape_string(strtolower($search));
      if($userOnly)
      {
        $cond.= " (LOWER(CONVERT(BINARY sender USING latin1)) LIKE '%".$search."%') ";
      }
      else if($msgOnly)
      {
        $cond.= " (LOWER(CONVERT(BINARY message USING latin1)) LIKE '%".$search."%') ";
      }
      else
      {
        $cond.= " (LOWER(CONVERT(BINARY sender USING latin1)) LIKE '%".$search."%' || LOWER(CONVERT(BINARY message USING latin1)) LIKE '%".$search."%') ";
      }
      $userOnly = FALSE;
      $msgOnly = FALSE;
    }
  }
  return $cond;
}

//$msgTemp = $message['message'];
/*if(FALSE && strpos($msgTemp, "data:image/") !== FALSE)
{
  $imgStr = substr($msgTemp, strpos($msgTemp, "data:image/"));
  if(strpos($imgStr, " ") !== FALSE)
  {
    $imgStr = substr($imgStr, 0, strpos($imgStr, " "));
  }
  $imgExt = substr($imgStr, strpos($imgStr, "/")+1);
  $imgExt = substr($imgExt, 0, strpos($imgExt, ";"));
  echo $imgStr;
  $imgPath = 'tmp/' . md5($imgStr) . '.' . $imgExt;
  $stream = @fopen( $imgPath, 'w' );
  fwrite($stream, base64_decode($imgStr));
  fclose($stream);

  $pdf->Image($imgPath, 0, 0, 110);
  //$msgTemp = substr($msgTemp, 0, strpos($msgTemp, "data:image/"));
}
else {*/
  //$pdf->MultiCell(110,5,$message['message'], 1, 1, '');
//}

?>
