<?php
require_once("db_connect.php");
require_once("PDFCreator.php");

$esc_room = "Traders Helping Traders";
$searchStr = "user:S5_ AND msg:profile";
$cond = composeCondition($searchStr);
$lower_headline = "Filter applied: " . $searchStr;
$fromNow = 2592000;

//echo $lower_headerline . "<br>";
//echo $cond . "<br><br>";
$zone = date_default_timezone_get();
date_default_timezone_set('America/Chicago');
$issued = "Issued " . date("Y-m-d \\a\\t H:i") . " (Central Time)";
$earliestMoment = date("Y-m-d H:i:s", time() - $fromNow);
date_default_timezone_set($zone);
$messages = array();
//echo $issued . "<br>";
//echo $earliestMoment . "<br>";
if($query = $mysql->query("SELECT * FROM messages WHERE `datetime`>'".$earliestMoment."' && room='".$esc_room."' && (" . $cond . ") ORDER BY `datetime`"))
{
  while($raw = $query->fetch_array())
  {
    $messages[] = array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']);
    //print_r(array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']));
    //echo "<br><br>";
  }
}

$headline = $room . " - Custom logs for the past " . (($fromNow > 86400) ? (round($fromNow/60/60/24) . " days") : (round($fromNow/60/60) . " hours"));
$pdf = createPDF($roomName . "_custom.pdf", $issued, $room, $headline, $messages, $lower_headline);
$pdf->Output("F");
?>
