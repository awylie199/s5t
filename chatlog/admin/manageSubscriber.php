<?php
include "styleHeader.php";

if(isset($_POST['subscriber_save']))
{
  foreach($_POST as $optName=>$optValue)
  {
    if($name != "subscriber_save")
    {
      $optName = $mysql->real_escape_string($optName);
      $optValue = $mysql->real_escape_string($optValue);
      $mysql->query("UPDATE subscriber_settings SET optValue='".$optValue."' WHERE optName='".$optName."'");
    }
  }
  echo "<center><p style='color: green'>Changes saved</p></center>";
}
else if(isset($_POST['subscriber_unsub']))
{
  $subId = $mysql->real_escape_string($_POST['subId']);
  $underSubId = $mysql->real_escape_string($_POST['underSubId']);
  $mysql->query("UPDATE subscribtion_ass_reportusers SET unsubTime=CURRENT_TIMESTAMP WHERE id=".$underSubId);
}
else if(isset($_POST['subscriber_sub']))
{
  $subId = $mysql->real_escape_string($_POST['subId']);
  $underSubId = $mysql->real_escape_string($_POST['underSubId']);
  $mysql->query("UPDATE subscribtion_ass_reportusers SET unsubTime='0000-00-00 00:00:00' WHERE id=".$underSubId);
  $mysql->query("UPDATE subscriptions SET nextMail='0000-00-00 00:00:00' WHERE id=".$subId);
}




$subscriber_settings = array();
if($query = $mysql->query("SELECT * FROM subscriber_settings"))
{
  while($raw = $query->fetch_array())
  {
    $subscriber_settings[$raw['optName']] = $raw['optValue'];
  }
}
?>
<style>
input {
  width: 100%;
}
textarea {
  width: 100%;
  height: 200px;
  resize: vertical;
}

#sendHour, #sendMinute {
  width:40px;
}

</style>
<form action="" method="POST">
  <table border="1" style="margin: 0 auto; width: 50%;">
    <tr>
      <td>
        <b>Time mails go out: </b>
        <input id="sendHour" name="sendHour" type="number" min="0" max="23" value="<?php echo $subscriber_settings['sendHour']; ?>"> : <input id="sendMinute" name="sendMinute" type="number" min="0" max="59" value="<?php echo $subscriber_settings['sendMinute']; ?>">
        <?php
        $chicTime = strtotime($subscriber_settings['sendHour'] . ":" . $subscriber_settings['sendMinute']);
        $zone = date_default_timezone_get();
        date_default_timezone_set('America/Chicago');
        $chicTime = date("H:i", $chicTime);
        date_default_timezone_set($zone);
        ?>
        <i>Time is in GMT (<?php echo date("H:i", strtotime($subscriber_settings['sendHour'] . ":" . $subscriber_settings['sendMinute'])); ?>) which is <?php echo $chicTime; ?> in Chicargo</i>
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="fromMail" placeHolder="From mail" value="<?php echo $subscriber_settings['fromMail']; ?>">
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="fromName" placeHolder="From name" value="<?php echo $subscriber_settings['fromName']; ?>">
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="subject" placeHolder="Subject" value="<?php echo $subscriber_settings['subject']; ?>">
      </td>
    </tr>
    <tr>
      <td>
        <textarea name="content" placeHolder="Content"><?php echo $subscriber_settings['content']; ?></textarea>
      </td>
    </tr>
    <tr>
      <td>
        <input type="submit" name="subscriber_save" value="Save">
      </td>
    </tr>
  </table>
</form>
<p>
  [unsubKey] = The key to unsubscribe<br>
  [reportType] = The name of the report type
</p>
<hr>
<form id="subscriber_listChooseForm" action="" method="GET" style="margin: 0px auto; width: 25%;">
  <table style="margin: 0px auto; width: 340px; line-height: 40px;">
    <tr>
      <td colspan="5"><center><a href="getSubscriberReport.php" target="_blank">Get full report</a></center></td>
    </tr>
    <tr>
      <td><label for="sub">Show subscribed:</label></td>
      <td><input onchange="subscriber_listChooseForm.submit()" id="sub" type="radio" name="showUnsub" value="0" style="width: 20px; height: 20px;" <?php echo ((!isset($_GET['showUnsub']) || $_GET['showUnsub'] == 0) ? "checked" : ""); ?>></td>
      <td style="width: 10px;">|</td>
      <td><label for="unsub">Show unsubscribed:</label></td>
      <td><input onchange="subscriber_listChooseForm.submit()"  id="unsub" type="radio" name="showUnsub" value="1" style="width: 20px; height: 20px;" <?php echo ((isset($_GET['showUnsub']) && $_GET['showUnsub'] == 1) ? "checked" : ""); ?>></td>
    </tr>
  </table>
</form>
<table border="1" style="margin: 0 auto; width: 80%;">
  <tr>
    <th>E-mail address</th>
    <th>Filter</th>
    <th>Time subscribed</th>
    <th>Room</th>
    <th>Report Type</th>
    <th><?php echo ((!isset($_GET['showUnsub']) || $_GET['showUnsub'] == 0) ? "Next e-mail" : "Time unsubscribed"); ?></th>
    <th>Action</th>
  </tr>
  <?php
  if(!isset($_GET['showUnsub']) || $_GET['showUnsub'] == 0)
  {
    if($query = $mysql->query("SELECT s.id,s.created,email,name,room,nextMail,sar.roomId,filter,sar.id AS underSubId FROM subscriptions s
      JOIN subscribtion_ass_reportusers sar ON s.id=sar.subId && sar.unsubTime='0000-00-00 00:00:00'
      JOIN reportupdates t ON s.typeId=t.id
      JOIN reportusers u ON sar.roomId=u.id ORDER BY created DESC"))
    {
      while($raw = $query->fetch_array())
      {
        echo "<tr>";
          echo "<td>" . $raw['email'] . "</td>";
          echo "<td>" . $raw['filter'] . "</td>";
          echo "<td>" . $raw['created'] . "</td>";
          echo "<td>" . $raw['room'] . "</td>";
          echo "<td>" . $raw['name'] . "</td>";
          echo "<td>" . $raw['nextMail'] . "</td>";
          echo "<td>";
            echo "<form action='' method='POST'>";
              echo "<input type='submit' name='subscriber_unsub' value='Unsubscribe' style='color: red;'>";
              echo "<input type='hidden' name='subId' value='".$raw['id']."'>";
              echo "<input type='hidden' name='underSubId' value='".$raw['underSubId']."'>";
            echo "</form>";
          echo "</td>";
        echo "</tr>";
      }
    }
    echo $mysql->error;
  }
  else if(isset($_GET['showUnsub']) || $_GET['showUnsub'] == 1)
  {
    if($query = $mysql->query("SELECT s.id,s.created,email,name,room,nextMail,sar.roomId,filter,sar.unsubTime,sar.id AS underSubId FROM subscriptions s
      JOIN subscribtion_ass_reportusers sar ON s.id=sar.subId && sar.unsubTime!='0000-00-00 00:00:00'
      JOIN reportupdates t ON s.typeId=t.id
      JOIN reportusers u ON sar.roomId=u.id ORDER BY created DESC"))
    {
      while($raw = $query->fetch_array())
      {
        echo "<tr>";
          echo "<td>" . $raw['email'] . "</td>";
          echo "<td>" . $raw['filter'] . "</td>";
          echo "<td>" . $raw['created'] . "</td>";
          echo "<td>" . $raw['room'] . "</td>";
          echo "<td>" . $raw['name'] . "</td>";
          echo "<td>" . $raw['unsubTime'] . "</td>";
          echo "<td>";
            echo "<form action='' method='POST'>";
              echo "<input type='submit' name='subscriber_sub' value='Resubscribe' style='color: green;'>";
              echo "<input type='hidden' name='subId' value='".$raw['id']."'>";
              echo "<input type='hidden' name='underSubId' value='".$raw['underSubId']."'>";
            echo "</form>";
          echo "</td>";
        echo "</tr>";
      }
    }
  }
  ?>
</table>
<i>Time is in GMT(<?php echo date("r"); ?>)</i>
