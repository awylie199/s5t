<?php
require_once("../db_connect.php");
require_once("security.php");
if(!sessionCheck() && !isset($loginPage))
{
	header("Location: login.php");
}
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
	</head>
	<body style="background-image: url('background.jpg'); background-repeat: repeat; margin-top: 40px;">
		<div style="width: 98%; margin: 0 auto; left: 0px; right: 0px;">
			<table id="menuBar" border="2" style="width: 100%; background: rgba(255, 255, 255, 0.9); font-size: 20px; font-family: 'Trebuchet MS', Helvetica, sans-serif; text-align: center;">
				<tr style="height: 40px;">
					<td onclick="location.href='editChat.php'"><a unselectable="on">Edit chat</a></td>
					<td onclick="location.href='index.php'"><a unselectable="on">System Settings</a></td>
					<!--<td onclick="location.href='manageReportPasswords.php'"><a unselectable="on">Chat Rooms</a></td>-->
					<td onclick="location.href='manageGroups.php'"><a unselectable="on">Groups</a></td>
					<td onclick="location.href='manageSubscriber.php'"><a unselectable="on">Subscriber</a></td>
					<td onclick="confirmUpdate()"><a unselectable="on">Update Reports</a></td>
					<td onclick="location.href='logout.php'"><a unselectable="on">Logout</a></td>
				</tr>
			</table>
		</div>
		<div style="width: 98%; margin: auto; left: 0px; right: 0px; text-align: center; background: rgba(255, 255, 255, 0.9); padding-top: 20px; ">

<script>
function confirmUpdate()
{
	var r=confirm('Are you sure to update all the reports?\n Please do not leave the page before the update is complete.');
	if(r)
	{
		location.href='updateReportsPanel.php';
	}
}
</script>
