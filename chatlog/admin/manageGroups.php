<?php
include "styleHeader.php";

if(isset($_POST['addGroup']))
{
	if(isset($_POST['name']) && strlen($_POST['name']) == 0)
	{
		$error = true;
		echo "<p style='color: red'>Please choose a name for the new group</p>";
	}

	$autogen = false;
	if(strlen($_POST['accessKey']) == 0)
	{
		$autogen = true;
		$accessKey = substr(str_shuffle(md5(time() . str_shuffle("abcdefghijklmmnopqrstuvqxyzæøå123456789"))), 10);
	}
	else if(strlen($_POST['accessKey']) < 8)
	{
		$error = true;
		echo "<p style='color: red'>Access key should contain at least 8 characters or non if you want the system to auto generate one</p>";
	}
	else {
		$accessKey = $_POST['accessKey'];
	}


	if(!$error)
	{
		$accessKey = $mysql->real_escape_string($accessKey);
		while(($query = $mysql->query("SELECT * FROM groups WHERE accessKey='".$accessKey."'")) && $query->num_rows > 0)
		{
			if(!$autogen)
			{
				$error = true;
				echo "<p style='color: red'>The accessKey should be unique</p>";
				break;
			}
			else
			{
				$accessKey = substr(str_shuffle(md5(time() . str_shuffle("abcdefghijklmmnopqrstuvqxyzæøå123456789"))), 10);
			}
		}

		if(!$error)
		{
			$name = $mysql->real_escape_string($_POST['name']);
			$roomsAllowed = (isset($_POST['roomsAllowed']) ? $_POST['roomsAllowed'] : array());
			$roomsAllowed = (is_array($roomsAllowed) ? $roomsAllowed : array($roomsAllowed));
			$roomsAllowed = $mysql->real_escape_string(json_encode($roomsAllowed));
			if($mysql->query("INSERT INTO groups (name,accessKey,roomsAllowed) VALUES ('".$name."', '".$accessKey."', '".$roomsAllowed."')"))
			{
				echo "<br><span style='color: green'>" . $name . " is now added.</span>";
				$_POST['name'] = "";
				$_POST['accessKey'] = "";
				$_POST['roomsAllowed'] = array();
			}
			else
			{
				echo "<br><span style='color: red'>Something went wrong when creating a new group.<br>" . $mysql->error . "</span>";
			}
		}
	}
}
if(isset($_POST['changeGroup']))
{
	if(strlen($_POST['accessKey']) == 0)
	{
		$accessKey = substr(str_shuffle(md5(time() . str_shuffle("abcdefghijklmmnopqrstuvqxyzæøå123456789"))), 10);
	}
	else
	{
		$accessKey = $_POST['accessKey'];
	}
	$accessKey = $mysql->real_escape_string($accessKey);
	$roomsAllowed = (isset($_POST['roomsAllowed']) ? $_POST['roomsAllowed'] : array());
	$roomsAllowed = (is_array($roomsAllowed) ? $roomsAllowed : array($roomsAllowed));
	$roomsAllowed = $mysql->real_escape_string(json_encode($roomsAllowed));
	$name = $mysql->real_escape_string($_POST['name']);
	$groupId = $_POST['groupId'];
	if($mysql->query("UPDATE groups SET name='".$name."', accessKey='".$accessKey."',roomsAllowed='".$roomsAllowed."' WHERE id=".$groupId))
	{
		echo "<br><span style='color: green'>" . $name . " has been changed.</span>";
	}
	else
	{
		echo "<br><span style='color: red'>Something went wrong when creating a new group.<br>" . $mysql->error . "</span>";
	}
	$_POST['name'] = "";
	$_POST['accessKey'] = "";
	$_POST['roomsAllowed'] = array();
}

if(isset($_POST['deleteGroup']) && isset($_POST['groupId']))
{
	$groupId = $mysql->real_escape_string($_POST['groupId']);
	if($mysql->query("DELETE FROM groups WHERE id=".$groupId))
	{
		echo "<br><span style='color: green'>" . $name . " has been deleted.</span>";
	}
	else
	{
		echo "<br><span style='color: red'>Something went wrong when creating a new group.<br>" . $mysql->error . "</span>";
	}
	$_POST['name'] = "";
	$_POST['accessKey'] = "";
	$_POST['roomsAllowed'] = array();
}

?>
<table border="1" style="margin: 0 auto;">
	<tr>
		<th>Name</th>
		<th>Access key</th>
		<th>Rooms allowed</th>
		<th>Action</th>
	<tr>
	<?php
	$query = $mysql->query("SELECT * FROM groups ORDER BY id DESC");
	while($raw = $query->fetch_array())
	{
		$roomsAllowed = json_decode($raw['roomsAllowed']);
		if(json_last_error() != JSON_ERROR_NONE || !is_array($roomsAllowed))
		{
			$roomsAllowed = array();
		}
		echo "<form action='' method='POST'>";
			echo "<tr>";
					echo "<td><input type='text' name='name' value='" . $raw['name'] . "'></td>";
					echo "<td><input type='text' name='accessKey' value='" . $raw['accessKey'] . "'></td>";
					echo "<td>";
						createAllowList($roomsAllowed);
					echo "</td>";
					echo "<input type='hidden' name='groupId' value=" . $raw['id'] . ">";
					echo "<td>";
						echo "<input type='submit' value='Save' name='changeGroup'>";
						echo "<input type='submit' value='X' name='deleteGroup'>";
					echo "</td>";
				echo "</tr>";
			echo "</tr>";
		echo "</form>";
	}
	$roomsAllowed = (isset($_POST['roomsAllowed']) ? $_POST['roomsAllowed'] : array());
	$roomsAllowed = (is_array($roomsAllowed) ? $roomsAllowed : array($roomsAllowed));
	echo "<form action='' method='POST'>";
		echo "<tr>";
				echo "<td><input type='text' name='name' value='".$_POST['name']."' placeholder='New group name'></td>";
				echo "<td><input type='text' name='accessKey' value='".$_POST['accessKey']."' placeholder='Auto generate'></td>";
				echo "<td>";
					createAllowList($roomsAllowed);
				echo "</td>";
				echo "<td>";
					echo "<input type='submit' value='Add' name='addGroup'>";
				echo "</td>";
			echo "</tr>";
		echo "</tr>";
	echo "</form>";
	?>
</table>

<?php
function createAllowList($rooms=array())
{
	global $mysql;
	echo "<table>";
	if($query = $mysql->query("SELECT * FROM reportusers"))
	{
		while($raw = $query->fetch_array())
		{
			echo "<tr>";
				echo "<td>" . $raw['room'] . "</td>";
				echo "<td><input type='checkbox' name='roomsAllowed[]' value='" . $raw['id'] . "' " . (in_array($raw['id'], $rooms) ? "checked" : "") . "></td>";
			echo "</tr>";
		}
	}
	echo "</table>";
}
?>
