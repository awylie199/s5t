<?php
include "styleHeader.php";

$query = $mysql->query("SELECT * FROM users WHERE username='".$_SESSION['username']."'");
$user = $query->fetch_array();

/*if(isset($_POST['changeEmail']))
{
	if(strpos($_POST['email'], "@") !== FALSE)
	{
		$email = $mysql->real_escape_string($_POST['email']);
		if($mysql->query("UPDATE users SET email='".$email."'"))
		{
			echo "<a style='color: green'>Email changed</a>";
		}
		else
		{
			echo "<a style='color: red'>Something went wrong when trying to change the email</a>";
		}
	}
	else
	{
		echo "<a style='color: red'>The e-mail is not valid</a>";
	}
}*/

if(isset($_POST['changePassword']))
{
	if(strcmp($user['password'], sha1($_POST['oldPassword'])) != 0)
	{
		echo "<a style='color: red'>The old password is not correct</a>";
	}
	else if(strcmp($_POST['newPassword_one'], $_POST['newPassword_two']) != 0)
	{
		echo "<a style='color: red'>The passwords does not match</a>";
	}
	else if(strlen($_POST['newPassword_one']) <= 4)
	{
		echo "<a style='color: red'>The new password must be minimum 4 characters long</a>";
	}
	else
	{
		if($mysql->query("UPDATE users SET password = '".sha1($_POST['newPassword_one'])."'"))
		{
			echo "<a style='color: green'>Password changed</a>";
		}
		else
		{
			echo "<a style='color: red'>Something went wrong, when changing the password</a>";
		}
	}
}

function deletePathsFromReportUsers($path)
{
	global $mysql;
	if($query = $mysql->query("SELECT id,paths FROM reportusers"))
	{
		while($reportUser = $query->fetch_array())
		{
			$newPaths = array();
			foreach(unserialize($reportUser['paths']) as $reportPath)
			{
				$position = strpos($reportPath, $path);
				if($position !== FALSE && $position == strlen($reportPath)-strlen($path))
				{
					unlink("update/" . $reportPath);
				}
				else
				{
					$newPaths[] = $reportPath;
				}
			}
			$mysql->query("UPDATE reportusers set paths='".serialize($newPaths)."' WHERE id='".$reportUser['id']."'");
		}
	}
}

if(isset($_POST['deleteReportUpdate']))
{
	if($mysql->query("DELETE FROM reportupdates WHERE id=".$_POST['reportUpdateId'].""))
	{
		deletePathsFromReportUsers($_POST['fileName'] . ".pdf");
		echo "<a style='color: green'>Report udpate deleted</a>";
	}
	else
	{
		echo "<a style='color: red'>Something went wrong, when deleting the report update</a>";
	}
}

if(isset($_POST['changeReportUpdate']))
{
	if($query = $mysql->query("SELECT * FROM reportupdates WHERE id='".$_POST['reportUpdateId']."'"))
	{
		$reportUpdate = $query->fetch_array();
		if(strcmp($reportUpdate['fileName'], $_POST['fileName']) != 0)
		{
			deletePathsFromReportUsers($reportUpdate['fileName'] . ".pdf");
		}

		$name = $mysql->real_escape_string($_POST['name']);
		$fileName = $mysql->real_escape_string($_POST['fileName']);
		$fromNow = $mysql->real_escape_string($_POST['fromNow'])*60;
		$updateInterval = $mysql->real_escape_string($_POST['updateInterval'])*60;
		$reportUpdateId = $mysql->real_escape_string($_POST['reportUpdateId']);
		$mailingTime = $mysql->real_escape_string($_POST['mailingTime']);
		if($mysql->query("UPDATE reportupdates SET name='".$name."', fileName='".$fileName."', fromNow='".$fromNow."', updateInterval='".$updateInterval."', mailingTime='".$mailingTime."' WHERE id=".$reportUpdateId.""))
		{
			echo "<a style='color: green'>Report Updated</a>";
		}
		else
		{
			echo "<a style='color: red'>Something went wrong, when changing the report update</a>";
		}
	}
	else
	{
		echo "<a style='color: red'>Something went wrong, when changing the report update</a>";
	}
}

if(isset($_POST['addReportUpdate']))
{
	if(strlen($_POST['fileName']) == 0 || strlen($_POST['fromNow']) == 0 || strlen($_POST['updateInterval']) == 0)
	{
		echo "<a style='color: red'>Not all fields is filled out</a>";
	}
	else
	{
		$name = $mysql->real_escape_string($_POST['name']);
		$fileName = $mysql->real_escape_string($_POST['fileName']);
		$fromNow = $mysql->real_escape_string($_POST['fromNow'])*60;
		$updateInterval = $mysql->real_escape_string($_POST['updateInterval'])*60;
		$mailingTime = $mysql->real_escape_string($_POST['mailingTime']);
		if($mysql->query("INSERT INTO reportupdates (name, fileName, fromNow, updateInterval) VALUES ('".$name."''".$fileName."', '".$fromNow."', '".$updateInterval."', '".$mailingTime."')"))
		{
			echo "<a style='color: green'>Report Added</a>";
		}
		else
		{
			echo "<a style='color: red'>Something went wrong, when adding the report update</a>";
		}
	}
}

?>

<form action="" method="POST">
	<table align="center" border="1">
		<tr>
			<td>Old Password:</td>
			<td><input type="password" name="oldPassword" value=""></td>
		</tr>
		<tr>
			<td>New Password:</td>
			<td><input type="password" name="newPassword_one" value=""></td>
		</tr>
		<tr>
			<td>New Password again:</td>
			<td><input type="password" name="newPassword_two" value=""></td>
		</tr>
		<tr><td colspan="2"><center><input style="margin: 4px; "type="submit" name="changePassword" value="Change Password"</center></td></tr>
	</table>
</form>

<!--<form action="" method="POST">
	<table align="center" border="1">
		<tr>
			<td>E-mail Address:</td>
			<td><input type="email" name="email" value="<?php echo $user['email']; ?>"></td>
		</tr>
		<tr><td colspan="2"><center><input style="margin: 4px; "type="submit" name="changeEmail" value="Change E-mail Address"</center></td></tr>
	</table>
</form>-->

<table border="1" style="margin: 0 auto;">
	<tr>
		<th>Shown name</th>
		<th>File name</th>
		<th>Minutes log go back</th>
		<th>Update interval</th>
		<th>When to mail out</th>
		<th>Action</th>
	<tr>
	<?php
	$query = $mysql->query("SELECT * FROM reportupdates");
	$mailingTimeOptions = array("As minutes the log goes back", "Daily", "Weekly", "Monthly", "Yearly");
	while($reportUpdate = $query->fetch_array())
	{
		echo "<form action='' method='POST'>";
			echo "<tr>";
					echo "<td><input type='text' name='name' value='" . $reportUpdate['name'] . "'></td>";
					echo "<td><input type='text' name='fileName' value='" . $reportUpdate['fileName'] . "'></td>";
					echo "<td><input type='text' name='fromNow' value=" . $reportUpdate['fromNow']/60 . "></td>";
					echo "<td><input type='text' name='updateInterval' value=" . $reportUpdate['updateInterval']/60 . "></td>";
					echo "<td>";
						echo "<select name='mailingTime'>";
							foreach($mailingTimeOptions as $optId => $opt)
							{
								echo "<option value='.$optId.' " . ($optId==$reportUpdate['mailingTime'] ? "selected" : "") . ">" . $opt . "</option>";
							}
						echo "</select>";
					echo "</td>";
					echo "<input type='hidden' name='reportUpdateId' value=" . $reportUpdate['id'] . ">";
					echo "<td>";
						echo "<input type='submit' value='Save' name='changeReportUpdate'>";
						echo "<input type='submit' value='X' name='deleteReportUpdate'>";
					echo "</td>";
				echo "</tr>";
			echo "</tr>";
		echo "</form>";
	}

	echo "<form action='' method='POST'>";
		echo "<tr>";
			echo "<td><input type='text' name='name' value=''></td>";
				echo "<td><input type='text' name='fileName' value=''></td>";
				echo "<td><input type='text' name='fromNow' value=''></td>";
				echo "<td><input type='text' name='updateInterval' value=''></td>";
				echo "<td>";
					echo "<select name='mailingTime'>";
						foreach($mailingTimeOptions as $optId => $opt)
						{
							echo "<option value='.$optId.'>" . $opt . "</option>";
						}
					echo "</select>";
				echo "</td>";
				echo "<td>";
					echo "<input type='submit' value='Add' name='addReportUpdate'>";
				echo "</td>";
			echo "</tr>";
		echo "</tr>";
	echo "</form>";
	?>
</table>
