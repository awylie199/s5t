<?php
require_once("../db_connect.php");
require_once("security.php");
if(!sessionCheck() && !isset($loginPage))
{
	header("Location: login.php");
}

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename='Subscriptions ".date("Y-m-d H:i").".csv'");

$stream = @fopen( 'php://output', 'w' );
fputcsv($stream, array("E-mail address", "Time subscribed", "Room", "Report Type", "Next e-mail", "Time unsubscribed"));
if($query = $mysql->query("SELECT s.id,s.created,email,name,room,nextMail,unsubTime FROM subscriptions s
	JOIN subscribtion_ass_reportusers sar on sar.subId=s.id
	JOIN reportupdates t ON s.typeId=t.id
	JOIN reportusers u ON sar.roomId=u.id"))
{
  while($raw = $query->fetch_array())
  {
    fputcsv($stream, array($raw['email'], $raw['created'], $raw['room'], $raw['name'], $raw['nextMail'], $raw['unsubTime']));
  }
}
fclose($stream);
?>
