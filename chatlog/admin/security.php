<?php
session_start();
if(!isset($_SESSION['initiated']))
{
    session_regenerate_id();
    $_SESSION['initiated'] = true;
}

function sessionCheck()
{
	if(!isset($_SESSION['HTTP_USER_AGENT']) || !isset($_SESSION['IP']) || !isset($_SESSION['username']) || !isset($_SESSION['password']))
	{
		return false;
	}
	
	if($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'] . "35b4j6t5yu4"))
	{
		return false;
	}
	if($_SESSION['IP'] != md5($_SERVER['REMOTE_ADDR'] . "6dfg5h46gdf4"))
	{
		return false;
	}
	
	if(!userValidation($_SESSION['username'], $_SESSION['password']))
	{
		return false;
	}
	return true;
}

function userValidation($username, $password)
{
	global $mysql;
	$username = $mysql->real_escape_string($username);
	$password = $mysql->real_escape_string($password);
	
	if($query = $mysql->query("SELECT password FROM users WHERE username = '".$username."'"))
	{
		$user = $query->fetch_array();
		if($user['password'] == sha1($password))
		{
			return true;
		}
	}
	return false;
}
?>