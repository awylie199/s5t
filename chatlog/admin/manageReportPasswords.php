<?php
include "styleHeader.php";

if(isset($_POST['changeReportUser']))
{
	$username = $mysql->real_escape_string($_POST['username']);
	$accessKey = $mysql->real_escape_string($_POST['accessKey']);
	$reportUserId = $mysql->real_escape_string($_POST['reportUserId']);
	if($query = $mysql->query("SELECT * FROM reportusers WHERE id='".$reportUserId."'"))
	{
		$reportUser = $query->fetch_array();
		if(strcmp($_POST['username'], $reportUser['username']) == 0 && strcmp($_POST['accessKey'], $reportUser['accessKey']) == 0)
		{
			echo "<a style='color: blue;'>Nothing to change</a>";
		}
		else if($query = $mysql->query("SELECT * FROM reportusers WHERE username='".$username."' AND accessKey='".$accessKey."'"))
		{
			if($query->num_rows > 0)
			{
				echo "<a style='color: red;'>There is already a report with same username and password</a>";
			}
			else if($mysql->query("UPDATE reportusers SET username='".$username."', accessKey='".$accessKey."' WHERE id = '".$reportUserId."'"))
			{
				echo "<a style='color: green;'>Report credentials changed</a>";
			}
			else
			{
				echo "<a style='color: red;'>Something went wrong when changing report credentials</a>";
			}

		}
	}
	else
	{
		echo "<a style='color: red;'>Something went wrong</a>";
	}

}

?>
<table border="1" style="margin: 0 auto; width: 96%;">
	<tr>
		<th>Username</th>
		<th>Access Key</th>
		<th>Room</th>
		<!--<th>Paths</th>-->
		<th>Action</th>
	<tr>
	<?php
	$query = $mysql->query("SELECT * FROM reportusers ORDER BY room");
	while($reportUsers = $query->fetch_array())
	{
		echo "<form action='' method='POST'>";
			echo "<tr>";
					echo "<td><input type='text' style='width:100%;' name='username' value='" . $reportUsers['username'] . "'></td>";
					echo "<td><input type='text' style='width:100%;' name='accessKey' value='" . $reportUsers['accessKey'] . "'></td>";
					echo "<td>" . $reportUsers['room'] . "</td>";
					/*echo "<td>";
						foreach(unserialize($reportUsers['paths']) as $path)
						{
							echo $path . "</br>";
						}
					echo "</td>";*/
					echo "<input type='hidden' name='reportUserId' value='" . $reportUsers['id'] . "'>";
					echo "<td>";
						echo "<input type='submit' style='width:100%;' value='Save' name='changeReportUser'>";
					echo "</td>";
				echo "</tr>";
			echo "</tr>";
		echo "</form>";
	}
	?>
</table>
