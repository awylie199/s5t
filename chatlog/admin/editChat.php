<?php

include "styleHeader.php";

if(!isset($_POST['submit']))
{
	$_POST['WC'] = 1;
	$_POST['PC'] = 1;
}

if(!isset($_POST['days']))
{
	$_POST['days'] = 4;
}
if(!isset($_POST['room']))
{
	$_POST['room'] = "";
}

$params = "";
if(isset($_POST['WC']))
{
	$params.= " AND messageType != 'WC'";
}
if(isset($_POST['PC']))
{
	$params = " AND messageType != 'PC'";
}
if(isset($_POST['show']))
{
	if(strcasecmp($_POST['show'], "both") != 0)
	{
		$params.= " AND included = " . $_POST['show'];
	}
}
else
{
	$_POST['show'] = "both";
}

if(isset($_POST['excludeMessage']))
{
	if($mysql->query("UPDATE messages set included=0 WHERE messageId='".$_POST['messageId']."'"))
	{
		echo "Message excluded from log";
	}
	else
	{
		echo $mysql->error;
	}
	unset($_POST['excludeMessage']);
	unset($_POST['messageId']);
}

if(isset($_POST['includeMessage']))
{
	if($mysql->query("UPDATE messages set included=1 WHERE messageId='".$_POST['messageId']."'"))
	{
		echo "Message include in the log again";
	}
	else
	{
		echo $mysql->error;
	}
	unset($_POST['includeMessage']);
	unset($_POST['messageId']);
}
?>
<form action="" method="POST">
	<table border="1" style="margin: 0 auto; width: 90%">
		<tr>
			<td>
			From the last 
				<select name="days">
					<?php
					for($i=0; $i<=30; $i++)
					{
						echo "<option value='" . $i . "'" . (($_POST['days'] == $i) ? "SELECTED" : "") . ">" . $i . "</option>";
					}
					?>
				</select> Days before
			</td>
			<td>
				Room: 
				<select name="room">
					<?php
					$query = $mysql->query("SELECT DISTINCT room FROM messages");
					while($room = $query->fetch_array())
					{
						echo "<option value='" . $room['room'] . "' " . ((strcasecmp($_POST['room'], $room['room']) == 0) ? "SELECTED" : "") . ">" . $room['room'] . "</option>";
					}
					?>
				</select>
			</td>
			<td>
				Show: 
				<select name="show">
					<?php
					echo "<option value='both' " . ((strcasecmp($_POST['show'], "both") == 0) ? "SELECTED" : "") . ">Both Types</option>";
					echo "<option value='1' " . ((strcasecmp($_POST['show'], "1") == 0) ? "SELECTED" : "") . ">Included Messages</option>";
					echo "<option value='0' " . ((strcasecmp($_POST['show'], "0") == 0) ? "SELECTED" : "") . ">Excluded Messages</option>";
					?>
				</select>
			</td>
			<td>
				Exclude Whisper: <input type="checkbox" name="WC" value="1" <?php echo (isset($_POST['WC']) ? "CHECKED" : ""); ?>>
			</td>
			<td>
				Exclude Private Chat: <input type="checkbox" name="PC" value="1" <?php echo (isset($_POST['PC']) ? "CHECKED" : ""); ?>>
			<td>
				<input type="submit" name="submit">
			</td>
		</tr>
	</table>
</form>

<!--<form action="writePDF.php" method="POST">
	<input type="submit" value="Make PDF">
	<input type="hidden" name="days" value="<?php echo $_POST['days']; ?>">
	<input type="hidden" name="room" value="<?php echo $_POST['room']; ?>">
</form>-->
</br>
<?php
if(isset($_POST['submit']))
{
?>
	<div style="border: solid 1px; width: 90%; margin: auto auto;">
		<?php
		$room = $mysql->real_escape_string($_POST['room']);
		$fromDate = date("Y-m-d", time()-(86400*$_POST['days']));
		echo "<center><h2 style='margin: 6px;'>" . $_POST['room'] . "</h2></center>";
		echo "<center><h3 style='margin: 4px;'>From " . $fromDate . " to " . date("Y-m-d") . "</h3></center>";
		if($query = $mysql->query("SELECT * FROM messages WHERE datetime > '".$fromDate."' AND room='".$room."'" . $params . " ORDER BY datetime DESC"))
		{
			$messageCount = 0;
			while($message = $query->fetch_array())
			{
				echo "<div style='text-align: left; padding: 10px;'>";
					echo "<form action='' method='POST' style='position: absolute; margin-left: -38px;'>";
						if($message['included'] == 1)
						{
							echo "<input type='submit' style='color: red; font-weight: bold; width: 26px;' value='E' name='excludeMessage'>";
						}
						else
						{
							echo "<input type='submit' style='color: green; font-weight: bold; width: 26px;' value='I' name='includeMessage'>";
						}
						echo "<input type='hidden' value='" . $message['messageId'] . "' name='messageId'>";
						foreach($_POST as $name=>$value)
						{
							echo "<input type='hidden' value='" . $value . "' name='" . $name . "'>";
						}
					echo "</form>";
					echo "<a style='color: rgb(10,20," . ($messageCount%2==0 ? "0" : "100") . ");'>" . $message['sender'] . "</a>" . "<a style='color: rgb(20,60," . ($messageCount%2==0 ? "0" : "100") . ");'>" . " [" . $message['datetime'] . "]: </a>" . "<a style='color: rgb(40,20," . ($messageCount%2==0 ? "0" : "100") . ");'>" . $message['message'] . "</a><br><br>";
				echo "</div";
				$messageCount++;
			}
		}
		else
		{
			echo $mysql->error;
		}
		?>
	</div>
<?php
}
?>