<?php
set_time_limit ("900");
require_once("../db_connect.php");
require_once("../PDFCreator.php");
?>

<form action="" method="GET">
  Filter: <input type='text' name='filter' value="<?php echo (isset($_GET['filter']) ? $_GET['filter'] : 'user:S5_ AND msg:profile'); ?>"><br>
  Iterations: <input type='number' min="1" name='iterations' value="<?php echo (isset($_GET['iterations']) ? $_GET['iterations'] : 100); ?>"><br>
  <input type='submit'>
</form>

<?php

if(isset($_GET['filter']) && isset($_GET['iterations']))
{
  $startTime = time();
  $esc_room = "Traders Helping Traders";
  $searchStr = $_GET['filter'];
  $cond = composeCondition($searchStr);
  $lower_headline = "Filter applied: " . $searchStr;
  $fromNow = 2592000;
  $iterations = $_GET['iterations'];
  echo "Room: " . $esc_room . "<br>";
  echo "Filter: " . $searchStr . "<br>";
  echo "PDF generated " . $iterations . " times.<br>";
  echo "Output file: <a href='test.pdf' target='_blank'>PDF</a><br>";
  echo "File to check time used: <a href='time.txt' target='_blank'>time.txt</a>";
  //echo $lower_headerline . "<br>";
  //echo $cond . "<br><br>";

  for($i=0; $i<$iterations; $i++)
  {
    $zone = date_default_timezone_get();
    date_default_timezone_set('America/Chicago');
    $issued = "Issued " . date("Y-m-d \\a\\t H:i") . " (Central Time)";
    $earliestMoment = date("Y-m-d H:i:s", time() - $fromNow);
    date_default_timezone_set($zone);
    $messages = array();
    //echo $issued . "<br>";
    //echo $earliestMoment . "<br>";
    if($query = $mysql->query("SELECT * FROM messages WHERE `datetime`>'".$earliestMoment."' && room='".$esc_room."' && (" . $cond . ") ORDER BY `datetime`"))
    {
      while($raw = $query->fetch_array())
      {
        $messages[] = array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']);
        //print_r(array("datetime" => $raw['datetime'], "sender" => $raw['sender'], "message" => $raw['message']));
        //echo "<br><br>";
      }
    }

    $headline = $room . " - Custom logs for the past " . (($fromNow > 86400) ? (round($fromNow/60/60/24) . " days") : (round($fromNow/60/60) . " hours"));
    $pdf = createPDF($roomName . "_custom.pdf", $issued, $room, $headline, $messages, $lower_headline);
    $pdf->Output("test.pdf", "F");
  }

  file_put_contents("time.txt", "Time used in total: " . (time() - $startTime) . " seconds.");

  echo "Time used in total: " . (time() - $startTime) . " seconds.";
}

?>
