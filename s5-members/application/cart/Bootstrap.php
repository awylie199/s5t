<?php

class Bootstrap_Cart extends Am_Module
{

    const
        UPLOAD_PREFIX = 'product-img-cart';
    const
        FRONT_ALL_PRODUCTS = 0;
    const
        FRONT_CATEGORIES = 1;
    const
        FRONT_PRODUCTS_FROM_CATEGORY = 2;

    /** @var Am_ShoppingCart */
    protected
        $cart;

    function onSavedFormTypes(Am_Event $event)
    {
        $event->getTable()->addTypeDef(array(
            'type' => SavedForm::T_CART,
            'title' => ___('Shopping Cart Signup'),
            'class' => 'Am_Form_Signup_Cart',
            'defaultTitle' => 'Create Customer Profile',
            'defaultComment' => 'shopping cart signup form',
            'isSingle' => true,
            'isSignup' => true,
            'noDelete' => true,
            'urlTemplate' => 'signup/cart',
        ));
    }

    public
        function deactivate()
    {
        unset($this->getDi()->session->cart);
        parent::deactivate();
    }

    public
        function onLoadSignupForm(Am_Event $event)
    {
        $di = $this->getDi();
        $type = $di->auth->getUserId() ? SavedForm::D_MEMBER : SavedForm::D_SIGNUP;
        $formRecord = $event->getReturn();
        if ($formRecord &&
            $formRecord->isDefault($type) &&
            $this->getConfig('redirect_to_cart'))
        {

            $di->response->redirectLocation(REL_ROOT_URL . '/cart');
        }
        if ($formRecord && ($formRecord->type == SavedForm::T_CART) && $di->session->cart && $di->session->cart->getItems() && $event->getRequest()->getParam('checkout'))
        {

            $formRecord->title = $title = $di->config->get('site_title') . ' : ' . ___('Checkout');
            $di->view->headLink()->appendStylesheet($di->view->_scriptCss('cart.css'));
            $di->blocks->add(new Am_Block("signup/login/before", ___("Your Cart"), "_basket", null, array($this, 'renderCheckout'), Am_Block::TOP));
        }
    }

    function renderCheckout()
    {
        $view = $this->getDi()->view;
        $view->assign('cart', $this->getCart());
        $view->assign('isBasket', false);
        $view->assign('paysystems', $this->getAvailablePaysystems($this->getCart()->getInvoice()));
        $view->display('cart/_checkout.phtml');
    }

    public
        function onAdminMenu(Am_Event $event)
    {
        $event->getMenu()->addPage(array(
            'id' => 'cart',
            'controller' => 'admin-shopping-cart',
            'action' => 'index',
            'module' => 'cart',
            'label' => ___('Shopping Cart'),
            'resource' => 'grid_carthtmlgenerate'
        ));
    }

    function onUserMenu(Am_Event $event)
    {
        if ($this->getDi()->config->get('cart.show_menu_cart_button'))
        {
            $menu = $event->getMenu();
            $menu->addPage(array(
                'id' => 'cart',
                'controller' => 'index',
                'module' => 'cart',
                'action' => 'index',
                'label' => ___($this->getDi()->config->get('cart.show_menu_cart_button_label', 'Shopping Cart')),
                'order' => 150,
            ));
            $page = $menu->findOneBy('id', 'add-renew');
            if ($page)
                $menu->removePage($page);
        }
    }

    function init()
    {
        parent::init();
        $this->getDi()->uploadTable->defineUsage(
            self::UPLOAD_PREFIX, 'product', 'img', UploadTable::STORE_FIELD, "Image for product: [%title%]", '/admin-products?_product_a=edit&_product_id=%product_id%'
        );
    }

    function checkPath($path, $grid)
    {
        if (!$path)
            return true;

        $record = $grid->getRecord();

        return !$this->getDi()->db->selectCell("SELECT COUNT(*) FROM ?_product WHERE path=? {AND product_id<>?}", $path, $record->isLoaded() ? $record->pk() : DBSIMPLE_SKIP );
    }

    function onGridProductInitGrid(Am_Event $event)
    {
        $event->getGrid()->setFormValueCallback('meta_robots', array('RECORD', 'unserializeList'), array('RECORD', 'serializeList'));
    }

    function onGridProductInitForm(Am_Event $event)
    {

        $form = $event->getGrid()->getForm();

        $fs = $form->addAdvFieldset('cart')
            ->setLabel(___('Shopping Cart'));

        $fs->addUpload('img', null, array('prefix' => self::UPLOAD_PREFIX))
            ->setLabel(___("Product Picture\n" .
                    'for shopping cart pages. Only jpg, png and gif formats allowed'))
            ->setAllowedMimeTypes(array(
                'image/png', 'image/jpeg', 'image/gif',
        ));

        $fs->addText('path', array('class' => 'el-wide'))
            ->setId('product-path')
            ->setLabel(___("Path\n" .
                    'will be used to construct user-friendly url, in case of you ' .
                    'leave it empty aMember will use id of this product to do it'))
            ->addRule('callback', ___('Path should be unique across all products'), array(
                'callback' => array($this, 'checkPath'),
                'arguments' => array($event->getGrid())));

        $root_url = Am_Html::escape(Am_Di::getInstance()->config->get('root_url'));

        $fs->addStatic()
            ->setLabel(___('Permalink'))
            ->setContent(<<<CUT
<div data-root_url="$root_url" id="product-permalink"></div>
CUT
        );

        $fs->addScript()
            ->setScript(<<<CUT
jQuery('#product-path').bind('keyup', function(){
    jQuery('#product-permalink').closest('.row').toggle(jQuery(this).val() != '');
    jQuery('#product-permalink').html(jQuery('#product-permalink').data('root_url') + '/product/' + encodeURIComponent(jQuery(this).val()).replace(/%20/g, '+'))
}).trigger('keyup')
CUT
        );

        $fs->addText('tags', array('class' => 'el-wide', 'id' => 'cart-tags'))
            ->setLabel(___("Tags\n" .
                    "comma separated list of tags"));

        $fs->addScript()
            ->setScript(<<<CUT
jQuery(function() {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
    jQuery( "#cart-tags" )
       // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === jQuery.ui.keyCode.TAB &&
            jQuery( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          jQuery.getJSON(window.rootUrl + "/cart/admin-shopping-cart/autocomplete", {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  });
CUT
        );

        $fs->addHtmlEditor('cart_description', null, array('showInPopup' => true))
            ->setLabel(___("Product Description\n" .
                    'displayed on the shopping cart page'));

        $fs = $form->addAdvFieldset('meta', array('id' => 'meta'))
            ->setLabel(___('Meta Data'));

        $fs->addText('meta_title', array('class' => 'el-wide'))
            ->setLabel(___('Title'));

        $fs->addText('meta_keywords', array('class' => 'el-wide'))
            ->setLabel(___('Keywords'));

        $fs->addText('meta_description', array('class' => 'el-wide'))
            ->setLabel(___('Description'));

        $gr = $fs->addGroup()->setLabel(___("Robots\n" .
                "instructions for search engines"));
        $gr->setSeparator(' ');
        $gr->addCheckbox('meta_robots[]', array('value' => 'noindex'), array('content' => 'noindex'));
        $gr->addCheckbox('meta_robots[]', array('value' => 'nofollow'), array('content' => 'nofollow'));
        $gr->addCheckbox('meta_robots[]', array('value' => 'noarchive'), array('content' => 'noarchive'));
        $gr->addFilter('array_filter');
    }

    public
        function onGridProductValuesToForm(Am_Event_Grid $e)
    {
        $v = $e->getArg(0);
        if (isset($v['tags']))
        {
            $v['tags'] = trim($v['tags'], ',');
            $e->setArg(0, $v);
        }
    }

    function onGridProductValuesFromForm(Am_Event $e)
    {
        $vars = $e->getArg(0);
        if (!$vars['path'])
            $vars['path'] = null;

        $tags = array_map('trim', explode(',', $vars['tags']));
        sort($tags, SORT_STRING);
        $tags = implode(',', $tags);
        $vars['tags'] = $tags ? sprintf(',%s,', $tags) : null;

        $e->setArg(0, $vars);
    }

    function onGridProductAfterSave(Am_Event $event)
    {
        $product = $event->getGrid()->getRecord();
        $vars = $event->getGrid()->getForm()->getValue();

        if (empty($vars['img']))
        {
            $product->img = null;
            $product->img_path = null;
            $product->img_cart_path = null;
            $product->img_detail_path = null;
            $product->img_orig_path = null;
            $product->update();
            return;
        }

        $sizes = array(
            'img' => array(
                'w' => $this->getConfig('product_image_width', 200),
                'h' => $this->getConfig('product_image_height', 200),
                't' => Am_Image::RESIZE_CROP
            ),
            'img_cart' => array(
                'w' => $this->getConfig('img_cart_width', 50),
                'h' => $this->getConfig('img_cart_height', 50),
                't' => Am_Image::RESIZE_CROP
            ),
            'img_detail' => array(
                'w' => $this->getConfig('img_detail_width', 400),
                'h' => $this->getConfig('img_detail_height', 400),
                't' => Am_Image::RESIZE_GIZMO
            )
        );

        if ($product->img)
        {
            $upload = $this->getDi()->uploadTable->load($product->img);
            if ($upload->prefix != self::UPLOAD_PREFIX)
                throw new Am_Exception_InputError('Incorrect prefix requested [%s]', $upload->prefix);

            switch ($upload->getType())
            {
                case 'image/gif' :
                    $ext = 'gif';
                    break;
                case 'image/png' :
                    $ext = 'png';
                    break;
                case 'image/jpeg' :
                    $ext = 'jpg';
                    break;
                default :
                    throw new Am_Exception_InputError(sprintf('Unknown MIME type [%s]', $mime));
            }


            $name = str_replace('.' . self::UPLOAD_PREFIX . '.', '', $upload->path);
            $filename = $upload->getFullPath();

            $image = new Am_Image($upload->getFullPath(), $upload->getType());

            foreach ($sizes as $id => $size)
            {
                $newName = 'cart/' . $size['w'] . '_' . $size['h'] . '/' . $name . '.jpg';
                $newFilename = ROOT_DIR . '/data/public/' . $newName;

                if (!file_exists($newFilename))
                {
                    if (!is_dir(dirname($newFilename)))
                    {
                        mkdir(dirname($newFilename), 0777, true);
                    }

                    $i = clone $image;
                    $i->resize($size['w'], $size['h'], $size['t'])->save($newFilename);
                }
                $product->{$id . '_path'} = $newName;
            }

            $newOrigName = 'cart/orig/' . $name . '.' . $ext;
            $newOrigFilename = ROOT_DIR . '/data/public/' . $newOrigName;
            if (!file_exists($newOrigFilename))
            {
                if (!is_dir(dirname($newOrigFilename)))
                {
                    mkdir(dirname($newOrigFilename), 0777, true);
                }
                copy($filename, $newOrigFilename);
                $product->img_orig_path = $newOrigName;
            }

            $product->update();
        }
    }

    function onGetUploadPrefixList(Am_Event $event)
    {
        $event->addReturn(array(
            Am_Upload_Acl::IDENTITY_TYPE_ADMIN => Am_Upload_Acl::ACCESS_ALL,
            Am_Upload_Acl::IDENTITY_TYPE_USER => Am_Upload_Acl::ACCESS_READ,
            Am_Upload_Acl::IDENTITY_TYPE_ANONYMOUS => Am_Upload_Acl::ACCESS_READ
            ), self::UPLOAD_PREFIX);
    }

    function onDbUpgrade(Am_Event $e)
    {
        if (version_compare($e->getVersion(), '4.2.16') < 0)
        {
            $nDir = opendir(ROOT_DIR . '/data/');
            $baseDir = ROOT_DIR . '/data/';
            while (false !== ( $file = readdir($nDir) ))
                if (preg_match('/^.' . self::UPLOAD_PREFIX . '.*$/', $file, $matches) && !file_exists($baseDir . 'public/' . $matches[0] . ".png"))
                    if (!@copy($baseDir . $matches[0], $baseDir . 'public/' . $matches[0] . ".png"))
                        echo sprintf('<span style="color:red">Could not copy file [%s] to [%s]. Please, copy and rename manually.</span><br />', $baseDir . $matches[0], $baseDir . 'public/' . $matches[0] . ".png");

            closedir($nDir);
            $this->getDi()->db->query("
                UPDATE ?_product
                SET img_path = CONCAT(img_path,'.png')
                WHERE
                    img IS NOT NULL
                    AND img_path NOT LIKE '%.png'
                    AND img_path NOT LIKE '%.jpg'
                    AND img_path NOT LIKE '%.jpeg'
                    AND img_path NOT LIKE '%.gif'
            ");
        }
    }

    public
        function onInitFinished()
    {
        $router = $this->getDi()->router;
        ;
        $router->addRoute('cart-product', new Am_Mvc_Router_Route(
            'product/:path', array(
            'module' => 'cart',
            'controller' => 'index',
            'action' => 'product'
            )
        ));
        $router->addRoute('cart-tag', new Am_Mvc_Router_Route(
            'tag/:tag', array(
            'module' => 'cart',
            'controller' => 'index',
            'action' => 'tag'
            )
        ));
        $router->addRoute('cart-checkout', new Am_Mvc_Router_Route(
            'signup/cart/checkout', array(
            'module' => 'default',
            'controller' => 'signup',
            'c' => 'cart',
            'action' => 'index',
            'checkout' => 1
            )
        ));
    }

    /**
     * Get Enabled paysystems that could be used for invoice;
     * @param Invoice $invoice
     * @return Array 
     */
    function getAvailablePaysystems(Invoice $invoice)
    {
        $paysys = array();
        if ($paysystems = $this->getConfig('paysystems', array()))
        {
            if (!in_array('free', $paysystems))
                $paysystems[] = 'free';
            foreach ($paysystems as $paysystem_id)
            {
                try
                {
                    $ps = $this->getDi()->paysystemList->get($paysystem_id);
                }
                catch (Exception $e)
                {
                    $this->getDi()->errorLogTable->logException($e);
                    continue;
                }
                if ($ps)
                {//it is enabled now
                    $plugin = $this->getDi()->plugins_payment->get($ps->paysys_id);
                    if (!($err = $plugin->isNotAcceptableForInvoice($invoice)))
                    {
                        $paysys[$ps->getId()] = $ps;
                    }
                }
            }
        }
        //fault tolerance: if we did not find any enabled plugin that was configured
        //just fall back to default behaviour
        if (!$paysys)
        {
            foreach ($this->getDi()->paysystemList->getAll() as $ps)
            {
                $plugin = $this->getDi()->plugins_payment->get($ps->paysys_id);
                if (!($err = $plugin->isNotAcceptableForInvoice($invoice)))
                {
                    $paysys[$ps->getId()] = $ps;
                }
            }
        }
        return $paysys;
    }

    public
        function loadCart()
    {
        $this->cart = @$this->getSession()->cart;
        if ($this->cart && $this->cart->getInvoice()->isCompleted())
            $this->cart = null;
        if (!$this->cart)
        {
            $this->cart = new Am_ShoppingCart($this->getDi()->invoiceRecord);
            /** @todo not serialize internal data in Invoice class */
            $this->getSession()->cart = $this->cart;
        }
        if ($this->getDi()->auth->getUserId())
            $this->cart->setUser($this->getDi()->user);
        $this->cart->getInvoice()->calculate();
    }

    function getCart()
    {
        if (!isset($this->cart))
            $this->loadCart();
        return $this->cart;
    }

    function getSession()
    {
        return $this->getDi()->session;
    }

}
