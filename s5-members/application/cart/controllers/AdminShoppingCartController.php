<?php

require_once APPLICATION_PATH . '/default/controllers/AdminSetupController.php';

class Cart_AdminShoppingCartController extends Am_Mvc_Controller_Pages
{
    public function checkAdminPermissions(Admin $admin)
    {
        return $admin->hasPermission(Am_Auth_Admin::PERM_SETUP);
    }

    public function initPages()
    {
        $this->addPage(array($this, 'configCartController'), 'cart', ___('Shopping Cart Settings'))
            ->addPage(array($this, 'createButtonController'), 'button', ___('Button/Link HTML Code'))
            ->addPage(array($this, 'createBasketController'), 'basket', ___('Basket HTML Code'));
    }

    public function configCartController($id, $title, Am_Mvc_Controller $controller)
    {
        return new AdminShoppingCart_Settings($controller->getRequest(), $controller->getResponse(), $this->_invokeArgs);
    }

    public function createButtonController($id, $title, Am_Mvc_Controller $controller)
    {
        return new AdminCartHtmlGenerateController_Button($controller->getRequest(), $controller->getResponse(), $this->_invokeArgs);
    }

    public function createBasketController($id, $title, Am_Mvc_Controller $controller)
    {
        return new AdminCartHtmlGenerateController_Basket($controller->getRequest(), $controller->getResponse(), $this->_invokeArgs);
    }

    public function autocompleteAction()
    {
        $term = $this->getParam('term');
        if (!$term)
            return null;

        $ret = array();
        $tags = $this->getDi()->db->selectCell('SELECT GROUP_CONCAT(tags) FROM ?_product');
        $tags = array_filter(array_unique(explode(',', $tags)));
        foreach ($tags as $tag) {
            if (strpos($tag, $term)!==false) {
                $ret[] = array(
                    'label' => $tag,
                    'value' => $tag
                );
            }
        }
        $this->_response->ajaxResponse($ret);
    }
}

class AdminShoppingCart_Settings extends AdminSetupController
{

    public function indexAction()
    {
        $this->_request->setParam('page', 'cart');

        $this->p = filterId($this->_request->getParam('page'));
        $this->initSetupForms();
        $this->form = $this->getForm($this->p, false);
        $this->form->prepare();
        if ($this->form->isSubmitted()) {
            $this->form->setDataSources(array($this->_request));
            if ($this->form->validate() && $this->form->saveConfig()) {
                $this->_response->redirectLocation($this->getUrl());
            }
        } else {
            $this->form->setDataSources(array(
                new HTML_QuickForm2_DataSource_Array($this->getConfigValues()),
                new HTML_QuickForm2_DataSource_Array($this->form->getDefaults()),
            ));
        }
        $this->view->assign('p', $this->p);
        $this->form->replaceDotInNames();

        $this->view->assign('pageObj', $this->form);
        $this->view->assign('form', $this->form);
        $this->view->display('admin/cart/config.phtml');
    }
}

class AdminCartHtmlGenerateController_Button extends Am_Mvc_Controller
{

    public function checkAdminPermissions(Admin $admin)
    {
        return $admin->hasPermission(Am_Auth_Admin::PERM_SETUP);
    }

    public function indexAction()
    {
        if ($this->getRequest()->getParam('title') && $this->getRequest()->getParam('actionType')) {
            $productIds = $this->getRequest()->getParam('productIds');
            $prIds = $productIds ?
                implode(',', $productIds) :
                implode(',', array_keys($this->getDi()->productTable->getOptions(true)));

            $htmlcode = '
<!-- Button/Link for aMember Shopping Cart -->
<script type="text/javascript">
if (typeof cart  == "undefined")
    document.write("<scr" + "ipt src=\'' . REL_ROOT_URL . '/application/cart/views/public/js/cart.js\'></scr" + "ipt>");
</script>
';
            if ($this->getRequest()->getParam('isLink')) {
                $htmlcode .= '<a href="#" onclick="cart.' . $this->getRequest()->getParam('actionType') . '(this,' . $prIds . '); return false;" >' . $this->getRequest()->getParam('title') . '</a>';
            } else {
                $htmlcode .= '<input type="button" onclick="cart.' . $this->getRequest()->getParam('actionType') . '(this,' . $prIds . '); return false;" value="' . $this->getRequest()->getParam('title') . '">';
            }
            $htmlcode .= '
<!-- End Button/Link for aMember Shopping Cart -->
';

            $this->view->assign('htmlcode', $htmlcode);
            $this->view->display('admin/cart/button-code.phtml');
        } else {
            $form = new Am_Form_Admin();

            $form->addMagicSelect('productIds')
                ->setLabel(___('Select Product(s)
if nothing selected - all products'))
                ->loadOptions($this->getDi()->productTable->getOptions());

            $form->addSelect('isLink')
                ->setLabel(___('Select Type of Element'))
                ->loadOptions(array(
                    0 => 'Button',
                    1 => 'Link',
                ));

            $form->addSelect('actionType')
                ->setLabel(___('Select Action of Element'))
                ->loadOptions(array(
                    'addExternal' => ___('Add to Basket only'),
                    'addBasketExternal' => ___('Add & Go to Basket'),
                    'addCheckoutExternal' => ___('Add & Checkout'),
                ));

            $form->addText('title')
                ->setLabel(___('Title of Element'))
                ->addRule('required');

            $form->addSaveButton(___('Generate'));

            $this->view->assign('form', $form);
            $this->view->display('admin/cart/button-code.phtml');
        }
    }
}

class AdminCartHtmlGenerateController_Basket extends Am_Mvc_Controller
{
    public function checkAdminPermissions(Admin $admin)
    {
        return $admin->hasPermission(Am_Auth_Admin::PERM_SETUP);
    }

    public function indexAction()
    {
        $htmlcode = '
<!-- Basket for aMember Shopping Cart -->
<script type="text/javascript">
if (typeof(cart) == "undefined")
    document.write("<scr" + "ipt src=\'' . REL_ROOT_URL . '/application/cart/views/public/js/cart.js\'></scr" + "ipt>");
</script>
<script type="text/javascript">
    jQuery(function(){cart.loadOnly();});
</script>
<div class="am-basket-preview"></div>
<!-- End Basket for aMember Shopping Cart -->
';
        $this->view->assign('htmlcode', $htmlcode);
        $this->view->display('admin/cart/basket-code.phtml');
    }
}

class Am_Form_Setup_Cart extends Am_Form_Setup
{
    public function __construct()
    {
        parent::__construct('cart');
        $this->setTitle(___('Shopping Cart'));
    }

    public function initElements()
    {
        $options = Am_Di::getInstance()->paysystemList->getOptions();
        unset($options['free']);
        $this->addSortableMagicSelect('cart.paysystems')
            ->setLabel(___("Payment Options\n" .
                'if none selected, all enabled will be displayed'))
            ->loadOptions($options);

        $this->addAdvCheckbox('cart.use_coupons')
            ->setLabel(___("Enable use of coupons\n".
                "allow use coupons on shopping cart checkout page"));

        $this->addSelect('cart.category_id')->setLabel(___("Category\n" .
                    "root category of hierarchy which included to shopping cart\n" .
                    "all categories is included by default"))
                ->loadOptions(array('' => '-- ' . ___('Root') . ' --') + Am_Di::getInstance()->productCategoryTable->getAdminSelectOptions());

        $this->addAdvCheckbox('cart.redirect_to_cart')
            ->setLabel(___('Redirect Default Signup Page to Cart'));

        $gr = $this->addGroup()
                ->setLabel(___("Hide 'Add/Renew Subscription' tab (User Menu)\n" .
                        "and show 'Shopping Cart' tab instead"));

        $gr->addAdvCheckbox('cart.show_menu_cart_button')
            ->setId('show_menu_cart_button');

        $gr->addText('cart.show_menu_cart_button_label')
            ->setId('show_menu_cart_button_label');
        $this->setDefault('cart.show_menu_cart_button_label', 'Shopping Cart');

        $this->addScript()
            ->setScript(<<<CUT

jQuery('#show_menu_cart_button').change(function(){
    jQuery('#show_menu_cart_button_label').toggle(this.checked);
}).change();
CUT
        );

        $this->addAdvCheckbox('cart.require_login')
            ->setLabel(___('Allow use Cart only to Registered Users'));

        $fs = $this->addAdvFieldset('img')
            ->setLabel(___('Product Image'));

        $imgSize = $fs->addGroup()
                ->setLabel(___("List View\nWidth × Height"));

        $imgSize->addText('cart.product_image_width', array('size' => 3, 'placeholder' => 200))
            ->addRule('regex', ___('Image width must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');
        $imgSize->addHtml()
            ->setHtml(' &times; ');
        $imgSize->addText('cart.product_image_height', array('size' => 3, 'placeholder' => 200))
            ->addRule('regex', ___('Image height must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');

        $imgSize = $fs->addGroup()
                ->setLabel(___("Detail View\nWidth × Height"));

        $imgSize->addText('cart.img_detail_width', array('size' => 3, 'placeholder' => 400))
            ->addRule('regex', ___('Image width must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');
        $imgSize->addHtml()
            ->setHtml(' &times; ');
        $imgSize->addText('cart.img_detail_height', array('size' => 3, 'placeholder' => 400))
            ->addRule('regex', ___('Image height must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');

        $imgSize = $fs->addGroup()
                ->setLabel(___("Cart View\nWidth × Height"));

        $imgSize->addText('cart.img_cart_width', array('size' => 3, 'placeholder' => 50))
            ->addRule('regex', ___('Image width must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');
        $imgSize->addHtml()
            ->setHtml(' &times; ');
        $imgSize->addText('cart.img_cart_height', array('size' => 3, 'placeholder' => 50))
            ->addRule('regex', ___('Image height must be number greater than %d', 0), '/^$|^[1-9](\d+)?$/');

        $fs = $this->addAdvFieldset('layout')
                ->setLabel(___('Layout'));

        $this->setDefault('cart.layout', 0);

        $gr = $fs->addGroup();
        $gr->setSeparator('<br />');
        $gr->setLabel(___('Front Page'));
        $gr->addAdvRadio('cart.front')
            ->loadOptions(array(
               Bootstrap_Cart::FRONT_ALL_PRODUCTS => ___('All Products List'),
               Bootstrap_Cart::FRONT_CATEGORIES => ___('Categories List'),
               Bootstrap_Cart::FRONT_PRODUCTS_FROM_CATEGORY => ___('Products List From Category')
            ));
        $gr->addSelect('cart.front_category_id', array('id' => 'cart-front-category_id'))
            ->loadOptions(Am_Di::getInstance()->productCategoryTable->getAdminSelectOptions());

        $v = json_encode(Bootstrap_Cart::FRONT_PRODUCTS_FROM_CATEGORY);
        $this->addScript()
            ->setScript(<<<CUT
jQuery(function(){
    jQuery('[type=radio][name$=front]').change(function(){
        jQuery('#cart-front-category_id').toggle(jQuery('[type=radio][name$=front]:checked').val() == $v);
    }).change();
});
CUT
                );

        $this->setDefault('cart.front', Bootstrap_Cart::FRONT_ALL_PRODUCTS);

        $fs->addAdvRadio('cart.add_to_basket_action')
            ->setLabel(___('Add To Basket Button Behaviour'))
            ->loadOptions(array(
                'add' => ___('Add to Basket'),
                'addAndCheckout' => ___('Add to Basket and Checkout'),
                'addAndPopupBasket' => ___('Add to Basket and Popup Basket')
            ));
        $this->setDefault('cart.add_to_basket_action', 'add');

        $fs->addAdvCheckbox('cart.layout_no_quick_order')
            ->setLabel(___('Hide Quick Order Button'));

        $fs->addAdvRadio('cart.layout')
            ->setLabel(___('Layout'))
            ->loadOptions(array(
                0 => ___('One Column'),
                1 => ___('Two Columns')
            ));

        $fs->addAdvCheckbox('cart.layout_no_category')
            ->setLabel(___('Hide Choose Category Widget'));

        $fs->addAdvCheckbox('cart.layout_no_search')
            ->setLabel(___('Hide Product Search Widget'));

        $fs->addAdvCheckbox('cart.layout_no_auth')
            ->setLabel(___('Hide Authentication Widget'));

        $fs->addAdvCheckbox('cart.layout_no_basket')
            ->setLabel(___('Hide Your Basket Widget'));
        $fs->addAdvCheckbox('cart.layout_no_tags')
            ->setLabel(___('Hide Tags Widget'));

        $fs->addText('cart.records_per_page', array('size' => 3))
            ->setLabel(___('Products per Page'));

        $this->setDefault('cart.records_per_page', Am_Di::getInstance()->config->get('admin.records-on-page', 10));
    }
}