<?php

/**
 *     Author: Alex Scott
 *      Email: alex@cgi-central.net
 *        Web: http://www.cgi-central.net
 *    Details: Signup Page
 *    FileName $RCSfile$
 *    Release: 5.1.8 ($Revision: 4867 $)
 *
 * Please direct bug reports,suggestions or feedback to the cgi-central forums.
 * http://www.cgi-central.net/forum/
 *
 * aMember PRO is a commercial software. Any distribution is strictly prohibited.
 *
 */

class Cart_IndexController extends Am_Mvc_Controller
{
    /** @var Am_Query */
    protected $query;
    protected $hiddenCatCodes = array();

    public function preDispatch()
    {
        if ($this->getModule()->getConfig('require_login'))
            $this->getDi()->auth->requireLogin(REL_ROOT_URL . '/cart');
    }

    public function init()
    {
        parent::init();
        $this->view->cart = $this->getCart();

        $cc = $this->getCategoryCode();
        $this->view->cc = $cc;

        $cats = explode(',', $this->getRequest()->getCookie('am-cart-cats', ''));
        if (!in_array($cc, $cats)) {
            $cats[] = $cc;
            Am_Cookie::set('am-cart-cats', implode(',', $cats));
        }
        $this->hiddenCatCodes = $cats;

        $this->view->productCategorySelected = $this->getCategoryCode();
        $this->view->productCategoryOptions = array(null => ___('-- Home --')) +
            $this->getDi()->productCategoryTable->getUserSelectOptions(array(
                ProductCategoryTable::EXCLUDE_EMPTY => true,
                ProductCategoryTable::COUNT => true,
                ProductCategoryTable::EXCLUDE_HIDDEN => true,
                ProductCategoryTable::INCLUDE_HIDDEN => $this->getHiddenCatCodes(),
                ProductCategoryTable::ROOT => $this->getModule()->getConfig('category_id', null)
                )
        );
        $this->view->productCategories = array();
        foreach ($this->getDi()->productCategoryTable->findBy() as $cat) {
            $this->view->productCategories[$cat->pk()] = $cat;
        }

        if (!$this->getModule()->getConfig('layout_no_category')) {
            $this->getDi()->blocks->add(
                new Am_Block('cart/right', ___('Category'), 'cart-category', $this->getModule(), 'category.phtml', Am_Block::TOP)
            );
        }

        if (!$this->getModule()->getConfig('layout_no_search')) {
            $this->getDi()->blocks->add(
                new Am_Block('cart/right', ___('Search Products'), 'cart-search', $this->getModule(), 'search.phtml', Am_Block::TOP)
            );
        }
        if (!$this->getModule()->getConfig('layout_no_basket')) {
            $this->getDi()->blocks->add(
                new Am_Block('cart/right', ___('Your Basket'), 'cart-basket', $this->getModule(), 'basket.phtml', Am_Block::TOP)
            );
        }
        if (!$this->getModule()->getConfig('layout_no_auth')) {
            $this->getDi()->blocks->add(
                new Am_Block('cart/right', ___('Authentication'), 'cart-auth', $this->getModule(), 'auth.phtml')
            );
        }
        if (!$this->getModule()->getConfig('layout_no_tags') && ($tags = $this->getAllTags())) {
            $this->getDi()->blocks->add(
                new Am_Block('cart/right', ___('Tags'), 'cart-tags', $this->getModule(), function(Am_View $v) use ($tags) {
                    $v->tags = $tags;
                    return $v->render('blocks/tags.phtml');
                })
            );
        }
    }

    public function indexAction()
    {
        $this->view->category = $category = $this->loadCategory();

        if ($category) {
            $this->view->header = $category->title;
            $this->view->description = $category->description;
        }

        if (!$category && $this->getModule()->getConfig('front') == Bootstrap_Cart::FRONT_PRODUCTS_FROM_CATEGORY) {
            $category = $this->getDi()->productCategoryTable->load($this->getModule()->getConfig('front_category_id'));
        }

        if (!$category && $this->getModule()->getConfig('front') == Bootstrap_Cart::FRONT_CATEGORIES) {
            $this->view->display('cart/index-categories.phtml');
            return;
        }

        $query = $this->getProductsQuery($category);

        $count = $this->getModule()->getConfig('records_per_page', $this->getConfig('records-on-page', 10));
        $page = $this->getInt('cp');
        $this->view->products = $query->selectPageRecords($page, $count);
        $total = $query->getFoundRows();
        $pages = floor($query->getFoundRows() / $count);
        if ($pages * $count < $total) {
            $pages++;
        }
        $this->view->paginator = new Am_Paginator($pages, $page, null, 'cp');

        $this->view->display('cart/index.phtml');
    }

    public function tagAction()
    {
        $category = $this->loadCategory();
        $this->view->category = $category;

        $tag = $this->getParam('tag');
        $query = $this->getProductsQuery();
        $query->addWhere('tags LIKE ?', "%,$tag,%");

        $count = $this->getModule()->getConfig('records_per_page', $this->getConfig('records-on-page', 10));
        $page = $this->getInt('cp');
        $this->view->products = $query->selectPageRecords($page, $count);
        $total = $query->getFoundRows();
        $pages = floor($query->getFoundRows() / $count);
        if ($pages * $count < $total) {
            $pages++;
        }
        $this->view->paginator = new Am_Paginator($pages, $page, null, 'cp');

        $this->view->display('cart/index.phtml');
    }

    public function productAction()
    {
        $id = null;
        if ($path = $this->getParam('path')) {
            if ($product = $this->getDi()->productTable->findFirstByPath($path)) {
                $id = $product->pk();
            }
        }

        $id = $id ?: $this->getInt('id');
        if ($id <= 0)
            throw new Am_Exception_InputError("Invalid product id specified [$id]");

        $this->view->category = $category = $this->loadCategory();

        $query = $this->getProductsQuery($category);
        $query->addWhere("p.product_id=?d", $id);
        $productsFound = $query->selectPageRecords(0, 1);
        if (!$productsFound)
            throw new Am_Exception_InputError("Product #[$id] not found (category code [" . $this->getCategoryCode() . "])");
        $product = current($productsFound);

        $this->view->meta_title = $product->meta_title;
        if ($product->meta_keywords)
            $this->view->headMeta()->setName('keywords', $product->meta_keywords);
        if ($product->meta_description)
            $this->view->headMeta()->setName('description', $product->meta_description);
        if ($product->meta_robots)
            $this->view->headMeta()->setName('robots', $product->meta_robots);

        $this->view->assign('product', $product);
        $this->view->display('cart/product.phtml');
    }

    public function searchAction()
    {
        $this->view->header = ___('Search Results');
        if ($q = $this->getEscaped('q')) {
            $query = $this->getProductsQuery(null);
            $query->addWhere("p.title LIKE ? OR p.description LIKE ? OR p.cart_description LIKE ?", "%$q%", "%$q%", "%$q%");
        }

        $count = $this->getModule()->getConfig('records_per_page', $this->getConfig('records-on-page', 10));
        $page = $this->getInt('cp');
        $this->view->products = $query->selectPageRecords($page, $count);
        $total = $query->getFoundRows();
        $pages = floor($query->getFoundRows() / $count);
        if ($pages * $count < $total) {
            $pages++;
        }
        $this->view->paginator = new Am_Paginator($pages, $page, null, 'cp');
        $this->view->display('cart/index.phtml');
    }

    public function viewBasketAction()
    {
        if ($this->getParam('do-return')) {
            if ($this->getParam('b'))
                return $this->_response->redirectLocation($this->getParam('b'));
            return $this->_redirect('cart');
        }
        $d = (array) $this->getParam('d', array());
        $qty = (array) $this->getParam('qty', array());
        foreach ($qty as $item_id => $newQty) {
            if ($item = $this->getCart()->getInvoice()->findItem('product', intval($item_id)))
                if ($item->is_countable && $item->variable_qty) {
                    if ($newQty == 0) {
                        $this->getCart()->getInvoice()->deleteItem($item);
                    } else {
                        $item->qty = 0;
                        $item->add($newQty);
                    }
                }
        }
        foreach ($d as $item_id => $val)
            if ($item = $this->getCart()->getInvoice()->findItem('product', intval($item_id)))
                $this->getCart()->getInvoice()->deleteItem($item);
        if (($code = $this->getParam('coupon')) !== null)
            $this->view->coupon_error = $this->getCart()->setCouponCode($code);
        if ($this->getDi()->auth->getUserId())
            $this->getCart()->setUser($this->getDi()->user);
        $this->getCart()->calculate();
        if (!$this->view->coupon_error && $this->getParam('do-checkout'))
            return $this->checkoutAction();
        $this->getDi()->blocks->remove('cart-basket');
        $this->view->isAjax = $this->getRequest()->isXmlHttpRequest();
        $this->view->b = $this->getParam('b', '');
        $this->view->display('cart/basket.phtml');
    }

    public function addAndCheckoutAction()
    {
//        $this->addFromRequest();
        $this->checkoutAction();
    }

    public function checkoutAction()
    {
        if($this->getFiltered('a')!= 'checkout') $this->getCart()->getInvoice()->paysys_id=null;
        return $this->doCheckout();
    }



    public function setPaysysAction(){
        try{
            $paysystems = $this->getModule()->getAvailablePaysystems($this->getCart()->getInvoice());

            if (!$paysystems)
                throw new Am_Exception_InternalError("Sorry, no payment plugins enabled to handle this invoice");
            if ($paysys_id = $this->getFiltered('paysys_id'))
            {
                if (!in_array($paysys_id, array_keys($paysystems)))
                    throw new Am_Exception_InputError("Sorry, paysystem [$paysys_id] is not available for this invoice");


                $this->getCart()->getInvoice()->setPaysystem($paysys_id);
                $this->getDi()->response->ajaxResponse(array('ok'=>true));
            }
        }
        catch(Exception $e)
        {
            $this->getDi()->response->ajaxResponse(array('ok'=>false, 'error'=>$e->getMessage()));
        }
    }
    public function choosePaysysAction()
    {
        $this->view->paysystems = $this->getModule()->getAvailablePaysystems($this->getCart()->getInvoice());

        if (count($this->view->paysystems) == 1) {
            $firstps = array_shift($this->view->paysystems);
            $this->getCart()->getInvoice()->setPaysystem($firstps->getId());
            return $this->doCheckout();
        }
        $this->view->display('cart/checkout.phtml');
    }

    public function loginAction()
    {
        return $this->_response->redirectLocation(REL_ROOT_URL . '/login?saved_form=cart&_amember_redirect_url=' . base64_encode($this->view->url()));
    }

    public function ajaxAddAction()
    {
        $this->addFromRequest();
        $this->view->display('blocks/basket.phtml');
    }

    public function ajaxAddOnlyAction()
    {
        $this->addFromRequest();
    }

    public function ajaxRemoveOnlyAction()
    {
        $data = json_decode($this->getParam('data'));
        try {
            if (!$data)
                throw new Am_Exception_InternalError(___('Shopping Cart Module. No data input'));

            foreach ($data as $item) {
                $item_id = intval($item->id);
                if (!$item_id)
                    throw new Am_Exception_InternalError(___('Shopping Cart Module. No product id input'));

                if ($i = $this->getCart()->getInvoice()->findItem('product', intval($item_id)))
                    $this->getCart()->getInvoice()->deleteItem($i);
            }

            if ($this->getDi()->auth->getUserId())
                $this->getCart()->setUser($this->getDi()->user);
            $this->getCart()->calculate();
        } catch (Exception $e) {
            $this->getDi()->errorLogTable->logException($e);
            $this->_response->ajaxResponse(
                array(
                    'status' => 'error',
                    'message' => $e->getPublicError()
            ));
            return;
        }
        $this->_response->ajaxResponse(array('status' => 'ok'));


    }

    public function ajaxLoadOnlyAction()
    {
        $this->view->display('blocks/basket.phtml');
    }

    public function getProductsQuery(ProductCategory $category = null)
    {
        if (!$this->query) {
            $scope = false;
            if($root = $this->getModule()->getConfig('category_id', null)) {
                $scope = array_merge($this->getDi()->productCategoryTable->getSubCategoryIds($root), array($root));
            }

            $this->query = $this->getDi()->productTable->createQuery($category ? $category->pk() : null, $this->getHiddenCatCodes(), $scope);
            if($user = $this->getDi()->auth->getUser()) {
                $products = $this->query->selectAllRecords();

                $filtered = $this->getDi()->productTable->filterProducts(
                        $products,
                        $user->getActiveProductIds(),
                        $user->getExpiredProductIds(),
                        true);

                $hide_pids = array_diff(
                        array_map(function($p){return $p->pk();}, $products),
                        array_map(function($p){return $p->pk();}, $filtered));

                if(!empty($hide_pids)) {
                    $this->query->addWhere('p.product_id NOT IN (?a)', $hide_pids);
                }
            }

            $e = new Am_Event('cartGetProductsQuery', array('query' => $this->query));
            $this->getDi()->hook->call($e);
        }

        return $this->query;
    }

    public function addFromRequest()
    {
        //data = [{id:id,qty:qty,plan:plan,type:type},{}...]
        $data = json_decode($this->getParam('data'));
        try {
            if (!$data)
                throw new Am_Exception_InternalError(___('Shopping Cart Module. No data input'));

            foreach ($data as $item) {
                $item_id = intval($item->id);
                if (!$item_id)
                    throw new Am_Exception_InternalError(___('Shopping Cart Module. No product id input'));
                $qty = (!empty($item->qty) && $q = intval($item->qty)) ? $q : 1;

                $p = $this->getDi()->productTable->load($item_id);
                if (!empty($item->plan) && $bp = intval($item->plan))
                    $p->setBillingPlan($bp);
                $this->getCart()->addItem($p, $qty);
            }

            if ($this->getDi()->auth->getUserId())
                $this->getCart()->setUser($this->getDi()->user);
            $this->getCart()->calculate();
        } catch (Exception $e) {
            $this->getDi()->errorLogTable->logException($e);
            $this->_response->ajaxResponse(
                array(
                    'status' => 'error',
                    'message' => $e->getPublicError()
            ));
            return;
        }
        $this->_response->ajaxResponse(array('status' => 'ok'));
    }

    public function signupRedirect($url)
    {
        return $this->_response->redirectLocation(REL_ROOT_URL . '/signup/cart/checkout?amember_redirect_url=' . urlencode($url));
    }

    private function doLoginOrSignup()
    {
        $pos = stripos($this->view->url(), $this->_request->getBaseUrl() . '/cart/');
        $url = substr($this->view->url(), $pos);
        $url = strpos($url, '?') !== false ? $url .= '&a=checkout' : $url .= '?a=checkout';
        $this->signupRedirect($url);
    }

    protected function doCheckout()
    {
        do {
            if (!$this->getCart()->getItems()) {
                $errors[] = ___("You have no items in your basket - please add something to your basket before checkout");
                return $this->view->display('cart/basket.phtml');
            }
            if (!$this->getDi()->auth->getUserId())
                return $this->doLoginOrSignup();
            else
                $this->getCart()->setUser($this->getDi()->user);
            if (empty($this->getCart()->getInvoice()->paysys_id))
                return $this->choosePaysysAction();

            $invoice = $this->getCart()->getInvoice();
            $this->getDi()->hook->call(Am_Event::CART_INVOICE_CHECKOUT, array(
                'invoice' => $invoice
            ));
            $errors = $invoice->validate();
            if ($errors) {
                $this->view->assign('errors', $errors);
                return $this->view->display('cart/basket.phtml');
            }
            // display confirmation
            if (!$this->getInt('confirm') && $this->getDi()->config->get('shop.confirmation'))
                return $this->view->display('cart/confirm.phtml');
            ///
            $invoice->save();

            $payProcess = new Am_Paysystem_PayProcessMediator($this, $invoice);
            $result = $payProcess->process();
            if ($result->isFailure()) {
                $this->view->error = ___("Checkout error: ") . current($result->getErrorMessages());
                $this->getCart()->getInvoice()->paysys_id = null;
                $this->_request->set('do-checkout', 0);
                return $this->doCheckout();
            }
        } while (false);
    }

    public function getCategoryCode()
    {
        return $this->getFiltered('c', @$_GET['c']);
    }

    public function getHiddenCatCodes()
    {
        return $this->hiddenCatCodes;
    }

    public function loadCategory()
    {
        $code = $this->getCategoryCode();
        if ($code) {
            $category = $this->getDi()->productCategoryTable->findByCodeThenId($code);
            if (null == $category)
                throw new Am_Exception_InputError(___('Category [%s] not found', $code));
        } else
            $category = null;
        return $category;
    }

    /**
     *
     * @return Am_ShoppingCart
     */
    public function getCart()
    {
        return $this->getModule()->getCart();
    }

    public function getAllTags()
    {
        /* @var $q Am_Query */
        $q = clone $this->getProductsQuery();
        $this->query = null; //reset query, we may need to apply category condition later
        $q->clearFields();
        $q->clearOrder();
        $q->addField('tags');
        $tags = array();
        $_ = $this->getDi()->db->selectCol($q->getSql());
        foreach ($_ as $t) {
            foreach (array_filter(explode(',', $t)) as $tag) {
                @$tags[$tag]++;
            }
        }
        return $tags;
    }
}