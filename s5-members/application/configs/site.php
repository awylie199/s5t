<?php

if (!defined('INCLUDED_AMEMBER_CONFIG')) 
    die("Direct access to this location is not allowed");
  
/*
*  aMember Pro site customization file
*
*  Rename this file to site.php and put your site customizations, 
*  such as fields additions, custom hooks and so on to this file
*  This file will not be overwritten during upgrade
*                                                                               
*/



// ADD PAYMENTS HISTORY TAB BACK INTO MEMBERS DASHBOARD
Am_Di::getInstance()->hook->add('userMenu', 'siteUserMenu');
function siteUserMenu(Am_Event $event){
	$menu = $event->getMenu();

	if ($page = $menu->findOneById('member')) {
		$menu->removePage($page);
	}

	$menu->addPage(array(
		'id' => 'payment-history',
		'label' => ___('Payment History'),
		'uri' => REL_ROOT_URL . '/member/payment-history',
		'order' => 200,
	));

	$menu->addPage(array(
		'id' => 'members',
		'label' => ___('Dashboard'),
		'uri' => REL_ROOT_URL . '/member',
		'order' => 100,
	));

}