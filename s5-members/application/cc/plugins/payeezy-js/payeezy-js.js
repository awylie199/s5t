var responseHandler = function(status, response) 
{
	function showError(errorMessages) {
		$('#payment-errors').html(errorMessages);
		$('#row-payment-errors-0').show();
		$('#payment-errors').show();
	}
	
	var $form = $('#cc');
		
	if (status !== 201) {
		 if (response.error && status !== 400) {
		   var errormsg = response.error.messages;
		   var errorMessages = (errormsg[0].description); 
		   showError(errorMessages);
		} else if (status === 400 || status === 500) {
			var errormsg = response.error.messages;
			var errorMessages = "";
			
			for(var i in errormsg)
			{
				var eMessage = errormsg[i].description;
				
				if (i > 0) {
					errorMessages += '<br/>';
				}
				
				errorMessages = 
					errorMessages + 
					eMessage;
			}

			showError(errorMessages);
		}
	} else {	
		var result = response.token.value;
		
		if (result) {
			$('#token_value-0').val(result);
			$form.submit();
		} else {
			var errorMessage = "Token value isn't returned!";
			showError(errorMessage);
		}
	}
	
	$form.find(':button').prop('disabled', false);
};

jQuery(function($) {
	$('#row-payment-errors-0').hide();
					
	function reChech() {
		$('.row').each(function(index, value) {
			if (index > 0 && index < 4) {
				if($('#use_saved-0').is(':checked')){
					$(this).hide();
				} else {
					$(this).show();
				}
			}
		});
	}
	
	$('#use_saved-0').change(function() {
		reChech();
    });
					
	$('#use_saved-0').prop('checked', true);
	reChech();
});

jQuery(function($) {
	$('#input_button-0').click(function(e) 
	{			
		var $form = $('#cc');
		
		$form.find(':button').prop('disabled', true);
		
		if ($('#use_saved-0').prop('checked')) {
			$form.submit();
		} else {
			Payeezy.setApiKey(apiKey);
			Payeezy.setJs_Security_Key(jsSecurityKey);
			Payeezy.setTa_token('NOIW');
			Payeezy.setAuth(true);
			Payeezy.setCurrency(currency);
			Payeezy.createToken(responseHandler);
		}
	});
});