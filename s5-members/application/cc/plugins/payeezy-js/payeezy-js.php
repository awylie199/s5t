<?php

class Am_Paysystem_PayeezyJs extends Am_Paysystem_CreditCard
{
    const PLUGIN_STATUS = self::STATUS_BETA;
    const PLUGIN_REVISION = '5.1.5';
	
	const ACTION_PROCESSED = 'processed';
	
	const PAYEEZY_SUCCESS = 'approved';
	const PAYEEZY_DECLINED = 'declined';
	const PAYEEZY_NOT_PROCESSED = 'not processed';
	const DEBUG = true;
	
	/** 
	 * @var Am_Paysystem_PayeezyJs Plugin
	 */
	private static $plugin;
	
	protected $defaultTitle = "Payeezy JS";
    protected $defaultDescription  = "Integration with Payeezy.com using Payeezy JS method";
	
	function cosherCCNum($num) {
		$subnum = substr($num, -4);
		return str_pad($subnum, 16, "*", STR_PAD_LEFT);
	}
	
	function init() {
		parent::init();
		
		if ($this->getConfig('test')) {
			$host = 'api-cert.payeezy.com';
		} else {
			$host = 'api.payeezy.com';
		}
		
		define('PAYEEZY_HOST', $host);
		define('PAYEEZY_GATEWAY', 'https://' . $host . '/v1/transactions');
		
		self::$plugin = $this;
	}

	protected function _initSetupForm(Am_Form_Setup $form) {
		$apiKey = $form->addText('api_key', array('size' => 64))
			->setLabel("API Key");
		$apiKey->addRule('required')
			->addRule(
				'regex', 
				'API Keys must be in form of 32 alphanumeric signs', 
				'/^[a-zA-Z0-9]{32}$/');
		
		$apiSecret = $form->addText('api_secret', array('size' => 64))
			->setLabel("API Secret");
		$apiSecret->addRule('required')
			->addRule(
				'regex', 
				"API Secret must be in form of "
				. "64 lower-case hexadecimal digits", 
				'/^[a-z0-9]{64}$/');
		
		$token = $form->addText('token', array('size' => 64))
			->setLabel("Merchant Token");
		$token->addRule('required')
			->addRule(
				'regex', 
				"fdoa-[48 lower-case hexadecimal digits]", 
				'/^fdoa-[a-z0-9]{48}$/');
		
		$jsKey = $form->addText('js_security_key', array('size' => 64))
			->setLabel("JS Security Key");
		$jsKey->addRule('required')
			->addRule(
				'regex', 
				"js-[48 lower-case hexadecimal digits]", 
				'/^js-[a-z0-9]{48}$/');
		
		$form->addAdvCheckbox('test')
			->setLabel('Use test environment');
	}
	
	function isConfigured() {
		return	$this->getConfig('api_key') && 
				$this->getConfig('api_secret') &&
				$this->getConfig('js_security_key') &&
				$this->getConfig('token');
	}
	
	function getReadme() {
		return <<<CUT
Create account in payeezy.com
Create API key.
Save it here.
CUT;
	}
	
	static function debug($data) {
		if (self::DEBUG) {
			error_log(print_r($data, 1));
			self::$plugin->logRequest($data);
		}
	}
	
	private function makeRandomString($bits = 256) {
		$bytes = ceil($bits / 8);
		$return = '';
		
		for ($i = 0; $i < $bytes; $i++) {
			$return .= chr(mt_rand(0, 255));
		}
		
		return $return;
	}
	
	private function getNonce() {
		return hash('sha512', $this->makeRandomString());
	}
	
	private function sendXMLviaCurl($body, $validate = true) {
		// TODO: use our HTTP request classes
		
		$curl = curl_init(PAYEEZY_GATEWAY);
		
		$apiKey = $this->getConfig('api_key');
		$apiSecret = $this->getConfig('api_secret');
		$token = $this->getConfig('token');
		$nonce = $this->getNonce();
		$timestamp = strval(time()*1000);
		$data = $apiKey . $nonce . $timestamp . $token . $body;
		//self::debug($data);

		$hmac = hash_hmac("sha256" , $data , $apiSecret, false);
		//self::debug($hmac);
		
		$headers = array(
			'Content-Type: application/json',
			'apikey:' . strval($apiKey),
			'token:' . strval($token),
			'Authorization:' . base64_encode($hmac),
			'nonce:' . $nonce,
			'timestamp:'. $timestamp,
		);
		
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$json_response = curl_exec($curl);
		
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response = json_decode($json_response, true);
		
		if ($status != 201 && $validate) {
			if (isset($response['message'])) {
				$code = $response['code'];
				$message = $response['message'];
				
				throw new Exception(""
					. "Error: call to URL $serviceURL "
					. "failed with status $status, "
					. "code $code, "
					. "message $message");
			} else {
				throw new Exception(""
					. "Error: call to URL $serviceURL "
					. "failed with status $status, "
					. "response $json_response, "
					. "curl_error " . curl_error($curl) . ", "
					. "curl_errno " . curl_errno($curl));
			}
		}
		
		curl_close($curl);

		self::debug($response);
		
		return $response;
	}
	
	private function getInvoice($invoiceId) {
		//die($invoiceId);

		if (!$invoiceId) {
			throw new Am_Exception_InputError(""
					. "invoice_id is empty - "
					. "seems you have followed wrong url, "
					. "please return back to continue");
		}

		return $this->getDi()->invoiceTable->findFirstByPublicId($invoiceId);		
	}
	
	private function processedAction(\Am_Mvc_Request $request, \Am_Mvc_Response $response, array $invokeArgs) {
		//self::debug($_REQUEST);
		
		$tokenValue = $request->get('token_value');	
		$cardType = $request->get('payeezy_card_type');
		$holderName = $request->get('holder_name');
		$expiration = $request->get('expiration');
		$expMonth = $expiration['m'];
		$expYear = $expiration['y'];
		$expDate = $expMonth . $expYear;
		$invoiceId = $request->getFiltered('invoice_id');
		$invoice = $this->getInvoice($invoiceId);
		
		$data = array(
			"transaction_type" => "purchase",
			"method" => "token",
			"amount" => $invoice->first_total * 100,
			"currency_code" => $invoice->currency,
			"token" => array(
				"token_type" => "FDToken",
				"token_data" => array(
					"type" => $cardType,
					"value" => $tokenValue,
					"cardholder_name" => $holderName,
					"exp_date" => $expDate
				)
			)
		);
		
		self::debug($data);
		
		$payload = json_encode($data, JSON_FORCE_OBJECT);
		
		$ret = $this->sendXMLviaCurl($payload, false);
		//self::debug($response);
		
		return $this->handleResponse($ret, $request, $response, $invokeArgs);
	}
	
	function createThanksTransaction(\Am_Mvc_Request $request, \Am_Mvc_Response $response, array $invokeArgs) {
		return new Am_Paysystem_Payeezy_Transaction_Thanks($this, $request, $response, $invokeArgs);
	}
	
	public function cancelPaymentAction(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        $id = $request->getFiltered('invoice_id');
		
        if (!$id && isset($_GET['id'])) $id = filterId($_GET['id']);
		
        $invoice = $this->getDi()->invoiceTable->findFirstByPublicId($id);
		
        if (!$invoice) {
            throw new Am_Exception_InputError("No invoice found [$id]");
		}
		
        if ($invoice->user_id != $this->getDi()->auth->getUserId()) {
            throw new Am_Exception_InternalError("User tried to access foreign invoice: [$id]");
		}
		
        $this->invoice = $invoice;
        
		// find invoice and redirect to default "cancel" page
        $response->setRedirect($this->getCancelUrl());
    }
	
	private function handleResponse(
		array $gwResponse, 
		\Am_Mvc_Request $request, 
		\Am_Mvc_Response $response, 
		array $invokeArgs) {
		$result = strtolower($gwResponse['transaction_status']);
		
		switch ($result) {
			case self::PAYEEZY_SUCCESS:
				return $this->thanksAction($request, $response, $invokeArgs);
			case self::PAYEEZY_DECLINED:
			case self::PAYEEZY_NOT_PROCESSED:
				$message = print_r($gwResponse, 1);
				$this->getDi()->errorLogTable->log($message);
				
				return $this->cancelPaymentAction($request, $response, $invokeArgs);	
			default:
				$message = (string)$gwResponse['gateway_message'];
				throw new Am_Exception(""
					. "Error while processing Payeezy transaction: $message"
					. "Body: " . print_r($gwResponse, 1));
		}		
	}
	
	function directAction(\Am_Mvc_Request $request, \Am_Mvc_Response $response, array $invokeArgs) {
		switch ($request->getActionName()) {
			case self::ACTION_PROCESSED:
				return $this->processedAction($request, $response, $invokeArgs);
			default:
				return parent::directAction($request, $response, $invokeArgs);
		}
	}
	
	function createForm($actionName) {
		$url = $this->getPluginUrl(self::ACTION_PROCESSED);
		return new Am_Form_CreditCard_PayeezyJS($this, $url, $this->invoice);
	}
	
	function createController(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        return new Am_Mvc_Controller_CreditCard_PayeezyJS($request, $response, $invokeArgs);
    }

	public function _doBill(\Invoice $invoice, $doFirst, \CcRecord $cc, \Am_Paysystem_Result $result) {
		throw new Am_Exception_NotImplemented();
	}
}

class Am_Mvc_Controller_CreditCard_PayeezyJS extends Am_Mvc_Controller_CreditCard
{	
	function ccAction() {
        // invoice must be set to this point by the plugin
        if (!$this->invoice)
            throw new Am_Exception_InternalError('Empty invoice - internal error!');
        $this->form = $this->createForm();

        $this->getDi()->hook->call(Bootstrap_Cc::EVENT_CC_FORM, array('form' => $this->form));

        if ($this->form->isSubmitted() && $this->form->validate()) {
            if ($this->processCc()) return;
        }
        $this->view->form = $this->form;
        $this->view->invoice = $this->invoice;
        $this->view->display_receipt = true;
        $this->view->layoutNoMenu = true;

		$helper = $this->view->headScript();
		$helper->appendFile(
				REL_ROOT_URL . "/application/cc/plugins/payeezy-js/payeezy_us_v5.1.js");
		$helper->appendFile(
				REL_ROOT_URL . "/application/cc/plugins/payeezy-js/payeezy-js.js");
		
		$this->view->display('cc/info.phtml');
	}
}

class Am_Form_CreditCard_PayeezyJS extends Am_Form_CreditCard
{	
	private $invoice;
	private $url;
	
	public function __construct(
		Am_Paysystem_CreditCard $plugin,
		$url,
		Invoice $invoice)
    {		
		$this->invoice = $invoice;	
		$this->url = $url;
		parent::__construct($plugin);
    }
	
	private function getCardTypes() {
		return array(
				"visa" => 'Visa',
				"mastercard" => 'Master Card',
				"American Express" => 'American Express',
				"discover" => 'Discover'
		);
	}

	public function init()
    {
        Am_Form::init();
		
		$invoice = $this->invoice;
		$user = $invoice->getUser();
		$account_id = $user->data()->get('payeezy_js_token');
		
		$tokenHidden = $this->addHidden('token_value');
		
		if ($account_id) {
			$fourlast = 
				$this
					->plugin
					->cosherCCNum(
						$this
							->invoice
							->getUser()
							->data()
							->get('payeezy_js_four_last'));
			
			$this->addAdvCheckbox('use_saved')
				->setLabel(___(
						"Use saved card: $fourlast"));
			
			$tokenHidden->setValue($account_id);
		}
		
		$this->addStatic('payment-errors')
			->setContent(<<<CUT
<span id='payment-errors' class='error'></span>
CUT
);

		$this->addHidden('invoice_id')
			->setValue($this->invoice->public_id);
		
		$this->addHidden('currency')
			->setAttribute('payeezy-data', 'currency')
			->setValue($this->invoice->currency);
		
		$this->addHidden('holder_name')
			->setAttribute('payeezy-data', 'cardholder_name')
			->setValue($user->getName());
		
		$this->addHidden('city')
			->setAttribute('payeezy-data', 'city')
			->setValue($user->city);
		
		$this->addHidden('country')
			->setAttribute('payeezy-data', 'country')
			->setValue($user->country);
		
		$this->addHidden('email')
			->setAttribute('payeezy-data', 'email')
			->setValue($user->email);
		
		$this->addHidden('type')
			->setAttribute('payeezy-data', 'type')
			->setValue('home');
		
		$this->addHidden('number')
			->setAttribute('payeezy-data', 'number')
			->setValue($user->phone);
		
		$this->addHidden('street')
			->setAttribute('payeezy-data', 'street')
			->setValue($user->street);	
		
		$this->addHidden('state')
			->setAttribute('payeezy-data', 'state_province')
			->setValue($user->state);
		
		$this->addHidden('state')
			->setAttribute('payeezy-data', 'zip_postal_code')
			->setValue($user->zip);		
		
		$this->addSelect('payeezy_card_type')
			->setLabel('Card Type')
			->setAttribute('payeezy-data', 'card_type')
			->loadOptions($this->getCardTypes())
			->addRule('required', ___('Please select card type'));
		
		$cc = $this->addText('billing-cc-number', array('autocomplete'=>'off', 'size'=>22, 'maxlength'=>22))
			->setAttribute('payeezy-data', 'cc_number')
			->setLabel(___(
						"Credit Card Number\n" .
						"for example: 1111-2222-3333-4444"));
		$cc->addRule('required', ___('Please enter Credit Card Number'))
			->addRule('regex', ___('Invalid Credit Card Number'), '/^[0-9 -]+$/');


        $this->addElement(new Am_Form_Element_CreditCardExpire_PayeezyJS('expiration'))
            ->setLabel(___("Card Expire\n" .
                'Select card expiration date - month and year'))
            ->addRule('required');

		$code = $this->addPassword('cvv', array('autocomplete'=>'off', 'size'=>4, 'maxlength'=>4))
				->setAttribute('payeezy-data', 'cvv_code')
				->setLabel(___("Credit Card Code\n" .
					'The "Card Code" is a three- or four-digit security code ' .
					'that is printed on the back of credit cards in the card\'s ' .
					'signature panel (or on the front for American Express cards)'));
		$code->addRule('required', ___('Please enter Credit Card Code'))
			 ->addRule('regex', ___('Please enter Credit Card Code'), '/^\s*\d{3,4}\s*$/');

		$buttons = $this->addGroup();
        $buttons->setSeparator(' ');
        $buttons->addInputButton('input_button', array('value'=> $this->payButtons[ $this->formType ]));
		
		$apiKey = $this->plugin->getConfig('api_key');
		$apiSecret = $this->plugin->getConfig('api_secret');
		$jsSecurityKey = $this->plugin->getConfig('js_security_key');
		$token = $this->plugin->getConfig('token');
		$currency = $this->invoice->currency;
		$host = PAYEEZY_HOST;
		
		$this->addScript()
			->setScript(<<<CUT
var apiKey = '$apiKey';
var apiSecret = '$apiSecret';
var jsSecurityKey = '$jsSecurityKey';
var token = '$token';
var currency = '$currency';
var payeezyHost = '{$host}';
CUT
			);		
		
		$this->setAction($this->url);
		$this->plugin->onFormInit($this);
	}
}

class Am_Form_Element_CreditCardExpire_PayeezyJS extends Am_Form_Element_CreditCardExpire
{
    public function __construct($name = null, $attributes = null, $data = null)
    {
        HTML_QuickForm2_Container_Group::__construct($name, $attributes, $data);
        $this->setSeparator(' ');
        $require = !$data['dont_require'];
        $years = @$data['years'];
        if (!$years) $years = 10;
        
		$m = $this
			->addSelect('m')
			->setAttribute('payeezy-data', 'exp_month')
			->loadOptions($this->getMonthOptions());
		
        if ($require)
            $m->addRule('required', ___('Invalid Expiration Date - Month'));
		
        $y = $this->addSelect('y')
			->setAttribute('payeezy-data', 'exp_year')
			->loadOptions($this->getYearOptions($years));
        
		if ($require)
            $y->addRule('required', ___('Invalid Expiration Date - Year'));
    }

    public function getMonthOptions()
    {
        $locale = Zend_Registry::get('Am_Locale');
        $months = array('' => '');

        foreach ($locale->getMonthNames('wide', false) as $k => $v) {
			$key = str_pad($k, 2, "0", STR_PAD_LEFT);
			$months[$key] = sprintf('(%02d) %s', $k, $v);
		}
		
        ksort($months);
        return $months;
    }

    public function getYearOptions($add){
        $years = range(date('y'), date('y')+$add);
		$display = range(date('Y'), date('Y')+$add);

		array_unshift($years, '');
		array_unshift($display, '');
        return array_combine($years, $display);
    }
}

class Am_Paysystem_Payeezy_Transaction_Thanks extends Am_Paysystem_Transaction_Incoming_Thanks
{
	public function getUniqId() {
		return time();
	}

	public function validateSource() {
		return true;
	}

	public function validateStatus() {
		return true;
	}

	public function validateTerms() {
		return true;
	}
	
	function findInvoiceId() {
		return $this->request->get('invoice_id');
	}
}