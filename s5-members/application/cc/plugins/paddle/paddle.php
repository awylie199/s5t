<?php

class Am_Paysystem_Paddle extends Am_Paysystem_CreditCard
{
	const PLUGIN_STATUS = self::STATUS_BETA;
    const PLUGIN_REVISION = '5.1.6';

    protected $defaultTitle = 'Paddle';
    protected $defaultDescription = 'Payment via Paddle gateway';

	const DEBUG = true;
    protected $_pciDssNotRequired = true;

	public function isNotAcceptableForInvoice(Invoice $invoice)
    {
		if (count($invoice->getProducts()) > 1) {
			return array(
				___("This paysystem "
					. "can not work with multiple products in card"));
		} else {
			return parent::isNotAcceptableForInvoice($invoice);
		}
	}

	function _initSetupForm(\Am_Form_Setup $form) {
		$form->addText('vendor_id', array('size' => 10))
			->setLabel("Vendor ID")
			->addRule('required');

		$form->addTextarea(
			'public_key',
			array(
				'class' => 'el-wide textlocal',
				'id' => 'comment',
				'maxlength' => 900),
			array('label' =>
				___("Public Key\n"
					. "Please include -----BEGIN PUBLIC KEY-----")))
			->addRule('required');
	}

	function createForm($actionName) {
		$url = $this->getReturnUrl();
		return new Am_Form_CreditCard_Paddle($this, $url, $this->invoice);
	}

	function createTransaction(
		Am_Mvc_Request $request,
		Am_Mvc_Response $response,
		array $invokeArgs)
    {
		return new Am_Paysystem_Paddle_Transaction($this, $request, $response, $invokeArgs);
    }

	function init() {
		$prod_id = new Am_CustomFieldText_Paddle(
						'paddle_prod_id',
						___('Product ID'),
						___("Product ID at paddle.com"),
						'number',
						array('size' => 8));

		$key = new Am_CustomFieldText_SecretKey(
						'checkout_key',
						___('Secret Key'),
						___("Checkout Secret Key at paddle.com"),
						null,
						array('size' => 8));

		$this->getDi()->productTable->customFields()->add($prod_id);
		$this->getDi()->productTable->customFields()->add($key);
	}

	function isConfigured() {
		return $this->getConfig('vendor_id') &&
				$this->getConfig('public_key');
	}

	function getReadme() {
		return <<<CUT
1. Create account in https://www.paddle.com
2. Provide vendor ID on this configuration page.
3. Provide Public Key from account settings
3. Create products in Padle.com:
	a. Select type of product
	b. On question: "Does your product use the Paddle SDK?", answer "NO"!!!
	c. On question: "How will your product be delivered?", select Webhook!!!
	d. Set prices
	e. Upload some necessary stuff (even empty file or image)
	f. Set Webhook url to: http://amember_url/payment/paddle/ipn
		1. Request Method: POST
		2. Choose: Checkouts produce a single webhook request
			with a quantity attribute
	g. Release!!!
4. In the main products grid, click on "Checkout link" of every product,
   and specify "Product ID" and "Checkout Secret Key" field for every product
   in amember product configuration page.
5. Product need appear in top grid, not "Under Development"


CUT;
	}

	function createController(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        return new Am_Mvc_Controller_CreditCard_Paddle($request, $response, $invokeArgs);
    }

	public function _doBill(\Invoice $invoice, $doFirst, \CcRecord $cc, \Am_Paysystem_Result $result) {
		throw new Am_Exception_NotImplemented();
	}

}

class Am_Paysystem_Paddle_Transaction extends Am_Paysystem_Transaction_Incoming
{
	function findInvoiceId()
    {
        return $this->request->getPost('passthrough');
		//return '8LRVY';
    }

	function getUniqId() {
		return $this->request->getPost('p_order_id');
	}

	function validateSource() {
		$fields = $this->request->getParams();
		//$this->plugin->logRequest($params);

		// Your Paddle 'Public Key'
		$public_key = $this->plugin->getConfig('public_key');

		// Get the p_signature parameter & base64 decode it.
		$signature = base64_decode($this->request->getPost('p_signature'));
		//$this->plugin->logRequest($this->request->getPost('p_signature'));
		//$this->plugin->logRequest($signature);

		// Get the fields sent in the request, and remove the p_signature parameter
		//$fields = $this->request->getParams();
		unset($fields['p_signature']);
		$this->filterFields($fields);

		// ksort() and serialize the fields
		ksort($fields);

		foreach($fields as $k => $v) {
			if(!in_array(gettype($v), array('object', 'array'))) {
				$fields[$k] = "$v";
			}
		}

		$data = serialize($fields);
		$this->plugin->logRequest($data);

		// Veirfy the signature
		return openssl_verify(
			$data,
			$signature,
			$public_key,
			OPENSSL_ALGO_SHA1);
	}

	private function filterFields(&$array) {
		$fields = array(
			'p_product_id',
			'p_price',
			'p_country',
			'p_currency',
			'p_sale_gross',
			'p_tax_amount',
			'p_paddle_fee',
			'p_coupon_savings',
			'p_earnings',
			'p_order_id',
			'p_coupon',
			'p_used_price_override',
			'passthrough',
			'p_quantity',
			'quantity'
		);

		foreach($array as $key => $value) {
			if (!in_array($key, $fields)) {
				unset($array[$key]);
			}
		}
	}

	function validateStatus() {
		return true;
	}

	function validateTerms() {
		return true;
	}
}

class Am_CustomFieldText_Paddle extends Am_CustomFieldText
{
    function addToQF2(
		HTML_QuickForm2_Container $container,
		$attr = array(),
		$data = array(),
		$runAt = HTML_QuickForm2_Rule::CLIENT_SERVER)
    {
		$el = $container->addElement($this->qfType, $this->name, $attr, $data)
				->setLabel(
					!empty($this->description) ?
						___($this->title) . "\n" . ___($this->description) :
						___($this->title));

        if (!empty($this->size)) $el->setAttribute('size', $this->size);
		if (!empty($this->default)) $el->setValue($this->default);

		$this->addValidateFunction($el, $runAt);

        return $el;
	}
}

class Am_CustomFieldText_SecretKey extends Am_CustomFieldText_Paddle
{
    function addToQF2(
		HTML_QuickForm2_Container $container,
		$attr = array(),
		$data = array(),
		$runAt = HTML_QuickForm2_Rule::CLIENT_SERVER)
    {
		$el = parent::addToQF2($container, $attr, $data, $runAt);

		$r = $el->addRule(
			'regex',
			___("5-10 lowercase alphanumeriv characters"),
			'/^[a-z0-9]{5,10}$/',
			$runAt);

        return $el;
	}
}

class Am_Mvc_Controller_CreditCard_Paddle extends Am_Mvc_Controller_CreditCard
{
	function ccAction() {
        // invoice must be set to this point by the plugin
        if (!$this->invoice)
            throw new Am_Exception_InternalError('Empty invoice - internal error!');
        $this->form = $this->createForm();

        $this->getDi()->hook->call(Bootstrap_Cc::EVENT_CC_FORM, array('form' => $this->form));

        if ($this->form->isSubmitted() && $this->form->validate()) {
            if ($this->processCc()) return;
        }
        $this->view->form = $this->form;
        $this->view->invoice = $this->invoice;
        $this->view->display_receipt = true;
        $this->view->layoutNoMenu = true;

		$helper = $this->view->headScript();
		$helper->appendFile(
				"https://cdn.paddle.com/paddle/paddle.js");
		$helper->appendFile(
				ROOT_SURL . "/application/cc/plugins/paddle/scripts.js");

		$this->view->display('cc/info.phtml');
	}
}

class Am_Form_CreditCard_Paddle extends Am_Form_CreditCard
{
	/**
	 * @var Invoice
	 */
	private $invoice;
	private $url;

	public function __construct(
		Am_Paysystem_CreditCard $plugin,
		$url,
		Invoice $invoice)
    {
		$this->invoice = $invoice;
		$this->url = $url;
		parent::__construct($plugin);
    }

	public function init()
    {
        Am_Form::init();

		$invoice = $this->invoice;
		$user = $invoice->getUser();

		$this->addStatic('payment-errors')
			->setContent(<<<CUT
<span id='payment-errors' class='error'></span>
CUT
);

		$products = $this->invoice->getProducts();

		/* @var $product Product */
		$product = $products[0];
		$product_id = $product->data()->get('paddle_prod_id');

		if (!$product_id) {
			throw new Am_Exception(
					"No paddle_prod_id product "
					. "is specified for {$product->title}");
		}

		$checkout_key = $product->data()->get('checkout_key');

		if (!$checkout_key) {
			throw new Am_Exception(
					"No checkout_key "
					. "is specified for {$product->title}");
		}

		$price = $invoice->first_total;
		$priceAuthentication = md5($price.$checkout_key);

		$buttons = $this->addGroup();
        $buttons->setSeparator(' ');
		$buttons->addInputButton('input_button',
									array(
										'value'=>
										$this->payButtons[ $this->formType ]
									))
				->addClass('paddle_button')
				->setAttribute('data-product', $product_id)
				->setAttribute('data-email', $user->email)
				->setAttribute('data-passthrough', $invoice->public_id)
				->setAttribute('data-url', $this->url)
				->setAttribute('data-price', $price)
				->setAttribute('data-auth', $priceAuthentication);

		$vedor_id = $this->plugin->getConfig('vendor_id');
		$debug = Am_Paysystem_Paddle::DEBUG;

		$this->addScript()
			->setScript(<<<CUT
var vendor = $vedor_id;
var debug = $debug;
CUT
			);

		$this->setAction($this->url);
		$this->plugin->onFormInit($this);
	}
}