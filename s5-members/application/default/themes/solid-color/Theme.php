<?php

class Am_Theme_SolidColor extends Am_Theme_Default
{
    protected $publicWithVars = array(
        'css/theme.css',
    );

    public function initSetupForm(Am_Form_Setup_Theme $form)
    {
        parent::initSetupForm($form);
        $form->removeElementByName('header');
        $el = HTML_QuickForm2_Factory::createElement('advradio', 'logo_align', null, array(
            'options' => array(
                'left' => ___('Left'),
                'center' => ___('Center'),
                'right' => ___('Right')
            )
        ));
        $el->setLabel(___('Logo Position'));

        $form->insertBefore($el, $form->getElementById('logo-link-group'));
        $el = HTML_QuickForm2_Factory::createElement('advradio', 'logo_width', null, array(
            'options' => array(
                'auto' => ___('As Is'),
                '100%' => ___('Responsive')
            )
        ));
        $el->setLabel(___('Logo Width'));

        $form->insertBefore($el, $form->getElementById('logo-link-group'));

        $form->addText('color', array('size' => 7))
            ->setLabel("Theme Color\n" .
                'you can use any valid <a href="http://www.w3schools.com/html/html_colors.asp" class="link" target="_blank" rel="noreferrer">HTML color</a>, you can find useful color palette <a href="http://www.w3schools.com/TAGS/ref_colornames.asp" class="link" target="_blank" rel="noreferrer">here</a>');
        $gr = $form->addGroup()
            ->setLabel(___('Layout Width'));
        $gr->setSeparator(' ');
        $gr->addText('max_width', array('size' => 3));
        $gr->addHtml()->setHtml('px');

        $form->addSaveCallbacks(array($this, 'findInverseColor'), null);
    }

    public function findInverseColor(Am_Config $before, Am_Config $after)
    {
        $t_id = "themes.{$this->getId()}.color";
        $t_new = "themes.{$this->getId()}.color_c";
        $after->set($t_new, $this->inverse($after->get($t_id)));
    }

    protected function inverse($color)
    {
        $color = str_replace('#', '', $color);
        if (strlen($color) != 6){ return '#ffffff'; }
        $rgb = '';
        for ($x=0; $x<3; $x++){
            $c = 255 - hexdec(substr($color,(2*$x),2));
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
        return '#'.$rgb;
    }

    public function getDefaults()
    {
        return parent::getDefaults() + array(
            'color' => '#446787',
            'color_c' => '#fffff',
            'logo_align' => 'left',
            'max_width' => 800,
            'logo_width' => 'auto'
        );
    }
}