<?php
/**
 * @table paysystems
 * @id gourl
 * @title GoUrl
 * @visible_link https://gourl.io/
 * @recurring none
 */
class Am_Paysystem_Gourl extends Am_Paysystem_Abstract
{
    const PLUGIN_STATUS = self::STATUS_BETA;
    const PLUGIN_REVISION = '5.1.7';

    protected $defaultTitle = 'GoUrl';
    protected $defaultDescription = 'paid by bitcoins';

    public function getRecurringType()
    {
        return self::REPORTS_NOT_RECURRING;
    }

    public function getSupportedCurrencies()
    {
        return array('USD', 'BTC');
    }

    public function _initSetupForm(Am_Form_Setup $form)
    {
        $form->addText('box_id')
            ->setLabel("Cryptocoin Payment BoxID");
        $form->addText('public_key', array('class' => 'el-wide'))
            ->setLabel("Public Key");
        $form->addPassword('private_key', array('class' => 'el-wide'))
            ->setLabel("Private Key");
    }

    function isConfigured()
    {
        return $this->getConfig('box_id') &&
            $this->getConfig('public_key') &&
            $this->getConfig('private_key');
    }

    public function _process(Invoice $invoice, Am_Mvc_Request $request, Am_Paysystem_Result $result)
    {
        $a = new Am_Paysystem_Action_HtmlTemplate_Gourl(dirname(__FILE__), 'gourl.phtml');
        $a->boxID = $this->getConfig('box_id');
        $a->coinName = 'bitcoin';
        $a->public_key = $this->getConfig('public_key');
        $a->amount = $invoice->currency == 'BTC' ? $invoice->first_total : 0;
        $a->amountUSD = $invoice->currency == 'BTC' ? 0 : $invoice->first_total;
        $a->period = '1 HOUR';

        $locale = Zend_Locale::getDefault();
        list($locale) = explode('_', key($locale));

        $a->language = $locale;
        $a->iframeID = 'am-gourl-widget';
        $a->userID = $invoice->getUser()->pk();
        $a->userFormat = 'MANUAL';
        $a->orderID = $invoice->public_id;
        $a->cookieName = '';
        $a->webdev_key = '';
        $a->width = 530;
        $a->height = 230;
        $a->hash = $this->getHash($a->getVars());

        foreach (array_map('json_encode', $a->getVars()) as $k => $v) {
            $a->$k = $v;
        }
        $a->invoice = $invoice;
        $a->return_url = $this->getReturnUrl();

        $result->setAction($a);
    }

    protected function getHash($a)
    {
        extract($a);

        return md5($boxID . $coinName . $public_key . $this->getConfig('private_key') .
            $webdev_key . $amount . $period . $amountUSD . $language . $amount .
            $iframeID . $amountUSD . $userID . $userFormat . $orderID . $width . $height);
    }

    public function createTransaction(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        if ($request->isGet()) {
            echo "Only POST Data Allowed";
            throw new Am_Exception_Redirect;
        }
        return new Am_Paysystem_Transaction_Gourl($this, $request, $response, $invokeArgs);
    }

    function getReadme()
    {
        $ipn = $this->getPluginUrl('ipn');
        return <<<CUT
Create new bitcoin/altcoin payment box in your GoUrl account.
Set Callback URL to:
<strong>$ipn</strong>
Fill in form above with proper value for your payment box.
CUT;
    }
}

class Am_Paysystem_Transaction_Gourl extends Am_Paysystem_Transaction_Incoming
{
    public function getUniqId()
    {
        return $this->request->getParam('tx');
    }

    public function validateSource()
    {
        return $this->request->getParam('private_key') == $this->plugin->getConfig('private_key');
    }

    public function validateStatus()
    {
        return $this->request->getParam('status') == 'payment_received';
    }

    public function validateTerms()
    {
        return true;
    }

    public function findInvoiceId()
    {
        return $this->request->getParam('order');
    }

    public function processValidated()
    {
        parent::processValidated();
        echo "cryptobox_newrecord";
        throw new Am_Exception_Redirect;
    }
}

class Am_Paysystem_Action_HtmlTemplate_Gourl extends Am_Paysystem_Action_HtmlTemplate
{
    protected $_template;
    protected $_path;

    public function __construct($path, $template)
    {
        $this->_template = $template;
        $this->_path = $path;
    }

    public function process(Am_Mvc_Controller $action = null)
    {
        $action->view->addScriptPath($this->_path);

        $action->view->assign($this->getVars());
        $action->renderScript($this->_template);

        throw new Am_Exception_Redirect;
    }
}