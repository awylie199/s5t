<?php

include_once('EpayTransaction.php');

class Am_Paysystem_Epay extends Am_Paysystem_Abstract
{
	protected $defaultTitle = "ePay";
    protected $defaultDescription = "ePay plugin configuration";
	
	function init()
    {	
		parent::init();
		
		$this->ipn_url = $this->getPluginUrl('ipn');
		$this->submit_url = $this->getConfig('testing') ? 
			'https://devep2.datamax.bg/ep2/epay2_demo/' : 
			'https://www.epay.bg/';
    }
	
	public function _initSetupForm(Am_Form_Setup $form)
    {    
		$form
			->addText("min", array('class' => 'el-wide'))
			->setLabel(	"MIN\n"
					.	"This value is provided by ePay.bg")
			->addRule(	'regex', 
                        'MIN must be 10 hexadecimal digits', 
                        '/^[A-F0-9]{10}$/')
			->addRule('required');
		
		$form
			->addAdvCheckbox("testing")
			->setLabel("Is it a Sandbox(Testing) Account?");

        $form
			->addPassword("secret", array('class' => 'el-wide'))
			->setLabel(	"Secret\n"
					.	"This value is provided by ePay.bg")
			->addRule('required');       
    }
	
	public function isConfigured()
    {
        return	!empty($this->config['min']) &&
				!empty($this->config['secret']);
    }
	
	function _process(Invoice $invoice, Am_Mvc_Request $request, Am_Paysystem_Result $result)
    {
		$product = $invoice->getProducts()[0];
		$secret     = $this->config['secret'];
		$min        = $this->config['min'];
        $invoice_id = $invoice->invoice_id; 
        $sum        = $invoice->first_total - $invoice->first_tax - $invoice->first_shipping;                                   
        # XXX Expiration date '01.08.2020'
		$exp_date   = strftime("%d.%m.%Y", time()+7*24*3600);
        $descr      = strip_tags($product->getDescription());

        $data = "MIN={$min}\nINVOICE={$invoice_id}\nAMOUNT={$sum}\nEXP_TIME={$exp_date}\nDESCR={$descr}\nDATA";

        $ENCODED  = base64_encode($data);
        $CHECKSUM = $this->hmac('sha1', $ENCODED, $secret);
        
		$arr['PAGE']       = 'paylogin';
        $arr['ENCODED']    = $ENCODED;
        $arr['CHECKSUM']   = $CHECKSUM;
        $arr['URL_OK']     = $this->getReturnUrl();
        $arr['URL_CANCEL'] = $this->getCancelUrl();
        $arr['AMOUNT']     = $sum;
		
		$action = new Am_Paysystem_Action_Form($this->submit_url);
		//$action->setAutoSubmit(false);
		
		foreach ($arr as $k => $v)
        {
            $action->$k = $v;
        }
		
		$result->setAction($action);
	}
	
	public function createTransaction($request, $response, array $invokeArgs) {
		return new Am_Paysystem_Transaction_Epay($this, $request, $response, $invokeArgs);
	}
	
	public function hmac($algo, $data, $passwd) {
        /* md5 and sha1 only */
        $algo=strtolower($algo);
        $p=array('md5'=>'H32','sha1'=>'H40');
        
		if(strlen($passwd)>64)
            $passwd=pack($p[$algo],$algo($passwd));
        
		if(strlen($passwd)<64)
            $passwd=str_pad($passwd,64,chr(0));

        $ipad=substr($passwd, 0, 64) ^ str_repeat(chr(0x36), 64);
        $opad=substr($passwd, 0, 64) ^ str_repeat(chr(0x5C), 64);

        return($algo($opad.pack($p[$algo],$algo($ipad.$data))));
    }

	public function getReadme()
    {		
		return <<<CUT
<b>ePay payment plugin installation</b>

1. Configure plugin at aMember CP -> Setup/Configuration -> ePay
2. Set PostBack URL in ePay control panel to 
$this->ipn_url/amember/plugins/payment/epay/ipn 
CUT;
    }
}

