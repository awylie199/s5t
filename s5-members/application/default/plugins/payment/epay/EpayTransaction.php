<?php

class Am_Paysystem_Transaction_Epay extends Am_Paysystem_Transaction_Incoming 
{
	public function getUniqId() {
		$vars = $this->request->getPost();
		return crc32($vars['encoded']);
	}
	
	function autoCreate() {
		return;
	}
	
	public function process() {
		if ($this->request->getMethod() == Am_Mvc_Request::METHOD_GET) {
			return true;
		}

		$vars = $this->request->getPost();
		
		$this->log->add(
			"ePay DEBUG: process_thanks \$vars=<br />" . 
			print_r($vars, true)
		);

		$this->validateSource();

        $data = base64_decode($vars['encoded']);
        $lines_arr = split("\n", $data);
        $info_data = '';

        foreach ($lines_arr as $line) {
            if (preg_match(
					"/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/", 
					$line, 
					$regs)) {
                $invoice  = $regs[1];
                $status   = $regs[2];
                $pay_date = $regs[4]; # XXX if PAID
                $stan     = $regs[5]; # XXX if PAID
                $bcode    = $regs[6]; # XXX if PAID

                # XXX process $invoice, $status, $pay_date, $stan, $bcode here
                $paymentData = Am_Di::getInstance()->invoiceTable->load($invoice);

				if (!$paymentData) {
                    $info_data .= "INVOICE=$invoice:STATUS=NO\n";
                } elseif ('PAID' == $status && $paymentData->status != Invoice::PAID) {                							
					$err = $paymentData->addPayment($this);    					
                    $info_data .= ($err) ? "INVOICE=$invoice:STATUS=OK\n" :
                                           "INVOICE=$invoice:STATUS=ERR\n";
                } else if ('PAID' == $status && $paymentData->status == Invoice::PAID) {				
                    $info_data .= "INVOICE=$invoice:STATUS=OK\n";
                }
            }
        }
        
		$this->log->add($info_data);
		$this->response->setBody($info_data."\n");
	}

	public function validateSource() {		
		$vars = $this->request->getPost();
			       
        if (!$vars['encoded'] || !$vars['checksum']) {
			throw new Am_Exception_Paysystem_TransactionEmpty(
				"encoded or checksum are empty");
		}
		
		$ENCODED  = $vars['encoded'];
        $CHECKSUM = $vars['checksum'];
        
        $secret = $this->plugin->getConfig('secret');
        $hmac   = $this->plugin->hmac('sha1', $ENCODED, $secret);

        if ($hmac == $CHECKSUM) {
            return true;
		} else {
			$data = "Checksum comparision:\n";
			$data .= $hmac . "\n";
			$data .= $CHECKSUM;
			$this->plugin->logRequest($data);
			
			throw new Am_Exception_Paysystem_TransactionSource(""
					. "IPN seems to be received from unknown source, not from the paysystem<br/>"
					. "CHECKSUM: $CHECKSUM<br/>"
					. "RESULT:   $hmac");
		}
	}

	public function validateStatus() {
		// No option to validate stus, thus ipn is agregative
		return true;
	}

	public function validateTerms() {
		return true;
	}
}

