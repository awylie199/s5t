<?php

class RocketGateEncoder {
	public static function Encode($params, $secret) {
		$string = '';
		
		if ($params)
        {
            foreach ($params as $k => $v) {
                $string .= $k . '=' . $v . '&';
			}
        }
		
		$string .= 'secret=' . $secret;
		$string = hash("sha1", $string, true);
		$string = base64_encode($string);
		
		return $string;
	}
}

