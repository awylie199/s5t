<?php

//die('here');
include_once('Encoder.php');
include_once('RgActionRedirect.php');
include_once('RgTransaction.php');
include_once('RgTransactionThanks.php');
include_once('RgTransactionFail.php');

class Am_Paysystem_Rocketgate extends Am_Paysystem_Abstract
{
	protected $defaultTitle = "RocketGate";
    protected $defaultDescription = "Secure credit card payment";
	
	function init()
    {	
		$this->rg_url = $this->getConfig('debug') ? 
			'https://dev-secure.rocketgate.com/hostedpage/servlet/HostedPagePurchase?' : 
			'https://secure.rocketgate.com/hostedpage/servlet/HostedPagePurchase?';
    }
	
	public function _initSetupForm(Am_Form_Setup $form)
    {
        $form
			->addText("merchant_id", array('class' => 'el-wide'))
			->setLabel("Merchant ID")
			->addRule(	'regex', 
                        'Merchant ID must be 10 decimal signs', 
                        '/^\d{10}$/')
			->addRule('required');
		
		$form
			->addAdvCheckbox("debug")
			->setLabel("Is it a Sandbox(Testing) Account?");

        $form
			->addPassword("gateway_pass", array('class' => 'el-wide'))
			->setLabel("Gateway Password")
			->addRule(	'regex', 
                        'The format is: 16 alpanumeric characters', 
                        '/^[a-zA-Z0-9_]{16}$/')
			->addRule('required');
		
		$form
			->addPassword("gateway_sec", array('class' => 'el-wide'))
			->setLabel("Hash Secret")
			->addRule(	'regex', 
                        'The format is: 16 alpanumeric characters', 
                        '/^[a-zA-Z0-9_]{16}$/')
			->addRule('required');        
    }
	
	public function isConfigured()
    {
        return	!empty($this->config['merchant_id']) &&
				!empty($this->config['gateway_pass']) &&
				!empty($this->config['gateway_sec']);
    }
		
	function _process(Invoice $invoice, Am_Mvc_Request $request, Am_Paysystem_Result $result)
    {
		$product = $invoice->getProducts()[0];
		
		$a = new Am_Paysystem_Action_Redirect_Rocketgateway($this->rg_url);
		$a->merch = $this->config['merchant_id'];
		$a->id = $invoice->user_id;
		$a->time = time();
		$a->method = 'CC';
		$a->purchase = 'true';
		$a->invoice = $invoice->invoice_id;
		$a->prodid = $product->product_id;
		$a->udf01 = time();
		$a->fail = $this->getPluginUrl('fail');
		$a->success = $this->getPluginUrl('thanks');
		
		$a->amount = $invoice->first_total - $invoice->first_tax - $invoice->first_shipping;
		$a->hash = $a->getHash($this->config['gateway_sec']);
		$result->setAction($a);
	}
	
	public function failAction(/*Am_Mvc_Request*/ $request, /*Am_Mvc_Response*/ $response, array $invokeArgs)
    {
        $log = $this->logRequest($request);

		$transaction = $this->createFailTransaction($request, $response, $invokeArgs);
        $transaction->setInvoiceLog($log);
		
        $transaction->validate();
		$this->invoice = $transaction->getInvoice();
        $response->setRedirect($this->getCancelUrl());
    }
	
	public function directAction(/*Am_Mvc_Request*/ $request, /*Am_Mvc_Response*/ $response,  $invokeArgs) {
		$actionName = $request->getActionName();
		
		switch ($actionName)
        {
            case 'fail':
                //die('fail');
				$this->failAction($request, $response, $invokeArgs);
                break;
            default: // standard action handling via transactions
                parent::directAction($request, $response, $invokeArgs);
        }
	}
	
	public function createFailTransaction(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        //die('create transaction');
		return new Am_Paysystem_Rocketgate_TransactionFail($this, $request, $response, $invokeArgs);
    }

	public function createTransaction(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        throw new Exception('No need to create transaction. It must be Thanx or Fail transaction');
    }
	
	public function createThanksTransaction(Am_Mvc_Request $request, Am_Mvc_Response $response, array $invokeArgs)
    {
        return new Am_Paysystem_Rocketgate_TransactionThanks($this, $request, $response, $invokeArgs);
    }
	
	public function getReadme()
    {
return <<<CUT
<b>Rocketgate payment plugin installation</b>

Please fill all require data.<br/>
You can get it from Rocketgate account management interface:<br/>
Navigate to <a href="https://dev-my.rocketgate.com" target="_blank">development</a>
or <a href="https://my.rocketgate.com" target="_blank">production</a> environment.
<br/>		
Find follow parameters in Rocketgate account management:
<ul>
<li>Merchant ID: Admin -> Merch details -> ID</li>
<li>Gateway password: Admin -> Merch details -> Merchant Options -> Gateway Password</li>
<li>Hash Secret: Admin -> Merch details -> Merchant Options -> HashSecret</li>
</ul>	
Check "Is it a Sandbox(Testing) Account?" checkbox, if you want work with development environment. 
CUT;
    }
}