<?php

class Am_Paysystem_Rocketgate_TransactionThanks extends Am_Paysystem_Transaction_Incoming_Thanks 
{
	// @var Am_Paysystem_Rocketgate_Transaction
	private $transaction;
    
	function __construct(
		$plugin, 
		$request, 
		$response, 
		$invokeArgs) {
		parent::__construct($plugin, $request, $response, $invokeArgs);
		
		$this->transaction = new Am_Paysystem_Rocketgate_Transaction($plugin, $request, $response, $invokeArgs);
	}
	
	public function validate() {
		parent::validate();
		$this->transaction->autoCreate();
		$this->invoice = $this->transaction->getInvoice();
	}
	
	public function validateSource()
    {
		//error_log('thanx validate source');
		return $this->transaction->validateSource();
    }

    public function validateTerms()
    {
		//error_log('thanx validate terms');
        return $this->transaction->validateTerms();
    }

    public function validateStatus()
    {
        //error_log('thanx validate status');
		return $this->transaction->validateStatus();
    }

    public function getUniqId()
    {
        //error_log('thanx get unique id');
		return $this->transaction->getUniqId();
    }

    public function findInvoiceId()
    {
		//error_log('thanx find invoice');
		return $this->transaction->findInvoiceId();
    }
}