<?php

class Am_Paysystem_Rocketgate_Transaction extends Am_Paysystem_Transaction_Incoming 
{
	public function getUniqId() {
		return $this->request->get('udf01');
	}
	
	public function findInvoiceId() {
		//debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 20);
		//die();
		$id = $this->request->get('invoiceID');
		
		if ($invoice = 
				Am_Di::getInstance()
					->invoiceTable
					->load($id))
		{
			//var_dump($invoice);
			//die('here');
			return $invoice->public_id;
		}
		
		//die('findinvoiceid');
	}
	
	public function validateSource() {
		$vars = $this->request->getQuery();
        $oldhash = $vars['hash'];
        unset($vars['hash']);
        $secret = $this->plugin->getConfig('gateway_sec');
        $newhash = RocketGateEncoder::Encode($vars, $secret);
		
		//var_dump($oldhash, $newhash);
		//die();
		
        if ($oldhash != $newhash)
        {
            //die('here');
			throw new Am_Exception_Paysystem_TransactionInvalid("Verify sign is incorrect.");
        }

        return true;
	}
	
	public function validateStatus() {
		//error_log('validateStatus');
		$vars = $this->request->getQuery();
		
		if (!empty($vars['errcode'])) {
			error_log($vars['errcode']);
		}
		
		return empty($vars['errcode']);
	}
	
	public function validateTerms() {
		//error_log('validateTerms');
		return true;
	}
}

