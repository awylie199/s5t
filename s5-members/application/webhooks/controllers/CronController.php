<?php

class Webhooks_CronController extends Am_Mvc_Controller
{
    public function indexAction()
    {
        $time_limit = 300;
        // get lock
        if (!$this->getDi()->db->selectCell("SELECT GET_LOCK(?, 0)", self::getLockId())) {
            $this->getDi()->errorLogTable->log("Could not obtain MySQL's GET_LOCK() to run webhooks cron. Probably attempted to execute two cron processes simultaneously. ");
            return;
        }
        $start = time();
        foreach($this->getDi()->webhookQueueTable->findBy(array('sent' => null)) as $webhook_queue)
        {
            if(time() - $start >= $time_limit) break;
            try{
                $req = new Am_HttpRequest($webhook_queue->url, Am_HttpRequest::METHOD_POST);
                $params = unserialize($webhook_queue->params);
                foreach($params as $name => $data) {
                    if (is_array($data)) {
                        unset($params[$name]);
                        foreach($data as $k => $v) {
                            $params[$name . '[' . $k . ']'] = $v;
                        }
                    }
                }
                $req->addPostParameter($params);
                $res = $req->send();
                $st = $res->getStatus();
                if($st == 200) {
                    $webhook_queue->updateQuick(array('sent' => $this->getDi()->time));
                } else {
                    $webhook_queue->updateQuick(array('last_error'=>$st, 'failures'=>$webhook_queue->failures + 1));
                }
            } catch(Exception $e)  {
                $this->getDi()->errorLogTable->logException($e);
            }
        }
        //release lock
        $this->getDi()->db->query("SELECT RELEASE_LOCK(?)", self::getLockId());
    }

    static function getLockId()
    {
        return 'webhooks-cron-lock-' . md5(ROOT_DIR);
    }
}