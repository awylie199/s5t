<?php

/**
 * User menu at top of member controller
 * @package Am_Utils
 */
class Am_Navigation_User extends Am_Navigation_Container
{
    function addDefaultPages()
    {
        $this->addPage(array(
            'id' => 'member',
            'controller' => 'member',
            'label' => ___('Dashboard'),
            'title' => ___('Dashboard'),
            'order' => 0
        ));
        $forms = Am_Di::getInstance()->savedFormTable->findBy(array(
            'type' => SavedForm::T_SIGNUP,
            'hide' => 0), null, null, 'sort_order');
        if (!$forms) {
            //nop
        } elseif (count($forms) == 1) {
            list($f) = $forms;
            $page = array(
                'id' => 'add-renew',
                'controller' => 'signup',
                'action' => 'index',
                'route' => 'signup',
                'label' => ___('Add/Renew Subscription'),
                'order' => 100,
            );
            if (!$f->isDefault(SavedForm::D_MEMBER)) {
                $page['params'] = array(
                    'c' => $f->code
                );
            }
            $this->addPage($page);
        } else {
            $pages = array();
            foreach ($forms as $f) {
                $params = $f->isDefault(SavedForm::D_MEMBER) ?
                    array() : array('c' => $f->code);
                $pages[] = array(
                    'id' => 'add-renew-' . ($f->code ? $f->code : 'default'),
                    'label' => ___($f->title),
                    'controller' => 'signup',
                    'action' => 'index',
                    'route' => 'signup',
                    'params' => $params
                );
            }
            $this->addPage(array(
                'id' => 'add-renew',
                'uri' => 'javascript:;',
                'label' => ___('Add/Renew Subscription'),
                'order' => 100,
                'pages' => $pages
            ));
        }

        $forms = Am_Di::getInstance()->savedFormTable->findBy(array(
            'type' => SavedForm::T_PROFILE,
            'hide' => 0), null, null, 'sort_order');
        if (!$forms) {
            //nop
        } elseif (count($forms) == 1) {
            list($f) = $forms;
            $page = array(
                'id' => 'profile',
                'controller' => 'profile',
                'route' => 'profile',
                'label' => ___('Profile'),
                'order' => 300,
            );
            if (!$f->isDefault(SavedForm::D_PROFILE)) {
                $page['params'] = array(
                    'c' => $f->code
                );
            }
            $this->addPage($page);
        } else {
            $pages = array();
            foreach ($forms as $f) {
                $params = $f->isDefault(SavedForm::D_PROFILE) ?
                    array() : array('c' => $f->code);
                $pages[] = array(
                    'id' => 'profile-' . ($f->code ? $f->code : 'default'),
                    'label' => ___($f->title),
                    'controller' => 'profile',
                    'route' => 'profile',
                    'params' => $params
                );
            }

            $this->addPage(array(
                'id' => 'profile',
                'uri' => 'javascript:;',
                'label' => ___('Profile'),
                'order' => 300,
                'pages' => $pages
            ));
        }

        try {
            $user = Am_Di::getInstance()->user;
        } catch (Am_Exception_Db_NotFound $e) {
            $user = null;
        }

        if ($user) {
            $tree = Am_Di::getInstance()->resourceCategoryTable->getAllowedTree($user);
            $pages = array();
            foreach ($tree as $node) {
                $pages[] = $this->getContentCategoryPage($node);
            }

            if (count($pages))
                $this->addPages($pages);
        }

        if ($user) {
            $pages = Am_Di::getInstance()->resourceAccessTable
                ->getAllowedResources($user, ResourceAccess::PAGE);
            foreach ($pages as $p) {
                if ($p->onmenu) {
                    $this->addPage(array(
                        'id' => 'page-' . $p->pk(),
                        'uri' => $p->getUrl(),
                        'label' => ___($p->title)
                    ));
                }
            }

            $links = Am_Di::getInstance()->resourceAccessTable
                ->getAllowedResources($user, ResourceAccess::LINK);
            foreach ($links as $l) {
                if ($l->onmenu) {
                    $this->addPage(array(
                        'id' => 'link-' . $l->pk(),
                        'uri' => $l->getUrl(),
                        'label' => ___($l->title)
                    ));
                }
            }
        }

        Am_Di::getInstance()->hook->call(Am_Event::USER_MENU, array(
            'menu' => $this,
            'user' => $user));

        /// workaround against using the current route for generating urls
        foreach (new RecursiveIteratorIterator($this, RecursiveIteratorIterator::SELF_FIRST) as $child)
            if ($child instanceof Am_Navigation_Page_Mvc && $child->getRoute()===null)
                $child->setRoute('default');
    }

    protected function getContentCategoryPage($node) {

        $page = $node->self_cnt ? array(
            'id' => 'content-category-' . $node->pk(),
            'route' => 'content-c',
            'controller' => 'content',
            'action' => 'c',
            'label' => ___($node->title),
            'order' => 500 + $node->sort_order,
            'params' => array(
                'id' => $node->pk(),
                'title' => $node->title
            )
        ) : array(
            'id' => 'content-category-' . $node->pk(),
            'uri' => 'javascript:;',
            'label' => ___($node->title),
            'order' => 500 + $node->sort_order
        );

        $subpages = array();
        foreach ($node->getChildNodes() as $n)
            $subpages[] = $this->getContentCategoryPage($n);

        if (count($subpages)) {
            $page['pages'] = $subpages;
        }

        return $page;
    }
}